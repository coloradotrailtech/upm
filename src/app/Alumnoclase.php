<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alumnoclase extends Model
{
    protected $table = "alumnosclases";
    protected $fillable = ['asistio', 'motivo_inasistencia', 'desempeno', 'alumno_id', 'clase_id'];

    public function alumno() {
        return $this->belongsTo('App\Alumno');
    }
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function clase() {
        return $this->belongsTo('App\Clase');
    }

    public function getAsistioFormateadoAttribute()
    {
        $asistio = "No";
        if ($this->asistio) {
            $asistio = "Sí";
        }
        return $asistio;
    }
    
    public function datos_para_editar()
    {
        $datos = [
            'asistio' => $this->asistio,
            'motivo_inasistencia' => $this->motivo_inasistencia,
            'alumno' => $this->alumno->estudiante->persona->getNombreCompletoAttribute(),
            'id' => $this->id,
        ];
        return json_encode($datos);
    }

}



