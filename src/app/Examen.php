<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examen extends Model
{
    protected $table = "examenes";
    protected $fillable = ['fecha', 'modalidad_id', 'curso_id', 'unidad_id'];

    protected $dates = ['fecha'];

    /**
     * Mutadores
     */

    public function getFechaFormateadoAttribute()
    {
        return $this->fecha->format('d/m/Y');
    }

    public function setFechaAttribute($value)
    {
        if (!is_null($value)) {
            $fecha = str_replace('/', '-', $value);
            $this->attributes['fecha'] = date('Y-m-d', strtotime($fecha));
        }
    }

    public function curso()
    {
        return $this->belongsTo('App\Curso');
    }

    public function modalidad()
    {
        return $this->belongsTo('App\Modalidad');
    }

    public function examenesalumnos()
    {
        return $this->hasMany('App\Examenalumno');
    }

    public function examenesunidades()
    {
        return $this->hasMany('App\Examenunidad');
    }

    public function examenesprofesorescursos()
    {
        return $this->hasMany('App\Examenprofesor');
    }

    public function datos_para_editar()
    {
        $ids = [];
        foreach ($this->examenesprofesorescursos as $profesorexamen) {
            array_push($ids, $profesorexamen->profesorcurso_id);
        }

        $ids_unidades = [];
        foreach ($this->examenesunidades as $examenunidad) {
            array_push($ids_unidades, $examenunidad->unidad_id);
        }

        $datos = [
            'id' => $this->id,
            'fecha' => $this->getFechaFormateadoAttribute(),
            'modalidad' => $this->modalidad_id,
            'curso_id' => $this->curso_id,
            'unidades' => $ids_unidades,
            'ids_profesores' => $ids
        ];
        return json_encode($datos);
    }

    public function getunidadesstring()
    {
        $nombres_unidades = "";
        foreach ($this->examenesunidades as $examenunidad) {
            $nombres_unidades = $nombres_unidades." ".$examenunidad->unidad->nombre;
        }
        return $nombres_unidades;
    }

}