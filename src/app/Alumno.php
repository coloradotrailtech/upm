<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alumno extends Model
{
    protected $table = "alumnos";
    protected $fillable = [
        'estudiante_id',
        'curso_id',
        'estado_curso', #
        'estado_certificado',
        'realizo_tareas', #"realizo_tareas" indica si realizo acciones relacionadas al curso antes de ser inscripto. Refleja conocimiento basico de los contenidos
        'asistencia',
        'interes_trabajar'
    ];

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /*------------------------------------------------------------------------*/

    public function curso()
    {
        return $this->belongsTo('App\Curso');
    }

    public function estudiante()
    {
        return $this->belongsTo('App\Estudiante');
    }

    public function examenesalumnos()
    {
        return $this->hasMany('App\Examenalumno');
    }

    public function alumnosclases()
    {
        return $this->hasMany('App\Alumnoclase');
    }


    public function inasistencias()
    {

        $cont_inasistencias = 0;
        foreach ($this->alumnosclases as $alumno_clase) {
            if ($alumno_clase->asistio != 1) {                 #1= asistio
                $cont_inasistencias++;
            }
        }
        return $cont_inasistencias;
    }

    public function devolver_condicion()
    {
        $condicion = "cursando";
        switch ($this->estado_curso) {
            case "libre":
                $condicion = "alumno-desafectable";
                break;
            case "abandono":
                $condicion = "alumno-desafectable";
                break;
            case "aprobado":
                $condicion = "aprobado";
                break;
        }
        return $condicion;
    }
}
