<?php

namespace App;

use App\Curso;
use App\Material;
use App\Materialprestamo;
use App\Materialsolicitudprestamo;
use App\Materialtitulo;
use Illuminate\Database\Eloquent\Model;

class Deposito extends Model
{

    protected $table = "depositos";
    protected $fillable = ['descripcion', 'sede_id', 'localidad_id', 'direccion', 'habilitado'];

    public function sede()
    {
        return $this->belongsTo('App\Sede');
    }

    public function localidad()
    {
        return $this->belongsTo('App\Localidad');
    }

    public function materialesdeposito()
    {
        return $this->hasMany('App\Materialdeposito');
    }

    public function prestamos()
    {
        return $this->hasMany('App\Prestamo');
    }

    public function prestamosactivos()
    {
        return $this->hasMany('App\Prestamo')->where('estado', 'realizado');
    }

    public function prestamosinactivos()
    {
        return $this->hasMany('App\Prestamo')->where('estado', 'culminado');
    }

    public function solicitantesprestamosactivos()
    {
        $ids_prestamos_curso_activos = array_unique($this->prestamosactivos()->pluck('curso_id')->toArray());
        $cursos = Curso::all()->whereIn('id', $ids_prestamos_curso_activos);
        return $cursos;
    }

    public function solicitantesprestamosinactivos()
    {
        $ids_prestamos_curso_activos = array_unique($this->prestamosinactivos()->pluck('curso_id')->toArray());
        $cursos = Curso::all()->whereIn('id', $ids_prestamos_curso_activos);
        return $cursos;
    }

    public function fechasprestamosactivos()
    {
        $fechas = [];
        foreach ($this->prestamosactivos()->get() as $prestamo) {
            array_push($fechas, $prestamo->created_at);
        }
        return array_unique($fechas);
    }

    public function fechasprestamosinactivos()
    {
        $fechas = [];
        foreach ($this->prestamosinactivos()->get() as $prestamo) {
            array_push($fechas, $prestamo->created_at);
        }
        return array_unique($fechas);
    }

    public function solicitudesprestamo()
    {
        return $this->hasMany('App\Solicitudprestamo');
    }

    public function solicitudespendientes()
    {
        return $this->solicitudesprestamo()->where('estadosolicitudprestamo_id', '1');
    }

    public function solicitudeshistorico()
    {
        return $this->solicitudesprestamo()->where('estadosolicitudprestamo_id', '<>', '1');
    }


    public function solicitantessolicitudespendientes()
    {
        $ids_solcitudes_curso = array_unique($this->solicitudespendientes()->pluck('curso_id')->toArray());
        $cursos = Curso::all()->whereIn('id', $ids_solcitudes_curso);
        return $cursos;
    }

    public function materialessolicitudespendientes()
    {
        $ids_solicitudes = $this->solicitudespendientes()->pluck('id')->toArray();
        $ids_materiales = Materialsolicitudprestamo::whereIn('solicitudprestamo_id', $ids_solicitudes)->pluck('materialtitulo_id')->toArray();
        $materiales = Materialtitulo::whereIn('id', array_unique($ids_materiales))->get();
        return $materiales;
    }

    public function fechassolicitudespendientes()
    {
        $fechas = [];
        foreach ($this->solicitudespendientes()->get() as $solicitud) {
            array_push($fechas, $solicitud->created_at);
        }
        return array_unique($fechas);
    }

    public function solicitantessolicitudeshistorico()
    {
        $ids_solcitudes_curso = array_unique($this->solicitudeshistorico()->pluck('curso_id')->toArray());
        $cursos = Curso::all()->whereIn('id', $ids_solcitudes_curso);
        return $cursos;
    }

    public function materialessolicitudeshistorico()
    {
        $ids_solicitudes = $this->solicitudeshistorico()->pluck('id')->toArray();
        $ids_materiales = Materialsolicitudprestamo::whereIn('solicitudprestamo_id', $ids_solicitudes)->pluck('materialtitulo_id')->toArray();
        $materiales = Materialtitulo::whereIn('id', array_unique($ids_materiales))->get();
        return $materiales;
    }

    public function fechassolicitudeshistorico()
    {
        $fechas = [];
        foreach ($this->solicitudeshistorico()->get() as $solicitud) {
            array_push($fechas, $solicitud->created_at);
        }
        return array_unique($fechas);
    }

    public function get_stock_material($id)
    {
        $material_en_deposito = $this->materialesdeposito()->where('material_id', $id)->first();
        if (is_null($material_en_deposito)) {
            return 0;
        } else {
            return $material_en_deposito->stock;
        }
    }

    public function get_cantidad_material_prestado($id)
    {

        $cantidad_prestada = 0;

        $ids_cursos_activos = Curso::all()->where('activa', true)->pluck('id')->toArray();
        $ids_prestamos = $this->prestamos->whereIn('curso_id', $ids_cursos_activos)->pluck('id')->toArray();

        $materialdeposito_id = $this->materialesdeposito->where('material_id', $id)->first()->id;

        $materiales_prestamos = Materialprestamo::whereIn('prestamo_id', $ids_prestamos)
            ->where('materialdeposito_id', $materialdeposito_id)->get();

        foreach ($materiales_prestamos as $material_prestamo) {
            $cantidad_prestada = $cantidad_prestada + $material_prestamo->cantidad_prestada;
        }

        return $cantidad_prestada;
    }

    public function material_solicitado($material_id, $solicitudprestamo_id)
    {
        $materialsolicitudprestamo = Materialsolicitudprestamo::where('material_id', $material_id)
            ->where("solicitudprestamo_id", $solicitudprestamo_id)->first();
        if (is_null($materialsolicitudprestamo)) {
            return 0;
        } else {
            return $materialsolicitudprestamo->cantidad;
        }
    }

}
