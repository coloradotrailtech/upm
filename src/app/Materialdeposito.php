<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materialdeposito extends Model {

    protected $table = "materialesdepositos";
    protected $fillable = ['material_id', 'deposito_id', 'stock'];

    public function material() {
        return $this->belongsTo('App\Material');
    }

    public function deposito() {
        return $this->belongsTo('App\Deposito');
    }

    public function materialesprestamos() {
        return $this->hasMany('App\Materialprestamo');
    }

    public function stock_prestado() {

        $stock_prestado = 0;

        foreach ($this->materialesprestamos as $material_prestado) {
            /*
             * Por ahora como en este spring el curso no fue abordado usamos como variable de control la cantidad prestada.
             */
            if (is_null($material_prestado->cantidad_devuelta)) {
                $stock_prestado += $material_prestado->cantidad_prestada;
            }
        }

        return $stock_prestado;
    }

    public function stock_total() {

        $stock_prestado = $this->stock_prestado();

        return $this->stock + $stock_prestado;
    }






    public function stock_disponible() {

        $stock_prestado = $this->stock_prestado();

        return $this->stock - $stock_prestado;
    }

}
