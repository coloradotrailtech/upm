<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estadosolicitudprestamo extends Model
{
    protected $table =  "estadossolicitudesprestamos";

    protected $fillable = ['nombre'];

    public function solicitudesprestamos()
    {
        return $this->hasMany('App\Solicitudprestamo');
    }
}
