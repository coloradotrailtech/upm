<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examenprofesor extends Model
{

    protected $table = "examenesprofesorescursos";
    
    protected $fillable = ['examen_id', 'profesorcurso_id'];

    public function profesorcurso()
    {
        return $this->belongsTo('App\Profesorcurso');
    }

    public function examen()
    {
        return $this->belongsTo('App\Examen');
    }

}
