<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prestamo extends Model {

    protected $table = "prestamos";
    protected $fillable = ['estado', 'deposito_id', 'curso_id', 'solicitudprestamo_id'];

    public function deposito() {
        return $this->belongsTo('App\Deposito');
    }

    public function curso() {
        return $this->belongsTo('App\Curso');
    }

    public function solicitudprestamo() {
        return $this->belongsTo('App\Solicitudprestamo');
    }

    public function materialesprestamos() {
        return $this->hasMany('App\Materialprestamo');
    }

}
