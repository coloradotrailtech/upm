<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Titulacion extends Model
{
    public $table= "titulaciones";
    public $fillable= ['nombre', 'nivel', 'descripcion'];

    public function Profesores(){
        return $this->hasMany('App\Profesores');
    }
}
