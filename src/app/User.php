<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'rol_id', 'imagen',
    ];
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function persona()
    {
        return $this->hasOne('App\Persona');
    }

    public function rol()
    {
        return $this->belongsTo('App\Rol');
    }

    public function notificaciones()
    {
        return $this->hasMany('App\Notificacion');
    }

    public function encargado(){
        return $this->hasOne('App\Encargado');
    }

    /************ Verificacion de Roles ***********/

    public function isBedel()
    { #devuelve true si usuario es Bedel
        if ($this->rol->nombre == 'Bedel') {
            return true;
        } else {
            return false;
        }
    }

    public function isAdmin()
    { #devuelve true si usuario es Administrador del Sistema
        if ($this->rol->nombre == 'Admin') {
            return true;
        } else {
            return false;
        }
    }

    public function isEncargadoDeposito()
    { #devuelve true si el usuario es Encargado de Depositos
        if ($this->rol->nombre == 'Encargado de Deposito') {
            return true;
        } else {
            return false;
        }
    }

    public function isEncargadoSede()
    { #devuelve true si el usuario es Encargado de una Sede
        if ($this->rol->nombre == 'Encargado de Sede') {
            return true;
        } else {
            return false;
        }
    }

    #Encargado Sedes Temporales
    public function isEncargadoTemporal(){
        if ($this->rol->nombre == 'Encargado Sedes Temporales') {
            return true;
        } else {
            return false;
        }
    }
}
