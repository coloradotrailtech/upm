<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examenalumno extends Model
{
    protected $table = "examenesalumnos";
    protected $fillable = ['asistio', 'situacion', 'motivo_inasistencia', 'nota', 'observacion', 'alumno_id', 'examen_id'];

    public function alumno()
    {
        return $this->belongsTo('App\Alumno');
    }

    public function examen()
    {
        return $this->belongsTo('App\Examen');
    }

    public function getAsistioFormateadoAttribute()
    {
        $asistio = "No";
        if ($this->asistio) {
            $asistio = "Sí";
        }
        return $asistio;
    }

    public function datos_para_editar()
    {
        $datos = [
            'asistio' => $this->asistio,
            'motivo_inasistencia' => $this->motivo_inasistencia,
            'situacion' => $this->situacion,
            'alumno' => $this->alumno->estudiante->persona->getNombreCompletoAttribute(),
            'id' => $this->id,
        ];
        return json_encode($datos);
    }
}
