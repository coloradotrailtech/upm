<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materialprestamo extends Model {

    protected $table = "materialesprestamos";
    protected $fillable = ['cantidad_prestada', 'cantidad_devuelta', 'observacion', 'materialdeposito_id', 'prestamo_id'];

    public function materialdeposito() {
        return $this->belongsTo('App\Materialdeposito');
    }

    public function prestamo() {
        return $this->belongsTo('App\Prestamo');
    }

}
