<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipomaterial extends Model {

    protected $table = "tiposmateriales";
    protected $fillable = ['nombre', 'familiamaterial_id'];

    public function materiales() {
        return $this->hasMany('App\Material');
    }

    public function familiamaterial() {
        return $this->belongsTo('App\Familiamaterial');
    }
}
