<?php

namespace App;

use App\Deposito;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{

    protected $table = "materiales";
    protected $fillable = ['nombre', 'umedida_id', 'tipomaterial_id'];

    public function umedida()
    {
        return $this->belongsTo('App\Umedida');
    }

    public function tipomaterial()
    {
        return $this->belongsTo('App\Tipomaterial');
    }

    public function materialestraferencias()
    {
        return $this->hasMany('App\Materialtransferencia');
    }

    public function materialesdeposito()
    {
        return $this->hasMany('App\Materialdeposito');
    }

    public function depositos_disponibles()
    {
        $ids_depositos = [];
        foreach ($this->materialesdeposito as $materialdeposito) {
            array_push($ids_depositos, $materialdeposito->deposito_id);
        }
        $depositos = Deposito::all()->whereIn('id', array_unique($ids_depositos));

        return $depositos;
    }

}
