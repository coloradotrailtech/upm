<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examenunidad extends Model
{
    protected $table = "examenesunidades";

    protected $fillable = ['examen_id', 'unidad_id'];

    public function examen()
    {
        return $this->belongsTo('App\Examen');
    }

    public function unidad()
    {
        return $this->belongsTo('App\Unidad');
    }

}
