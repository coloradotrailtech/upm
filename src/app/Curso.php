<?php

namespace App;

use App\Material;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    #representa un cursado, que otorga un titulo y es dado durante un tiempo tiempo en una sede en especifico por edicion y con profesores asignados desde su comienzo de clases hasta su finalización.

    protected $table = "cursos";
    protected $fillable = ['fecha_inicio', 'fecha_fin', 'sede_id', 'titulo_id', 'activa', 'comision','descripcion'];
    protected $dates = ['fecha_inicio', 'fecha_fin'];

    public function getFechaInicioFormateadoAttribute()
    {
        return $this->fecha_inicio->format('d/m/Y');
    }

    public function getFechaFinFormateadoAttribute()
    {
        return $this->fecha_fin->format('d/m/Y');
    }

    public function setFechaInicioAttribute($value)
    {
        if (!is_null($value)) {
            $fecha = str_replace('/', '-', $value);
            $this->attributes['fecha_inicio'] = date('Y-m-d', strtotime($fecha));
        }
    }

    public function setFechaFinAttribute($value)
    {
        if (!is_null($value)) {
            $fecha = str_replace('/', '-', $value);
            $this->attributes['fecha_fin'] = date('Y-m-d', strtotime($fecha));
        }
    }

    public function get_rango_fechas()
    {
        return $this->fecha_inicio->format('d/m/Y')." - ". $this->fecha_fin->format('d/m/Y');
    }

    public function solicitudesprestamo()
    {
        return $this->hasMany('App\Solicitudprestamo');
    }

    public function prestamos()
    {
        return $this->hasMany('App\Prestamo');
    }

    public function unidades()
    {
        return $this->hasMany('App\Unidad');
    }

    public function alumnos()
    {
        return $this->hasMany('App\Alumno');
    }

    public function clases()
    {
        return $this->hasMany('App\Clase');
    }

    public function sede()
    {
        return $this->belongsTo('App\Sede');
    }

    public function titulo()
    {
        return $this->belongsTo('App\Titulo');
    }

    public function profesores()
    {
        return $this->hasMany('App\Profesorcurso');
    }

    public function examenes()
    {
        return $this->hasMany('App\Examen');
    }

    public function notificaciones()
    {
        return $this->hasMany('App\Notificacion');
    }

    public function determinar_estado()
    {
        $fecha_inicio = date('Y-m-d', strtotime($this->fecha_inicio->format('Y-m-d')));
        $fecha_fin = date('Y-m-d', strtotime($this->fecha_fin->format('Y-m-d')));
        $fecha_hoy = date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));

        if ($fecha_inicio > $fecha_hoy) {
            $estado = "Por iniciar";
        } elseif ($fecha_inicio <= $fecha_hoy && $fecha_fin > $fecha_hoy) {
            $estado = "Activo";
        } else {
            $estado = "Dictado finalizado";
        }

        return $estado;
    }

    public function discriminar_profesores($rol)
    {
        /*
         * El rol puede ser titular o suplente
         * El tipo refieren id: a los objetos; objeto: solamente los ids (se utilizan para setear selects)
         */
        $profesores = [];
        foreach ($this->profesores as $profesor) {
            if ($profesor->rol === $rol) {
                array_push($profesores, $profesor);
            }
        }
        return $profesores;
    }

    public function datos_para_editar()
    {

        $ids_titulares = [];
        $ids_suplentes = [];

        foreach ($this->profesores as $profesor) {
            if ($profesor->rol === "titular") {
                array_push($ids_titulares, $profesor->profesor_id);
            } else {
                array_push($ids_suplentes, $profesor->profesor_id);
            }
        }
        return json_encode([
            'fecha_inicio' => $this->getFechaInicioFormateadoAttribute(),
            'fecha_fin'    => $this->getFechaFinFormateadoAttribute(),
            'activa'       => $this->activa,
            'comision'     => $this->comision,
            'id'           => $this->id,
            'sede_id'      => $this->sede_id,
            'titulo_id'    => $this->titulo_id,
            'descripcion'  => $this->descripcion,
            'titulares'    => $ids_titulares,
            'suplentes'    => $ids_suplentes,
        ]);
    }

    public function recursos_prestados()
    {

        $ids_recursos = [];
        $recursos = [];
        
        foreach ($this->prestamos as $prestamo) {
            foreach ($prestamo->materialesprestamos as $materialprestamo) {               
                    array_push($ids_recursos, $materialprestamo->materialdeposito->material_id);                
            }
        }

        $materiales = Material::all()->whereIn('id', array_unique($ids_recursos));

        return $materiales;
    }


}
