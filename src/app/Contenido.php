<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contenido extends Model
{

    protected $table = "contenidos";
    protected $fillable = ['objetivos', 'contenidos', 'actividades', 'tipo_evaluacion', 'unidad_id'];

    public function titulo()
    {
        return $this->belongsTo('App\Titulo');
    }

}
