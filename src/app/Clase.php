<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clase extends Model
{

    protected $table = "clases";
    protected $fillable = ['fecha', 'curso_id', 'unidad_id'];

    protected $dates = ['fecha'];

    /**
     * Mutadores
     */

    public function getFechaFormateadoAttribute()
    {
        return $this->fecha->format('d/m/Y');
    }

    public function setFechaAttribute($value)
    {
        if (!is_null($value)) {
            $fecha = str_replace('/', '-', $value);
            $this->attributes['fecha'] = date('Y-m-d', strtotime($fecha));
        }
    }

    public function curso()
    {
        return $this->belongsTo('App\Curso');
    }

    public function unidad()
    {
        return $this->belongsTo('App\Unidad');
    }

    public function alumnosclases()
    {
        return $this->hasMany('App\Alumnoclase');
    }

    public function profesoresclases()
    {
        return $this->hasMany('App\Profesorclase');
    }

    public function datos_para_editar()
    {
        $ids = [];
        foreach ($this->profesoresclases as $profesorclase) {
            array_push($ids, $profesorclase->profesorcurso_id);
        }
        $datos = [
            'id' => $this->id,
            'fecha' => $this->getFechaFormateadoAttribute(),
            'curso_id' => $this->curso_id,
            'unidad_id' => $this->unidad_id,
            'ids_profesores' => $ids
        ];
        return json_encode($datos);
    }
}
