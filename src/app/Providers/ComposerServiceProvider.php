<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composers
        ([
            //'App\Http\ViewComposers\SearchInmueblesComposer' => ['front.inicio.banner_busqueda', 'front.listado.panel_busqueda'],
            'App\Http\ViewComposers\NotificacionesComposer' => 'vendor.adminlte.layouts.partials.navtop.notificaciones',           
        ]);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
