<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Contenido;
use App\Familiamaterial;
use App\Http\Controllers\Controller;
use App\Http\Requests\TituloStoreRequest;
use App\Material;
use App\Modalidad;
use App\Materialtitulo;
use App\Titulo;
use App\Unidad;
use Illuminate\Http\Request;
use Session;

class Libro_aulaController extends Controller
{
    public function index()
    {        
        $titulos = Titulo::all();
        return view('vendor.adminlte.layouts.libro_aula.main')
            ->with('titulos', $titulos);
    }

}