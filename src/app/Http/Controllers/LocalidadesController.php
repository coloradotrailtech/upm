<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Localidad;
use App\Provincia;
use Illuminate\Http\Request;
use Session;

class LocalidadesController extends Controller {

    public function index() {
        $localidades = Localidad::all();
        $provincias = Provincia::all();
        return view('vendor.adminlte.layouts.localidades.main')->with('localidades', $localidades)->with('provincias', $provincias); // se devuelven los registros
    }

    public function create() {
        
    }

    public function store(Request $request) {
        $localidad = new Localidad($request->all());
        $localidad->save();
        Session::flash('message', 'Se ha registrado a una nueva localidad.');
        return redirect()->route('localidades.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    public function update(Request $request, $id) {
        $localidad = Localidad::find($id);
        $localidad->fill($request->all());
        $localidad->save();
        Session::flash('message', 'Se ha actualizado el registro');
        return redirect()->route('localidades.index');
    }

    
    public function destroy($id) {
        $localidad = Localidad::find($id);
        $localidad->delete();
        Session::flash('message', 'La localidad ha sido eliminada');
        return redirect()->route('localidades.index');
    }

}
