<?php

namespace App\Http\Controllers;

use App\Alumnoclase;
use App\Alumno;
use App\Estudiante;
use App\Clase;
use App\Curso;
use App\Examen;
use App\Examenalumno;
use App\Examenprofesor;
use App\Examenunidad;
use App\Localidad;
use App\Material;
use App\Materialsolicitudprestamo;
use App\Modalidad;
use App\Notificacion;
use App\Pais;
use App\Profesor;
use App\Profesorclase;
use App\Profesorcurso;
use App\Sede;
use App\Solicitudprestamo;
use App\Titulo;
use App\User;
use App\Persona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class CursosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /** Reglas para mostrar cursos:
         1) El usuario es "Encargado de Sede" de la sede donde se dicta curso.
            aclaración: si es "Encargado de Sede" de la central (Posadas) tambien puede ver cursos de aulas moviles de Posadas.
         2) El usuario es "encargado de aulas moviles", y el curso se dicta en un aula movil.
         */
        if (Auth::user()->isAdmin()) {
            $cursos = Curso::all();
            $sedes = Sede::all()->where('activa', 1)->where('movil', 0);
            $sedes_moviles = Sede::all()
                ->where('movil', 1);
            #->where('sede_id', 1);  #1= "Posadas"

        } elseif (Auth::user()->isEncargadoSede()) {
            if (Auth::user()->encargado->sede->central) { #trato especial sede Posadas
                $cursos = Curso::join("sedes", "cursos.sede_id", "=", "sedes.id")
                    ->join('localidades', 'sedes.localidad_id', "=", 'localidades.id')
                    ->where('localidades.nombre', '=', 'Posadas') # ← hardcodeo "suave"
                    ->select('cursos.*')->get();
            } else {
                $cursos = Curso::all()->where('sede_id', Auth::user()->encargado->sede->id)->where('movil', 0);
            }
            $sedes = Sede::all()->where('activa', 1)->where('movil', 0);
            $sedes_moviles = Sede::all()->where('movil', 1);


        } elseif (Auth::user()->isEncargadoTemporal()) {
            # ↓ Se obtienen los cursos dictados en "sedes temporales" y que "no son de la central (Posadas)". Esto es asi porque el los cursos en aulas moviles de Posadas solo deben ser visibles por el 'Encargado de Sede' de Posadas.
            $cursos = Curso::join("sedes", "cursos.sede_id", "=", "sedes.id")
                ->join('localidades', 'sedes.localidad_id', "=", 'localidades.id')
                ->where('localidades.nombre', '!=', 'Posadas') # ← hardcodeo "necesario"
                ->where('sedes.movil', '=', 1)
                ->select('cursos.*')    # esta linea soluciona alto bug con los id (traia "id" de localidad en vez de curso)
                ->get();

            $sedes = Sede::all()->where('activa', 1)->where('movil', 0);
            $sedes_moviles = Sede::all()->where('movil', 1)->whereNotIn('sede_id', 9);
        } elseif (Auth::user()->isBedel()) { #Bedel necesita tener acceso a todos los cursos que se den en Posadas ↓
            $cursos = Curso::join("sedes", "cursos.sede_id", "=", "sedes.id")
                ->join('localidades', 'sedes.localidad_id', "=", 'localidades.id')
                ->where('localidades.nombre', '=', 'Posadas')
                ->select('cursos.*')->get();
            #Sedes que seran visibles, esto resta determinar bien
            $sedes = Sede::all()->where('activa', 1)->where('movil', 0);
            $sedes_moviles = Sede::join("sedes", "cursos.sede_id", "=", "sedes.id")
                ->join('localidades', 'sedes.localidad_id', "=", 'localidades.id')
                ->where('localidades.nombre', '=', 'Posadas');
        }

        $hoy = date("Y-m-d");      

        $cont_programados = 0;
        $cont_finalizados = 0;
        $cont_activos     = 0;
        foreach ($cursos as $curso){
            //$fecha_inicio = strtotime($curso->fecha_inicio);
            $fecha_inicio = date($curso->fecha_inicio);
            $fecha_fin    = date($curso->fecha_fin);
            
            
            if( $hoy < $fecha_inicio ){
                $estado_curso = 'Por iniciar';
                $cont_programados++;
            }
            if ($hoy > $fecha_fin ) {
                $estado_curso = 'Finalizado';
                $cont_finalizados++;
            }
            if(($fecha_inicio<$hoy) && ($hoy<$fecha_fin)) {
                $estado_curso = 'activo';
                $cont_activos++;
            }
        }


        $cont_cursos = sizeof($cursos);
        $contenedor_conts = [
            'cursos'      => $cont_cursos, 
            'programados' => $cont_programados, 
            'activos'     => $cont_activos, 
            'finalizados' => $cont_finalizados
        ];
        /*
        echo('<pre>----Cont programados-----');
        var_dump($cont_programados);
        echo('<pre>');
        echo('<pre>----Cont finalizados-----');
        var_dump($cont_finalizados);
        echo('<pre>');
        echo('<pre>----Cont activos-----');
        var_dump($cont_activos);
        echo('<pre>');
        */

        $titulos = Titulo::all();
        $profesores = Profesor::all();
        
        return view('vendor.adminlte.layouts.cursos.main')
            ->with('sedes_moviles', $sedes_moviles)
            ->with('cursos', $cursos)
            ->with('contenedor_conts',$contenedor_conts)
            ->with('profesores', $profesores)
            ->with('sedes', $sedes)
            ->with('titulos', $titulos);
        
    }

    public function create()
    {
        //
    }

    public function sede_estadisticas(Request $request){
        $sede_id     = $request->select_sede;
        $daterange   = $request->daterange;
        $est_deseado = $request->est_deseado;

        $f_ini = substr($daterange,0,10);
        $f_fin = substr($daterange,-10);

        $sede   = Sede::find($sede_id);
        $cursos = Curso::whereBetween('fecha_inicio',[$f_ini,$f_fin])->get();

        if(count($cursos) == 0){            
            echo('<pre>---- No se han encontrado Cursos -----');
            echo('<pre>');
            var_dump($sede->localidad->nombre);
            echo('<pre>');
            var_dump($daterange);
            
        }else{
            $myData = $this->estadisticas_cursos($cursos,$sede);
            return view('vendor.adminlte.layouts.cursos.alumnado.sede_interes')
            ->with('cursos_presentes',$myData['cursos_presentes'])
            ->with('localidad',$myData['localidad'])
            ->with('contadores',$myData['contadores'])
            ->with('data_edades',$myData['data_edades'])
            ->with('data_trabaja',$myData['data_trabaja']);
        }   
    }

    public function sede_interes ($sede_id){

        $sede   = Sede::find($sede_id);
        $cursos = Curso::all();     
        $myData = $this->estadisticas_cursos($cursos,$sede);


        return view('vendor.adminlte.layouts.cursos.alumnado.sede_interes')
            ->with('cursos_presentes',$myData['cursos_presentes'])
            ->with('localidad',$myData['localidad'])
            ->with('contadores',$myData['contadores'])
            ->with('data_edades',$myData['data_edades'])
            ->with('data_trabaja',$myData['data_trabaja']);
    }

    public function estadisticas_cursos($cursos,$sede){
        $hoy = date("Y-m-d");
        $cursos_presentes = [];
        $cont_abandonado = 0; $cont_libre = 0; $cont_aprobado = 0; $cont_sin = 0; $cont_inscriptos = 0;
        $cont_10 = 0;$cont_20 = 0;$cont_30 = 0;$cont_40 = 0;
        $cont_50 = 0;$cont_60 = 0;$cont_70 = 0;$cont_80 = 0;
        $tot_ed  = 0;$max     = 0;$min   = 100;$tot_est = 0;
        $cont_masculino = 0; $cont_femenino = 0;
        $cont_trabaja = 0; $cont_no_trabaja =0;
        foreach ($cursos as $key => $myCurso) {            
            if($myCurso->sede_id == $sede->id){
                $cant_incriptos = $myCurso->alumnos->count();

                $cont_inscriptos = $cont_inscriptos + $cant_incriptos;

                $titulo = Titulo::find($myCurso->titulo_id);
                
                
                $info_est = [];
                $alumnos = $myCurso->alumnos;
               
                foreach($alumnos as $alumno){
                    $tot_est++;
                    $estado_curso = $alumno->estado_curso;
                    if($estado_curso == 'libre'){
                        $cont_libre++;
                    }else if($estado_curso == 'abandono'){
                        $cont_abandonado++;
                    }else if($estado_curso == 'aprobado'){
                        $cont_aprobado++;
                    }else{
                        $cont_sin++;
                    }


                    $persona = $alumno->estudiante->persona;

                    if($persona['sexo'] == 'Masculino'){
                        $cont_masculino++;
                    }elseif($persona['sexo'] == 'Femenino'){
                        $cont_femenino++;
                    }

                    $fecha_nac = $persona->fecha_nac;
                    $diferencia = date_diff(date_create($fecha_nac), date_create($hoy));     
                    
                    $edad = $diferencia->y;

                    $aux_estudiante = [
                        'per_id'    => $persona['id'],
                        'sexo'      => $persona['sexo'],
                        'educacion' => $persona['educacion'],
                        'trabaja'   => $alumno->estudiante->trabaja,
                        'edad'      => $edad
                    ];

                    if($edad < 20){
                        $cont_10++;
                    }elseif(($edad >= 20)&&($edad<30)) {
                        $cont_20++;
                    }elseif(($edad >= 30)&&($edad<40)) {
                        $cont_30++;
                    }elseif(($edad >= 40)&&($edad<50)) {
                        $cont_40++;
                    }elseif(($edad >= 50)&&($edad<60)) {
                        $cont_50++;
                    }elseif(($edad >= 60)&&($edad<70)) {
                        $cont_60++;
                    }elseif(($edad >= 70)&&($edad<80)) {
                        $cont_70++;
                    }elseif($edad >= 80) {
                        $cont_80++;
                    };            
        
                    $tot_ed = $tot_ed + $edad;
        
        
                    if($aux_estudiante['trabaja'] == 1){
                        $cont_trabaja++;
                    }else{
                        $cont_no_trabaja++;
                    }
                    
        
                    
                    array_push($info_est,$aux_estudiante);
                }
                
                

                $curso = [                
                    'id'            => $myCurso->id,
                    'curso'         => $titulo->nombre,  
                    'plan'          => $myCurso->titulo->plan,
                    'comision'      => $myCurso->comision,
                    'inscriptos'    => $cant_incriptos,
                    'fecha_inicio'  => $myCurso->getFechaInicioFormateadoAttribute(),
                    'fecha_fin'     => $myCurso->getFechaFinFormateadoAttribute(),
                    'estado'        => $myCurso->determinar_estado(),
                    'data_alu'      => $info_est
                ];

                array_push($cursos_presentes,$curso);
            }
        }        
        
        $contadores = [
            'inscriptos'  => $cont_inscriptos,  
            'aprobados'   => $cont_aprobado,
            'libres'      => $cont_libre,
            'abandonados' => $cont_abandonado,
            'sin_estado'  => $cont_sin
        ];          
        
        $data_edades = [
            'cont_10' => $cont_10,
            'cont_20' => $cont_20,
            'cont_30' => $cont_30,
            'cont_40' => $cont_40,
            'cont_50' => $cont_50,
            'cont_60' => $cont_60,
            'cont_70' => $cont_70,
            'cont_80' => $cont_80
        ];
        $p_trab  = ($cont_trabaja * 100)/$tot_est;
        $p_no_trab   = ($cont_no_trabaja  * 100)/$tot_est;
        $p_trab  = round($p_trab, 2);
        $p_no_trab   = round($p_no_trab , 2);

        $data_trabaja = [
            'trabaja'   => $cont_trabaja,
            'p_trab'    => $p_trab,
            'no_trabaja'=> $cont_no_trabaja,
            'p_no_trab' => $p_no_trab
        ];
        
        $localidad = $sede->localidad->nombre;

        $data_return = [
            'cursos_presentes' => $cursos_presentes,
            'localidad'        => $localidad,
            'contadores'       => $contadores,
            'data_edades'      => $data_edades,
            'data_trabaja'     => $data_trabaja   
        ];
        return $data_return;
        
    }

    public function localidades(){
        
        $sedes  = Sede::all();
        $cursos = Curso::all();
        $mySede = [];
        
        
        foreach ($sedes as $key => $sede) {

            $cont_cursos = 0;

            foreach($cursos as $myCurso){

                if($myCurso->sede_id == $sede->id){
                    $cont_cursos++;
                }
            }
            

            $localidad = $sede->localidad;
            
            $locs = [                
                'nombre' => $localidad->nombre,
                'cp'     => $localidad->cod_postal,
                'lat'    => $localidad->latitud,
                'lng'    => $localidad->longitud,

                'sede_id'=> $sede->id,
                'movil'  => $sede->movil,               
                'central'=> $sede->central,
                'cont'   => $cont_cursos
            ];
            
            array_push($mySede,$locs);               
        }
        sort($mySede);   


        
        return view('vendor.adminlte.layouts.cursos.alumnado.localidades')
            ->with('sedes_',$mySede);
        
    }

    public function alumnado(){

        $titulos  = Titulo::all();
        $titulos_ = [];

        $cursos  = Curso::all();
        $cant_tot = count($cursos);
        foreach ( $titulos as $tit ){
            $cant_curso   = 0;
            $cant_inscrip = 0;
            foreach ( $cursos as $curso ){
                if($curso->titulo_id == $tit->id){
                    $cant_curso++;
                }
            }

            $myti = [                
                'nombre' => $tit->nombre,
                'plan'   => $tit->plan,
                'id'     => $tit->id,
                'cant'   => $cant_curso
            ];
            array_push($titulos_,$myti);
            /*
            echo('<pre>---- Titulos -----');
            var_dump($tit);s
            echo('<pre>');
            */
            
        }
        sort($titulos_);
        

       
        return view('vendor.adminlte.layouts.cursos.alumnado.main')
            ->with('titulos_',$titulos_)
            ->with('cant_tot',$cant_tot);
     

    }

    public function alumnos_en_curso ($curso_id){
        
        $titulo  = Titulo::find($curso_id);
        $cursos  = Curso::all();
        $alumnos = Alumno::all();

        

        //Cursos realizados de una planificacion 
        $cursos_realizados = [];
        $cursos_leng = count($cursos);
        for ($i=0; $i<$cursos_leng; $i++){

            $myCurso = $cursos[$i];
            $tit_id = $myCurso->titulo_id;

            if($tit_id == $curso_id){
                array_push($cursos_realizados,$myCurso);
            }
        }   

        if($cursos_realizados != null){
            $cont_abandonado = 0; $cont_libre = 0; $cont_aprobado = 0; $cont_sin = 0; $inscriptos = 0;

            foreach ($cursos_realizados as $myCurso){
                $alumnos_myCurso = [];
                
                $cont_alumnos = count($alumnos);
                for($x=0; $x<$cont_alumnos; $x++){
    
                    if($myCurso->id == $alumnos[$x]->curso_id){
                        $alumno = $alumnos[$x];
    
                        $estudiante = Estudiante::find($alumno->estudiante_id);
                        $persona    = Persona::find($estudiante->persona_id);
                        $localidad  = Localidad::find($persona->localidad_id);
    
                        $alumno_curso = [
                            'apellido'      => $persona->apellido,
                            'estudiante_id' => $alumno->estudiante_id,
                            'nombre'        => $persona->nombre,                        
                            'dni'           => $persona->dni,  
                            'localidad'     => $localidad->nombre,
                            'email'         => $persona->email,                     
                            'estado_curso'  => $alumno->estado_curso
                        ];
                        
                        $inscriptos++;
                        $estado = $alumno->estado_curso;
                        if($estado == 'libre'){
                            $cont_libre++;
                        }else if($estado == 'abandono'){
                            $cont_abandonado++;
                        }else if($estado == 'aprobado'){
                            $cont_aprobado++;
                        }else{
                            $cont_sin++;
                        }
                        
                        array_push($alumnos_myCurso,$alumno_curso);
                    }
                }
    
                $sede            = Sede::find($myCurso->sede_id);
                $localidad_curso = Localidad::find($sede->localidad_id);
    
                sort($alumnos_myCurso);
    
                $data_cursos[] = [ 
                    'curso_id'  => $myCurso->id,
                    'localidad' => $localidad_curso->nombre,
                    
                    'fec_inicio'=> $myCurso->getFechaInicioFormateadoAttribute(),
                    'fec_fin'   => $myCurso->getFechaFinFormateadoAttribute(),
                    'comision'  => $myCurso->comision,
                    'descrip'   => $myCurso->descripcion,
    
                    'alumnado'  => $alumnos_myCurso
                ];
            }


            $respuesta = [
                'titulo_id'     => $titulo->id,        
                'nombre'        => $titulo->nombre,
                'plan'          => $titulo->plan,
                'data_cursos'   => $data_cursos,                
            ];
            $contadores = [
                'inscriptos'  => $inscriptos,  
                'aprobados'   => $cont_aprobado,
                'libres'      => $cont_libre,
                'abandonados' => $cont_abandonado,
                'sin_estado'  => $cont_sin
            ];

            
            return view('vendor.adminlte.layouts.cursos.alumnado.lista_alumnado')
                ->with('planificacion',$respuesta)
                ->with('contadores',$contadores);
            

        }else{

            return view('vendor.adminlte.errors.404');

        }
           
    }

    public function exportar_xlsx($curso_id){

        $titulo  = Titulo::find($curso_id);
        $cursos  = Curso::all();
        $alumnos = Alumno::all();

        //Cursos realizados de una planificacion 
        $cursos_realizados = [];
        $cursos_leng = count($cursos);
        for ($i=0; $i<$cursos_leng; $i++){

            $myCurso = $cursos[$i];
            $tit_id = $myCurso->titulo_id;

            if($tit_id == $curso_id){
                array_push($cursos_realizados,$myCurso);
            }
        }

        foreach ($cursos_realizados as $myCurso){
            $alumnos_myCurso = [];
            
            $cont_alumnos = count($alumnos);
            for($x=0; $x<$cont_alumnos; $x++){

                if($myCurso->id == $alumnos[$x]->curso_id){
                    $alumno = $alumnos[$x];

                    $estudiante = Estudiante::find($alumno->estudiante_id);
                    $persona    = Persona::find($estudiante->persona_id);
                    $localidad  = Localidad::find($persona->localidad_id);

                    $alumno_curso = [
                        'apellido'      => $persona->apellido,
                        'estudiante_id' => $alumno->estudiante_id,
                        'nombre'        => $persona->nombre,                        
                        'dni'           => $persona->dni,  
                        'localidad'     => $localidad->nombre,
                        'email'         => $persona->email,                    
                        'estado_curso'  => $alumno->estado_curso
                    ];
                    
                    array_push($alumnos_myCurso,$alumno_curso);
                }
            }

            $sede            = Sede::find($myCurso->sede_id);
            $localidad_curso = Localidad::find($sede->localidad_id);

            sort($alumnos_myCurso);

            $data_cursos[] = [
                'curso_id'  => $myCurso->id,
                'localidad' => $localidad_curso->nombre,
                'fec_inicio'=> $myCurso->getFechaInicioFormateadoAttribute(),
                'fec_fin'   => $myCurso->getFechaFinFormateadoAttribute(),
                'comision'  => $myCurso->comision,
                'descrip'   => $myCurso->descripcion,

                'alumnado'  => $alumnos_myCurso
            ];
        }
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet charset=UTF-8');
        header('Content-Disposition: attachment; filename= '.$titulo->nombre.'- plan '.$titulo->plan.'.xls');
        echo("<h3>".$titulo->nombre."Plan".$titulo->plan."</h3>");
        echo('<meta charset="UTF-8"/>');
        foreach($data_cursos as $i=>$curso){
            $cont = $i+1;
            echo('<table>');
            echo("<thead style='background-color: #000;color:#fff;'>
                <tr>
                    <th> #   </th>
                    <th> Curso N°   </th>
                    <th> Localidad  </th>
                    <th> Inicio     </th>
                    <th> Fin        </th>
                    <th> Descrip    </th>
                    <th> Comisión   </th>                    
                </tr>
                </thead>");
            echo("<tbody>
                    <tr>
                        <td>".$cont." </td>
                        <td>".$curso['curso_id']." </td>
                        <td>".$curso['localidad']." </td>
                        <td>".$curso['fec_inicio']  ." </td>
                        <td>".$curso['fec_fin']." </td>
                        <td>".$curso['descrip']." </td>
                        <td>".$curso['comision'] ." </td>                        
                    </tr>
                </tbody>");
            echo('</table>');


            echo('<table>');
            echo("<thead>
                <tr>
                    <th>            </th>
                    <th> Mat        </th>
                    <th> Apellido   </th>
                    <th> Nombre     </th>
                    <th> DNI        </th>
                    <th> Localidad  </th>
                    <th> Email      </th>
                    <th> Estado Curso </th>
                </tr>
                </thead>");
            echo('<tbody>');
                foreach($curso['alumnado'] as $x=>$alumno){
                    $cont_alumnos = $x+1;
                    echo("<tr>
                        <td> ". $cont_alumnos."</td>
                        <td> ". $alumno['estudiante_id']."</td>
                        <td> ". $alumno['apellido']."</td>
                        <td> ". $alumno['nombre']."</td>
                        <td> ". $alumno['dni']."</td>
                        <td> ". $alumno['localidad']."</td>
                        <td> ". $alumno['email']."</td>
                        <td> ". $alumno['estado_curso']."</td>
                    </tr>");
                }

            echo('</tbody>');
            echo('</table>');
            echo('<hr>');
            $cont = 0;
            $cont_alumnos = 0;
        }   

    } 

    public function store(Request $request)
    {
        $curso = new Curso($request->all());
        $curso->fecha_inicio = $request->fecha_inicio;
        $curso->fecha_fin = $request->fecha_fin;
        $curso->activa = true;

        $curso->save();

        if ($request->suplentes) {
            foreach ($request->suplentes as $value) {
                $profesorcurso = new Profesorcurso();
                $profesorcurso->profesor_id = $value;
                $profesorcurso->curso_id = $curso->id;
                $profesorcurso->rol = "suplente";
                $profesorcurso->save();
            }
        }

        foreach ($request->titulares as $value) {
            $profesorcurso = new Profesorcurso();
            $profesorcurso->profesor_id = $value;
            $profesorcurso->curso_id = $curso->id;
            $profesorcurso->rol = "titular";
            $profesorcurso->save();
        }

        /* Creamos la notificación para el curso */
        if (count($curso->titulo->materiales_titulos) > 0) {

            $solicitudprestamo = new Solicitudprestamo();
            $solicitudprestamo->curso_id = $curso->id;
            $solicitudprestamo->deposito_id = $curso->sede->depositos->first()->id;
            $solicitudprestamo->estadosolicitudprestamo_id = 1; //enviado
            $solicitudprestamo->save();

            foreach ($curso->titulo->materiales_titulos as $material) {
                $materialsolicitudprestamo = new Materialsolicitudprestamo();
                $materialsolicitudprestamo->cantidad = 0;
                $materialsolicitudprestamo->materialtitulo_id = $material->id;
                $materialsolicitudprestamo->solicitudprestamo_id = $solicitudprestamo->id;
                $materialsolicitudprestamo->save();
            }

            $users = User::whereIn('rol_id', [1, 3])->get(); //admin y encargado de depósito

            foreach ($users as $user) {
                $notificacion = new Notificacion();
                $notificacion->mensaje = "<b>Nuevo curso:</b> " . $curso->titulo->nombre . " " .
                    $curso->titulo->plan . " el cual inicia el " . $curso->getFechaInicioFormateadoAttribute() . " y culminará el " .
                    $curso->getFechaFinFormateadoAttribute() . ". Recuerde realizar el envío de los recursos que necesite.";
                $notificacion->ocultar = false;
                $notificacion->tipo = "curso_por_iniciar";
                $notificacion->estado_leido = false;
                $notificacion->user_id = $user->id;
                $notificacion->curso_id = $curso->id;
                $notificacion->save();
            }
        }

        Session::flash('message', 'Se ha registrado un nuevo curso.');
        return redirect()->route('cursos.index');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $curso = Curso::find($id);
        $materiales  = Material::all();
        $modalidades = Modalidad::all();
        $titulo = Titulo::all();
        $sedes = Sede::all()->where('activa', 1)->where('movil', 0);
        $sedes_moviles = Sede::all()->where('activa', 1)->where('movil', 1);
        $profesores = Profesor::all();
        $paises = Pais::all();
        $localidades = Localidad::all();
        $alumnos = Alumno::all();

        $profes = [];
        $profes_cursos = Profesorcurso::all();
        foreach ($profes_cursos as $element){
            if($element->curso_id == $curso->id){               

                $profe = Profesor::find($element->profesor_id);
                $persona = Persona::find($profe->persona_id);

                $dataprofe = [
                    'nombre'    =>  $persona->nombre,
                    'apellido'  =>  $persona->apellido,
                    'rol'       =>  $element->rol,
                ];

                array_push($profes,$dataprofe);
            };  
        };
        $profes = array_reverse($profes);

        $cont_total = 0; $cont_abandono = 0; $cont_libre = 0; $cont_aprobado = 0; $cont_sin = 0;

        $myAlumnos=[];
        foreach( $curso->alumnos as $alumno ){
            
            $dataAlumno = [
                'apellido'  =>  $alumno->estudiante->persona->apellido,
                'nombre'    =>  $alumno->estudiante->persona->nombre,
                'dni'       =>  $alumno->estudiante->persona->dni,
                'estado_curso'  =>  $alumno->estado_curso,
                'mat'           =>  $alumno->estudiante->id,
            ];
            

            $estado_curso = $alumno->estado_curso;           
            $cont_total++;

            if($estado_curso == 'libre'){
                $cont_libre++;
            }
            if($estado_curso == 'abandono'){
                $cont_abandono++;
            }
            if($estado_curso == 'aprobado'){
                $cont_aprobado++;
            }
            if($estado_curso == null){
                $cont_sin++;
            }    
            
            array_push($myAlumnos,$dataAlumno);
        }

        $contadores = [
            'total'     => $cont_total,
            'abandono'  => $cont_abandono,
            'aprobado'  => $cont_aprobado,
            'libre'     => $cont_libre,
            'sin_estado'=> $cont_sin
        ];


        $curso->profes = $profes;
        sort($myAlumnos);
        
        return view('vendor.adminlte.layouts.cursos.show.main')
            ->with('sedes_moviles', $sedes_moviles)
            ->with('curso', $curso)
            ->with('modalidades', $modalidades)
            ->with('profesores', $profesores)
            ->with('sedes', $sedes)
            ->with('titulos', $titulo)
            ->with('materiales', $materiales)
            ->with('paises', $paises)
            ->with('localidades', $localidades)
            ->with('contadores', $contadores)
            ->with('myAlumnos',$myAlumnos)
        ;
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curso = Curso::find($id);
        $modalidades = Modalidad::all();
        return view('vendor.adminlte.layouts.cursos.show.examenes.administrar')
            ->with('curso', $curso)
            ->with('modalidades', $modalidades);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id){
        $curso = Curso::find($id);
        $curso->fill($request->all());
        $curso->fecha_inicio = $request->fecha_inicio;
        $curso->fecha_fin = $request->fecha_fin;

        $estado = false;
        if ($request->activa === 'on') {
            $estado = true;
        }
        $curso->activa = $estado;
        $curso->save();

        //Suspendido el cambio de profesores, 
        //hay que ver como hacer cuando existen asistencias y examenes  
        /*
        foreach ($curso->profesores as $profesor) {
            $profesor->delete();
        }

        if($request->suplentes){
            foreach ($request->suplentes as $value) {
                $profesorcurso = new Profesorcurso();
                $profesorcurso->profesor_id = $value;
                $profesorcurso->curso_id = $curso->id;
                $profesorcurso->rol = "suplente";
                $profesorcurso->save();
            }
        }

        foreach ($request->titulares as $value) {
            $profesorcurso = new Profesorcurso();
            $profesorcurso->profesor_id = $value;
            $profesorcurso->curso_id = $curso->id;
            $profesorcurso->rol = "titular";
            $profesorcurso->save();
        }
        */

        Session::flash('message', 'Se ha actualizado el registro.');
        return redirect()->route('cursos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $curso = Curso::find($id);
        $curso->delete();
        Session::flash('message', 'El curso ha sido dado de baja');
        return redirect()->route('cursos.index');
    }

    public function registrar_examen(Request $request){
        $examen = new Examen($request->all());
        $examen->fecha = $request->fecha;
        $examen->save();

        foreach ($request->unidades as $value) {
            $examenunidad = new Examenunidad();
            $examenunidad->examen_id = $examen->id;
            $examenunidad->unidad_id = $value;
            $examenunidad->save();
        }

        foreach ($request->profesores as $value) {
            $profesor = new Examenprofesor();
            $profesor->profesorcurso_id = $value;
            $profesor->examen_id = $examen->id;
            $profesor->save();
        }

        foreach ($request->lista as $value) {
            $examenalumno = new Examenalumno();
            $examenalumno->asistio = $value['asistio'] ? true : false;
            $examenalumno->motivo_inasistencia = $value['motivo_inasistencia'];
            $examenalumno->situacion = $value['situacion'];
            $examenalumno->alumno_id = $value['alumno_id'];
            $examenalumno->examen_id = $examen->id;
            $examenalumno->save();
        }
        Session::flash('message', 'Se ha registrado a una nuevo examen.');
        return response()->json("ok");
    }

    public function registrar_clase(Request $request){
        $curso = Curso::find($request->curso_id);
        $clase = new Clase($request->all());
        $clase->save();

        foreach ($request->profesores as $value) {
            $profesor = new Profesorclase();
            $profesor->profesorcurso_id = $value;
            $profesor->clase_id = $clase->id;
            $profesor->save();
        }

        foreach ($curso->alumnos as $alumno) {
            $alumnoclase = new Alumnoclase();
            $alumnoclase->asistio = true;
            $alumnoclase->alumno_id = $alumno->id;
            $alumnoclase->clase_id = $clase->id;
            $alumnoclase->save();
        }

        if ($request->lista_ina) {
            foreach ($request->lista_ina as $alumno_ina) {
                $alumnoclase = Alumnoclase::where('clase_id', $clase->id)
                    ->where('alumno_id', $alumno_ina['alumno_id'])->first();
                $alumnoclase->asistio = false;
                $alumnoclase->motivo_inasistencia = $alumno_ina['motivo_inasistencia'];
                $alumnoclase->save();
            }
        }

        Session::flash('message', 'Se ha registrado a una nueva clase.');
        return response()->json("ok");
    }

    public function comprobar_validez(Request $request){
        $curso = Curso::all()
            ->where('sede_id', $request->sede_id)
            ->where('activa', 1)
            ->where('titulo_id', $request->titulo_id)
            ->where('titulo_id', $request->comision)
            ->last();

        if ($curso) {
            return response()->json("si");
        } else {
            return response()->json("no");
        }
    }

    public function ingresar_solicitud($id){
        $curso = Curso::find($id);
        return view('vendor.adminlte.layouts.cursos.show.recursos.alta_solicitud')
            ->with('curso', $curso);
    }

    public function registrar_solicitud(Request $request){
        $curso = Curso::find($request->curso_id);
        $solicitudprestamo = new Solicitudprestamo();
        $solicitudprestamo->curso_id = $curso->id;
        $solicitudprestamo->deposito_id = $curso->sede->depositos->first()->id;
        $solicitudprestamo->estadosolicitudprestamo_id = 1;
        $solicitudprestamo->save();

        foreach ($request->lista as $value) {
            $materialsolicitudprestamo = new Materialsolicitudprestamo();
            $materialsolicitudprestamo->cantidad = $value['cantidad'];
            $materialsolicitudprestamo->materialtitulo_id = $value['materialtitulo_id'];
            $materialsolicitudprestamo->solicitudprestamo_id = $solicitudprestamo->id;
            $materialsolicitudprestamo->save();
        }

        /**
         * Creamos la notificación para el curso
         */

        if (count($curso->titulo->materiales_titulos) > 0) {

            $users = User::whereIn('rol_id', [1, 3])->get(); //admin y encargado de depósito

            foreach ($users as $user) {
                $notificacion = new Notificacion();
                $notificacion->mensaje = "<b>Nueva solicitud:</b> " . $curso->titulo->nombre . " " .
                    $curso->titulo->plan . " el cual inicia el " . $curso->getFechaInicioFormateadoAttribute() . " y culminará el " .
                    $curso->getFechaFinFormateadoAttribute() . ". Recuerde realizar el envío de los recursos que se hayan solicitado.";
                $notificacion->ocultar = false;
                $notificacion->tipo = "solicitud_materiales";
                $notificacion->estado_leido = false;
                $notificacion->user_id = $user->id;
                $notificacion->curso_id = $curso->id;
                $notificacion->save();
            }
        }

        Session::flash('message', 'Se ha registrado a una nueva solicitud.');
        return response()->json("ok");
    }

}