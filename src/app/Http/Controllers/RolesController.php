<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
Use Session;

class RolesController extends Controller {

    public function __construct() {
        Carbon::setlocale('es');
    }


    public function index(Request $request) {
        $roles = DB::table('roles')->get();      
        return view('admin/roles/main')->with('roles', $roles); // se devuelven los registros
    }


}
