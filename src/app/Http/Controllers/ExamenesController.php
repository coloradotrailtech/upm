<?php

namespace App\Http\Controllers;
use App\Curso;
use App\Examen;
use App\Modalidad;
use App\Examenprofesor;
use App\Examenunidad;
use Illuminate\Http\Request;
use Session;

class ExamenesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response()->json("no");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $examen = Examen::find($id);
        $examen->fill($request->all());
        $examen->save();

        foreach ($examen->examenesprofesorescursos as $examenprofesorcurso) {
            $examenprofesorcurso->delete();          
        }

        foreach ($examen->examenesunidades as $unidad) {
            $unidad->delete();          
        }

        foreach ($request->profesores as $value) {
            $profesor = new Examenprofesor();
            $profesor->profesorcurso_id = $value;
            $profesor->examen_id = $examen->id;
            $profesor->save();
        }

        foreach ($request->unidades as $value) {
            $examenunidad = new Examenunidad();
            $examenunidad->examen_id = $examen->id;
            $examenunidad->unidad_id = $value;
            $examenunidad->save();
        }

        Session::flash('message', 'Se ha actualizado el registro');
        return redirect()->route('cursos.show', $examen->curso_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $examen = Examen::find($id);
        $examen->delete();
        Session::flash('message', 'El examen ha sido dado de baja');
        return redirect()->route('cursos.show', $examen->curso_id);
    }
}
