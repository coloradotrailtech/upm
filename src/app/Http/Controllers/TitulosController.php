<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Contenido;
use App\Familiamaterial;
use App\Http\Controllers\Controller;
use App\Http\Requests\TituloStoreRequest;
use App\Material;
use App\Modalidad;
use App\Materialtitulo;
use App\Titulo;
use App\Unidad;
use Illuminate\Http\Request;
use Session;

class TitulosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titulos = Titulo::all();
        $categorias = Categoria::all();
        return view('vendor.adminlte.layouts.titulos.main')
            ->with('categorias', $categorias)
            ->with('titulos', $titulos);
    }

    public function create()
    {
        //
    }

    public function store(TituloStoreRequest $request)
    {
        $titulo = new Titulo($request->all());
        $titulo->planificacion_aprobada = false;
        $titulo->save();
        Session::flash('message', 'Se ha registrado a un nuevo oficio.');
        Session::flash('nuevo-titulo', 'ok');
        return redirect()->route('titulos.show', $titulo->id);
    }

    public function show($id)
    {
        $titulo = Titulo::find($id);
        $materiales = Material::all();
        $familiasmateriales = Familiamaterial::all();
        $categorias = Categoria::all();
        return view('vendor.adminlte.layouts.titulos.show.show')
            ->with('categorias', $categorias)
            ->with('materiales', $materiales)
            ->with('familiamateriales', $familiasmateriales)
            ->with('titulo', $titulo);
    }

    public function edit($id)
    {
        $titulo = Titulo::find($id);
        $materiales = Material::all();
        $modalidades = Modalidad::all();
        return view('vendor.adminlte.layouts.titulos.formulario.create_unidad')
            ->with('modalidades', $modalidades)
            ->with('materiales', $materiales)
            ->with('titulo', $titulo);
    }

    public function update(Request $request, $id)
    {
        $titulo = Titulo::find($id);
        $titulo->fill($request->all());
        if ($request->planificacion_aprobada === "on") {
            $titulo->planificacion_aprobada = true;
        } else {
            $titulo->planificacion_aprobada = false;
        }
        $titulo->save();
        Session::flash('message', 'Se ha actualizado el registro');
        return redirect()->route('titulos.show', $titulo->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $titulo = Titulo::find($id);
        $titulo->delete();
        Session::flash('message', 'El registro ha sido eliminado');
        return redirect()->route('titulos.index');
    }

    public function agregar_unidad(Request $request)
    {
        $titulo = Titulo::find($request->titulo_id);
        $unidad = new Unidad($request->all());
        if ($request->auditable === "true") {
            $unidad->auditable = "Sí";
        } else {
            $unidad->auditable = "No";
        }
        $unidad->numero_clase = count($titulo->unidades) + 1;
        $unidad->save();
        foreach ($request->lista_contenidos as $valor) {
            $contenido = new Contenido();
            $contenido->objetivos = $valor['objetivos'];
            $contenido->contenidos = $valor['contenidos'];
            $contenido->actividades = $valor['actividades'];
            $contenido->tipo_evaluacion = $valor['tipo_evaluacion'];
            $contenido->unidad_id = $unidad->id;
            $contenido->save();
        }
        Session::flash('message', '¡Se han registrado con éxito la nueva clase!');
        return response()->json($request);
    }

    public function ordenar_planificacion(Request $request)
    {
        $titulo = Titulo::find($request->titulo_id);
        foreach ($titulo->unidades as $unidad) {
            foreach ($request->planificacion_clases as $valor) {
                if ($unidad->id == $valor['unidad_id']) {
                    $unidad->numero_clase = $valor['numero_clase'];
                    $unidad->save();
                }
            }
        }
        $returnHTML = view('vendor.adminlte.layouts.titulos.show.unidades')->with('titulo', $titulo)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function store_material_titulo(Request $request)
    {
        $material = new Materialtitulo($request->all());
        $material->save();
        Session::flash('message', 'Se ha registrado a un nuevo recurso');
        return redirect()->route('titulos.show', $request->titulo_id);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_recurso($id)
    {
        $material = Materialtitulo::find($id);
        $titulo_id = $material->titulo_id;
        $material->delete();
        Session::flash('message', 'El registro ha sido eliminado');
        return redirect()->route('titulos.show', $titulo_id);
    }

    public function export_info($id){

        $titulo     = Titulo::find($id);
        $unidades   = Unidad::where('titulo_id','=',$id)->get();
        $cant_clases   = count($unidades);
        $carga_horaria = $titulo->obtener_carga_horaria_total('horas');
        $plan_str = strval($titulo->plan);

        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet charset=UTF-8');
        header('Content-Disposition: attachment; filename= Oficio '.$titulo->nombre.' - Plan '.$titulo->plan.'.xls');
        echo("<h4>Oficio ".$titulo->nombre." - Plan ".$titulo->plan."</h4>");
        echo('<meta charset="UTF-8"/>');
        echo('<table>');
        echo("<thead>
            <tr style='text-align: left;'>
                <th>Oficio</th>
                <td>".$titulo->nombre."</td>               
            </tr>
            <tr style='text-align: left;'>
                <th>Plan</th>
                <td>".$plan_str."</td>               
            </tr>
            <tr style='text-align: left;'>
                <th>Categoria</th>
                <td>".$titulo->categoria["nombre"]."</td>               
            </tr>
            <tr style='text-align: left;'>
                <th>Carga horaria</th>
                <td>".$carga_horaria." horas</td>               
            </tr>
            <tr style='text-align: left;'>
                <th>Cantidad de clases</th>
                <td>".$cant_clases."</td>               
            </tr>
            <tr></tr>
        </thead>");
        echo('</table>');

        echo ('<table>');
        echo ('<thead>
            <tr>
                <th>#</th>
                <th>Clase</th>
                <th>Tiempo</th>
            </tr>
        </thead>');
        echo ('<tbody>');
        foreach($unidades as $e_unidad){
            echo ("<tr>
                <td style='text-align: center;'>".$e_unidad['numero_clase']."</td>
                <td>".$e_unidad['nombre']."</td>
                <td>".$e_unidad['carga_horaria']." ".$e_unidad['unidad_tiempo']."</td>
                </tr>"    
            );
        }
        echo ('</tbody>');
        echo ('</table>');
    }
}
