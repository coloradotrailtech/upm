<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Curso;
use App\Estudiante;
use App\Localidad;
use App\Pais;
use App\Persona;
use App\Titulo;
use App\Sede;
use App\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class EstudiantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        

        $estudiantes = Estudiante::select('personas.apellido', 'personas.nombre', 'estudiantes.id', 'personas.dni', 'personas.telefono','localidades.nombre as localidad')
        ->join('personas','estudiantes.persona_id','=','personas.id')
        ->join('localidades','personas.localidad_id','=','localidades.id')
        ->get();


        /*
        return response()->json($estudiantes);
        */
        
        return view('vendor.adminlte.layouts.estudiantes.main')
            ->with('estudiantes', $estudiantes);
        
    }



    public function export_rango_etario(Request $request){

        $ed_min = $request->ed_min;
        $ed_max = $request->ed_max;
        $estado_deseado = $request->est_deseado;
        $hoy = date("Y-m-d");
        $data_estudiantes = [];
        $estudiantes = Estudiante::all();   

        $cont_encontrados=0;
        $cont_else=0;

        foreach($estudiantes as $est){
            $persona = Persona::find($est->persona_id);
            $fecha_nac = $persona->fecha_nac;
            $diferencia = date_diff(date_create($fecha_nac), date_create($hoy));

            if(($ed_min <= $diferencia->y)&&($diferencia->y < $ed_max)){
                $estudiante_alumno = Alumno::where('estudiante_id',$est->id)->get();

                foreach($estudiante_alumno as $est_alu){

                    if($estado_deseado == 'todos'){
                        $cont_encontrados++;
                        array_push($data_estudiantes,$est_alu); 
                    }elseif(($est_alu->estado_curso == $estado_deseado)){
                        $cont_encontrados++;
                        array_push($data_estudiantes,$est_alu); 
                    }
                    
                }     
                
            }else{
                $cont_else++;
            }
        }
        
        
        $cursos = Curso::all();
        $contenedor = [];

        foreach($cursos as $curso){
            $est_list= [];
            
            foreach($data_estudiantes as $alumno){
                if($curso->id == $alumno->curso_id){

                    $info_est = [
                        'apellido'  => $alumno->estudiante->persona->apellido,
                        'mat'       => $alumno->estudiante->id,                        
                        'nombre'    => $alumno->estudiante->persona->nombre,  
                        'dni'    => $alumno->estudiante->persona->dni,                      
                        'localidad' => $alumno->estudiante->persona->localidad->nombre,
                        'fecha_nac' => $alumno->estudiante->persona->getFechaNacFormateadoAttribute(),
                        'email' => $alumno->estudiante->persona->email,
                        'estado_curso' => $alumno->estado_curso,
                    ];

                    array_push($est_list,$info_est);
                }
            }


            if(count($est_list) == 0){

                
            }else{
                sort($est_list);

                $data_curso = [
                    'id' => $curso->id,
                    'nombre' =>$curso->titulo->nombre,
                    'localidad' => $curso->sede->localidad->nombre,
                    'f_inicio'  => $curso->getFechaInicioFormateadoAttribute(),
                    'f_fin'     => $curso->getFechaFinFormateadoAttribute(),
                ];

                $data_cursos_list_est = [
                    'curso'    => $data_curso,
                    'est_list' => $est_list 
                ];
                array_push($contenedor, $data_cursos_list_est);
            }                
            
            
        }



        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet charset=UTF-8');
        header('Content-Disposition: attachment; filename= '.$ed_min.' - '.$ed_max. 'alumnos'.$estado_deseado.'.xls');
        echo('<meta charset="UTF-8"/>');
        echo('<h4> Rango etario de '.$ed_min.' a '.$ed_max. ' | para Alumnos '.$estado_deseado.'</h4>');
        echo('<hr>');

        foreach($contenedor as $i=>$element){
            //var_dump($cont);
            $cont = $i+1;
            $data_curso = $element['curso'];
            echo('<table>');
            echo("<thead style='background-color: #000;color:#fff;'>
                <tr>
                    <th>    </th>
                    <th> Curso N°   </th>
                    <th> Localidad  </th>
                    <th> Nombre     </th>  
                    <th> Inicio     </th>
                    <th> Fin        </th>                 
                </tr>
                </thead>");
            echo("<tbody>
                    <tr>
                        <td>".$cont." </td>
                        <td>".$data_curso['id']." </td>
                        <td>".$data_curso['localidad']." </td>
                        <td>".$data_curso['nombre']." </td>                          
                        <td>".$data_curso['f_inicio']." </td> 
                        <td>".$data_curso['f_fin']." </td>                    
                    </tr>
                </tbody>");
            echo('</table>');
            

            //Tabla Alumnos
            echo('<table>');
            echo("<thead>
                <tr>
                    <th>            </th>
                    <th> Matricula  </th>
                    <th> Apellido   </th>
                    <th> Nombre     </th>
                    <th> DNI     </th>
                    <th> Localidad  </th>
                    <th> Fecha Nac  </th>
                    <th> Email  </th>
                    <th> Estado Curso </th>
                </tr>
                </thead>");
            echo('<tbody>');
                foreach($element['est_list'] as $x=>$alumno){
                    $cont_alumnos = $x+1;
                    echo("<tr>
                        <td> ". $cont_alumnos."</td>
                        <td> ". $alumno['mat']."</td>
                        <td> ". $alumno['apellido']."</td>
                        <td> ". $alumno['nombre']."</td>
                        <td> ". $alumno['dni']." </td>  
                        <td> ". $alumno['localidad']."</td>
                        <td> ". $alumno['fecha_nac']."</td>
                        <td> ". $alumno['email']."</td>
                        <td> ". $alumno['estado_curso']."</td>
                    </tr>");
                }

            echo('</tbody>');
            echo('</table>');
            echo('<hr>');
            $cont = 0;
            $cont_alumnos = 0;

        }
        
    }

    public function export_por_categoria(Request $request){
        $cat_id = $request->por_cat;
        $est_deseado = $request->est_deseado;

        $list_titulos = [];
        $info_est = [];
        $est_list = [];
        $contenedor = [];

        $categoria   = Categoria::find($cat_id);        
        $titulos_cat = Titulo::all()->where('categoria_id','=',$categoria->id);

        foreach ($titulos_cat as $i => $element) {
            $cursos_ele = Curso::all()->where('titulo_id','=',$element['id']);

            $info_tit = [
                'id'    => $element->id,
                'nombre'=> $element->nombre,
                'plan'  => $element->plan
            ];
            array_push($list_titulos,$info_tit);

            foreach ($cursos_ele as $key => $value) {
                $alu_curso = Alumno::all()->where('curso_id','=',$value->id);

                foreach ($alu_curso as $j => $alumno) {
                    if($alumno->estado_curso == $est_deseado){
                        $info_est = [
                            'apellido'  => $alumno->estudiante->persona->apellido,
                            'mat'       => $alumno->estudiante->id,                        
                            'nombre'    => $alumno->estudiante->persona->nombre,  
                            'dni'       => $alumno->estudiante->persona->dni,
                            'cel'       => $alumno->estudiante->persona->telefono,               
                            'localidad' => $alumno->estudiante->persona->localidad->nombre,
                            'fecha_nac' => $alumno->estudiante->persona->getFechaNacFormateadoAttribute(),
                            'email'     => $alumno->estudiante->persona->email,
                            'estado_curso' => $alumno->estado_curso,
                        ];
    
                        array_push($est_list,$info_est);
                    }
                }

                if(count($est_list) == 0){
                    //Ningun estudiante que cumpla con estado deseado                
                }else{
                    sort($est_list);
                    $data_curso = [
                        'id'        => $value->id,
                        'nombre'    => $value->titulo->nombre,
                        'localidad' => $value->sede->localidad->nombre,
                        'f_inicio'  => $value->getFechaInicioFormateadoAttribute(),
                        'f_fin'     => $value->getFechaFinFormateadoAttribute(),
                    ];
                    $data_cursos_list_est = [
                        'curso'    => $data_curso,
                        'est_list' => $est_list 
                    ];
                    array_push($contenedor, $data_cursos_list_est);

                }            
                $est_list = [];
            }

        } 

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet charset=UTF-8');
        header('Content-Disposition: attachment; filename= Categoria '.$categoria->nombre. '. Estado_alumnos'.' '.$est_deseado.'.xls');
        echo('<meta charset="UTF-8"/>');
        echo('<h4> Categoria '.$categoria->nombre. '. Estado alumnos: '.$est_deseado.'</h4>');
        
        foreach ($list_titulos as $i => $titulo) {
            $cont_tit = $i+1;
            echo("<h5>".$titulo['nombre'].' '.$titulo['plan']." </h5>");
        }

        echo('<hr>');
        
        foreach ($contenedor as $j => $element) {
            $cont = $j+1;
            $data_curso = $element['curso'];
            echo('<table>');
            echo("<thead style='background-color: #000;color:#fff;'>
                <tr>
                    <th>    </th>
                    <th> Curso N°   </th>
                    <th> Localidad  </th>
                    <th> Nombre     </th>  
                    <th> Inicio     </th>
                    <th> Fin        </th>                 
                </tr>
                </thead>");
            echo("<tbody>
                    <tr>
                        <td>".$cont." </td>
                        <td>".$data_curso['id']." </td>
                        <td>".$data_curso['localidad']." </td>
                        <td>".$data_curso['nombre']." </td>                          
                        <td>".$data_curso['f_inicio']." </td> 
                        <td>".$data_curso['f_fin']." </td>                    
                    </tr>
                </tbody>");
            echo('</table>');
            

            //Tabla Alumnos
            echo('<table>');
            echo("<thead>
                <tr>
                    <th>            </th>
                    <th> Matricula  </th>
                    <th> Apellido   </th>
                    <th> Nombre     </th>
                    <th> DNI        </th>
                    <th> Celular    </th>
                    <th> Localidad  </th>
                    <th> Fecha Nac  </th>
                    <th> Email      </th>
                    <th> Estado Curso </th>
                </tr>
                </thead>");
            echo('<tbody>');
                foreach($element['est_list'] as $x=>$alumno){
                    $cont_alumnos = $x+1;
                    echo("<tr>
                        <td> ". $cont_alumnos."</td>
                        <td> ". $alumno['mat']."</td>
                        <td> ". $alumno['apellido']."</td>
                        <td> ". $alumno['nombre']."</td>
                        <td> ". $alumno['dni']." </td>  
                        <td> ". $alumno['cel']." </td>  
                        <td> ". $alumno['localidad']."</td>
                        <td> ". $alumno['fecha_nac']."</td>
                        <td> ". $alumno['email']."</td>
                        <td> ". $alumno['estado_curso']."</td>
                    </tr>");
                }

            echo('</tbody>');
            echo('</table>');
            echo('<hr>');
            $cont = 0;
            $cont_alumnos = 0;


        }
        

    }




    public function legajo($id){
        $estudiante = Estudiante::find($id);

        $persona = Persona::find($estudiante->persona_id);
        $localidad = Localidad::find($persona->localidad_id);
        $localidad_name = $localidad->nombre;
        $alumnos = Alumno::all();



        $cont_libre = 0;
        $cont_abandono = 0;
        $cont_aprobado = 0;
        $cont_sin = 0;

        $cursos_realizados = [];
        $data_cursos = [];
        foreach ($alumnos as $element){            
            if($element->estudiante_id == $estudiante->id){
                array_push($cursos_realizados,$element);
            };  
        };
        
        foreach ($cursos_realizados as $element){

            $curso  = Curso::find($element->curso_id);
            $titulo = Titulo::find($curso->titulo_id);

            $estado_curso = $element->estado_curso;           

            if($estado_curso == 'libre'){
                $cont_libre++;
            }
            if($estado_curso == 'abandono'){
                $cont_abandono++;
            }
            if($estado_curso == 'aprobado'){
                $cont_aprobado++;
            }
            if($estado_curso == null){
                $cont_sin++;
            }         

            $sede = Sede::find($curso->sede_id);
            $localidad_sede = Localidad::find($sede->localidad_id);
            $sede_name = $localidad_sede->nombre;
            
            $data_curso = [
                'curso'     =>$curso->id,
                'sede'      =>$sede_name,
                'titulo'    =>$titulo->nombre,
                'plan'      =>$titulo->plan,
                'fecha_inicio'=>$curso->fecha_inicio,
                'fecha_fin' =>$curso->fecha_fin,
                'estado_curso'=>$estado_curso
            ];

            array_push($data_cursos,$data_curso);
        }

        $cont_total = count($cursos_realizados);
        $contadores = [
            'total'     => $cont_total,
            'abandono'  => $cont_abandono,
            'aprobado'  => $cont_aprobado,
            'libre'     => $cont_libre,
            'sin_estado'=> $cont_sin
        ];
        
        /*
        foreach ($data_cursos as $element){
            echo('<pre>');
            var_dump($element);
            echo('<pre>');
        }        
       */
        $localidades = Localidad::all();
        $paises = Pais::all();
        
        return view('vendor.adminlte.layouts.estudiantes.legajo')
            ->with('estudiante', $estudiante)
            ->with('persona', $persona)
            ->with('cursos',$data_cursos)
            ->with('contadores',$contadores)
            ->with('localidad', $localidad_name)
            ->with('localidades', $localidades)
            ->with('paises', $paises);
    }   

    public function estadisticas(){
        $estudiantes = Estudiante::all();
        $hoy = date("Y-m-d");
       
        $cont_10 = 0;$cont_20 = 0;$cont_30 = 0;$cont_40 = 0;
        $cont_50 = 0;$cont_60 = 0;$cont_70 = 0;$cont_80 = 0;
        $tot_ed  = 0;$max     = 0;$min   = 100;

        $cont_masculino = 0; $cont_femenino = 0;

        $cont_trabaja = 0; $cont_no_trabaja =0;

        $info_est = [];
        foreach($estudiantes as $est){
            
            $persona = Persona::find($est->persona_id);            
            if($persona['sexo'] == 'Masculino'){
                $cont_masculino++;
            }elseif($persona['sexo'] == 'Femenino'){
                $cont_femenino++;
            }

            $fecha_nac = $persona->fecha_nac;
            $diferencia = date_diff(date_create($fecha_nac), date_create($hoy));     
            
            $edad = $diferencia->y;

            $aux_estudiante = [
                'per_id'    => $persona['id'],
                'sexo'      => $persona['sexo'],
                'educacion' => $persona['educacion'],
                'edad'      => $edad
            ];

            if($edad < 20){
                $cont_10++;
            }elseif(($edad >= 20)&&($edad<30)) {
                $cont_20++;
            }elseif(($edad >= 30)&&($edad<40)) {
                $cont_30++;
            }elseif(($edad >= 40)&&($edad<50)) {
                $cont_40++;
            }elseif(($edad >= 50)&&($edad<60)) {
                $cont_50++;
            }elseif(($edad >= 60)&&($edad<70)) {
                $cont_60++;
            }elseif(($edad >= 70)&&($edad<80)) {
                $cont_70++;
            }elseif($edad >= 80) {
                $cont_80++;
            };            

            $tot_ed = $tot_ed + $edad;


            if($est->trabaja == 1){
                $cont_trabaja++;
            }else{
                $cont_no_trabaja++;
            }
            

            array_push($info_est,$aux_estudiante);
        }

        $media = $tot_ed / count($estudiantes);

        $data_edades = [
            'cont_10' => $cont_10,
            'cont_20' => $cont_20,
            'cont_30' => $cont_30,
            'cont_40' => $cont_40,
            'cont_50' => $cont_50,
            'cont_60' => $cont_60,
            'cont_70' => $cont_70,
            'cont_80' => $cont_80,

            'max'   => $max,
            'min'   => $min,
            'media' => $media
        ];

        
        $tot_est = count($estudiantes);
        $p_masc  = ($cont_masculino * 100)/$tot_est;
        $p_fem   = ($cont_femenino  * 100)/$tot_est;
        $p_masc  = round($p_masc, 2);
        $p_fem   = round($p_fem , 2);

        $sexo_cont = [
            'tot' => $tot_est,

            'mas' => $cont_masculino,
            'p_mas' => $p_masc,

            'fem' => $cont_femenino,
            'p_fem' => $p_fem
        ];

        $p_trab  = ($cont_trabaja * 100)/$tot_est;
        $p_no_trab   = ($cont_no_trabaja  * 100)/$tot_est;
        $p_trab  = round($p_trab, 2);
        $p_no_trab   = round($p_no_trab , 2);

        $trabaja_cont = [
            'trabaja'   => $cont_trabaja,
            'p_trab'    => $p_trab,
            'no_trabaja'=> $cont_no_trabaja,
            'p_no_trab' => $p_no_trab
        ];
    


        $categorias = Categoria::all();
        return view('vendor.adminlte.layouts.estudiantes.estadistica')
            ->with('cont_sex', $sexo_cont)
            ->with('data_edades', $data_edades)
            ->with('cont_trab', $trabaja_cont)
            ->with('categorias',$categorias);
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        /*** Si no llega un id de persona se crea un usuario, un persona y se notifica   */
        $nombreImagen = 'sin_imagen.png';
        if ($request->file('imagen')) {
            $file = $request->file('imagen');
            $nombreImagen = 'persona_' . time() . '.png';
            Storage::disk('personas')->put($nombreImagen, \File::get($file));
        }
        /** datos de persona*/
        $persona = new Persona($request->all());
        $persona->educacion = $request->estudios;  //QF: variable "educacion" venia llamada como "estudios" desde view.
        $persona->foto_perfil = $nombreImagen;

        if ($request->discapacidad == 'on') {
            $persona->discapacidad = true;
        } else {
            $persona->discapacidad = false;
        }

        if ($request->carnet_discapacidad == 'on') {
            $persona->carnet_discapacidad = true;
        } else {
            $persona->carnet_discapacidad = false;
        }

        if ($request->mano_habil == 'on') {
            $persona->mano_habil = "derecha";
        } else {
            $persona->mano_habil = "izquierda";
        }

        $persona->save();
        
        $estudiante = new Estudiante();
        $estudiante->persona_id = $persona->id;
        $estudiante->trabaja = $request->trabaja;
        if ($request->trabaja == 'on') {
            $estudiante->trabaja= true;
        } else {
            $estudiante->trabaja = false;
        }

        $estudiante->rubro_trabaja = $request->rubro_trabaja;
        $estudiante->save();
        if ($request->curso_id) {
            $alumno = new Alumno();
            $alumno->estudiante_id = $estudiante->id;
            $alumno->curso_id = $request->curso_id;
            if($request->realizo_tareas == 'on'){
                $alumno->realizo_tareas = true;
            }else{
                $alumno->realizo_tareas = false;
            }
            $alumno->save();
        }
        Session::flash('message', 'Se ha registrado un nuevo estudiante.');
        return redirect()->route('estudiantes.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $estudiante = Estudiante::find($id);
        $persona = Persona::find($estudiante->persona_id);
        $persona->fill($request->all());

        //Si llega sin modificacion, mantiene los datos de la base de datos
        $estudios = $request->estudios;
        if ($estudios != null ){
            $persona->educacion = $estudios;
        }
         

        if ($request->discapacidad == 'on') {
            $persona->discapacidad = true;
        } else {
            $persona->discapacidad = false;
        }

        if ($request->carnet_discapacidad == 'on') {
            $persona->carnet_discapacidad = true;
        } else {
            $persona->carnet_discapacidad = false;
        }

        if ($request->mano_habil == 'on') {
            $persona->mano_habil = "derecha";
        } else {
            $persona->mano_habil = "izquierda";
        }


        $persona->save();
        $estudiante->fill($request->all());
        if($request->trabaja=='on'){
            $estudiante->trabaja= true;
        } else {
            $estudiante->trabaja = false;
        }
        $estudiante->save();
        Session::flash('message', 'Se ha actualizado el registro');
        return redirect()->route('estudiantes.index');
    }

    public function getEstudiante($dni, $curso_id){
        $persona = Persona::where('dni', $dni)->first();
        $estudiante = Estudiante::where('persona_id', $persona->id)->first();

        if ($estudiante) {    #(existe el estudiante)
            $alumno = Alumno::where('estudiante_id', $estudiante->id)->where('curso_id', $curso_id)->first();

            $cursadas = Alumno::join('cursos', 'alumnos.curso_id', '=', 'cursos.id')
                ->join('titulos', 'cursos.titulo_id', '=', 'titulos.id')
                ->select('alumnos.*','titulos.nombre','cursos.*')
                ->where('estudiante_id',$estudiante->id)->get();

            if ($alumno) {    #existe el alumno en este curso
                return response()->json(-1); #devolvemos -1 si estudiante ya esta inscripto al curso
            }else{
                return response()->json(['persona'=> $persona, 'cursada'=>$cursadas]);
            }
        }else if($persona){
            return response()->json(['persona'=> $persona, 'msg'=>'Existe persona, pero no es estudiante']);
        } else {
            return response()->json("No existe el estudiante");
        }
    }

    public function destroy($id)
    {
        $estudiante = Estudiante::find($id);
        $estudiante->delete();     #soft delete
        Session::flash('message', 'Se ha desabilitado al docente satisfactoriamente.');
        return redirect()->route('estudiantes.index');
    }
}
