<?php

namespace App\Http\Controllers;

use App\Encargado;
use App\Sede;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use Storage;

class UsersController extends Controller
{

    public function index()
    {
        $usuarios = User::all();
        $roles = DB::table('roles')->get();
        $sedes= Sede::whereNull('encargado_id')->get();
        return view('vendor.adminlte.layouts.usuarios.main')
            ->with('usuarios', $usuarios)
            ->with('roles', $roles)
            ->with('sedes_sin_encargado', $sedes);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $nombreImagen = 'sin_imagen.png';
        if ($request->file('imagen')) {
            $file = $request->file('imagen');
            $nombreImagen = 'usuario_' . time() . '.png';
            Storage::disk('usuarios')->put($nombreImagen, \File::get($file));
        }
        $usuario = new User($request->all());
        $usuario->name = $request->name;
        $usuario->imagen= $nombreImagen;
        $usuario->password = bcrypt($request->password);
        $usuario->save();

        /**Verificamos si usuario es un encargado de sede, si lo es → crear encargado */
        if($request->sede_id != null){
            $encargado = new Encargado();
            //$encargado->persona_id = $persona->id;
            //$encargado->horario_entrada = $request->horario_entrada;
            //$encargado->horario_salida = $request->horario_salida;
            $encargado->activo = true;
            $encargado->user_id = $usuario->id;
            $encargado->save();
            $sede = Sede::find($request->sede_id);
            $sede->encargado_id= $encargado->id;
            $sede->save();
        }


        Session::flash('message', '¡Se ha registrado a un nuevo usuario con éxito!');
        return redirect()->route('usuarios.index');
        #return response()->json('ok');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function actPass(Request $request, $id)
    {
        $usuario = User::find($id);
        $usuario->password = bcrypt($request->password);
        $usuario->save();
        Session::flash('message', '¡Se ha actualizado el password del usuario con éxito!');
        return redirect()->route('admin.usuarios.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = User::find($id);
        $nombreImagen = "sin_imagen.png";

        if ($request->file('imagen')) {
            $file = $request->file('imagen');
            response()->json($request);
            // dd($file);
            $nombreImagen = 'usuario_' . time() . '.png';
            if ((Storage::disk('usuarios')->exists($usuario->imagen)) && ($usuario->imagen !== "sin_imagen.png")) {
                Storage::disk('usuarios')->delete($usuario->imagen); // Borramos la imagen anterior.
            }
            Storage::disk('usuarios')->put($nombreImagen, \File::get($file)); // Movemos la imagen nueva al directorio /imagenes/usuarios
        }

        $usuario->fill($request->all());
        if ($nombreImagen !== "") {
            $usuario->imagen = $nombreImagen; // Actualizamos el nombre de la nueva imagen.
        }
        $usuario->rol_id= $request->rol_usuario;
        $usuario->save();

        Session::flash('message', '¡Se ha actualizado la información del usuario con éxito!');
        return response()->json('ok');
    }

    
    public function destroy($id)
    {
        $usuario = User::find($id);
        if ($usuario->imagen != 'sin_imagen.png') {
            Storage::disk('usuarios')->delete($usuario->imagen); // Borramos la imagen asociada.
        }
        $usuario->delete();
        Session::flash('message', '¡El usuario seleccionado a sido eliminado!');
        return redirect()->route('usuarios.index');
    }

}
