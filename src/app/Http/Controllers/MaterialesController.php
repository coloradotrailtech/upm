<?php

namespace App\Http\Controllers;

use App\Material;
use App\Familiamaterial;
use App\Umedida;
use Illuminate\Http\Request;
use Session;

class MaterialesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $familias_materiales = FamiliaMaterial::all();
        $unidades_medidas = Umedida::all();
        $tipos_unidades = ['Unidad básica', 'Volumen', 'Superficie', 'Masa', 'Capacidad', 'Longitud'];
        $materiales = Material::all();

        return view('vendor.adminlte.layouts.materiales.main')
            ->with('materiales', $materiales)
            ->with('familias_materiales', $familias_materiales)
            ->with('tipos_unidades', $tipos_unidades)
            ->with('unidades_medidas', $unidades_medidas); // se devuelven los registros
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $material = new Material($request->all());
        $material->save();
        Session::flash('message', 'Se ha registrado a un nuevo material.');
        return redirect()->route('materiales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $material = Material::find($id);
        $material->fill($request->all());
        $material->save();
        Session::flash('message', 'Se ha actualizado el registro');
        return redirect()->route('materiales.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $material = Material::find($id);
        $material->delete();
        Session::flash('message', 'El registro de material ha sido eliminado');
        return redirect()->route('materiales.index');
    }

}
