<?php

namespace App\Http\Controllers;

use App\Http\Requests\EncargadoStoreRequest;
use App\Localidad;
use App\Persona;
use App\Encargado;
use App\Sede;
use App\Titulacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Illuminate\Database\Eloquent\SoftDeletes;

class EncargadosController extends Controller
{

    public function index()
    {
        $encargados = Encargado::all();
        $sedes = Sede::all();

        $localidades = Localidad::all();
        #$personas = Persona::all()->whereNotIn('id', $inquilinos->pluck('id')->toArray());
        return view('vendor.adminlte.layouts.encargados.main')
            ->with('encargados', $encargados)
            ->with('sedes', $sedes)
            ->with('localidades', $localidades);
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EncargadoStoreRequest $request)
    {
        /*** Si no llega un id de persona se crea un usuario, un persona y se notifica   */
        $nombreImagen = 'sin_imagen.png';
         if ($request->file('imagen')) {
            $file = $request->file('imagen');
            $nombreImagen = 'persona_' . time() .'.png';
            Storage::disk('personas')->put($nombreImagen, \File::get($file));
         }
            /**
             * datos de persona
             */
         $persona = new Persona($request->all());
         $persona->foto_perfil = $nombreImagen;
         $persona->save();

        $encargado = new Encargado();
        $encargado->persona_id = $persona->id;
        $encargado->horario_entrada = $request->horario_entrada;
        $encargado->horario_salida = $request->horario_salida;
        $encargado->activo = true;
        $encargado->save();
        # fijamos el encargado en la sede ↓
        $sede = Sede::find($request->sede_id);
        $sede->encargado_id = $encargado->id;
        $sede->save();
        Session::flash('message', 'Se ha registrado un nuevo encargado de sede.');
        return redirect()->route('encargados.index');


    }



    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $encargado= Encargado::find($id);
        $encargado->delete();     #soft delete
        Session::flash('message', 'Se ha desabilitado al docente satisfactoriamente.');
        return redirect()->route('encargados.index');
    }
}
