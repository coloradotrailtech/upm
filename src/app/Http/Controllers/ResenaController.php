<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use App\Resena;
use Session;

class ResenaController extends Controller
{
    public function index() {

        $cursos  = Curso::all();
        $resenas = Resena::all();
        $fecha_hoy = date("Y-m-d");
        $cursos_finalizados = [];

        foreach ($cursos as $key => $myCurso) {
            if($fecha_hoy > $myCurso['fecha_fin']){
                $insert_curso = [                
                    'id'            => $myCurso->id,
                    'curso'         => $myCurso->titulo->nombre,  
                    'plan'          => $myCurso->titulo->plan,
                    'sede'          => $myCurso->sede->localidad->nombre,
                    'comision'      => $myCurso->comision,
                    'fecha_inicio'  => $myCurso->getFechaInicioFormateadoAttribute(),
                    'fecha_fin'     => $myCurso->getFechaFinFormateadoAttribute(),
                    'estado'        => $myCurso->determinar_estado()
                ];
                array_push($cursos_finalizados,$insert_curso);
            }
        }
        

        return response()->json([
            'fecha_hoy' => $fecha_hoy,
            'cursos' => $resenas
        ]);
    }
    
}
