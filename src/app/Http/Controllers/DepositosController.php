<?php

namespace App\Http\Controllers;

use App\Deposito;
use App\Estadosolicitudprestamo;
use App\Localidad;
use App\Material;
use App\Materialdeposito;
use App\Materialprestamo;
use App\Materialsolicitudprestamo;
use App\Notificacion;
use App\Prestamo;
use App\Sede;
use App\Solicitudprestamo;
use App\User;
use Illuminate\Http\Request;
use Session;

class DepositosController extends Controller
{

    public function index()
    {
        $sedes = Sede::all();
        $depositos = Deposito::all();
        $localidades = Localidad::where('provincia_id', 1)->get();
        return view('vendor.adminlte.layouts.depositos.main')
            ->with('sedes', $sedes)
            ->with('depositos', $depositos)
            ->with('localidades', $localidades);
    }

    public function store(Request $request)
    {
        $deposito = new Deposito($request->all());
        $deposito->save();
        Session::flash('message', 'Se ha registrado a un nuevo deposito.');
        return redirect()->route('depositos.index');
    }

    public function show($id)
    {
        $sedes = Sede::all();
        $deposito = Deposito::find($id);
        $localidades = Localidad::where('provincia_id', 1)->get();
        $prestamos_realizado = $deposito->prestamosactivos;
        $prestamos_culminado = $deposito->prestamosinactivos;
        $solicitudes_historial = $deposito->solicitudeshistorico;
        $solicitudes_pendiente = $deposito->solicitudespendientes;
        return view('vendor.adminlte.layouts.depositos.show.main')
            ->with('solicitudes_pendiente', $solicitudes_pendiente)
            ->with('solicitudes_historial', $solicitudes_historial)
            ->with('prestamos_realizado', $prestamos_realizado)
            ->with('prestamos_culminado', $prestamos_culminado)
            ->with('deposito', $deposito)
            ->with('sedes', $sedes)
            ->with('localidades', $localidades);
    }

    public function pantalla_agregar_inventario($id)
    {
        $deposito = Deposito::find($id);
        $ids_materiales_deposito = Materialdeposito::all()
            ->where('deposito_id', $id)->pluck('material_id')->toArray();

        $materiales = Material::all()->whereNotIn('id', $ids_materiales_deposito);

        return view('vendor.adminlte.layouts.depositos.formulario.agregar_a_inventario')
            ->with('deposito', $deposito)
            ->with('materiales', $materiales);
    }

    /**
     * administrar_stock the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pantalla_administrar_stock($id)
    {
        $deposito = Deposito::find($id);
        return view('vendor.adminlte.layouts.depositos.formulario.administrar_stock')
            ->with('deposito', $deposito);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $deposito = Deposito::find($id);
        $deposito->fill($request->all());
        $deposito->save();
        Session::flash('message', 'Se ha actualizado el registro');
        return redirect()->route('depositos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deposito = Deposito::find($id);
        $deposito->delete();
        Session::flash('message', 'El deposito ha sido dado de baja');
        return redirect()->route('depositos.index');
    }

    /**
     * quitar_existencia the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function quitar_existencia(Request $request)
    {
        foreach ($request->lista as $valor) {
            $material_deposito = Materialdeposito::find($valor);
            $material_deposito->delete();
        }
        Session::flash('message', '¡Los materiales fueron retirados del inventario con éxito!');
        return response()->json("OK, se eliminaron las existencias");
    }

    /**
     * quitar_existencia the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar_existencia(Request $request)
    {
        foreach ($request->lista as $valor) {
            $material_deposito = Materialdeposito::find($valor['id']);
            $material_deposito->stock = $valor['cantidad'];
            $material_deposito->save();
        }
        Session::flash('message', '¡Se han registrado con éxito todas las nuevas cantidades!');
        return response()->json("OK, se actualizaron las cantidades");
    }

    /**
     * agregar_existencia the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function agregar_existencia(Request $request)
    {
        foreach ($request->lista as $valor) {
            $material_deposito = new Materialdeposito();
            $material_deposito->material_id = $valor['id'];
            $material_deposito->deposito_id = $request->id_deposito;
            $material_deposito->stock = $valor['cantidad'];
            $material_deposito->save();
        }
        Session::flash('message', '¡Se han registrado con éxito todos los nuevos materiales!');
        return response()->json("OK, se actualizaron las cantidades");
    }

    public function ingresar_prestamo($id)
    {
        $solicitudprestamo = Solicitudprestamo::find($id);
        $estadossolicitudes = Estadosolicitudprestamo::all();
        return view('vendor.adminlte.layouts.depositos.show.prestamos.alta_prestamo')
            ->with('estadossolicitudes', $estadossolicitudes)
            ->with('solicitudprestamo', $solicitudprestamo);
    }

    public function detalle_solicitud(Request $request)
    {
        $solicitudprestamo = Solicitudprestamo::find($request->id);
        $returnHTML = view('vendor.adminlte.layouts.depositos.show.prestamos.body_modal_show_solicitud')->with('solicitudprestamoshow', $solicitudprestamo)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function detalle_prestamo(Request $request)
    {
        $prestamo = Prestamo::find($request->id);
        $returnHTML = view('vendor.adminlte.layouts.depositos.show.solicitudes.body_modal_show_prestamo')->with('prestamoshow', $prestamo)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function registrar_prestamo(Request $request)
    {
        $solicitudprestamo = Solicitudprestamo::find($request->solicitudprestamo_id);
        $solicitudprestamo->estadosolicitudprestamo_id = $request->estadosolicitudprestamo_id;
        $solicitudprestamo->save();

        if ($request->estadosolicitudprestamo_id !== '3') { // distinto de idrechazado
            $prestamo = new Prestamo();
            $prestamo->deposito_id = $solicitudprestamo->deposito_id;
            $prestamo->curso_id = $solicitudprestamo->curso_id;
            $prestamo->solicitudprestamo_id = $solicitudprestamo->id;
            $prestamo->estado = "realizado";
            $prestamo->save();

            foreach ($request->lista as $value) {
                $materialdeposito = Materialdeposito::find($value['id']);
                $materialdeposito->stock = $materialdeposito->stock - $value['cantidad'];
                $materialdeposito->save();

                $materialprestamo = new Materialprestamo();
                $materialprestamo->cantidad_prestada = $value['cantidad'];
                $materialprestamo->observacion = $value['observacion'];
                $materialprestamo->materialdeposito_id = $value['id'];
                $materialprestamo->prestamo_id = $prestamo->id;
                $materialprestamo->save();
            }
        }

        /**
         * Creamos la notificación para el curso
         */

        $users = User::where('rol_id', 1)->get(); //informamos a los admin
        foreach ($users as $user) {

            $notificacion = new Notificacion();
            $notificacion->mensaje = "<b>Respuesta solicitud:</b> El encargado del depósito ha respondido su solicitud de recursos. Puede verificar su respuesta desde la pantalla del detalle del curso: " . $solicitudprestamo->curso->titulo->nombre . " " .
            $solicitudprestamo->curso->titulo->plan;
            $notificacion->ocultar = false;
            $notificacion->tipo = "prestamo_materiales";
            $notificacion->estado_leido = false;
            $notificacion->user_id = $user->id;
            $notificacion->curso_id = $solicitudprestamo->curso->id;
            $notificacion->save();

        }

        if ($solicitudprestamo->curso->sede->encargado_id) { //notificación para el encargado de sede
            $encargado_id = $solicitudprestamo->curso->sede->encargado->persona->user->id;
            $notificacion = new Notificacion();
            $notificacion->mensaje = "<b>Respuesta solicitud:</b> El encargado del depósito ha respondido su solicitud de recursos. Puede verificar su respuesta desde la pantalla del detalle del curso: " . $solicitudprestamo->curso->titulo->nombre . " " .
            $solicitudprestamo->curso->titulo->plan;
            $notificacion->ocultar = false;
            $notificacion->tipo = "prestamo_materiales";
            $notificacion->estado_leido = false;
            $notificacion->user_id = $encargado_id;
            $notificacion->curso_id = $solicitudprestamo->curso->id;
            $notificacion->save();

        }

        Session::flash('message', 'Se ha registrado a un nuevo préstamo.');
        return response()->json("ok");
    }

    public function pantalla_transferir_stock($id)
    {
        $deposito = Deposito::find($id);
        $depositos = Deposito::all(); #despues reemplazar por verificacion de "habilitado"
        $materiales = Material::all();
        return view('vendor.adminlte.layouts.depositos.formulario.transferir_stock')
            ->with('deposito', $deposito)
            ->with('depositos', $depositos)
            ->with('materiales', $materiales);
    }

    /*** Devuelve los materiales por deposito seleccionado */
    public function buscar_stock_deposito(Request $request)
    {
        $materiales_deposito_origen = Materialdeposito::all()->where('deposito_id', $request->id_deposito_origen);
        $deposito_destino = Deposito::find($request->id_deposito_destino);
        $deposito_origen = Deposito::find($request->id_deposito_origen);
        return response()->json(view('vendor.adminlte.layouts.depositos.tablas.tabla_stock',
            compact('deposito_destino', 'materiales_deposito_origen', 'deposito_origen'))->render()); //devolvemos la vista de la tabla con la coleccion de objetos filtrados.
    }

    public function filtrar_prestamos(Request $request)
    {

        $vista = 'vendor.adminlte.layouts.depositos.show.prestamos.div_prestamos_' . $request->tipo;
        $returnHTML = "";
        $tipo = "";


        $deposito = Deposito::find($request->id);
        $ids_prestamos = [];

        if ($request->tipo === "realizado") {
            $tipo = "prestamos_realizado";
            $ids_prestamos = $deposito->prestamosactivos()->pluck('id')->toArray();
        } else {
            $tipo = "prestamos_culminado";
            $ids_prestamos = $deposito->prestamosinactivos()->pluck('id')->toArray();
        }

        $ids_prestamos_filtrados = Materialprestamo::whereIn('prestamo_id', $ids_prestamos);

        if ($request->materiales_id) {
            $ids_prestamos_filtrados = $ids_prestamos_filtrados->whereIn('materialdeposito_id', $request->materiales_id);
        }

        $ids_prestamos_filtrados = array_unique($ids_prestamos_filtrados->pluck('prestamo_id')->toArray());

        $prestamos = $deposito->prestamos->whereIn('id', $ids_prestamos_filtrados);

        if ($request->solicitantes_id) {
            $prestamos = $prestamos->whereIn('curso_id', $request->solicitantes_id);
        }

        if ($request->fechas) {
            $prestamos = $prestamos->whereIn('created_at', $request->fechas);
        }

        $returnHTML = view('vendor.adminlte.layouts.depositos.show.prestamos.div_' . $tipo)->with($tipo, $prestamos)->with('ids_solicitados', $request->materiales_id)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }




    public function filtrar_solicitudes(Request $request)
    {
     
        $returnHTML = "";
        $tipo = "";

        $deposito = Deposito::find($request->id);
        $ids_solicitudes = [];

        if ($request->tipo === "historial") {
            $tipo = "solicitudes_historial";
            $ids_solicitudes = $deposito->solicitudeshistorico()->pluck('id')->toArray();
        } else {
            $tipo = "solicitudes_pendiente";
            $ids_solicitudes = $deposito->solicitudespendientes()->pluck('id')->toArray();
        }

        $ids_solicitudes_filtrados = Materialsolicitudprestamo::whereIn('solicitudprestamo_id', $ids_solicitudes);

        if ($request->materiales_id) {
            $ids_solicitudes_filtrados = $ids_solicitudes_filtrados->whereIn('materialtitulo_id', $request->materiales_id);
        }

        $ids_solicitudes_filtrados = array_unique($ids_solicitudes_filtrados->pluck('solicitudprestamo_id')->toArray());

        $solicitudes = $deposito->solicitudesprestamo->whereIn('id', $ids_solicitudes_filtrados);

        if ($request->solicitantes_id) {
            $solicitudes = $solicitudes->whereIn('curso_id', $request->solicitantes_id);
        }

        if ($request->fechas) {
            $solicitudes = $solicitudes->whereIn('created_at', $request->fechas);
        }

        $returnHTML = view('vendor.adminlte.layouts.depositos.show.solicitudes.div_' . $tipo)->with($tipo, $solicitudes)->with('ids_solicitados', $request->materiales_id)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }



    public function transferir_materiales(Request $request)
    {
        try {
            foreach ($request->lista as $valor) {
                #(1) descontamos lo que pasamos de deposito origen ↓
                #$material_deposito_origen = Materialdeposito::where('material_id',$valor['material_id'])->where('deposito_id', $request->id_dep_origen)->first();
                $material_deposito_origen = Materialdeposito::find($valor['id']);
                $material_deposito_origen->stock = $material_deposito_origen->stock - $valor['cantidad'];
                $material_deposito_origen->save();

                #(2) incrementamos stock en deposito destino, verificamos si el material ya existia en él ↓
                $material_deposito_destino = Materialdeposito::where('material_id', $valor['material_id'])->where('deposito_id', $request->id_dep_destino)->first();
                if (is_null($material_deposito_destino)) { #si es null quiere decir que el material que se quiere incrementar no existe en el destino. Lo Creamos
                $material_deposito_destino = new Materialdeposito();
                    $material_deposito_destino->material_id = $valor['material_id'];
                    $material_deposito_destino->deposito_id = $request->id_dep_destino;
                    $material_deposito_destino->stock = $valor['cantidad'];
                } else {
                    $material_deposito_destino->stock = $material_deposito_destino->stock + $valor['cantidad'];
                }
                $material_deposito_destino->save();

                /*
            redirect('reportes.remito')
            ->with('items_cantidades', $request->lista) #materiales y cantidades transferidas
            ->with('deposito_destino',$material_deposito_destino->deposito_id)
            ->with('deposito_origen',$material_deposito_origen->deposito_id);
             */
            }
            return response()->json("OK, se actualizaron las cantidades...falta imprimir remito");
        } catch (Exception $e) {
            echo 'Error en servidor al procesar transferencia entre depositos: ', $e->getMessage(), "\n";
            return response()->json("Se ha producido un error en el servidor");
        }

        #Session::flash('message', '¡Se han registrado con éxito todas las nuevas cantidades!');
        #return response()->json("OK, se actualizaron las cantidades");
    }

/*
public function verRemito($ajax_request){     #devuelve vista de remito

dd($ajax_request);
$deposito_destino = Deposito::find($ajax_request->id_dep_origen);
$deposito_origen = Deposito::find($ajax_request->id_dep_destino);
return view('reportes.remito')
->with('deposito_destino',$deposito_destino)
->with('deposito_origen',$deposito_origen);
}*/

}
