<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfesorStoreRequest;
use App\Localidad;
use App\Persona;
use App\Profesor;
use App\Titulacion;
use App\Pais;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProfesoresController extends Controller
{

    public function index()
    {
        $profesores = Profesor::all();
        $titulaciones = Titulacion::all();
        $localidades = Localidad::all();
        $paises = Pais::all();
        
        return view('vendor.adminlte.layouts.profesores.main')
            ->with('profesores', $profesores)
            ->with('titulaciones', $titulaciones)
            ->with('paises', $paises)
            ->with('localidades', $localidades);
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfesorStoreRequest $request)
    {        
        $nombreImagen = 'sin_imagen.png';
        if ($request->file('imagen')) {
            $file = $request->file('imagen');
            $nombreImagen = 'persona_' . time() .'.png';
            Storage::disk('personas')->put($nombreImagen, \File::get($file));
        }

        $persona = new Persona($request->all());
        $persona->foto_perfil = $nombreImagen;

        if ($request->mano_habil == 'on') {
            $persona->mano_habil = "derecha";
        } else {
            $persona->mano_habil = "izquierda";
        }

        $persona->save();       

        $profesor = new Profesor();
        $profesor->persona_id    = $persona->id;
        $profesor->titulacion_id = $request->titulacion_id;
        $profesor->descripcion   = null;
        $profesor->activo        = true;

        $profesor->save();
        
        Session::flash('message', 'Se ha registrado un nuevo docente.');
        return redirect()->route('profesores.index');    
    }



    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $profesor= Profesor::find($request->prof_id);
        $persona = Persona::find($profesor->persona->id);

        $profesor->titulacion_id= $request->titulacion_id;
        
        if ($request->mano_habil == 'on') {
            $persona->mano_habil = "derecha";
        } else {
            $persona->mano_habil = "izquierda";
        }
        $persona->apellido      = $request->apellido;
        $persona->nombre        = $request->nombre;
        $persona->localidad_id  = $request->localidad_id;
        $persona->direccion     = $request->direccion;
        $persona->dni           = $request->dni;
        $persona->email         = $request->email;
        $persona->telefono      = $request->telefono;
        $persona->sexo          = $request->sexo;
        $persona->fecha_nac     = $request->fecha_nac;
        $persona->estado_civil  = $request->estado_civil;
        $persona->pais_id       = $request->pais_id;
        

        $persona->save();
        $profesor->save();
        Session::flash('message', 'Los datos del profesor fueron actualizados.');
        return redirect()->route('profesores.index');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profesor= Profesor::find($id);
        $profesor->delete();     #soft delete
        Session::flash('message', 'Se ha desabilitado al docente satisfactoriamente.');
        return redirect()->route('profesores.index');
    }
}