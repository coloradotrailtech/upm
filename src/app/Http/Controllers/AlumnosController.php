<?php

namespace App\Http\Controllers;

use App\Alumno;
use App\Alumnoclase;
use App\Examenalumno;
use App\Estudiante;
use App\Persona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Mockery\Exception;

class AlumnosController extends Controller
{

    public function index()
    {
        $alumnos = Alumno::all();

        //return view('')
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        /*** Si no llega un id de persona se crea un usuario, un persona y se notifica   */
        $nombreImagen = 'sin_imagen.png';
        if ($request->file('imagen')) {
            $file = $request->file('imagen');
            $nombreImagen = 'persona_' . time() . '.png';
            Storage::disk('personas')->put($nombreImagen, \File::get($file));
        }
        try {
            $persona = Persona::where('dni', $request->dni)->first();
            if (is_null($persona)) {
                error_log($request);
                $persona = new Persona($request->all());
                $persona->foto_perfil = $nombreImagen;
                $persona->fecha_nac = $request->fecha_nac;
                $persona->educacion = $request->estudios;
                /*** Esto es una chanchada pero como se entrega mañana dejo asi */
                //hay que convertir los valores del swich para que se graben bien en la tabla "personas"
                if ($request->discapacidad == 'on') {
                    $persona->discapacidad = true;
                    if ($request->carnet_discapacidad == 'on') {
                        $persona->carnet_discapacidad = true;
                    } else {
                        $persona->carnet_discapacidad = false;
                    }
                } else {
                    $persona->discapacidad = false;
                    $persona->carnet_discapacidad = false;
                    $persona->detalle_discapacidad = null;
                }

                if ($request->mano_habil == 'on') {
                    $persona->mano_habil = "derecha";
                } else {
                    $persona->mano_habil = "izquierda";
                }
                
                $persona->save();
                $estudiante = new Estudiante();
                $estudiante->persona_id = $persona->id;

                if($request->trabaja == 'on'){ $estudiante->trabaja = 1;}
                else{ $estudiante->trabaja = 0;}

                $estudiante->rubro_trabaja = $request->rubro_trabaja;
                $estudiante->save();
                $est_id = $estudiante->id;
            } else {  #si persona existia
                if (is_null($persona->estudiante)) {  #si estudiente NO EXISTE (porque no tiene cursos anteriores)
                    $estudiante = new Estudiante();
                    $estudiante->persona_id = $persona->id;
                    if($request->trabaja == 'on'){
                        $estudiante->trabaja = 1;
                    }else{
                        $estudiante->trabaja = 0;
                    }
                    $estudiante->rubro_trabaja = $request->rubro_trabaja;
                    $estudiante->save();
                    $est_id = $estudiante->id;
                }else{
                    $est_id = $persona->estudiante->id;
                }
            }
            /*** ****************************************************************/

            $alumno = new Alumno();
            $alumno->estudiante_id = $est_id;
            $alumno->curso_id = $request->curso_id;
            $alumno->interes_trabajar = $request->interes_trabajar;
            $alumno->realizo_tareas = $request->realizo_tareas;
            $alumno->save();

            Session::flash('message', 'Se registro un nuevo alumno al curso satisfactoriamente.');            
            return redirect()->route('cursos.show', $request->curso_id);
            
        } catch (Exception $e) {
            Session::flash('danger', 'Se produjo un error en el proceso de alta del alumno.');
            return redirect()->route('cursos.index');
        }
    }

    /** esta funcion sirve para cambiar el estado de un alumno dentro de un curso */
    public function cambiar_estado_curso(Request $request)
    {
        $alumno = Alumno::find($request->alumno_id);
        $estado = null;
        if ($request->estado_curso !== "null") {
            $estado = $request->estado_curso;
        }
        $alumno->estado_curso = $estado;
        $alumno->save();
    }

    public function update_estado_certificado(Request $request)
    {
        $alumno = Alumno::find($request->alumno_id);
        $estado = null;
        if ($request->estado_certificado !== "null") {
            $estado = $request->estado_certificado;
        }
        $alumno->estado_certificado = $estado;
        $alumno->save();
    }

    public function setear_estado_masivo($alumnos, $campo, $estado)
    {
        $array_ids_alumnos = json_decode($alumnos);
        $cantidad = 0; //para saber cuantos alumnos se actualizan

        $estado_seteado = null;
        if ($estado !== "null") {
            $estado_seteado = $estado;
        }

        if ($campo == "certificado") {
            foreach ($array_ids_alumnos as $alumno_id) {
                $alumno = Alumno::find($alumno_id);
                if($alumno->estado_curso === "aprobado"){
                    $alumno->estado_certificado = $estado_seteado;
                    $alumno->save();
                    $cantidad++;
                }               
            }
            if($cantidad > 0){               
                Session::flash('message', 'Se actualizó el estado de los certificados de '.$cantidad.' alumnos');
            }else{
                Session::flash('message_warning', 'No se actualizó el estado de ningún alumno');
            }

           
        } else {//estado_curso
            foreach ($array_ids_alumnos as $alumno_id) {
                $alumno = Alumno::find($alumno_id);
                if(is_null($alumno->estado_certificado)){
                    $alumno->estado_curso = $estado_seteado;
                    $alumno->save();
                    $cantidad++;
                }                   
            }
            if($cantidad > 0){
                Session::flash('message', 'Se actualizó el estado del curso de '.$cantidad.' alumnos');
            }else{
                Session::flash('message_warning', 'No se actualizó el estado de ningún alumno');
            }
        }
        return response()->json("ok");
    }


    public function eliminar_alumnos_array(Request $request){
        $alus_select = $request->all();
        $asis_elim = [];

        
        foreach ($alus_select as $alu) {
            $myAlumno = Alumno::find($alu['alumno_id']);
        
            $alu_clase = Alumnoclase::select('alumnosclases.id')
            ->join('clases','clases.id','=','alumnosclases.clase_id')
            ->where([['alumnosclases.alumno_id','=',$alu['alumno_id']],['clases.curso_id','=',$alu['curso_id']]])
            ->get();
            array_push($asis_elim,$alu_clase);            
            $myAlumno->delete();
        }

        foreach ($asis_elim as $alu_clase) {            
            foreach ($alu_clase as $as_id) {
                $row_clase  = Alumnoclase::find($as_id['id']);
                $row_examen = Examenalumno::find($as_id['id']);


                if($row_clase){ $row_clase->delete(); }
                if($row_examen){ $row_examen->delete(); }
            }
        }
        return response()->json(['mensaje'=>'Creqo que todo OK']);
    }

    public function show($id){ }

    public function edit($id){ }

    public function update(Request $request, $id){ }

    public function destroy($id){ }
}
