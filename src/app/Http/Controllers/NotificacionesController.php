<?php

namespace App\Http\Controllers;

use App\Notificacion;
use App\Curso;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class NotificacionesController extends Controller
{
    public function __construct()
    {
        Carbon::setlocale('es');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notificaciones = Notificacion::where('user_id', Auth::user()->id)
            ->where('ocultar', '<>', true)->latest()->paginate(5);
        return view('/vendor/adminlte/layouts/notificaciones/main', compact('notificaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $notificaciones = Notificacion::all()->where('estado_leido', '<>', true);

        foreach ($notificaciones as $notificacion) {
            if (!is_null($notificacion->visita)) {
                if (!is_null($notificacion->visita->confirmada)) {
                    $notificacion->estado_leido = true;
                }
            } else {
                $notificacion->estado_leido = true;
            }
            $notificacion->save();
        }
        return response()->json($notificaciones->count());
    }

    public function ocultar_notificaciones(Request $request)
    {
        if ($request->tipo === "todo") {
            $notificaciones = Notificacion::all()->where('ocultar', '<>', true);
        } else {
            $notificaciones = Notificacion::all()->whereIn('id', $request->lista);
        }
        foreach ($notificaciones as $notificacion) {
            $notificacion->ocultar = true;
            $notificacion->save();
        }
        return response()->json($notificaciones->count());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
