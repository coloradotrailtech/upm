<?php

namespace App\Http\Controllers;

use App\Alumnoclase;
use App\Curso;
use App\Clase;
use App\Profesorclase;
use Illuminate\Http\Request;
use Session;

class ClasesController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {    
        $curso = Curso::find($id);        
        return view('vendor.adminlte.layouts.cursos.show.clases.administrar')
        ->with('curso', $curso);
    }


    public function update(Request $request, $id)
    {
        $clase = Clase::find($id);
        $clase->fill($request->all());
        $clase->save();

        foreach ($clase->profesoresclases as $profesorclase) {
            $profesorclase->delete();          
        }

        foreach ($request->profesores as $value) {
            $profesor = new Profesorclase();
            $profesor->profesorcurso_id = $value;
            $profesor->clase_id = $clase->id;
            $profesor->save();
        }

        Session::flash('message', 'Se ha actualizado el registro');
        return redirect()->route('cursos.show', $clase->curso_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $clase = Clase::find($id);
        $clase->delete();
        Session::flash('message', 'La clase ha sido dada de baja');
        return redirect()->route('cursos.show', $clase->curso_id);
    }
}
