<?php

namespace App\Http\Controllers;

use App\Material;
use App\Familiamaterial;
use App\Tipomaterial;
use App\Umedida;
use Illuminate\Http\Request;
use Session;

class TiposMaterialesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $familias_materiales = FamiliaMaterial::all();
        $tipos_materiales = Tipomaterial::all();

        return view('vendor.adminlte.layouts.tiposmateriales.main')
            ->with('familias_materiales', $familias_materiales)
            ->with('tipos_materiales', $tipos_materiales);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipo_material = new Tipomaterial($request->all());
        $tipo_material->save();
        Session::flash('message', 'Se ha registrado a un nuevo tipo de material.');
        return redirect()->route('tiposmateriales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $tipo_material = Material::find($id);
        $tipo_material ->fill($request->all());
        $tipo_material ->save();
        Session::flash('message', 'Se ha actualizado el registro');
        return redirect()->route('tiposmateriales.index');
    }

    public function destroy($id)
    {
        $tipo_material  = Tipomaterial::find($id);
        $tipo_material ->delete();
        Session::flash('message', 'El registro de material ha sido eliminado');
        return redirect()->route('tiposmateriales.index');
    }

}
