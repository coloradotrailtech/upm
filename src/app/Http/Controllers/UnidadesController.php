<?php

namespace App\Http\Controllers;

use App\Contenido;
use App\Material;
use App\Materialcontenido;
use App\Modalidad;
use App\Unidad;
use Illuminate\Http\Request;
use Session;

class UnidadesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unidad = Unidad::find($id);
        $modalidades = Modalidad::all();
        $materiales = Material::all();
        return view('vendor.adminlte.layouts.titulos.formulario.editar_unidad')
            ->with('materiales', $materiales)
            ->with('modalidades', $modalidades)
            ->with('unidad', $unidad);
    }

    public function actualizar_unidad(Request $request)
    {
        $unidad = Unidad::find($request->unidad_id);
        $unidad->fill($request->all());
        if ($request->auditable === "true") {
            $unidad->auditable = "Si";
        } else {
            $unidad->auditable = "No";
        }
        $unidad->save();

        foreach ($unidad->contenidos as $registro) {
            $afectados = Materialcontenido::where('contenido_id', $registro->id)->delete();
        }

        $afectados = Contenido::where('unidad_id', $unidad->id)->delete();

        foreach ($request->lista_contenidos as $valor) {
            $contenido = new Contenido();
            $contenido->objetivos = $valor['objetivos'];
            $contenido->contenidos = $valor['contenidos'];
            $contenido->actividades = $valor['actividades'];
            $contenido->tipo_evaluacion = $valor['tipo_evaluacion'];
            $contenido->unidad_id = $unidad->id;
            $contenido->save();
        }

        Session::flash('message', 'Se ha actualizado el registro');
        return response()->json("ok");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unidad = Unidad::find($id);
        $titulo_id = $unidad->titulo_id;
        $unidad->delete();
        Session::flash('message', 'El registro ha sido eliminado');
        return redirect()->route('titulos.show', $titulo_id);
    }

    public function obtener_contenidos(Request $request)
    {
        $unidad = Unidad::find($request->id);
        $contenidos = [];
        foreach ($unidad->contenidos as $contenido) {
            array_push($contenidos, [
                'objetivos' => $contenido->objetivos,
                'contenidos' => $contenido->contenidos,
                'actividades' => $contenido->actividades,
                'tipo_evaluacion' => $contenido->tipo_evaluacion,
            ]);
        }
        return response()->json($contenidos);
    }
}
