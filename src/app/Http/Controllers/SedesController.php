<?php

namespace App\Http\Controllers;

use App\Deposito;
use App\Http\Controllers\Controller;
use App\Localidad;
use App\Sede;
use App\Encargado;
use Illuminate\Http\Request;
use Session;

class SedesController extends Controller {

    public function index() {
        $sedes = Sede::all();
        $encargados = Encargado::all();
        $localidades = Localidad::all();
        return view('vendor.adminlte.layouts.sedes.main')
                        ->with('sedes', $sedes)
                        ->with('encargados', $encargados)
                        ->with('localidades', $localidades);
    }

    public function store(Request $request) {
        
        
        $sede = new Sede($request->all());
        $sede->activa=1;
        if($request->movil=='on'){$sede->movil=1;
        }else{$sede->movil=0;}

        $sede->save();
        $deposito= new Deposito();
        $deposito->sede_id= $sede->id;
        $deposito->localidad_id = $sede->localidad_id;
        $deposito->direccion= $sede->direccion;
        $deposito->descripcion= 'deposito default generado por sistema';
        $deposito->save();
        Session::flash('message', 'Se ha registrado a una nueva sede con su correspondiente deposito.');
        return redirect()->route('sedes.index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    public function update(Request $request, $id) {
        $sede = Sede::find($id);
        $sede->fill($request->all());
        if($request->movil=='si'){$sede->movil=1;
        }else{$sede->movil=0;}
        $sede->save();
        Session::flash('message', 'Se ha actualizado el registro');
        return redirect()->route('sedes.index');
    }

    public function destroy($id) {
        $sede = Sede::find($id);
        $sede->delete();
        Session::flash('message', 'La sede ha sido eliminada');
        return redirect()->route('sedes.index');
    }

}
