<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TituloStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        #dd($this->plan);
        return [
            'nombre' => [   #request mejorado que controla que nombre SI PUEDE repetirse si no se repite fecha de plan de estudios
                'required',
                Rule::unique('titulos')->ignore($this->id)->where(function ($query) {
                    $query->where('plan',$this->plan);
                })
            ],
            'plan' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre.unique' => 'Ya se encuentra registrado un titulo con éste nombre y fecha de plan de estudios.',
        ];
    }
}
