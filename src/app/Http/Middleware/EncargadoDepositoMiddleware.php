<?php

namespace App\Http\Middleware;

use Closure;

class EncargadoDepositoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && !auth()->user()->isEncargadoDeposito())
            return $next($request);
        else dd('Sus credenciales de acceso no le permiten acceder a este menu. Pongase en contacto con un administrador.');
    }
}
