<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    #esta clase representa el contenido a darse para como unidad tematica para alcanzar "titulo" de curso. Un conjunto de unidades se conceptualiza como un programa.
    # El curso es considerado una nueva edicion de la capacitacion para alcanzar el "titulo"

    protected $table = "unidades";
    protected $fillable = ['nombre', 'seh', 'modalidad_id', 'numero_clase', 'titulo_id', 'auditable', 'carga_horaria', 'unidad_tiempo'];

    public function titulo()
    {
        return $this->belongsTo('App\Titulo');
    }

    public function modalidad()
    {
        return $this->belongsTo('App\Modalidad');
    }

    public function contenidos()
    {
        return $this->hasMany('App\Contenido');
    }

    public function obtener_carga_horaria($medida)
    {
        $carga = 0;
        if ($medida === "horas") {
            if($this->unidad_tiempo === "horas"){
                $carga = $this->carga_horaria;
            }else{
                $carga = $this->carga_horaria * 60;
            }
        }elseif($medida === "minutos") {
            if($this->unidad_tiempo === "minutos"){
                $carga = $this->carga_horaria;
            }else{
                $carga = floor($this->carga_horaria / 60).":".($this->carga_horaria % 60);
            }
        }
        return $carga; 
    }
}
	
