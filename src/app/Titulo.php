<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Titulo extends Model
{

    protected $table = "titulos";
    protected $fillable = ['nombre', 'plan', 'categoria_id', 'planificacion_aprobada'];

    public function categoria()
    {
        return $this->belongsTo('App\Categoria');
    }

    public function unidades()
    {
        return $this->hasMany('App\Unidad');
    }

    public function cursos()
    {
        return $this->hasMany('App\Curso');
    }

    public function materiales_titulos()
    {
        return $this->hasMany('App\Materialtitulo');
    }

    public function ultima_cursada()
    {
        return $this->cursos->last();
    }


    public function obtener_carga_horaria_total($medida)
    {
        /*
         * Esta función recorre todas las unidades vinculadas y obteniene la sumatoria 
         * de la carga horaria de cada una de ellas.
         */

        $carga = 0;
        foreach ($this->unidades as $unidad) {
            $carga = $carga + $unidad->obtener_carga_horaria("minutos");
        }

        if ($medida === "horas") {
            $carga = floor($carga / 60);//.":".($carga % 60) ;
        }

        return $carga;
    }
}
