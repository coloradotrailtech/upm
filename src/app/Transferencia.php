<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transferencia extends Model {

    protected $table = "transferencias";
    protected $fillable = ['fecha_envio', 'origen_id', 'destino_id'];

    public function origen() {
        return $this->belongsTo('App\Deposito');
    }

    public function destino() {
        return $this->belongsTo('App\Deposito');
    }

}
