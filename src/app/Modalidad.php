<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modalidad extends Model
{
    protected $table =  "modalidades";

    protected $fillable = ['nombre'];

    public function unidades()
    {
        return $this->hasMany('App\Unidad');
    }

    public function examenes()
    {
        return $this->hasMany('App\Examen');
    }
}
