<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesorclase extends Model
{
    protected $table = "profesoresclases";
    protected $fillable = ['asistio', 'motivo_inasistencia', 'observacion', 'profesorcurso_id', 'clase_id'];

    public function profesorcurso() {
        return $this->belongsTo('App\Profesorcurso');
    }

    public function clase() {
        return $this->belongsTo('App\Clase');
    }
}
