<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //línea necesaria

class Encargado extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    #representa a una persona que sera parte del equipo que capacite a los alumnos durante un curso
    protected $table = "encargados";
    protected $fillable = [
        'persona_id',
        'horario_entrada',
        'horario_salida',
        'descripcion',
        'activo',
        'user_id',
        'deleted_at'
    ];

    public function Persona() {
        return $this->belongsTo('App\Persona');
    }

    public function User(){
        return $this->belongsTo('App\User');
    }

    public function Sede(){
        return $this->hasOne('App\Sede');
    }

    public function Titulacion() {
        return $this->belongsTo('App\Titulacion');
    }

    public function profesorcursos() {
        return $this->hasMany('App\Profesorcurso');
    }

}
