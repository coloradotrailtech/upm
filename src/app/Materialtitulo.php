<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Materialtitulo extends Model
{
    use SoftDeletes;

    protected $table = "materialestitulos";
    protected $fillable = ['nombre', 'cantidad', 'familiamaterial_id', 'titulo_id'];

    public function getNombreAttribute()
    {
        if ($this->trashed()) {
            return $this->attributes['nombre']. " (borrado)";
        } else {
            return $this->attributes['nombre'];
        }
    }

    public function titulo()
    {
        return $this->belongsTo('App\Titulo');
    }

    public function familiamaterial()
    {
        return $this->belongsTo('App\Familiamaterial');
    }
}
