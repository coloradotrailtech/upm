<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Resena extends Model
{
    protected $table =  "resenas";

    protected $fillable = [
        'curso_id',
        'preg1',
        'preg2',
        'preg3'
    ];

    
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function curso()
    {
        return $this->belongsTo('App\Curso');
    }
}
