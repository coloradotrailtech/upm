<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Familiamaterial extends Model
{
    protected $table = "familiasmateriales";
    protected $fillable = ['nombre'];

    public function tiposmateriales() {
        return $this->hasMany('App\Tipomaterial');
    }

}
