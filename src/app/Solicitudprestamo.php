<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitudprestamo extends Model
{
    protected $table = "solicitudesprestamos";
    protected $fillable = ['estadosolicitudprestamo_id', 'deposito_id', 'curso_id'];

    public function deposito()
    {
        return $this->belongsTo('App\Deposito');
    }

    public function curso()
    {
        return $this->belongsTo('App\Curso');
    }

    public function prestamo()
    {
        return $this->hasOne('App\Prestamo');
    }

    public function estadosolicitudprestamo()
    {
        return $this->belongsTo('App\Estadosolicitudprestamo');
    }

    public function materialessolicitudprestamo()
    {
        return $this->hasMany('App\Materialsolicitudprestamo');
    }

    public function color_estado()
    {
        if (is_null($this->prestamo)) {
            return "#d9534f";
        } else {            
            return "#5cb85c";
        }
    }
}
