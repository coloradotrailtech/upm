<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profesor extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    #representa a una persona que sera parte del equipo que capacite a los alumnos durante un curso
    protected $table = "profesores";
    protected $fillable = [
        'persona_id',
        'titulacion_id',
        'descripcion',
        'activo',
        'deleted_at'
    ];

    public function persona() {
        return $this->belongsTo('App\Persona');
    }

    public function titulacion() {
        return $this->belongsTo('App\Titulacion');
    }

    public function profesorcursos() {
        return $this->hasMany('App\Profesorcurso');
    }

}
