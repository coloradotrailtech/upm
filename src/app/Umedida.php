<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Umedida extends Model {

    public $table = "umedidas";
    public $fillable = ['nombre', 'abreviatura', 'tipoMedida'];

    public function materiales() {
        return $this->hasMany('App\Material');
    }

}
