<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = "personas";
    protected $fillable = [
        'nombre',
        'apellido',
        'dni',
        'cuil',
        'sexo',
        'fecha_nac',
        'telefono',
        'telefono2',
        'email',
        'localidad_id',
        'direccion',
        'pais_id',
        'sangre',
        'estado_civil',
        'mano_habil',
        'discapacidad',
        'detalle_discapacidad',
        'carnet_discapacidad',
        'educacion',
        'foto_perfil',
        'user_id',
        'descripcion',
    ];

    protected $dates = ['fecha_nac'];

    /**
     * Mutadores
     */

    public function getNombreCompletoAttribute()
    {
        return $this->apellido . " " . $this->nombre;
    }

    public function getFechaNacFormateadoAttribute()
    {
        return $this->fecha_nac->format('d/m/Y');
    }

    public function setFechaNacAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['fecha_nac'] = date('Y-m-d', strtotime($value));
        }
    }

    /*--RELACIONES--*/

    public function pais()
    {
        return $this->belongsTo('App\Pais');
    }

    public function localidad()
    {
        return $this->belongsTo('App\Localidad');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function estudiante()
    {
        return $this->hasOne('App\Estudiante');
    }

}
