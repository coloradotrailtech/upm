<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Sede extends Model {

    public $table = "sedes";
    public $fillable = [
        'localidad_id', 
        'direccion', 
        'movil', 
        'encargado_id', 
        'central', 
        'activa', 
        'descripcion'
    ];

    public function localidad() {
        return $this->belongsTo('App\Localidad');
    }

    public function cursos() {
        return $this->hasMany('App\Curso');
    }

    public function depositos() {
        return $this->hasMany('App\Deposito');
    }

    public function encargado() {
        return $this->belongsTo('App\Encargado');
    }

    public function getnombre() {
        return $this->localidad->nombre . " (" . $this->direccion . ")";
    }
}
