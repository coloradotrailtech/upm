<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materialtransferencia extends Model {

    protected $table = "materialestransferencias";
    protected $fillable = ['cantidad', 'observacion', 'material_id', 'transferencia_id'];

    public function material() {
        return $this->belongsTo('App\Material');
    }

    public function transferencia() {
        return $this->belongsTo('App\Transferencia');
    }

}
