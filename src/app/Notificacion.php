<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    protected $table = "notificaciones";

    protected $fillable = [
        'mensaje',
        'tipo',
        'ocultar',
        'estado_leido',
        'user_id',
        'curso_id',
    ];

    /**
     * Relaciones
     */

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function curso()
    {
        return $this->belongsTo('App\Curso');
    }

    public function tipo_notificacion()
    {
        switch ($this->tipo) {
            case 'curso_por_iniciar':
                return "Curso por iniciar";
            case 'curso_por_culminar':
                return 'Curso por culminar';
            case 'prestamo_materiales':
                return "Préstamo de materiales";
            case 'solicitud_materiales':
                return 'Solicitud de materiales';
        }
    }
}
