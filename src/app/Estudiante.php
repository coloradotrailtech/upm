<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estudiante extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    #representa a una persona que sera parte del equipo que capacite a los alumnos durante un curso
    protected $table = "estudiantes";
    protected $fillable = [
        'persona_id',
        'trabaja',
        'rubro_trabaja',
        'descripcion',
        'deleted_at'
    ];

    public function persona() {
        return $this->belongsTo('App\Persona');
    }

    public function alumnos(){
        return $this->hasMany('App\Alumno');
    }

    #↓ este metodo devuelve un objeto "Alumno" con el ultimo alumno que fue este estudiante (alumno de ultimo curso tomado)
    public function getAlumno_last(){
        #dd($this->alumnos->where());
    }
}
