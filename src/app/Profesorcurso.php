<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesorcurso extends Model {

    protected $table = "profesorescursos";
    protected $fillable = ['rol', 'curso_id', 'profesor_id'];

    public function curso() {
        return $this->belongsTo('App\Curso');
    }

    public function profesor() {
        return $this->belongsTo('App\Profesor');
    }

}
