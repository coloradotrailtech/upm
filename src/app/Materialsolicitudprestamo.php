<?php

namespace App;

Use App\Materialtitulo;
use Illuminate\Database\Eloquent\Model;

class Materialsolicitudprestamo extends Model
{
    protected $table = "materialessolicitudesprestamos";
    protected $fillable = ['cantidad', 'observacion', 'materialtitulo_id', 'solicitudprestamo_id'];

    public function getNombreMaterialAttribute()
    {
        $material = Materialtitulo::withTrashed()->where('id', $this->materialtitulo_id)->first();
        return $material->nombre;
    }

    public function materialtitulo()
    {
        return $this->belongsTo('App\Materialtitulo');
    }

    public function solicitud()
    {
        return $this->belongsTo('App\Solicitudprestamo');
    }
}
