<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    #return view('welcome');
    return view('adminlte::auth.login');
});

Route::group(['middleware' => 'auth'], function () {
    

    Route::resource('depositos', 'DepositosController');
    Route::resource('materiales', 'MaterialesController');
    Route::resource('tiposmateriales', 'TiposMaterialesController');

    Route::group(['middleware' => 'admin'], function () {
        Route::resource('sedes', 'SedesController');
        Route::resource('localidades', 'LocalidadesController');
        Route::resource('usuarios', 'UsersController');
    });


    Route::resource('libro_aula','Libro_aulaController');



    Route::put('profesores','ProfesoresController@update');
    Route::resource('profesores', 'ProfesoresController');
    
    Route::get('estudiantes/estadistica','EstudiantesController@estadisticas');
    Route::get('estudiantes/export_rango_etario','EstudiantesController@export_rango_etario');
    Route::get('estudiantes/export_por_categoria','EstudiantesController@export_por_categoria');
    Route::get('estudiantes/{estudiante_id}','EstudiantesController@legajo');
    Route::resource('estudiantes', 'EstudiantesController');

    Route::post('/eliminar_alumnos_seleccionados', 'AlumnosController@eliminar_alumnos_array');#fetch
    Route::resource('alumnos', 'AlumnosController');
    Route::resource('encargados', 'EncargadosController'); #encargados de las sedes (encargados de todos los cursos de la sede)
    Route::resource('examenes', 'ExamenesController');
    Route::resource('examenes_alumnos', 'ExamenesAlumnosController');
    Route::resource('clases', 'ClasesController');
    Route::resource('clases_alumnos', 'ClasesAlumnosController');
    Route::resource('solicitudes_prestamos', 'SolicitudesPrestamosController');
    Route::resource('notificaciones', 'NotificacionesController');
    Route::get('/ocultar_notificaciones', 'NotificacionesController@ocultar_notificaciones')->name('ocultar_notificaciones'); #ajax


    Route::get('/pantalla_agregar_inventario/{id}', 'DepositosController@pantalla_agregar_inventario')->name('pantalla_agregar_inventario');
    Route::get('/pantalla_administrar_stock/{id}', 'DepositosController@pantalla_administrar_stock')->name('pantalla_administrar_stock');
    Route::get('/pantalla_transferir_stock/{id}', 'DepositosController@pantalla_transferir_stock')->name('pantalla_transferir_stock');

    Route::get('/ingresar_prestamo/{id}', 'DepositosController@ingresar_prestamo')->name('ingresar_prestamo');
    Route::get('/registrar_prestamo', 'DepositosController@registrar_prestamo')->name('registrar_prestamo');

    Route::get('/quitar_existencia', 'DepositosController@quitar_existencia')->name('quitar_existencia');
    Route::get('/actualizar_existencia', 'DepositosController@actualizar_existencia')->name('actualizar_existencia'); #ajax
    Route::get('/agregar_existencia', 'DepositosController@agregar_existencia')->name('agregar_existencia');
    Route::get('comprobantes/remito', function () {
        return view('reportes/remito');
    });
    Route::get('/detalle_solicitud', 'DepositosController@detalle_solicitud')->name('detalle_solicitud'); #ajax
    Route::get('/detalle_prestamo', 'DepositosController@detalle_prestamo')->name('detalle_prestamo'); #ajax
    Route::get('/filtrar_prestamos', 'DepositosController@filtrar_prestamos')->name('filtrar_prestamos'); #ajax
    Route::get('/filtrar_solicitudes', 'DepositosController@filtrar_solicitudes')->name('filtrar_solicitudes'); #ajax

    Route::get('/buscar_stock_deposito', 'DepositosController@buscar_stock_deposito')->name('buscar_stock_deposito'); #ajax
    Route::get('/transferir_materiales', 'DepositosController@transferir_materiales')->name('transferir_materiales'); #ajax
    Route::name('remito')->get('comprobantes/remito/{request}', 'DepositosController@verRemito');

    Route::get('/titulos/export_info/{id}','TitulosController@export_info');
    Route::resource('titulos', 'TitulosController');
    Route::get('/agregar_unidad', 'TitulosController@agregar_unidad')->name('agregar_unidad'); #ajax
    Route::get('/ordenar_planificacion', 'TitulosController@ordenar_planificacion')->name('ordenar_planificacion'); #ajax
    Route::post('/store_material_titulo', 'TitulosController@store_material_titulo')->name('store_material_titulo'); #ajax
    Route::delete('/destroy_recurso/{id}', 'TitulosController@destroy_recurso')->name('destroy_recurso'); #ajax
    

    Route::resource('unidades', 'UnidadesController');
    Route::get('/obtener_contenidos', 'UnidadesController@obtener_contenidos')->name('obtener_contenidos'); #ajax
    Route::get('/actualizar_unidad', 'UnidadesController@actualizar_unidad')->name('actualizar_unidad'); #ajax
    
    Route::get('sede/sede_estadisticas','CursosController@sede_estadisticas');
    Route::get('sede/{sede_id}','CursosController@sede_interes');
    Route::get('cursos/localidades','CursosController@localidades');
    Route::get('cursos/alumnado/{curso_id}','CursosController@alumnos_en_curso');
    Route::get('cursos/alumnado/exports/{titulo_id}','CursosController@exportar_xlsx');
    Route::get('cursos/alumnado','CursosController@alumnado');//Comienzo a analizar los cursos
    

    Route::resource('cursos', 'CursosController');
    Route::get('/comprobar_validez', 'CursosController@comprobar_validez')->name('comprobar_validez'); #ajax
    //Route::get('/')
    Route::get('/registrar_clase', 'CursosController@registrar_clase')->name('registrar_clase');
    Route::get('/registrar_examen', 'CursosController@registrar_examen')->name('registrar_examen'); #ajax
    Route::get('/ingresar_solicitud/{id}', 'CursosController@ingresar_solicitud')->name('ingresar_solicitud');
    Route::get('/registrar_solicitud', 'CursosController@registrar_solicitud')->name('registrar_solicitud');
    Route::get('/getEstudiante/{dni}/{curso_id}', 'EstudiantesController@getEstudiante')->name('getEstudiante');
    Route::get('/cambiar_estado_curso/{alumno_id}/{estado_curso}', 'AlumnosController@cambiar_estado_curso')->name('cambiar_estado_curso');
    Route::get('/cambiar_estado_certificado/{alumno_id}/{estado_certificado}', 'AlumnosController@update_estado_certificado')->name('update_estado_certificado');

    Route::get('/setear_estado_masivo/{alumnos}/{campo}/{estado}', 'AlumnosController@setear_estado_masivo')->name('setear_estado_masivo'); #ajax

    Route::get('/resenas','ResenaController@index');

});
