<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>UPM | Remito de Materiales</title>
	
	<link rel='stylesheet' type='text/css' href={{asset('css/estilos_remito.css')}} />
	<link rel='stylesheet' type='text/css' href={{asset('css/print.css')}} media="print" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
	<!--     Fonts and icons     -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<!-- CSS Files -->
	<link href="{{asset('plugins/ui-kit/assets/css/bootstrap.min.css')}}" rel="stylesheet" />
	<link href="{{ asset('plugins/ui-kit/assets/css/now-ui-kit.css?v=1.2.0')}}" rel="stylesheet" />
</head>



<?php
/*	$datos = json_decode($_GET["datos_factura"]);

    #################################################################

	$items = $datos->items;
	#################################################################
*/

$datos = json_decode($_GET['datos_remito']);
$dep_origen= $datos->dep_origen;
$dep_destino= $datos->dep_destino;
$items_cantidades= $datos->items;

$n_factura = "000"."1";
$nombre = "JP Cáceres";
$fecha_hoy = getFecha_de_hoy();
$documento = "N° 1";
$usuario = "Lisandro M. Blanco";


?>

<body style="font-family: 'Raleway', sans-serif;">
	<div id="page-wrap">
		<h1 id="header">*** Remito de Envio ***</h1>
		<div id="identity" class="row" style="padding-bottom: 50px">
			<div class="col-lg-7 text-center">
				<h1>Universidad Popular de Misiones</h1>
			</div>
			<div class="col-lg-5">
				<div id="">
					{{--
                    <div id="logoctr">
                        <a href="javascript:;" id="change-logo" title="Change logo">Cambiar logo</a>
                        <a href="javascript:;" id="save-logo" title="Save changes">Guardar</a>
                        |
                        <a href="javascript:;" id="delete-logo" title="Delete logo">Borrar Logo</a>
                        <a href="javascript:;" id="cancel-logo" title="Cancel changes">Cancelar</a>
                      </div>
                      --}}

					<div id="logohelp">
						<input id="imageloc" type="text" size="50" value="" /><br />
						(ancho maximo: 540px, altura maxima: 100px)
					</div>
					<img id="image" src="{{asset('img/logo-editado.png')}}" alt="logo" />
				</div>
			</div>

		
			</div>
		
		<div style="clear:both"></div>		
		<div id="customer">
		<!--
	            <textarea id="customer-title">Solicitud de Pedido 
	             para --- </textarea>
		-->		
            <table border='0' id="tabla_cliente" class="class">
                <tr border="0">
                    <td border="0" class="meta-head">Desde: </td>
                    <td border='0'><input type="text" read_only="read_only" id="nombre" name="nombre" value="Deposito {{$dep_origen}}"> </td>
                </tr>
                <tr>
                    <td class="meta-head">Hacia:</td>
                    <td><input type="text" read_only="read_only" id="documento" name="documento" value="Deposito {{$dep_destino}}"></td>
                </tr>
				<tr>
					<td class="meta-head">Asunto:</td>
					<td><input type="text" read_only="read_only" id="asunto" name="asunto" value=""></td>
				</tr>
            </table>

		</div>
		
		<table id="items">
			<tr>
				<th> Item ID	</th>
				<th> Material	</th>
				<th> Cantidad	</th>
			</tr>
			@foreach($items_cantidades as $item_cantidad)

			  <tr class="item-row">
				  <td class="item-name">
					  <div class="delete-wpr"><textarea> {{$item_cantidad->id}}</textarea><a class="delete" href="javascript:;" title="Quitar item">X</a></div>
				  </td>
				  <td class="description"><textarea>{{$item_cantidad->nombre}}</textarea></td>
				  <td><textarea class="qty">{{$item_cantidad->cantidad}} {{$item_cantidad->umedida}}</textarea></td>
			  </tr>
			@endforeach

		  <tr id="hiderow">
		    <td colspan="5"><a id="addrow" href="javascript:;" title="Add a row"></a></td>
		  </tr>
		  {{--
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Subtotal: $</td>
		      <td class="total-value">
		      	<div id="subtotal">
		      		$ xxxx
		      	</div>
		      </td>
		  </tr>
		  <tr>

		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Total: $</td>
		      <td class="total-value">
		      	<div id="total">
		      		$
		      	</div>
		      </td>
		  </tr>
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Cantidad Pagada: </td>

		      <td class="total-value">
		      	$
		      </td>
		  </tr>
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line balance">Restan abonar: </td>
		      <td class="total-value balance"><div class="due">$ </div></td>
		  </tr>
		  	--}}
		</table>
		<br><br><br>
		<div id="terms">
		  <h5>Atención</h5>
		  <textarea>Sobrepasados los 30 dias cumplida la fecha estimada de entrega el cliente pierde todo derecho a reclamo.</textarea>
		</div>
	
	</div>	
</body>
</html>

<?php  
	function getFecha_de_hoy(){
		$array_fecha = getdate();
        $año = $array_fecha['year'];
        $mes = $array_fecha['mon'];
        $dia = $array_fecha['mday'];
        if(strlen ($mes)==1){                       #si mes tiene un digito anteponer un 0 al mes
            if(strlen ($dia)==1){                       #si dia tambien tiene un digito anteponer un 0 al dia
                $fecha_hoy = $año.'-0'.$mes.'-0'.$dia;
            }else{
                $fecha_hoy = $año.'-0'.$mes.'-'.$dia;
            }
        }else{
            if(strlen ($dia)==1){                       #si dia tiene un digito anteponer un 0 al dia
                $fecha_hoy = $año.'-'.$mes.'-0'.$dia;
            }else{
                $fecha_hoy = $año.'-'.$mes.'-'.$dia;
            }
        }
        return $fecha_hoy;
    }
?>