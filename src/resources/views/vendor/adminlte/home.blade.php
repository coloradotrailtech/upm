@extends('adminlte::layouts.app')

@section('htmlheader_title')

@endsection

@section('contentheader_title')

@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel-body text-center">
                <div class="panel-header">
                    <h4>¡Bienvenido {{ Auth::user()->name }}!</h4>  
                </div>  

                
                <img src="{{ asset('/img/login/logo-up.png') }}" class="img-fluid" alt="logo-home" width= '500px' >             
                
                <div class="panel-footer">
                    <strong>@php echo date("d / m / Y"); @endphp</strong>
                </div>
            </div>            
        </div>
    </div>
</div>
@endsection
