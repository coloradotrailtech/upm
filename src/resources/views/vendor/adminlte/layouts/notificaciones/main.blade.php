@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Notificaciones
@endsection

@section('contentheader_title')
Notificaciones @if (count($notificaciones) > 0)
<div class="btn-group pull-right">
    <button type="button" class="btn btn-danger" onclick="borrar('lista')">Borrar las notificaciones
        seleccionadas</button>
    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><a onclick="borrar('todo')">Borrar todas las notificaciones</a></li>
    </ul>
</div>
@endif
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
@endsection

@section('main-content')

@if (count($notificaciones) > 0)
<section class="content">
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-widget widget-user-2 infinite-scroll">
                <div class="box-footer no-padding">
                    <ul class="nav nav-stacked">
                        @foreach($notificaciones as $notificacion)
                        <li>
                            <a href="#">
                                <h4>
                                    <b>
                                        <i class="fa fa-lg fa-exclamation-circle" style="color: #f39c12"></i> 
                                        {{$notificacion->tipo_notificacion()}}
                                    </b>
                                    <div class="pull-right">
                                        <small><i class="fa fa-clock-o"></i>
                                            {{$notificacion->created_at->diffForHumans()}} </small>
                                        <br>
                                        <label class="pull-right form-check-label">
                                            <input type="checkbox"
                                                onchange="notificaciones_a_borrar.push({{$notificacion->id}})"
                                                class="form-check-input"> <i class="fa fa fa-trash-o"></i>
                                        </label>
                                    </div>
                                </h4>
                                <p> {!! $notificacion->mensaje !!}</p>                              
                            </a>
                        </li>
                        @endforeach
                        {{ $notificaciones->links() }}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@else
<br>
<div class="col-md-12">
    <div class="alert alert-info alert-dismissible animated fadeIn">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-exclamation-circle"></i><strong>Sin notificaciones</strong></h4>
        Su lista se encuentra vacía.
    </div>
</div>
@endif


@endsection

@section('scripts_del_modulo')
@endsection