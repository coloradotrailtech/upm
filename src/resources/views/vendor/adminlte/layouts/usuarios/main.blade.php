
@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
    <!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
    Usuarios
@endsection

@section('contentheader_title')
    Usuarios
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-cogs"></i> Generales</a></li>
<li class="active">Usuarios</li>
@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <i class="fa fa-list" aria-hidden="true"></i>
                        <h3 class="box-title"> Registros</h3>
                    </div>
                    <div class="box-body ">
                        @include('vendor.adminlte.layouts.partials.msj_acciones')
                        <table id="datatable-usuarios" class="display" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Email  📧 </th>
                                <th class="text-center">Rol de Acceso</th>
                                <th class="text-center">Fecha alta</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($usuarios as $usuario)
                                <tr>
                                    <td class="text-center text-bold">{{$usuario->name}}</td>
                                    <td class="text-center text-maroon"><em>{{$usuario->email}}</em></td>
                                    <td class="text-center text-uppercase">"{{$usuario->rol->nombre}}" - {{--$usuario->encargado->sede->localidad->nombre--}}</td>

                                    <td class="text-center text-success">{{$usuario->created_at->format('d/m/Y')}}</td>
                                    <td class="text-center">
                                        <a onclick="completar_campos({{$usuario}})" title="Editar este registro" class="btn btn-social-icon btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                        <a onclick="abrir_modal_borrar({{$usuario->id}})" title="Eliminar este registro" class="btn btn-social-icon btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <button title="Registrar una usuario" type="button" id="boton-modal-crear" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-crear">
                            <i class="fa fa-plus-circle"></i> &nbsp;registrar nuevo usuario
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @include('vendor.adminlte.layouts.usuarios.formulario.create')
    @include('vendor.adminlte.layouts.usuarios.formulario.editar')
    @include('vendor.adminlte.layouts.usuarios.formulario.confirmar')

@endsection

@section('scripts_del_modulo')
    <script src="{{ asset('js/admin/user.js') }}"></script>
    <script src="{{ asset('js/admin/imagen_croppie.js') }}"></script>
    <script src="{{ asset('js/menu.js') }}"></script>
@endsection




