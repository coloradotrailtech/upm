<div class="modal fade" id="modal-crear">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Registrar nuevo usuario del sistema</h4>
            </div>
            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form id="form-create" action="/usuarios" method="POST" enctype="multipart/form-data">
                    <input id="token-create" type="hidden" name="_token" value="{{ csrf_token() }}">
                    <h3>Detalles de la Cuenta</h3>
                    <br>
                    <div class="form-group">
                        <label>Correo electrónico:</label>
                        <input name="email" type="email" maxlength="50" class="form-control"
                            aria-describedby="emailHelp" placeholder="campo requerido" required>
                    </div>
                    <div class="form-group">
                        <label>Password:</label>
                        <input name="password" type="password" maxlength="50" class="form-control"
                            placeholder="campo requerido" required>
                    </div>
                    <div class="form-group">
                        <label>Confirmar password:</label>
                        <input name="password_confirmation" type="password" maxlength="50" class="form-control"
                            placeholder="campo requerido" required>
                    </div>
                    <div class="form-group">
                        <label>Rol de permisos</label>
                        <select id="rol_id" name="rol_id" placeholder="campo requerido" class="selectpicker form-control"
                            data-live-search="true">
                            @foreach($roles as $rol)
                                <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                            @endforeach
                        </select>
                        <p class="pull-left form-text text-muted"><strong>Información:</strong> el rol de permisos
                            define que acciones podrá realizar este usuario en el sistema.</p>
                    </div>
                    <div class="form-group hide" id="div_sedes">
                        <label>¿De que Sede o aula movil es encargado?</label>
                        <select id="sede_id" name="sede_id" placeholder="campo requerido" class="selectpicker form-control"
                                data-live-search="true">
                            @foreach($sedes_sin_encargado as $sede)
                                <option data-title="{{$sede->descripcion}}" value="{{$sede->id}}">{{$sede->localidad->nombre}} | {{$sede->direccion}}</option>
                            @endforeach
                        </select>
                        <p class="pull-left form-text text-muted"><strong>Información:</strong> el rol de permisos
                            define que acciones podrá realizar este usuario en el sistema.</p>
                    </div>
                    <br>
                    <hr />
                    <h3>Detalles del perfil</h3>
                    <br>
                    <div class="form-group">
                        <label for="formGroupExampleInput">Nombre completo:</label>
                        <input name="name" type="text" maxlength="50" class="form-control" placeholder="campo requerido"
                            required>
                    </div>
                    @include('vendor.adminlte.layouts.usuarios.formulario.imagen_create')
                    <button id="boton_submit_crear" type="submit" class="btn btn-primary hide"></button>
                </form>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn btn-primary" onclick="mandar('create');">registrar usuario</button>
            </div>
        </div>
    </div>
</div>
