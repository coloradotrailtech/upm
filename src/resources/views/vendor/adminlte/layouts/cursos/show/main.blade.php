@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
    Detalle del curso
@endsection

@section('contentheader_title')
    Curso {{$curso->titulo->nombre}} - Sede: {{$curso->sede->getnombre()}}
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Cursos</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <input class="hide" value="{{$curso->id}}" id="curso_id">
            @include('vendor.adminlte.layouts.partials.msj_acciones')
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab" aria-expanded="true">
                            <i class="fa fa-list-ul fa-lg" aria-hidden="true"></i>
                            &nbsp;&nbsp;Información del curso
                        </a>
                    </li>
                    <li class="">
                        <a href="#tab_2" data-toggle="tab" aria-expanded="false">
                            <i class="fa fa-lg fa-users" aria-hidden="true"></i>
                            &nbsp;&nbsp;Alumnos
                        </a>
                    </li>
                    <li class="">
                        <a href="#tab_3" data-toggle="tab" aria-expanded="false">
                            <i class="fa fa-lg fa-list-alt" aria-hidden="true"></i>
                            &nbsp;&nbsp;Asistencias
                        </a>
                    </li>
                    <li class="">
                        <a href="#tab_4" data-toggle="tab" aria-expanded="false">
                            <i class="fa fa-lg fa-pencil" aria-hidden="true"></i>
                            &nbsp;&nbsp;Exámenes
                        </a>
                    </li>
                    @if($curso->titulo->materiales_titulos->count() > 0)
                    <li class="">
                        <a href="#tab_5" data-toggle="tab" aria-expanded="false">
                            <i class="fa fa-lg fa-flask" aria-hidden="true"></i>
                            &nbsp;&nbsp;Recursos&nbsp;
                            @if($curso->prestamos->count()==0)
                            <span class="label label-warning pull-right"><i
                                    class="icon fa fa-lg fa-exclamation-circle"></i></span>
                            @endif
                        </a>
                    </li>
                    @endif                
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        @include('vendor.adminlte.layouts.cursos.show.info_general.main')
                    </div>
                    <div class="tab-pane" id="tab_2">
                        @include('vendor.adminlte.layouts.cursos.show.alumnos')
                    </div>
                    <div class="tab-pane" id="tab_3">
                        @include('vendor.adminlte.layouts.cursos.show.clases.main')
                    </div>
                    <div class="tab-pane" id="tab_4">
                        @include('vendor.adminlte.layouts.cursos.show.examenes.main')
                    </div>
                    @if($curso->titulo->materiales_titulos->count() > 0)
                    <div class="tab-pane" id="tab_5">
                        @include('vendor.adminlte.layouts.cursos.show.recursos.main')
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@include('vendor.adminlte.layouts.cursos.formulario.registrar_alumno')
@include('vendor.adminlte.layouts.cursos.formulario.editar')
@include('vendor.adminlte.layouts.cursos.formulario.confirmar')
@include('vendor.adminlte.layouts.cursos.show.examenes.editar_examen')
@include('vendor.adminlte.layouts.cursos.show.examenes.editar_nota')
@include('vendor.adminlte.layouts.cursos.show.clases.editar_clase')
@include('vendor.adminlte.layouts.cursos.show.clases.editar_asistencia')

@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/titulo.js') }}"></script>
<script src="{{ asset('js/admin/logos.js')  }}"></script>
<script src="{{ asset('js/admin/curso/imagenes.js') }}"></script>
<script src="{{ asset('js/admin/curso/tablas.js')   }}"></script>
<script src="{{ asset('js/admin/curso/curso.js')    }}"></script>
@endsection