<div class="modal fade" id="modal-update-asistencias">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Actualizar asistencia</h4>
            </div>
            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form id="form-update-asistencias" action="" method="POST" accept-charset="UTF-8" autocomplete="off">
                    <input name="_method" type="hidden" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">                   
                    <div class="form-group">
                        <label><b>Alumno:</b></label>
                        <span id="alumno_asistencia" class="form-control"></span>
                    </div>
                    <div class="form-group">
                        <label><b>Asitió a la clase:</b></label><br>
                        <input  
                            onchange="cambiar_asistencia('clase')"
                            name="asistio"
                            class="form-control"
                            data-width="100"
                            id="asistio_asistencia"
                            type="checkbox"                                                                                                                   
                            data-toggle="toggle" 
                            data-on="Activo" 
                            data-off="Inactivo" 
                            data-onstyle="success"> 
                    </div>
                    <div class="form-group">
                        <label><b>Motivo inasistencia:</b></label>
                        <input disabled name="motivo_inasistencia" id="motivo_inasistencia_asistencia" type="text" maxlength="500" class="form-control" placeholder="campo opcional">
                    </div>
                </form>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn  btn-warning" onclick="$('#form-update-asistencias').submit()">actualizar
                    registro</button>
            </div>
        </div>
    </div>
</div>