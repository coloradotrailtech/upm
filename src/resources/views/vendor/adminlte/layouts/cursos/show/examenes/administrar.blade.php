@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Carga examen
@endsection

@section('contentheader_title')
Carga examen
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Cursos</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Detalle</h3>
                </div>
                <div class="box-body ">
                    <br>
                    <div class="row">
                        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label><b>Fecha del examen:</b></label>
                                
                                <div class="input-append date datepicker">
                                    <input type="text" name="fecha" id="fecha" placeholder="seleccione una fecha"
                                        style="width:100%" readonly="readonly" required name="date">
                                    <span class="add-on"><i class="icon-th"></i></span>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label><b>Modalidad del examen:</b></label>
                                <select name="modalidad" id="modalidad" class="selectpicker form-control"
                                    data-live-search="true" title="campo requerido"
                                    data-header="Selecciona una modalidad" required="">
                                    @foreach($modalidades as $modalidad)

                                    <option value="{{$modalidad->id}}">
                                        {{$modalidad->nombre}}
                                    </option>

                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Unidad evaluada:</b></label>

                                <select name="unidades" id="unidades" class="selectpicker form-control"
                                    data-live-search="true" title="campo requerido" multiple
                                    data-header="Selecciona una unidad" required="">
                                    @foreach($curso->titulo->unidades as $unidad)

                                    <option value="{{$unidad->id}}">
                                        {{$unidad->nombre}} 
                                    </option>

                                    @endforeach
                                </select>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Evaluadores:</b></label>

                                <select name="profesores" id="profesores" class="selectpicker form-control"
                                    data-live-search="true" title="campo requerido" multiple
                                    data-header="Selecciona a los evaluadores" required="">
                                    @foreach($curso->profesores as $profesorcurso)
                                    <option value="{{$profesorcurso->id}}">
                                        {{$profesorcurso->profesor->persona->getNombreCompletoAttribute()}} 
                                    </option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="datatable-examenes" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class   = "sortable text-center" 
                                            style   = 'cursor: pointer'>
                                            Alumno          
                                        </th>
                                        <th class="text-center"> Documento          </th>
                                        <th class="text-center"> Asistió            </th>
                                        <th class="text-center"> Situación          </th>
                                        <th class="text-center"> Motivo inasistencia</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($curso->alumnos as $alumno)
                                        @if($alumno->estado_curso != 'libre' && $alumno->estado_curso != 'abandono' && $alumno->estado_curso != 'desaprobado')
                                            <tr>
                                                <td class="text-bold text-uppercase" width="30%">
                                                    {{$alumno->estudiante->persona->getNombreCompletoAttribute()}}
                                                </td>
                                                <td class="text-center text-bold">
                                                    {{$alumno->estudiante->persona->dni}}
                                                </td>
                                                <td class="text-center" width="10%">
                                                    <input type="checkbox" data-toggle="tooltip" checked
                                                        title="indica si el alumno asistió o no al examen"
                                                        onchange="cambiar_asistencia('{{$alumno->id}}')"
                                                        id="check-asistencia-{{$alumno->id}}" />
                                                </td>
                                                <td width="15%">
                                                    <select name="situacion" id="situacion-{{$alumno->id}}"
                                                        class="selectpicker form-control" data-live-search="true"
                                                        title="campo requerido" data-header="Selecciona una opción" required=""
                                                        onchange="carga_lista('{{$alumno->id}}')">
                                                        <option value="Aprobado">Aprobado</option>
                                                        <option value="Desaprobado">Desaprobado</option>
                                                    </select>
                                                </td>
                                                <td width="100%">
                                                    <input id="input-motivo-{{$alumno->id}}" type="text" maxlength="500"
                                                        class="form-control" disabled placeholder="motivo..."
                                                        onkeyup = "carga_lista('{{$alumno->id}}')"
                                                        onchange= "carga_lista('{{$alumno->id}}')">
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="text-center"> Alumno             </th>
                                        <th class="text-center"> Documento          </th>
                                        <th class="text-center"> Asistió            </th>
                                        <th class="text-center"> Situación          </th>
                                        <th class="text-center"> Motivo inasistencia</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="{{ route('cursos.show', $curso->id) }}" data-toggle="tooltip"
                        title="Volver a la pantalla anterior" class="btn btn-default btn-sm"><i
                            class="fa fa-lg fa-arrow-left"></i> volver</a>
                    <div class="pull-right">
                        <a onclick="mandar_lista({{$curso->id}})" data-toggle="tooltip"
                            title="Proceder a registrar el examen para este curso" id="boton-update"
                            class="btn btn-warning btn-sm">
                            <i class="fa fa-lg fa-pencil"></i>
                            registrar examen
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts_del_modulo')
    <script>
        let alumnos_cursantes = {{ count($curso->alumnos->where('estado_curso', null )) }};  //null for someone who's not "desaprobado/libre/abandono"
        let cantidad_alumnos = alumnos_cursantes;
    </script>
    <script src="{{ asset('js/admin/curso/administrar_examen.js') }}"></script>
    <script src="{{ asset('js/admin/sortable.js') }}"></script>
@endsection
