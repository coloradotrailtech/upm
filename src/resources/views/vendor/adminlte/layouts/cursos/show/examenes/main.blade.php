<br>
<div class="row">
    <div class="col-md-12">
        @if($curso->examenes->count() > 0)
        @include('vendor.adminlte.layouts.cursos.show.examenes.listado')
        @else
        <div class="alert alert-info alert-dismissible">
            <h4><i class="icon fa fa-info-circle"></i> Información</h4> Aún no se ha cargado
            ningún
            examen para este curso. Puede ingresar una dando un click al botón: <b><i class="fa fa-lg fa-list-alt"></i>
                cargar examen</b>.
        </div>
        @endif
    </div>
</div>
<br>
<div class="box-footer">
    <a href="{{ route('cursos.index') }}" data-toggle="tooltip" title="Volver a la pantalla anterior"
        class="btn btn-default btn-sm"><i class="fa fa-lg fa-arrow-left"></i> volver</a>
    <div class="pull-right">
        <a href="{{ route('cursos.edit', $curso->id) }}" data-toggle="tooltip"
            title="Cargar un examen y el listado de notas para este curso" class="btn btn-primary btn-sm">
            <i class="fa fa-lg fa-list-alt"></i> registrar examen</a>
    </div>
</div>
