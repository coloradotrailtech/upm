<label style="margin-bottom: 1%;"><strong>Materiales necesarios para el curso:</strong></label>
<div style="background-color: #f4f4f4;">
    <div style="padding: 1%;">
        <div style="background-color: white;">
            <div style="padding: 1%;">
                <div class="row">
                    <div class="col-md-12">
                        @if($curso->titulo->materiales_titulos->count() > 0 && $curso->prestamos->count() == 0)
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-exclamation-circle"></i>Atención:</h4> Aún no se han realizado
                            transferencias de materiales al curso. Puede realizar la solicitud desde la pestaña:
                            <b><i aria-hidden="true" class="fa fa-lg fa-flask"></i> Recursos</b>.
                        </div>
                        @elseif($curso->titulo->materiales_titulos->count() == 0)
                        <div class="alert alert-info alert-dismissible">
                            <h4><i class="icon fa fa-exclamation-circle"></i>Información:</h4>
                            Para este curso no se requiere ningún recurso material según su planificación.
                        </div>
                        @endif
                        @if($curso->titulo->materiales_titulos->count() > 0)
                        <table id="datatable-materiales-unidades" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-center">Nombre</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($curso->titulo->materiales_titulos as $material)
                                <tr>
                                    <td class="text-center text-bold">{{$material->nombre}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="text-center">Nombre</th>
                                </tr>
                            </tfoot>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>