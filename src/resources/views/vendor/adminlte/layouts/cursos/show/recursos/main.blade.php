<br>
<div class="row">
    <div class="col-md-12">
        @if($curso->solicitudesprestamo->count()>0)
        @include('vendor.adminlte.layouts.cursos.show.recursos.listado_solicitudes')
        @else
        <div class="alert alert-warning alert-dismissible">
            <h4><i class="icon fa fa-exclamation-circle"></i> Información</h4> Aún no se ha registrado
            ninguna solicitud de materiales o recursos para este curso. Puede gestionar las clases dando un click al
            botón:
            <b><i class="fa fa-lg fa-pencil-square-o"></i> ingresar solicitud</b>.
        </div>
        @endif
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        @if($curso->prestamos->count()>0)
        @include('vendor.adminlte.layouts.cursos.show.recursos.listado_prestamos')
        @else
        <div class="alert alert-warning alert-dismissible">
            <h4><i class="icon fa fa-exclamation-circle"></i> Información</h4> Aún no se ha registrado
            ninguna solicitud de materiales o recursos para este curso. Puede gestionar las clases dando un click al
            botón:
            <b><i class="fa fa-lg fa-pencil-square-o"></i> ingresar préstamo</b>.
        </div>
        @endif
    </div>
</div>
<br>
<div class="box-footer">
    <a href="{{ route('cursos.index') }}" data-toggle="tooltip" title="Volver a la pantalla anterior"
        class="btn btn-default btn-sm"><i class="fa fa-lg fa-arrow-left"></i> volver</a>
    <div class="pull-right">

        <a href="{{ route('ingresar_solicitud', $curso->id) }}" data-toggle="tooltip"
            title="Ir al modulo de asitencias de este curso" class="btn btn-primary btn-sm"><i
                class="fa fa-lg fa-pencil-square-o"></i>
            ingresar solicitud</a>
    </div>
</div>