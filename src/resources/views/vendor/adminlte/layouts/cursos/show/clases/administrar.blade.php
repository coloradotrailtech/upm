@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Registrar Asistencia a Clase
@endsection

@section('contentheader_title')
Registrar Asistencia a Clase
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Cursos</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Detalle</h3>
                </div>
                <div class="box-body ">
                    <br>
                    <div class="row">
                        <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Fecha de la clase:</b></label>
                                <div class="input-append date datepicker">
                                    <input type="text" name="fecha" id="fecha" placeholder="seleccione una fecha"
                                        style="width:100%" readonly="readonly" required name="date">
                                    <span class="add-on"><i class="icon-th"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label><b>Unidad evaluada:</b></label>
                                <select name="unidad_id" id="unidad_id" class="selectpicker form-control"
                                    data-live-search="true" title="campo requerido" data-header="Selecciona una unidad"
                                    required="">
                                    @foreach($curso->titulo->unidades as $unidad)
                                    <option value="{{$unidad->id}}">N° {{$unidad->numero_clase}} | {{$unidad->nombre}} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label><b>Profesores:</b></label>
                                <select name="profesores" id="profesores" class="selectpicker form-control"
                                    data-live-search="true" title="campo requerido" multiple
                                    data-header="Selecciona a los profesores que dictaron la clase" required="">
                                    @foreach($curso->profesores as $profesorcurso)
                                    <option value="{{$profesorcurso->id}}">
                                        {{$profesorcurso->profesor->persona->getNombreCompletoAttribute()}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>       
                        <div class="col-md-8">
                            <div class="form-group">
                                <label><b>Observaciones de clase:</b></label>
                                <p><textarea name="obs-clase" rows="5" cols="50"></textarea></p>
                            </div>
                        </div>                 
                    </div>
                    <div>
                            <label><b>Alumnos:</b></label>
                            <table id="datatable-cursos" class="table table-striped table-hover" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class   = "sortable text-center" 
                                            style   = 'cursor: pointer'>
                                            Alumno          
                                        </th>
                                        <th class="text-center">Documento       </th>
                                        <th class="text-center">Asistió         </th>
                                        <th class="text-center">Motivo inasistencia</th>
                                    </tr>
                                </thead>
                                <tbody>                                  
                                    @foreach($curso->alumnos as $alumno)
                                        @if($alumno->estado_curso != 'libre' && $alumno->estado_curso !=  'abandono')
                                            <tr>
                                                <td class="text-bold text-uppercase" width="30%">
                                                    {{$alumno->estudiante->persona->getNombreCompletoAttribute()}}
                                                </td>
                                                <td class="text-center text-bold">
                                                    {{$alumno->estudiante->persona->dni}}
                                                </td>
                                                <td class="text-center" width="10%">
                                                    <input type="checkbox" data-toggle="tooltip" checked
                                                        title="indica si el alumno asistió o no a clases"
                                                        onchange="cambiar_asistencia('{{$alumno->id}}')"
                                                        id="check-asistencia-{{$alumno->id}}" />
                                                </td>
                                                <td width="100%">
                                                    <input id="input-motivo-{{$alumno->id}}" type="text" maxlength="500"
                                                        class="form-control" disabled placeholder="motivo..."
                                                        onkeyup="carga_motivo_inasistencia('{{$alumno->id}}')"
                                                        onchange="carga_motivo_inasistencia('{{$alumno->id}}')">
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="text-center">Alumno</th>
                                        <th class="text-center">Documento</th>
                                        <th class="text-center">Asistió</th>
                                        <th class="text-center">Motivo inasistencia</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                </div>
                <div class="box-footer">
                    <a href="{{ route('cursos.show', $curso->id) }}" data-toggle="tooltip"
                        title="Volver a la pantalla anterior" class="btn btn-default btn-sm"><i
                            class="fa fa-lg fa-arrow-left"></i> volver</a>
                    <div class="pull-right">
                        <a onclick="mandar_lista({{$curso->id}})" data-toggle="tooltip"
                            title="Proceder a registrar el examen para este curso" id="boton-update"
                            class="btn btn-warning btn-sm">
                            <i class="fa fa-lg fa-pencil"></i>
                            registrar clase
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts_del_modulo')
<script>
var cantidad_alumnos = "{{count($curso->alumnos)}}";
</script>
<script src="{{ asset('js/admin/curso/administrar_clase.js') }}"></script>
<script src="{{ asset('js/admin/sortable.js') }}"></script>
@endsection