<div class="modal fade" id="modal-update-clases">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Actualizar datos de la clase </h4>
            </div>

            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')

                <form id="form-update-clases" action="" method="POST" accept-charset="UTF-8" autocomplete="off">
                    <input type="hidden" name="_method"  value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label><b>Fecha de la clase:</b></label>
                        
                        <input type="date" name="fecha" id="fecha_miclase" style="width:100%" required />
                        
                    </div>

                    <div class="form-group">
                        <label><b>Unidad evaluada:</b></label>
                        <select name="unidad_id" id="unidad_id_clase" class="selectpicker form-control"
                            data-live-search="true" title="campo requerido" data-header="Selecciona una unidad"
                            required="">
                            @foreach($curso->titulo->unidades as $unidad)
                            <option value="{{$unidad->id}}">{{$unidad->nombre}} </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label><b>Profesores:</b></label>
                        <select name="profesores[]" id="profesores_clase" class="selectpicker form-control"
                            data-live-search="true" title="campo requerido" multiple
                            data-header="Selecciona a los evaluadores" required="">
                            @foreach($curso->profesores as $profesorcurso)
                            <option value="{{$profesorcurso->id}}">
                                {{$profesorcurso->profesor->persona->getNombreCompletoAttribute()}} </option>
                            @endforeach
                        </select>
                    </div>

                </form>

                <br>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn  btn-warning" onclick="$('#form-update-clases').submit()">actualizar registro</button>
            </div>

        </div>
    </div>
</div>