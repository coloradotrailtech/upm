<br>
<div class="row">
    <div class="col-md-12">
        <h4 style="margin-bottom: 2%;">Listado de alumnos:</h4>
        @if($curso->alumnos->count()>0)
        <div>
            Columnas:
            <a class="toggle-vis" href="" data-column="0">  Seleccionar     </a> ||
            <a class="toggle-vis" href="" data-column="1">  N°              </a> ||
            <a class="toggle-vis" href="" data-column="2">  Apellido        </a> ||
            <a class="toggle-vis" href="" data-column="3">  Nombre          </a> ||
            <a class="toggle-vis" href="" data-column="4">  DNI             </a> ||
            <a class="toggle-vis" href="" data-column="5">  F. Nac          </a> ||
            <a class="toggle-vis" href="" data-column="6">  Edad            </a> ||
            <a class="toggle-vis" href="" data-column="7">  CUIL            </a> ||
            <a class="toggle-vis" href="" data-column="8">  Estado Civil    </a> ||
            <a class="toggle-vis" href="" data-column="9">  Celular         </a> ||
            <a class="toggle-vis" href="" data-column="10"> Tel.Fijo        </a> ||
            <a class="toggle-vis" href="" data-column="11"> Email           </a> ||
            <a class="toggle-vis" href="" data-column="12"> Inasistencias   </a> ||
            <a class="toggle-vis" href="" data-column="13"> Domicilio       </a> ||
            <a class="toggle-vis" href="" data-column="14"> Estado del curso</a> ||
            <a class="toggle-vis" href="" data-column="15"> Mano Habil      </a> ||
            <a class="toggle-vis" href="" data-column="16"> Discapacidad    </a> ||
            <a class="toggle-vis" href="" data-column="17"> Carnet Disc.    </a> ||
            <a class="toggle-vis" href="" data-column="18"> Estudios        </a> ||
            <a class="toggle-vis" href="" data-column="19"> Trabaja         </a>
        </div>

        <br>

        <table id="datatable-alumnos" class="display table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th></th>
                    <th class="text-center">    ID              </th>
                    <th class="text-center">    Apellido        </th>
                    <th class="text-center">    Nombre          </th>
                    <th class="text-center">    DNI             </th>
                    <th class="text-center">    F.Nac.          </th>
                    <th class="text-center">    Edad            </th>
                    <th class="text-center">    CUIL            </th>
                    <th class="text-center">    Estado Civil    </th>
                    <th class="text-center">    Celular         </th>
                    <th class="text-center">    Tel.Fijo        </th>
                    <th class="text-center">    Email           </th>
                    <th class="text-center" class="no-exportar"> Inasistencias</th>
                    <th class="text-center">    Domicilio       </th>
                    <th class="text-center">    Estado en curso </th>
                    <th class="text-center">    Mano Habil      </th>
                    <th class="text-center">    Discapacidad    </th>
                    <th class="text-center">    Carnet Disc.    </th>
                    <th class="text-center">    Estudios        </th>
                    <th class="text-center">    Trabaja         </th>
                </tr>
            </thead>
            <tbody>
                @foreach($curso->alumnos as $alumno)                    
                <tr id="alumno-{{$alumno->id}}" class="{{$alumno->devolver_condicion()}}" alumno_id="{{$alumno->id}}" alumno="{{$alumno->estudiante->persona}}">
                    <td class="text-center" width="3%">
                        <input type="checkbox" class="check_alumno myCheck" value="{{$alumno->id}}">
                    </td>
                    <td class="text-center" width="3%">{{$alumno->estudiante->id}}</td>
                    <td width="" class="text-center text-bold text-blue text-uppercase ">{{ $alumno->estudiante->persona->apellido}}</td>
                    <td width="" class="text-center text-bold text-blue text-uppercase">{{ $alumno->estudiante->persona->nombre}}</td>
                    <td class="text-center">{{ $alumno->estudiante->persona->dni}}</td>
                    <td class="text-center">{{ $alumno->estudiante->persona->fecha_nac->format('d/m/Y')}}</td>
                    <td class="text-center">{{ $alumno->estudiante->persona->fecha_nac->age}}</td>
                    <td class="text-center">{{ $alumno->estudiante->persona->cuil}}</td>
                    <td class="text-center">{{ $alumno->estudiante->persona->estado_civil}}</td>
                    @if($alumno->estudiante->persona->telefono)
                        <td class="text-center">{{$alumno->estudiante->persona->telefono}}</td>
                    @else
                        <td class="text-center">-</td>
                    @endif
                    @if($alumno->estudiante->persona->telefono2)
                        <td class="text-center">{{$alumno->estudiante->persona->telefono2}}</td>
                    @else
                        <td class="text-center">-</td>
                    @endif
                    @if($alumno->estudiante->persona->email)
                        <td class="text-center">{{$alumno->estudiante->persona->email}}</td>
                    @else
                        <td class="text-center">-</td>
                    @endif

                    <!-- Inasistencias -->
                    @if($alumno->inasistencias()==0)
                    <td class="text-center text-green">{{$alumno->inasistencias()}}</td>
                    @elseif($alumno->inasistencias()<2) <td class="text-center text-black">
                        {{$alumno->inasistencias()}}</td>
                    @elseif($alumno->inasistencias()<4) <td class="text-center text-orange">
                        {{$alumno->inasistencias()}}</td>
                    @elseif($alumno->inasistencias()>=4) <td class="text-center text-red">
                        {{$alumno->inasistencias()}}</td>
                    @endif
                    <td class="text-center">{{$alumno->estudiante->persona->direccion}} ({{$alumno->estudiante->persona->localidad->nombre}})</td>
        
                    @if($alumno->estado_curso == null)
                    <td class="text-center bg-blue-gradient text-uppercase"
                        onclick="editarEstadoCurso('{{$alumno->estado_curso}}','{{$alumno->id}}', false)">
                        En cursado</td>
                    @elseif($alumno->estado_curso == 'aprobado')
                    @if($alumno->estado_certificado==null)
                    <td class="text-center bg-green-gradient">
                        {{$alumno->estado_curso}}
                        <b
                            onclick="editarCertificado('{{$alumno->estado_curso}}','{{$alumno->id}}', false)">(certificado
                            sin tramitar)
                        </b>
                    </td>
                    @else
                    <td class="text-center text-light-blue">
                        {{$alumno->estado_curso}}
                        <b class="text-success"
                            onclick="editarCertificado('{{$alumno->estado_curso}}','{{$alumno->id}}', false)">({{$alumno->estado_certificado}})
                        </b>
                    </td>
                    @endif
                    @elseif($alumno->estado_curso == 'libre' || $alumno->estado_curso == 'abandono' ||
                    $alumno->estado_curso == 'desaprobado')
                    <td class="text-center bg-red-gradient">{{$alumno->estado_curso}}</td>
                    @endif
                    <td class="text-center">{{$alumno->estudiante->persona->mano_habil}}</td>
                    @if($alumno->estudiante->persona->discapacidad)
                    <td class="text-center text-yellow">Si</td>
                    @else
                    <td class="text-center">No</td>
                    @endif
                    @if($alumno->estudiante->persona->carnet_discapacidad)
                    <td class="text-center">Si</td>
                    @else
                    <td class="text-center">No</td>
                    @endif
                    <td class="text-center">{{$alumno->estudiante->persona->educacion}}</td>
                    @if($alumno->estudiante->trabaja==1)
                        <td class="text-center">Si</td>
                    @else
                        <td class="text-center">No</td>
                    @endif
                    {{--<td class="text-center">{{$alumno->estudiante->persona->discapacidad}}</td> --}}
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th></th>
                    <th class="text-center">    N°              </th>
                    <th class="text-center">    Apellido        </th>
                    <th class="text-center">    Nombre          </th>
                    <th class="text-center">    DNI             </th>
                    <th class="text-center">    F. Nac.         </th>
                    <th class="text-center">    Edad            </th>
                    <th class="text-center">    CUIL            </th>
                    <th class="text-center">    Estado Civil    </th>
                    <th class="text-center">    Tel. Fijo       </th>
                    <th class="text-center">    Celular         </th>
                    <th class="text-center">    Email           </th>
                    <th class="text-center">    Inasistencias   </th>
                    <th class="text-center">    Domicilio       </th>
                    <th class="text-center">    Estado en curso </th>
                    <th class="text-center">    Mano Habil      </th>
                    <th class="text-center">    Discapacidad    </th>
                    <th class="text-center">    Carnet Disc.    </th>
                    <th class="text-center">    Estudios        </th>
                    <th class="text-center">    Trabaja         </th>
                </tr>
            </tfoot>
        </table>

        @else

        <div class="alert alert-info alert-dismissible">
            <h4><i class="icon fa fa-info-circle"></i> Información</h4> Aún no se ha inscripto
            ningún alumno a este curso. Puede gestionar inscripciones una dando un click al
            botón:
            <b><i class="fa fa-lg fa-user"></i> registrar alumno</b>.
        </div>
        
        @endif
    </div>
</div>

<br>

<div class="box-footer">

    <a href="{{ route('cursos.index') }}" data-toggle="tooltip" title="Volver a la pantalla anterior"
        class="btn btn-default btn-sm"><i class="fa fa-lg fa-arrow-left"></i> 
        volver
    </a>

    <div class="pull-right">

        @if($curso->determinar_estado() === "Dictado finalizado")

            <button class="btn btn-sm btn-warning" data-toggle="tooltip"
                title="Asignar estado del certificado de los alumnos."
                onclick="seteo_masivo_certificado()">
                <i class="fa fa-lg fa-users "></i> Editar estado de certificado
            </button>

            <span>&nbsp;<b>|</b>&nbsp;</span>

            <button class="btn btn-sm" data-toggle="tooltip"
                title="Imprimir estado final de los alumnos."
                onclick="estado_final_alumnos({{ $curso }})">
                <i class="fa fa-lg fa-users "></i> Imprimir estado final
            </button>
            

            @if(Auth::user()->isAdmin())
            <span>&nbsp;<b>|</b>&nbsp;</span>
            
            <button id="btn-exportar-pdf" class="btn btn-sm btn-danger" 
                data-toggle="tooltip" title="Gnerar certificados de aprobación" 
                onclick="configurar_certificado({{$curso}})">
                <i class="fa fa-lg fa-file-pdf-o "></i> Certificados aprobación
            </button>
            @endif

            <span>&nbsp;<b>|</b>&nbsp;</span>
        @else
            <!-- Button eliminar alumno modal -->
            <button type="button" class="btn btn-primary btn-sm  btn-danger" data-toggle="modal" 
                title='Eliminar Alumnos seleccionados' data-target="#myModal" 
                onclick="modal_eliminar_alumnos({{$curso->alumnos}},{{$curso->id}})">
                Eliminar Alumno 
            </button>
            <span>&nbsp;<b>|</b>&nbsp;</span>
        @endif

        <button class="btn btn-sm btn-warning" data-toggle="tooltip"
        title="Editar estado del Alumno. Solo se actualiza a los alumnos que no tengan Certificado en trámite."
         onclick="seteo_masivo_curso()">
           <i class="fa fa-lg fa-users "></i> Editar estado de curso
       </button>

        <button id="btn-exportar-excel" class="btn btn-sm btn-success"
            title="Exportar un listado de los alumnos del curso">
            <i class="fa fa-lg fa-file-excel-o"></i> Exportar Excel
        </button>
        
        <span>&nbsp;<b>|</b>&nbsp;</span>

        <a onclick="comprobar_estudiante({{$curso->id}});"
            title="Presione para rellenar el formulario de inscripción de un nuevo alumno del curso"
            class="btn btn-primary btn-sm"><i class="fa fa-lg fa-users"></i> 
            Registrar Alumno
        </a>
    </div>

</div>




<!-- Modal Cambiar Eliminar Alumno -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">Eliminar Alumno/s de este Curso</h3>
      </div>

      <div class="modal-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Matricula</th>
                    <th>Apellido</th>
                    <th>Nombre  </th>
                    <th>DNI     </th>
                </tr>
            </thead>
            <tbody id='modal_opciones_alumnos'></tbody>
        </table>
      </div>

      <div class="modal-footer" id='mo-foo'>
            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-danger" onclick="eliminar_alumnos_seleccionados()">Eliminar alumnos</button>
      </div>
      
    </div>
  </div>
</div>





<!-- Modal seteo Certificados -->
<div class="modal fade" id="modal_set_certificados" tabindex="-1" role="dialog" aria-labelledby="modal_set_certificadosLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

        <div class="modal-header">
            <h4 class="modal-title" id="modal_set_certificadosLabel">Configuración del Certificado</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">
        
            
            Se extiende el presente certificado al Sr/a ... DNI ... quién
            <select name="verbo_certificado" id="verbo_certificado" class="selectpicker">
                <option value="aprobó el" selected="selected">Aprobó el</option>
                <option value="participó del">Participó del</option>
            </select>

            <select name="curso_taller" id="curso_taller" class="selectpicker">
                <option value="Curso de" selected="selected">Curso de</option>
                <option value="Taller de" > Taller de</option>
                <option value=" "> Vacío</option>
            </select>   

            <span id='nombre_curso_'></span>
            <br>
            <span id="fecha_cert" style='float: right;'></span>       
            <hr>

            <label> Firmas </label>
            <select name="firmas_disponibles" id="firmas_disponibles" class="selectpicker">
                <option value="logo_inferior" selected>    Clásico    </option>
                <option value="firma_deporte">   Deportes   </option>
                <option value="firma_ciudadania"> Ciudadania </option>
                <option value="firma_inta">       INTA       </option>
            </select>

            <br>

            <img alt="firmas_asignadas" id="firmas_cert" width='100%'>

        </div>

        <div class="modal-footer">

            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

            <button type="button" class="btn btn-primary" onclick="imprimir_certificados({{$curso}})">
                Continuar
            </button>

        </div>

    </div>
  </div>
</div>




<!-- Modal Historial Cursos de Alumno Encontrado por DNI -->
<div class="modal fade" id="modal_historial_alumno" tabindex="-1" role="dialog" aria-labelledby="modal_historial_alumnoLabel" aria-hidden="true">
  <div class="modal-dialog modal-custom-size">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Persona encontrada</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div id="tabla_cursadas"></div>
      </div>
      <div class="modal-footer" id="modal_footer_inscripcion"></div>

    </div>
  </div>
</div>


<script type="text/javascript">

    let alumnos_a_eliminar=[];
    let id_alumnos_a_eliminar=[];
    function modal_eliminar_alumnos(alumnos){
        $("#modal_opciones_alumnos").empty()
        id_alumnos_a_eliminar=[];
        alumnos_a_eliminar=[];

        let myCheck = document.querySelectorAll('.myCheck')
        let myAlumno;
        
        myCheck.forEach(e => {
            if(e.checked == true){
                myAlumno =  alumnos.find(alu => alu.id == e.value)

                if(myAlumno.estado_curso == null){                    
                    id_alumnos_a_eliminar.push({alumno_id: myAlumno.id, curso_id: myAlumno.curso_id})
                    alumnos_a_eliminar.push(myAlumno)
                }else{
                    Swal.fire(
                        'No es posible esta acción!',
                        'No se puede eliminar un alumno con estado de curso asignado.',
                        'error'
                    )
                    $('#myModal').modal("toggle");
                }
            }
        })

        const tb_alumnos_a_eliminar = document.getElementById('modal_opciones_alumnos')

        alumnos_a_eliminar.forEach(alumno => {
            let tr_modal_eliminar = document.createElement('tr')
            
            let td_alumno_eliminar = `
                <tr>
                    <td> ${alumno.estudiante_id} </td>
                    <td> ${alumno.estudiante.persona.apellido} </td>
                    <td> ${alumno.estudiante.persona.nombre} </td>
                    <td> ${alumno.estudiante.persona.dni} </td>
                </tr>
            `

            tr_modal_eliminar.innerHTML = td_alumno_eliminar
            tb_alumnos_a_eliminar.appendChild(tr_modal_eliminar)
        });
    }


    function eliminar_alumnos_seleccionados(){

        Swal.fire({
            title: '¿Seguro que desea eliminar?',
            text: "Si procede se va eliminar estos Alumnos de este Curso",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, estoy seguro'

        }).then((result) => {

            if(result.isConfirmed){
                fetch(`/eliminar_alumnos_seleccionados`, {
                    method: 'POST',
                    body: JSON.stringify(id_alumnos_a_eliminar),
                    headers:{
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }).then(res =>res.json()
                ).catch(error => console.error('Error:', error)
                ).then(response => {
                    console.log('Success:', response)
                    
                    Swal.fire(
                        'Eliminado!',
                        'Acción completada con éxito.',
                        'success'
                    ).then(()=>{
                        window.location = `/cursos/${id_alumnos_a_eliminar[0].curso_id}`
                    })
                    
                });
            }
        })

    }

    document.addEventListener("DOMContentLoaded", ()=>{
        const img_firma_cert   = document.getElementById('firmas_cert')
        const slct_firmas_disp = document.getElementById('firmas_disponibles')
        img_firma_cert.src = imagenes[`${slct_firmas_disp.value}`]

        slct_firmas_disp.addEventListener("change",()=>{
            img_firma_cert.src = imagenes[`${slct_firmas_disp.value}`]
        });
    });

    function estado_final_alumnos(cursos){
        let head_table = [' ','Mat.','Apellido','Nombre','Dni','Estado','Certificado'];
        let body_table = [];
        
        cursos.alumnos.forEach(al => {

            const myAlu = [ 1 , al.estudiante_id, al.estudiante.persona.apellido.toUpperCase(), al.estudiante.persona.nombre,
                al.estudiante.persona.dni, al.estado_curso, al.estado_certificado
            ]

            body_table.push(myAlu)
        });

        body_table.sort((a, b) => a[2] > b[2] ? 1 : -1)
        body_table.map((a,i) => a[0]= i+1 )
        body_table.unshift(head_table)

        const fecha_inicio = moment(cursos.fecha_inicio).format('DD/MM/YYYY')
        const fecha_fecha  = moment(cursos.fecha_fecha).format('DD/MM/YYYY')

        const doc_Content = {
            content: [
                {
                    alignment: 'right', 
                    margin: [0, 0, 0, 5],
                    image: imagenes.logo_upm, 
                    width: 130
                },
                
                { 
                text:  `Curso N° ${cursos.id} | ${cursos.titulo.nombre} - ${cursos.titulo.plan}`, style: 'header' 
                },
                {
                    text:`\nSede: ${cursos.sede.localidad.nombre}, ${cursos.comision}  
                        Fecha inicio: ${fecha_inicio}  Fecha fin: ${fecha_fecha}
                        Cantidad clases: ${cursos.titulo.unidades.length} \n
                    `,
                    style: 'myStyle'
                },
                {
                    layout: 'lightHorizontalLines', // optional
                    table: {
                        headerRows: 1,
                        widths: [ 15, 30, '*', 90, '*', 60, 60],
                        body: body_table
                    }
                }
            ],
            styles: {
                header: {
                    fontSize: 15,
                    bold: true,
                },
                myStyle: {
                    italics: false,
                    alignment: 'left'
                }
            }
        };

        pdfMake.createPdf(doc_Content).download(`Estado Curso N° ${cursos.id} | ${cursos.titulo.nombre}`);

        Swal.fire(
            'Estado final del Curso',
            'Lista de alumnos',
            'info'
        )
    }

</script>