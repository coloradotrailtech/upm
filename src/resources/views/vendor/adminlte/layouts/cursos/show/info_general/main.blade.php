
<div class="row">
    
    <!-- Info Curso -->
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label><strong>Curso {{ $curso->id }}:</strong></label>
                    <span class="form-control"><a data-toggle="tooltip"
                            title="Ir a la planificación de clases para el título"
                            href="{{ route('titulos.show', $curso->titulo->id) }}"><b>{{$curso->titulo->nombre}}</b>
                            <label class="text-black">| Plan {{$curso->titulo->plan}}</label></a></span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label><strong>Sede:</strong></label>
                    <span class="form-control">{{$curso->sede->getnombre()}}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label><strong>Fecha de inicio y finalización:</strong></label>
                    <span class="form-control">{{$curso->getFechaInicioFormateadoAttribute()}} -
                        {{$curso->getFechaFinFormateadoAttribute()}}</span>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <table class='table text-center'>
                        <thead>
                            <tr>
                                <th> Insriptos   </th>
                                <th> Aprobados   </th>
                                <th> Abandonados </th>
                                <th> Libres      </th>
                                <th> Cursando    </th>
                            </tr>
                        </thead>
                        <tbody>                            
                            <tr>
                                <td> {{ $contadores['total'] }} </td>
                                <td> {{ $contadores['aprobado'] }} </td>
                                <td> {{ $contadores['abandono'] }} </td>
                                <td> {{ $contadores['libre'] }} </td>
                                <td> {{ $contadores['sin_estado'] }} </td>
                            </tr>                            
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label><strong>Instructores:</strong></label>
                    <table class='table'>
                        <tbody>
                            @foreach($curso->profes as $profe)
                            <tr>
                                <td>{{ $profe['apellido']   }}, 
                                    {{ $profe['nombre']     }} 

                                </td>
                                <td> {{ $profe['rol']       }} </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label><strong>Comisión:</strong></label>
                    <span class="form-control text-center">{{$curso->comision}}</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label><strong>Carga horaria:</strong></label>
                    <span id="span_carga" carga="{{$curso->titulo->obtener_carga_horaria_total("horas")}} horas" class="form-control text-center">{{$curso->titulo->obtener_carga_horaria_total("horas")}} horas</span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label><strong>Estado:</strong></label>
                    @if($curso->activa)
                        <span class="form-control bg-green text-center">Activo</span>
                    @else
                        <span class="form-control">Inactivo</span>
                    @endif
                </div>
            </div>
            @if($curso->descripcion)
            <div class="col-md-12">
                <div class="form-group">
                    <label><strong>Observaciones:</strong></label>
                    @if($curso->descripcion)
                    <div class="well" style="background-color: white;">{{$curso->descripcion}}</div>
                    @else
                    <div class="well" style="background-color: white;">No se incluyó una descrición del curso</div>
                    @endif
                </div>
            </div>
            @endif
        </div>
    </div>

    <!-- Constancias -->
    <div class="col-md-7">
        <div class="form-group">
            <label><strong>Constancias:</strong></label>
            <br>

            <div class="btn-group" role="group" aria-label="...">
            <button type="button" class="btn btn-default" data-toggle='modal' data-target='#const_regular'>
                Alumno Regular
            </button>

            <div class="btn-group" role="group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Justificaciones
                    <span class="caret"></span>
                </button>

                <ul class="dropdown-menu">
                    <li><a href="#">Inasistencia</a></li>
                    <li><a href="#">Tardanza</a></li>
                </ul>
            </div>
        </div>
        <br>
        <br>
        <div class="form-group">
            <label><strong>Libro Aula:</strong></label>
            <br>
            
            <a onclick="all_libro_aula({{ $curso }})"
                data-toggle="tooltip"
                title="Instrucciones Libro Aula"
                id='all_libro_aula'
                class="btn btn-default">
                <i class="fa fa-file" aria-hidden="true"></i>
                Instrucciones
            </a>
            
        </div>
        
    </div>
    
    <!-- Info Materiales -->
    <div class="col-md-12">
        @include('vendor.adminlte.layouts.cursos.show.info_general.materiales')
    </div>
    </div>
    <br>
    <br>


    <!-- Planificacion -->    
    <div class="col-md-12">
        <label style="margin-bottom: 1%;"><strong>Planificación de contenidos:</strong></label>
        <ul class="todo-list">
            @foreach($curso->titulo->unidades->sortBy('numero_clase') as $unidad)
            <li>
                <span class="" style="cursor: move">
                    <i class="fa fa-ellipsis-v"></i>
                    <i class="fa fa-ellipsis-v"></i>
                </span>
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseinfo{{$unidad->id}}" class="collapsed"
                    aria-expanded="false">
                    <span id_unidad="{{$unidad->id}}" numero_clase="{{$unidad->numero_clase}}"
                        class="todo-item text">Clase
                        Nº
                        {{$unidad->numero_clase}} ({{$unidad->modalidad->nombre}}).
                        Tema: {{$unidad->nombre}}</span>
                </a>
                <div id="collapseinfo{{$unidad->id}}" class="panel-collapse collapse" aria-expanded="false"
                    style="height: 0px;">
                    <div class="box-body">
                        <label style="margin-bottom: 1%;"><strong>Carga horaria:</strong><small
                                class="label label-primary" style="font-size: 75%"><i class="fa fa-clock-o"></i>
                                {{$unidad->carga_horaria}}
                                {{$unidad->unidad_tiempo}}</small></label>
                        <div class="table-responsive" style="background-color: #FFFFFF;">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Objetivos</th>
                                        <th>Contenidos</th>
                                        <th>Actividades</th>
                                        <th>Tipo de evaluación</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($unidad->contenidos as $contenido)
                                    <tr>
                                        <td>{{$contenido->objetivos}}</td>
                                        <td>{{$contenido->contenidos}}</td>
                                        <td>{{$contenido->actividades}}</td>
                                        <td>{{$contenido->tipo_evaluacion}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <br>
                        @if($unidad->seh)
                        <label style="margin-bottom: 1%;"><strong>Seguridad e Higiene:</strong><span
                                class="label label-success" style="font-size: 90%"><i class="fa fa-clock-o"></i>
                                {{$unidad->seh}} minutos</span></label>

                        @endif
                        @if($unidad->auditable =='Si')
                        <span>&nbsp;&nbsp;&nbsp;<b>|</b></span>
                        <label style="margin-bottom: 1%; font-size: 90%" class="label label-warning"><strong>Clase
                                auditable</strong></label>
                        @endif
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
        <br>
    </div>

</div>

<br>
<div class="box-footer">
    <a href="{{ route('cursos.index') }}" data-toggle="tooltip" title="Volver a la pantalla anterior"
        class="btn btn-default btn-sm"><i class="fa fa-lg fa-arrow-left"></i> volver</a>
    <div class="pull-right">
        <a onclick="completar_campos({{$curso->datos_para_editar()}})" data-toggle="tooltip"
            title="Editar este registro" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> editar</a>
        <a onclick="abrir_modal_borrar({{$curso->id}})" data-toggle="tooltip" title="Eliminar este registro"
            class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> eliminar</a>
    </div>
</div>


<!-- Modal Const_alu -->
<div class="modal fade" id="const_regular" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Solicitud constancia alumno regular</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
            <select class="custom-select" id='constancia-select'>
                <option selected>Seleccionar alumno</option>
                @foreach($myAlumnos as $x=>$alumno)
                    @if($alumno['estado_curso'] == null)
                    <option value="{{ $alumno ['mat'] }}">
                        {{ $alumno ['mat']      }} - 
                        {{ $alumno ['apellido'] }}
                        {{ $alumno ['nombre'  ] }}
                    </option>
                    @else
                    <option value="" disabled>
                        {{ $alumno ['mat']      }} - 
                        {{ $alumno ['apellido'] }}
                        {{ $alumno ['nombre'  ] }}
                    </option>
                    @endif
                @endforeach
            </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary"  
            onclick="constancia_alu_reg({{$curso}},$('#constancia-select').val())" 
            id='btn-constancia' >
            Imprimir
        </button>
      </div>
    </div>
  </div>
</div>