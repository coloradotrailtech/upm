<div class="modal fade" id="modal-update-examenes">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Actualizar datos del examen</h4>
            </div>
            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')

                <form id="form-update-examenes" action="" method="POST" accept-charset="UTF-8" autocomplete="off">
                    <input name="_method" type="hidden" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label><b>Fecha del examen:</b></label>
                        <input type="date" name="fecha" id="fecha_examen" style="width:100%" required >
                    </div>

                    <div class="form-group">
                        <label><b>Modalidad del examen:</b></label>
                        <select name="modalidad_id" id="modalidad_examen" class="selectpicker form-control"
                            data-live-search="true" title="campo requerido" data-header="Selecciona una modalidad"
                            required="">
                            @foreach($modalidades as $modalidad)
                            <option value="{{$modalidad->id}}">{{$modalidad->nombre}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label><b>Unidad evaluada:</b></label>
                        <select name="unidades[]" id="unidad_id_examen" class="selectpicker form-control"
                            data-live-search="true" multiple title="campo requerido" data-header="Selecciona una unidad"
                            required>
                            @foreach($curso->titulo->unidades as $unidad)
                            <option value="{{$unidad->id}}">
                                {{$unidad->nombre}} 
                            </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label><b>Evaluadores:</b></label>
                        <select name="profesores[]" id="profesores_examen" class="selectpicker form-control"
                            data-live-search="true" title="campo requerido" multiple
                            data-header="Selecciona a los evaluadores" required>
                            @foreach($curso->profesores as $profesorcurso)
                            <option value="{{$profesorcurso->id}}">
                                {{$profesorcurso->profesor->persona->getNombreCompletoAttribute()}} </option>
                            @endforeach
                        </select>
                    </div>

                </form>

                <br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn  btn-warning" onclick="$('#form-update-examenes').submit()">actualizar
                    registro</button>
            </div>
        </div>
    </div>
</div>