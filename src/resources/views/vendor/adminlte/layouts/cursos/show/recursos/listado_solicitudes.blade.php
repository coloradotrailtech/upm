<div class="col-md-12">
    <h4 style="margin-bottom: 2%;">Listado de solicitudes:</h4>
    <ul class="todo-list">
        @foreach($curso->solicitudesprestamo as $solicitudprestamo)
        <li>
            <span class="" style="cursor: move">
                <i class="fa fa-ellipsis-v"></i>
                <i class="fa fa-ellipsis-v"></i>
            </span>
            <div class="tools">
                <i onclick="abrir_modal_borrar({{$solicitudprestamo->id}}, 'solicitudes_prestamos')"
                    class="fa fa-trash-o" data-toggle="tooltip" title="Borrar este registro de clase"></i>
            </div>
            <a id="a-solicitudprestamo-{{$solicitudprestamo->id}}" data-toggle="collapse" data-parent="#accordion"
                href="#collapse-solicitudprestamo-{{$solicitudprestamo->id}}" class="collapsed" aria-expanded="false">
                <span onclick="instanciar_tabla('solicitudprestamo', '{{$solicitudprestamo->id}}')"
                    class="todo-item text" @if($solicitudprestamo->prestamo)
                    style="color: #5cb85c"
                    @endif
                    >
                    Fecha: {{$solicitudprestamo->created_at->format('d/m/Y')}} | Cantidad Materiales solicitados:
                    {{$solicitudprestamo->materialessolicitudprestamo->count()}} | Estado solicitud:
                    {{$solicitudprestamo->estadosolicitudprestamo->nombre}}</span>
            </a>



            <div id="collapse-solicitudprestamo-{{$solicitudprestamo->id}}" class="panel-collapse collapse"
                aria-expanded="false" style="height: 0px;">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><strong>Fecha:</strong></label>
                                <span class="form-control">{{$solicitudprestamo->created_at->format('d/m/Y')}}</span>
                            </div>
                            <label><strong>Observaciones:</strong></label>
                            <div class="well" style="background-color: white;">
                                @if($solicitudprestamo->prestamo)
                                <a id="a-prestamo-{{$solicitudprestamo->prestamo->id}}" data-toggle="collapse"
                                    data-parent="#accordion"
                                    href="#collapse-prestamo-{{$solicitudprestamo->prestamo->id}}" class="collapsed"
                                    aria-expanded="false">
                                    <span onclick="instanciar_tabla('prestamo', '{{$solicitudprestamo->prestamo->id}}')"
                                        class="todo-item text">
                                        Fecha: {{$solicitudprestamo->prestamo->created_at->format('d/m/Y')}} | Cantidad
                                        Materiales transferidos:
                                        {{$solicitudprestamo->prestamo->materialesprestamos->count()}} | Estado:
                                        {{$solicitudprestamo->estadosolicitudprestamo->nombre}}</span>
                                </a>
                                @else
                                {{$solicitudprestamo->estadosolicitudprestamo->nombre}}
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label><strong>Listado de materiales:</strong></label>
                                <div style="background-color: #FFFFFF;">
                                    <div style="padding: 1%;">
                                        <table id="datatable-solicitudprestamo-{{$solicitudprestamo->id}}"
                                            class="display" width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Material</th>
                                                    <th class="text-center">Cantidad</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($solicitudprestamo->materialessolicitudprestamo as
                                                $materialsolicitudprestamo)
                                                <tr>
                                                    <td>{{$materialsolicitudprestamo->materialtitulo->id}}
                                                    </td>
                                                    <td class="text-center">{{$materialsolicitudprestamo->cantidad}}
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</div>