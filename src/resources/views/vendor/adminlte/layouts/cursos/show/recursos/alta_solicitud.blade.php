@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Alta de solicitud
@endsection

@section('contentheader_title')
Alta de solicitud
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Cursos</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Listado de materiales indicados en la planificación</h3>
                </div>
                <div class="box-body ">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="alert alert-info">
                                <strong>Información:</strong> recuerde que la cantidad de alumnos inscriptos en este curso es de: <b>{{$curso->alumnos->count()}}</b>.
                            </div>

                            <table id="datatable-solicitud" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">Material</th>
                                        <th class="text-center">Stock a solicitar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($curso->titulo->materiales_titulos as $material)
                                    <tr>
                                        <td class="text-center text-bold">{{$material->nombre}}</td>
                                     
                                        <td>
                                            <input id="input-cantidad-{{$material->id}}" type="number" min="1"
                                                max="1000000" class="form-control"
                                                placeholder="Cantidad a solicitar..."
                                                onkeyup="carga_lista('{{$material->id}}')"
                                                onchange="carga_lista('{{$material->id}}')">
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="text-center">Material</th>
                                        <th class="text-center">Stock inicial</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="{{ route('cursos.show', $curso->id) }}" data-toggle="tooltip"
                        title="Volver a la pantalla anterior" class="btn btn-default btn-sm"><i
                            class="fa fa-lg fa-arrow-left"></i> volver</a>
                    <div class="pull-right">
                        <a onclick="mandar_lista({{$curso->id}})" data-toggle="tooltip"
                            title="Ingresar la solicitud de materiales al depósito de la sede" id="boton-update"
                            class="btn btn-success btn-sm">
                            <i class="fa fa-lg fa-pencil"></i>
                            ingresar solicitud
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/curso/alta_solicitud.js') }}"></script>
@endsection