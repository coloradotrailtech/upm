<br>
<div class="row">
    <div class="col-md-12">        
        @if($curso->clases->count()>0)
        @include('vendor.adminlte.layouts.cursos.show.clases.listado')
        @else
        <div class="alert alert-info alert-dismissible">
            <h4><i class="icon fa fa-info-circle"></i> Información</h4> Aún no se ha registrado
            ninguna clase en este curso. Puede gestionar las clases dando un click al
            botón:
            <b><i class="fa fa-lg fa-pencil-square-o"></i> registrar clase</b>.
        </div>
        @endif
    </div>
</div>
<br>
<div class="box-footer">
    <a href="{{ route('cursos.index') }}" data-toggle="tooltip" title="Volver a la pantalla anterior"
        class="btn btn-default btn-sm"><i class="fa fa-lg fa-arrow-left"></i> volver</a>
    <div class="pull-right">
        <a href="{{ route('clases.edit', $curso->id) }}" data-toggle="tooltip" title="Ir al modulo de asitencias de este curso"
            class="btn btn-primary btn-sm"><i class="fa fa-lg fa-pencil-square-o"></i>
            registrar clase</a>
    </div>
</div>