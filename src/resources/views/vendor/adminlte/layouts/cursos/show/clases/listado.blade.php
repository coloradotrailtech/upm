<div class="col-md-12">
<h4 style="margin-bottom: 2%;">Listado de clases:</h4>
    <ul class="todo-list">
        @foreach($curso->clases as $clase)
        <li>
            <span class="" style="cursor: move">
                <i class="fa fa-ellipsis-v"></i>
                <i class="fa fa-ellipsis-v"></i>
            </span>
            <div class="tools">
                <i onclick="completar_campos_clase({{$clase->datos_para_editar()}})" class="fa fa-edit" data-toggle="tooltip"
                    title="Actualizar este registro de clase"></i>
                <i onclick="abrir_modal_borrar({{$clase->id}}, 'clases')" class="fa fa-trash-o" data-toggle="tooltip"
                    title="Borrar este registro de clase"></i>
            </div>
            <a id="a-clase-{{$clase->id}}" data-toggle="collapse" data-parent="#accordion" href="#collapseclase{{$clase->id}}" class="collapsed"
                aria-expanded="false">
                <span onclick="instanciar_tabla('clase',{{$clase->id}})" class="todo-item text">                   
                    Número de clase: {{$clase->unidad->numero_clase}}. Fecha: {{$clase->getFechaFormateadoAttribute()}}</span>
            </a>
            <div id="collapseclase{{$clase->id}}" class="panel-collapse collapse"
                aria-expanded="false" style="height: 0px;">                
                <div class="box-body">                   
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><strong>Unidad evaluada:</strong></label>

                                <span class="form-control"><a data-toggle="tooltip" data-placement="right"
                                        title="para conocer los contenidos de la unidad puede dirigirse al detalle del registro del título que otorga este título dando un click aquí.">{{$clase->unidad->nombre}}</a></span>


                            </div>
                        
                            <div class="form-group">
                                <label><strong>Fecha:</strong></label>
                                <span class="form-control">{{$clase->getFechaFormateadoAttribute()}}</span>
                            </div>
                        
                            <div class="form-group">
                                <label><strong>Profesores:</strong></label>
                                @foreach($clase->profesoresclases as $profesorclase)
                                <span style="margin-bottom: 1%"
                                    class="form-control"><b>{{$profesorclase->profesorcurso->profesor->persona->getNombreCompletoAttribute()}}
                                        ({{$profesorclase->profesorcurso->rol}})</b></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label><strong>Listado de asistentes:</strong></label>
                                <div style="background-color: #FFFFFF;">
                                    <div style="padding: 1%;">
                                <table id="datatable-clase-{{$clase->id}}" class="display" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Alumno</th>
                                                <th class="text-center">Documento</th>
                                                <th class="text-center">Asistió</th>                                           
                                                <th class="text-center">acciones</th>
                                                <th class="text-center none">Motivo inasistencia</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($clase->alumnosclases as $alumnoclase)
                                            <tr>
                                                <td class='text-uppercase'>{{$alumnoclase->alumno->estudiante->persona->getNombreCompletoAttribute()}}
                                                </td>
                                                <td>{{$alumnoclase->alumno->estudiante->persona->dni}}</td>
                                                <td class="text-center">{{$alumnoclase->getAsistioFormateadoAttribute()}}</td>                                                                                          
                                                <td class="text-center"><a
                                                        onclick="completar_campos_asistencia({{$alumnoclase->datos_para_editar()}})"
                                                        data-toggle="tooltip" title="Editar este registro"
                                                        class="btn btn-social-icon btn-warning btn-sm"><i
                                                            class="fa fa-pencil"></i></a></td>
                                                @if($alumnoclase->motivo_inasistencia)
                                                <td class="text-center">{{$alumnoclase->motivo_inasistencia}}</td>
                                                @else
                                                <td class="text-center">-</td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</div>