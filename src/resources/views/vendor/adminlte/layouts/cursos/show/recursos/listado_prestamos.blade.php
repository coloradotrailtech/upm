<div class="col-md-12">
    <h4 style="margin-bottom: 2%;">Listado de préstamos:</h4>
    <ul class="todo-list">
        @foreach($curso->prestamos as $prestamo)
        <li>
            <span class="" style="cursor: move">
                <i class="fa fa-ellipsis-v"></i>
                <i class="fa fa-ellipsis-v"></i>
            </span>
            <a id="a-prestamo-{{$prestamo->id}}" data-toggle="collapse" data-parent="#accordion"
                href="#collapse-prestamo-{{$prestamo->id}}" class="collapsed" aria-expanded="false">
                <span onclick="instanciar_tabla('prestamo', '{{$prestamo->id}}')" class="todo-item text">
                    Fecha: {{$prestamo->created_at->format('d/m/Y')}} | Cantidad Materiales transferidos:
                    {{$prestamo->materialesprestamos->count()}}</span>
            </a>
            <div id="collapse-prestamo-{{$prestamo->id}}" class="panel-collapse collapse" aria-expanded="false"
                style="height: 0px;">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><strong>Fecha:</strong></label>
                                <span class="form-control">{{$prestamo->created_at->format('d/m/Y')}}</span>
                            </div>
                            <div class="form-group">
                                <label><strong>Proveedor:</strong></label>
                                <span class="form-control">Depósito {{$prestamo->deposito->id}}
                                    ({{$prestamo->deposito->direccion}},
                                    {{$prestamo->deposito->localidad->nombre}})</span>
                            </div>
                            <div class="form-group">
                                <label><strong>Corresponde a una solicitud:</strong></label>
                                @if($prestamo->solicitudprestamo)
                                <div class="well" style="background-color: white;">
                                    <a id="a-solicitudprestamo-{{$prestamo->solicitudprestamo->id}}"
                                        data-toggle="collapse" data-parent="#accordion"
                                        href="#collapse-solicitudprestamo-{{$prestamo->solicitudprestamo->id}}"
                                        class="collapsed" aria-expanded="false">
                                        <span
                                            onclick="instanciar_tabla('solicitudprestamo', '{{$prestamo->solicitudprestamo->id}}')"
                                            class="todo-item text">
                                            Fecha: {{$prestamo->solicitudprestamo->created_at->format('d/m/Y')}}.
                                            Cantidad
                                            Materiales solicitados:
                                            {{$prestamo->solicitudprestamo->materialessolicitudprestamo->count()}}.</span>
                                    </a>
                                </div>
                                @else
                                <span class="form-control">No</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label><strong>Listado de materiales:</strong></label>
                                <div style="background-color: #FFFFFF;">
                                    <div style="padding: 1%;">
                                        <table id="datatable-prestamo-{{$prestamo->id}}" class="display" width="100%"
                                            cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Material</th>
                                                    <th class="text-center">Cantidad prestada</th>
                                                    <th class="text-center">Unidad medida</th>
                                                    <th class="text-center">Observación</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($prestamo->materialesprestamos as $materialprestamo)
                                                <tr>
                                                    <td>{{$materialprestamo->materialdeposito->material->nombre}}
                                                        ({{$materialprestamo->materialdeposito->material->tipomaterial->familiamaterial->nombre}}
                                                        - {{$materialprestamo->materialdeposito->material->tipomaterial->nombre}})</td>
                                                    <td class="text-center">{{$materialprestamo->cantidad_prestada}}
                                                    </td>
                                                    <td class="text-center">
                                                        {{$materialprestamo->materialdeposito->material->umedida->nombre}}
                                                    </td>
                                                    @if($materialprestamo->observacion)
                                                    <td>{{$materialprestamo->observacion}}</td>
                                                    @else
                                                    <td class="text-center">-</td>
                                                    @endif
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</div>