<div class="col-md-12">
<h4 style="margin-bottom: 2%;">Listado de exámenes:</h4>  
    <ul class="todo-list">
        @foreach($curso->examenes as $examen)
        <li>
            <span class="" style="cursor: move">
                <i class="fa fa-ellipsis-v"></i>
                <i class="fa fa-ellipsis-v"></i>
            </span>
            <div class="tools">
                <i onclick="completar_campos_examen({{$examen->datos_para_editar()}})" class="fa fa-edit"
                    data-toggle="tooltip" title="Actualizar este registro de clase"></i>
                <i onclick="abrir_modal_borrar({{$examen->id}}, 'examenes')" class="fa fa-trash-o" data-toggle="tooltip"
                    title="Borrar este registro de clase"></i>
            </div>
            <a id="a-examen-{{$examen->id}}" data-toggle="collapse" data-parent="#accordion"
                href="#collapseexamen{{$examen->id}}" class="collapsed" aria-expanded="false">
                <span onclick="instanciar_tabla('examen', '{{$examen->id}}')" class="todo-item text">
                    Fecha: {{$examen->getFechaFormateadoAttribute()}}.
                    Tema/s: {{$examen->getunidadesstring()}} ({{$examen->modalidad->nombre}})</span>
            </a>
            <div id="collapseexamen{{$examen->id}}" class="panel-collapse collapse" aria-expanded="false"
                style="height: 0px;">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><strong>Fecha:</strong></label>
                                <span class="form-control">{{$examen->getFechaFormateadoAttribute()}}</span>
                            </div>
                            <div class="form-group">
                                <label><strong>Modalidad:</strong></label>
                                <span class="form-control">{{$examen->modalidad->nombre}}</span>
                            </div>
                            <div class="form-group">
                                <label><strong>Unidades evaluadas:</strong></label>
                                @foreach($examen->examenesunidades as $examenunidad)
                                <span style="margin-bottom: 1%" class="form-control"><a data-toggle="tooltip"
                                        data-placement="right"
                                        href="{{ route('titulos.show', $curso->titulo_id) }}"
                                        title="para conocer los contenidos de la unidad puede dirigirse al detalle del registro 
                                        del título que otorga este título dando un click aquí.">{{$examenunidad->unidad->nombre}}</a></span>
                                @endforeach
                            </div>
                            <div class="form-group">
                                <label><strong>Evaluadores:</strong></label>
                                @foreach($examen->examenesprofesorescursos as $profesorexamen)
                                <span style="margin-bottom: 1%"
                                    class="form-control"><b>{{$profesorexamen->profesorcurso->profesor->persona->getNombreCompletoAttribute()}}
                                        ({{$profesorexamen->profesorcurso->rol}})</b></span>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label><strong>Listado de notas:</strong></label>
                                <div style="background-color: #FFFFFF;">
                                    <div style="padding: 1%;">
                                        <table id="datatable-examen-{{$examen->id}}" class="display" width="100%"
                                            cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th class="text-center"> Alumno     </th>
                                                    <th class="text-center"> Documento  </th>
                                                    <th class="text-center"> Asistió    </th>
                                                    <th class="text-center"> Situación  </th>
                                                    <th class="text-center"> Acciones   </th>
                                                    <th class="text-center none"> Motivo inasistencia</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($examen->examenesalumnos as $examenalumno)
                                                <tr>
                                                    <td class="text-uppercase" width="30%">{{$examenalumno->alumno->estudiante->persona->getNombreCompletoAttribute()}}
                                                    </td>
                                                    <td>{{$examenalumno->alumno->estudiante->persona->dni}}</td>
                                                    <td class="text-center">
                                                        {{$examenalumno->getAsistioFormateadoAttribute()}}</td>
                                                    @if($examenalumno->situacion)
                                                    <td class="text-center">{{$examenalumno->situacion}}</td>
                                                    @else
                                                    <td class="text-center">-</td>
                                                    @endif
                                                    <td class="text-center"><a
                                                            onclick="completar_campos_nota({{$examenalumno->datos_para_editar()}})"
                                                            data-toggle="tooltip" title="Editar este registro"
                                                            class="btn btn-social-icon btn-warning btn-sm"><i
                                                                class="fa fa-pencil"></i></a></td>
                                                    @if($examenalumno->motivo_inasistencia)
                                                    <td class="text-center">{{$examenalumno->motivo_inasistencia}}</td>
                                                    @else
                                                    <td class="text-center">-</td>
                                                    @endif
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</div>