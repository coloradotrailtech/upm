<div class="row">
    <div class="col-md-12">
        @if($curso->alumnos->count()>0)
        <table id="datatable-alumnos" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="text-center">Apellido y Nombre</th>
                    <th class="text-center">DNI</th>
                    <th class="text-center">Teléfono</th>
                    <th class="text-center">Email</th>
                </tr>
            </thead>
            <tbody>
                @foreach($curso->alumnos as $alumno)
                <tr>
                    <td class="text-center text-bold">
                        {{$alumno->estudiante->persona->getNombreCompletoAttribute()}}</td>
                    <td class="text-center text-bold">{{$alumno->estudiante->persona->dni}}</td>
                    @if($alumno->estudiante->persona->telefono)
                    <td class="text-center">{{$alumno->estudiante->persona->telefono}}</td>
                    @else
                    <td class="text-center">-</td>
                    @endif
                    @if($alumno->estudiante->persona->email)
                    <td class="text-center">{{$alumno->estudiante->persona->email}}</td>
                    @else
                    <td class="text-center">-</td>
                    @endif
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th class="text-center">Apellido y Nombre</th>
                    <th class="text-center">DNI</th>
                    <th class="text-center">Teléfono</th>
                    <th class="text-center">Email</th>
                </tr>
            </tfoot>
        </table>
        @else
        <div class="alert alert-info alert-dismissible">
            <h4><i class="icon fa fa-info-circle"></i> Información</h4> Aún no se ha inscripto
            ningún alumno a este curso. Puede gestionar inscripciones una dando un click al
            botón:
            <b><i class="fa fa-lg fa-user"></i> gestionar alumnos</b>.
        </div>
        @endif
    </div>
</div>