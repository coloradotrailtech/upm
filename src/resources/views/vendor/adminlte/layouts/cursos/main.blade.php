@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Cursos
@endsection

@section('contentheader_title')
Cursos
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Cursos</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Cursos </h3>
                </div>
                <div class="box-body ">
                    <div>
                        <table class="table" width='100%'>
                            <thead>
                                <tr>
                                    <th class="text-center"> Cantidad cursos </th>
                                    <th class="text-center"> Programados     </th>
                                    <th class="text-center"> Activos         </th>
                                    <th class="text-center"> Finalizados     </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center"> {{ $contenedor_conts['cursos'] }} </td>
                                    <td class="text-center"> {{ $contenedor_conts['programados'] }}</td>
                                    <td class="text-center"> {{ $contenedor_conts['activos'] }} </td>
                                    <td class="text-center"> {{ $contenedor_conts['finalizados'] }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    @include('vendor.adminlte.layouts.partials.msj_acciones')

                    <hr>
                    <div>
                        
                        <p>
                            <span><b>Filtrar por fechas: </b></span>
                            Min <input name="min" id="min" type="text">
                            Max <input name="max" id="max" type="text">
                        </p>
                        
                        <br>

                        <table id="datatable-cursos" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Nro</th>
                                    <th class="text-center"> Curso          </th>
                                    <th class="text-center"> Sede           </th>
                                    <th class="text-center"> Comisión       </th>
                                    <th class="text-center"> Inscriptos     </th>
                                    <th class="text-center"> Inicio         </th>
                                    <th class="text-center"> Finalización   </th>
                                    <th class="text-center"> Estado         </th>
                                    <th class="text-center"> Acciones       </th>
                                    <th class="none text-center">Descripción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($cursos as $curso)
                                <tr>
                                    <td width="5%"> {{ $curso->id }} </td>
                                    <td class="text-bold bg-blue-active" width="25%" id='col-curso-plan'>                                    
                                        {{ $curso->titulo->nombre}} | {{ $curso->titulo->plan}}                                                                       
                                    </td>
                                    @if($curso->sede->movil)
                                    <td class="text-center text-bold">
                                        <b class="text-center text-bold text-orange">
                                            [Móvil]
                                        </b> 
                                        <label class="text-uppercase text-bold">
                                            {{$curso->sede->localidad->nombre}}
                                        </label>
                                    </td>
                                    @else
                                    <td class="text-center">
                                        <label class="text-uppercase text-bold">
                                            {{$curso->sede->localidad->nombre}}
                                        </label>
                                    </td>
                                    @endif


                                    @if($curso->comision)
                                    <td class="text-center text-bold" width="20%">{{$curso->comision}}</td>
                                    @else
                                    <td class="text-center text-bold">-</td>
                                    @endif


                                    <td class="text-center text-green text-bold"> {{ $curso->alumnos->count()   }} </td>
                                    <td class="text-center"> {{ $curso->getFechaInicioFormateadoAttribute()     }} </td>
                                    <td class="text-center"> {{ $curso->getFechaFinFormateadoAttribute()        }} </td>

                                    @if ($curso->determinar_estado() == 'Por iniciar')
                                    <td class="bg-yellow text-center" width="20%"><b> {{ $curso->determinar_estado()        }}</b></td>
                                    @elseif ($curso->determinar_estado() == 'Activo')
                                    <td class="bg-green-active text-center" width="20%"><b> {{ $curso->determinar_estado()  }}</b></td>
                                    @else
                                    <td class="bg-gray-active text-center" width="20%"><b> {{ $curso->determinar_estado()   }}</b></td>
                                    @endif
                                    
                                    <td class="text-center" width="15%">
                                        <a href="{{ route('cursos.show', $curso->id) }}" data-toggle="tooltip"
                                            title="Ver detalles del Curso"
                                            class="btn btn-social-icon btn-info btn-sm"
                                            target='_blank'><i class="fa fa-eye fa-lg"></i></a>
                                        @if(!auth()->user()->isBedel() )

                                        <a onclick="abrir_modal_borrar({{$curso->id}}, 'cursos')" data-toggle="tooltip"
                                            title="Eliminar este registro" class="btn btn-social-icon btn-sm btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                        @endif
                                    </td>

                                    @if($curso->descripcion)
                                    <td class="text-center"> {{ $curso->descripcion }}           </td>
                                    @else
                                    <td class="text-center"> no se incluyó ninguna observación.  </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Nro</th>
                                    <th class="text-center"> Curso          </th>
                                    <th class="text-center"> Sede           </th>
                                    <th class="text-center"> Comisión       </th>
                                    <th class="text-center"> Inscriptos     </th>
                                    <th class="text-center"> Inicio         </th>
                                    <th class="text-center"> Finalización   </th>
                                    <th class="text-center"> Estado         </th>
                                    <th class="text-center"> Acciones       </th>
                                    <th class="none text-center">Descripción</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>                    
                </div>
                <div class="box-footer">
                    <button title="Registrar nuevo curso" type="button" id="boton-modal-crear"
                        class="btn btn-primary pull-right" data-toggle="modal" data-toggle="tooltip"
                        data-target="#modal-crear">
                        <i class="fa fa-plus-circle"></i> &nbsp;nuevo curso
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>

@include('vendor.adminlte.layouts.cursos.formulario.create')
@include('vendor.adminlte.layouts.cursos.formulario.editar')
@include('vendor.adminlte.layouts.cursos.formulario.confirmar')

@endsection

@section('scripts_del_modulo')
<script>
    const cursos = @json($cursos);
    

    $(document).ready(function(){
        $.fn.dataTable.ext.search.push(
            function mod_table (settings, data, dataIndex) {
                var min = $("#min").datepicker("getDate")
                var max = $('#max').datepicker("getDate")

                var dd   = data[5].substr(0,2)
                var mm   = data[5].substr(3,2)
                var aaaa = data[5].substr(6,4)
                var fec_i= `${mm}/${dd}/${aaaa}`                
                var startDate = new Date(fec_i);

                if (min == null && max == null)           { return true; }
                if (min == null && startDate <= max)      { return true; }
                if (max == null && startDate >= min)      { return true; }
                if (startDate <= max && startDate >= min) { return true; }
                return false;        
            }
        );
        
        $("#min").datepicker({ dateFormat: 'dd-mm-yy' },{ onSelect: function mod_table () { table.draw(); }, changeMonth: true, changeYear: true });
        $("#max").datepicker({ dateFormat: 'dd-mm-yy' },{ onSelect: function mod_table () { table.draw(); }, changeMonth: true, changeYear: true });
        var table = $('#datatable-cursos').DataTable();
        
        $('#min, #max').change(function () {
            table.draw();
        });
    });

</script>
<script src="{{ asset('js/admin/curso/curso.js') }}"></script>
<script src="{{ asset('js/admin/curso/tablas.js') }}"></script>
@endsection