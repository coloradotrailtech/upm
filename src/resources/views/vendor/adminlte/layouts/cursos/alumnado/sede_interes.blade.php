@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Sede de interes
@endsection

@section('contentheader_title')
Sede de interes
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Sede de interes </a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Sede elegida </h3>
                </div>

                <div class="box-body ">
                    
                    <div>
                        <h3>Localidad de {{ $localidad }} </h3>
                        <h4>Cursos realizados: {{ count($cursos_presentes) }} </h4>
                    </div> 

                    <!-- Tabla de Cursos -->
                    <div>
                        <table class="table table-striped table-hover" id='myTable_alumnado' cellspacing="0" style='margin-top: 20px;margin-bottom:0px;'>
                            <thead>
                                <tr>
                                    <th class="text-center"> #           </th>
                                    <th class="text-center"> Nro         </th>
                                    <th class="text-center"> Curso       </th>
                                    <th class="text-center"> Comision    </th>
                                    <th class="text-center"> Inscriptos  </th>
                                    <th class="text-center"> Inicio      </th>
                                    <th class="text-center"> Fin         </th>
                                    <th class="text-center"> Estado      </th>
                                    <th class="text-center"> Acciones    </th>
                                </tr>    
                            </thead>
                                
                            <tbody>
                                @foreach($cursos_presentes as $key=>$myCurso)
                                <tr>
                                    <td class="text-center" width='3%'> 
                                        {{ $key+1 }} 
                                    </td>
                                    <td> {{ $myCurso['id'] }} </td>
                                    <td> 
                                        <div class="bg-blue-active text-uppercase text-bold" 
                                            style='padding-left:15px;padding-top:4px;padding-bottom:4px;'> 
                                            {{ $myCurso['curso'] }} | 
                                            {{ $myCurso['plan']  }}   
                                        </div>                                         
                                    </td>
                                    <td> {{ $myCurso['comision'] }} </td>
                                    <td class="text-center text-green text-bold"> {{ $myCurso['inscriptos'] }} </td>
                                    <td class="text-center"> {{ $myCurso['fecha_inicio'] }} </td>
                                    <td class="text-center"> {{ $myCurso['fecha_fin']    }} </td>
                                    
                                    @if ($myCurso['estado'] == 'Por iniciar')
                                    <td width='15%'> 
                                        <div class="bg-yellow text-center text-bold"
                                            
                                            style='padding-left:15px;padding-top:4px;padding-bottom:4px;padding-right: 15px;'> 
                                            {{ $myCurso['estado'] }}                                         
                                        </div> 
                                    </td>
                                    @elseif ($myCurso['estado'] == 'Activo')
                                    <td width='15%'> 
                                        <div class="bg-green text-center text-bold"
                                            
                                            style='padding-left:15px;padding-top:4px;padding-bottom:4px;padding-right: 15px;'> 
                                            {{ $myCurso['estado'] }}                                         
                                        </div> 
                                    </td>
                                    @else
                                    <td width='15%'> 
                                        <div class="bg-gray text-center text-bold" 
                                            
                                            style='padding-left:15px;padding-top:4px;padding-bottom:4px;padding-right: 15px;'>
                                            {{ $myCurso['estado'] }}                                         
                                        </div> 
                                    </td>
                                    @endif

                                    <td class="text-center">
                                        <a href="/cursos/{{ $myCurso['id'] }}" target='_blank' rel='noopener noreferrer'
                                            data-toggle="tooltip" 
                                            title="Ver detalles del Curso"
                                            class="btn btn-social-icon btn-info btn-sm">
                                            <i class="fa fa-eye fa-lg"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>   
                    
                    <br>

                    <!-- Segun estados -->
                    <div>
                    <div><h4> Según estado de curso </h4></div>

                        <br>
                        <table class="table" id='table-curso' cellspacing="0" width='100%' style='margin-top: 20px;margin-bottom:0px;'>
                            <thead>
                                <tr>
                                    <th class="text-center"> Inscriptos  </th>
                                    <th class="text-center"> Aprobados   </th>
                                    <th class="text-center"> Abandonados </th>
                                    <th class="text-center"> Libres      </th>
                                    <th class="text-center"> Sin estado  </th>
                                </tr>    
                            </thead>
                                <tr>
                                    <td class="text-center"> {{ $contadores['inscriptos']   }} </td>
                                    <td class="text-center"> {{ $contadores['aprobados']    }} </td>
                                    <td class="text-center"> {{ $contadores['abandonados']  }} </td>
                                    <td class="text-center"> {{ $contadores['libres']       }} </td>
                                    <td class="text-center"> {{ $contadores['sin_estado']   }} </td>
                                </tr>
                            <tbody>

                            </tbody>
                        </table>
                        <br>

                        <div id='div-chart'>
                            <canvas id="myChart_estados"></canvas>
                        </div>

                    </div>

                    <br> 
                    
                    <!-- Segun edades -->
                    <div>
                        <div><h4> Según edades </h4></div>

                        <br>

                        <table class="table table-hover" id='table-curso' cellspacing="0" width='80%' style='margin-bottom:0px;'>
                            <thead>
                                <tr>
                                    <th class="text-center"> < 20           </th>
                                    <th class="text-center"> Entre 20 y 30  </th>
                                    <th class="text-center"> Entre 30 y 40  </th>
                                    <th class="text-center"> Entre 40 y 50  </th>
                                    <th class="text-center"> Entre 50 y 60  </th>
                                    <th class="text-center"> Entre 60 y 70  </th>
                                    <th class="text-center"> Entre 70 y 80  </th>
                                    <th class="text-center"> > 80  </th>
                                </tr>    
                            </thead>
    
                            <tbody>
                                <tr>
                                    <td class="text-center"> {{ $data_edades['cont_10'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_20'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_30'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_40'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_50'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_60'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_70'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_80'] }} </td>
                                </tr>
                            </tbody>
                        </table>

                        <br>
                        <div id='div-chartBar'>
                            <canvas id="myChart_bar"></canvas>
                        </div>
                    </div>

                    <br>
                    
                    <!-- Segun Trabajo -->
                    <div>
                        <table class="table table-hover" id='table-curso' cellspacing="0" width='100%' style='margin-top: 20px;margin-bottom:0px;'>
                            <thead>
                                <tr>
                                    <th class="text-center"> Trabaja  </th>
                                    <th class="text-center"> No Trabaja              </th>
                                </tr>    
                            </thead>
                                <tr>
                                    <td class="text-center"> {{ $data_trabaja['trabaja']  }} => {{ $data_trabaja['p_trab'] }}% </td>
                                    <td class="text-center"> {{ $data_trabaja['no_trabaja']  }} => {{ $data_trabaja['p_no_trab'] }}% </td>
                                </tr>
                            <tbody>

                            </tbody>
                        </table>
                        <div id='div-chart'>
                            <canvas id="myChart_trabaja"></canvas>
                        </div>
                    </div>

                    <br>
                    
                </div>      

            </div>
        </div>
    </div>
</div>
    
@section('scripts_del_modulo')    

<script text='text/javascript'>

    const contadores = @json($contadores);    
    const ctx = document.getElementById('myChart_estados').getContext('2d');
    const myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ['Aprobados', 'Abandonados', 'Libres', 'Sin estado'],
            datasets: [{
                label: 'Estado de estudiante',
                data: [ 
                    contadores['aprobados'],
                    contadores['abandonados'],
                    contadores['libres'], 
                    contadores['sin_estado']
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.4)',
                    'rgba(54, 162, 235, 0.4)',                    
                    'rgba(75, 192, 192, 0.4)',
                    'rgba(153, 102, 255, 0.4)'                    
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',                    
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)'                    
                ]
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: false,
                    display: false
                },
                x: {
                    beginAtZero: false,
                    display: false
                }
            }
        }
    });


    const data_edades = @json($data_edades);
    const ctx_bar = document.getElementById('myChart_bar').getContext('2d');
    const myChart_bar = new Chart(ctx_bar, {
        type: 'bar',
        data: {
            labels: ['< 20', 'Entre 20 y 30', 'Entre 30 y 40', 'Entre 40 y 50', 
                'Entre 50 y 60', 'Entre 60 y 70', 'Entre 70 y 80', '> 80'],
            datasets: [{
                label: 'Cantidad',
                data: [ 
                    data_edades['cont_10'],
                    data_edades['cont_20'],
                    data_edades['cont_30'],
                    data_edades['cont_40'],
                    data_edades['cont_50'],
                    data_edades['cont_60'],
                    data_edades['cont_70'],
                    data_edades['cont_80'],
                ],
                backgroundColor: [
                    'rgba(185, 207, 189, 1)'
                ]
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    display: true
                },
                x: {
                    beginAtZero: false,
                    display: true
                }
            }
        }
    });

    const cont_trab = @json($data_trabaja);
    const ctx_trabaja = document.getElementById('myChart_trabaja').getContext('2d');    
    const myChart_trab = new Chart(ctx_trabaja, {
        type: 'pie',
        data: {
            labels: ['% Trabaja ', '% No Trabaja '],
            datasets: [{
                label: '',
                data: [ 
                    cont_trab['p_trab'],
                    cont_trab['p_no_trab']
                ],
                backgroundColor: [
                    '#ebddc4',
                    '#cabeb3'
                ],
                borderColor: [                    
                    '#ecc9ab',
                    '#857b70'
                ]
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: false,
                    display: false
                },
                x: {
                    beginAtZero: false,
                    display: false
                }
            }
        }
    });

</script>

<script src="{{ asset('js/admin/curso/tablas.js') }}"></script>
<script src="{{ asset('js/menu.js') }}"></script>
<script src="{{ asset('js/admin/curso/exports.js') }}"></script>
@endsection
@endsection