@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Estudiantes por Oficio
@endsection

@section('contentheader_title')
Estudiantes por Oficio
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Estudiantes por Oficio </a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Lista de Estudiantes por Oficio </h3>
                </div>

                <div class="box-body ">
                    
                    <div>
                        <h3> {{ $planificacion['nombre'] }} - Plan {{ $planificacion['plan'] }}</h3>
                        <br>
                        <h4> Cursos encontrados: {{ count($planificacion['data_cursos']) }} </h4>
                        <h4> Localidades: 
                        @foreach($planificacion['data_cursos'] as $curso)
                            {{ $curso['localidad'] }} ||                 
                        @endforeach
                        </h4>
                    </div>                   

                    
                    <table class="table" id='table-curso' cellspacing="0" width='100%' style='margin-top: 20px;margin-bottom:0px;'>
                        <thead>
                            <tr>
                                <th class="text-center">Inscriptos  </th>
                                <th class="text-center">Aprobados  </th>
                                <th class="text-center">Abandonados</th>
                                <th class="text-center">Libres     </th>
                                <th class="text-center">Sin estado </th>
                            </tr>    
                        </thead>
                            <tr>
                                <td class="text-center"> {{ $contadores['inscriptos']   }} </td>
                                <td class="text-center"> {{ $contadores['aprobados']   }} </td>
                                <td class="text-center"> {{ $contadores['abandonados'] }} </td>
                                <td class="text-center"> {{ $contadores['libres']      }} </td>
                                <td class="text-center"> {{ $contadores['sin_estado']  }} </td>
                            </tr>
                        <tbody>

                        </tbody>
                    </table>
                    
                    @foreach($planificacion['data_cursos'] as $curso)
                    <table class="table table-hover" id='table-curso' cellspacing="0" width='100%' style='margin-top: 20px;margin-bottom:0px;'>
                        <thead style='background-color: #000;color:#fff;'>
                            <tr>
                                <th> N° </th>
                                <th> Localidad  </th>
                                <th> Inicio     </th>
                                <th> Fin        </th>
                                <th> Comisión   </th>
                                <th> Descrip    </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> {{ $curso['curso_id'] }} </td>
                                <td> {{ $curso['localidad'] }} </td>
                                <td> {{ $curso['fec_inicio']   }} </td>
                                <td> {{ $curso['fec_fin'] }} </td>
                                <td> {{ $curso['comision']  }} </td>
                                <td> {{ $curso['descrip'] }} </td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-striped table-hover" id='table-alumnado' cellspacing="0" width='100%'>                  
                        <thead>
                            <tr>
                                <th></th>
                                <th> Mat        </th>
                                <th> Apellido   </th>
                                <th> Nombre     </th>
                                <th> DNI        </th>
                                <th> Localidad  </th>
                                <th> Email      </th>
                                <th> Estado Curso </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($curso['alumnado'] as $key => $alumno )
                            <tr>
                                <td> {{ $key+1    }} </td>
                                <td> {{ $alumno['estudiante_id']}}  </td>
                                <td> {{ $alumno['apellido']}}       </td>
                                <td> {{ $alumno['nombre']}}         </td>
                                <td> {{ $alumno['dni']}}            </td>
                                <td> {{ $alumno['localidad']}}      </td>
                                <td> {{ $alumno['email']}}          </td>
                                <td> {{ $alumno['estado_curso']}}   </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <hr>
                    @endforeach                  
                    <a href="/cursos/alumnado/exports/{{ $planificacion['titulo_id'] }}"
                        title="Exportar para Excel"
                        class="btn btn-sm btn-success">
                        <i class="fa fa-lg fa-file-excel-o">Exportar Excel</i>
                    </a>
                </div>                

            </div>
        </div>
    </div>
</div>
    
@section('scripts_del_modulo')    
<script src="{{ asset('js/menu.js') }}"></script>
<script src="{{ asset('js/admin/curso/exports.js') }}"></script>
@endsection
@endsection
