@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Cursos
@endsection

@section('contentheader_title')
Cursos
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Listado Oficios </a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Listados de Oficios </h3>
                    <h4>Cursos realizados: {{ $cant_tot }} </h4>
                </div>

                <div class="box-body ">
                    <div>
                        <table class="table table-striped table-hover" cellspacing="0" id='myTable_alumnado'>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-center"> Nombre     </th>
                                    <th class="text-center"> Plan       </th>
                                    <th class="text-center"> Veces realizado  </th>
                                    <th class="text-center"> Accion     </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($titulos_ as $key => $titulo)
                                <tr>
                                    <td class="text-center" width='3%' style='padding-top:11px;'> {{ $key+1 }} </td>
                                    <td width='50%'>
                                        <div class="bg-blue-active text-uppercase text-bold" style='padding-left:15px;padding-top:4px;padding-bottom:4px;'> 
                                        {{ $titulo['nombre'] }} 
                                        </div>
                                    </td> 
                                    <td class="text-center"> 
                                        <div style='padding-left:10px;padding-top:4px;padding-bottom:4px;'>
                                            {{ $titulo['plan']   }}
                                        </div>
                                    </td>
                                    @if($titulo['cant'] == 0)
                                    <td class="text-center text-red"> 
                                        {{ $titulo['cant'] }} 
                                    </td>
                                    <td class="text-center">
                                        <a href="/cursos/alumnado/{{ $titulo['id'] }}" target='_blank' rel='noopener noreferrer'
                                            data-toggle="tooltip" 
                                            title="Visualizar estudiantes que realizaron el Oficio"
                                            class="btn btn-social-icon btn-danger btn-sm"
                                            disabled>
                                            <i class="fa fa-eye fa-lg"></i>
                                        </a>
                                    </td>
                                    @else
                                    <td class="text-center text-green"> 
                                        <strong>{{ $titulo['cant'] }} </strong>
                                    </td>
                                    <td class="text-center">
                                        <a href="/cursos/alumnado/{{ $titulo['id'] }}" target='_blank' rel='noopener noreferrer'
                                            data-toggle="tooltip" 
                                            title="Visualizar estudiantes que realizaron el Oficio"
                                            class="btn btn-social-icon btn-info btn-sm">
                                            <i class="fa fa-eye fa-lg"></i>
                                        </a>
                                    </td>
                                    @endif
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/menu.js') }}"></script>
<script src="{{ asset('js/admin/curso/tablas.js') }}"></script>
@endsection