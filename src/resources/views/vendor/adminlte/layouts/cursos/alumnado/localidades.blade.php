@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<style>
#map{
  height: 900px;  
}
</style>
@endsection

@section('htmlheader_title')
Localidades con Sedes
@endsection

@section('contentheader_title')
Localidades con Sedes
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Localidades con Sedes</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Sedes </h3>
                </div>

                <div class="box-body ">
                    
                    <div>
                        <h3>Localidades</h3>
                    </div>                   

                    <div>

                        <table class="table table-striped table-hover" id='myTable_alumnado' cellspacing="0" style='margin-top: 20px;margin-bottom:0px;'>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-center"> Localidad          </th>
                                    <th class="text-center"> CP                 </th>
                                    <th class="text-center"> Tipo de Sede       </th>
                                    <th class="text-center"> Cursos realizados  </th>
                                    <th class="text-center"> Accion             </th>
                                </tr>    
                            </thead>
                                
                            <tbody>
                                @foreach($sedes_ as $key=>$mySede)
                                <tr>
                                    <td class="text-center" width='3%' style='padding-top:11px;'> 
                                        {{ $key+1 }} 
                                    </td>
                                    @if($mySede['movil'])
                                        <td> 
                                            <div class="bg-blue-active text-uppercase text-bold" 
                                                style='padding-left:15px;padding-top:4px;padding-bottom:4px;'> 
                                                {{ $mySede['nombre'] }}  
                                            </div>                                        
                                        </td>                                    
                                    @else
                                        <td> 
                                            <div class="bg-orange-active text-uppercase text-bold" 
                                                style='padding-left:15px;padding-top:4px;padding-bottom:4px;'> 
                                                {{ $mySede['nombre'] }}  
                                            </div>                                        
                                        </td>                                    
                                    @endif
                                        
                                    <td class="text-center">
                                        <div style='padding-left:10px;padding-top:4px;padding-bottom:4px;'>
                                            {{ $mySede['cp'] }}
                                        </div>
                                    </td>   


                                    @if($mySede['movil'])
                                        <td class="text-center"><div style='padding-left:10px;padding-top:4px;padding-bottom:4px;'>
                                            Movil
                                        </div></td>                                    
                                    @else
                                        <td class="text-center"><div style='padding-left:10px;padding-top:4px;padding-bottom:4px;'>
                                            Fija
                                        </div></td>                                      
                                    @endif

                                    @if($mySede['cont'] == 0)
                                    <td class="text-center text-red">
                                        <div style='padding-left:10px;padding-top:4px;padding-bottom:4px;'>
                                           {{ $mySede['cont'] }}
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <a href="#" target='_blank' rel='noopener noreferrer'
                                            data-toggle="tooltip" 
                                            title="Ver cursos realizados en esta Localidad"
                                            class="btn btn-social-icon btn-danger btn-sm"
                                            disabled>
                                            <i class="fa fa-eye fa-lg"></i>
                                        </a>
                                    </td>
                                    @else
                                    <td class="text-center">
                                        <div style='padding-left:10px;padding-top:4px;padding-bottom:4px;'>
                                           <strong>{{ $mySede['cont'] }}</strong> 
                                        </div>
                                    </td>


                                    <td class="text-center">                                        
                                        <a href="/sede/{{ $mySede['sede_id'] }}" target='_blank' rel='noopener noreferrer'
                                            data-toggle="tooltip" 
                                            title="Ver cursos realizados en esta Localidad"
                                            class="btn btn-social-icon btn-info btn-sm">
                                            <i class="fa fa-eye fa-lg"></i>
                                        </a>
                                    </td>
                                    @endif
                                    

                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>                    
                </div>      

            </div>
        </div>
    </div>

    <div id="map"></div>

    <br>

    <div class="box-footer">
        <button type="button" class="btn btn-sm" data-toggle='modal' data-target='#sede_interes'>
            <i class="fa fa-calendar fa-lg"></i>
            Filtrar por fechas
        </button>
    </div>
</div>



<!-- Modal daterange -->
<div class="modal fade" id="sede_interes" tabindex="-1" role="dialog" aria-labelledby="sede_interes" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title" id="sede_seleccionada">Estadísticas de Estudiantes por Localidad</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <form action="/sede/sede_estadisticas" method="get">

      <!-- Modal Body -->
      <div class="modal-body">
        <p>
        <span><b>Sede interés: </b></span>            
        <select name="select_sede"  class="selectpicker">
        @foreach($sedes_ as $key=>$mySede)
            @if($mySede['cont'] == 0)
                <option value="#" class="text-uppercase text-red text-bold" disabled>
                    {{ $mySede['nombre'] }} 
                </option>
            @else
                @if($mySede['movil'])
                    <option value="{{ $mySede['sede_id'] }}" class="text-uppercase text-blue text-bold">
                        {{ $mySede['nombre'] }} 
                    </option>              
                @else
                    <option value="{{ $mySede['sede_id'] }}" class="text-uppercase text-orange text-bold">
                        {{ $mySede['nombre'] }} 
                    </option>  
                @endif
            @endif
            
        @endforeach
        </select> 
        </p>

        

        <p>
        <span><b>Rango deseado: </b></span>
            <input width='10%' type="text" name='daterange' class="form-control">
        </p>
        <!-- 
        <p>
        <span><b>Estado de Curso: </b></span>
        <select name="est_deseado" id="" class="selectpicker">
            <option value="todos">      Todos       </option>
            <option value="aprobado">   Aprobados   </option>
            <option value="libre">      Libres      </option>
            <option value="abandono">   Abandonados </option>
        </select>
        </p>
        -->
        
      </div>
        
      

      <div class="modal-footer ">
        <input type="submit" id='sede_estadistica' class="btn" title="Enviar consulta">
      </div>

      </form>

    </div>
  </div>
</div>
    
@section('scripts_del_modulo')   
<script src="{{ asset('js/leaflet.js') }}"></script>
<script text='text/javascript'>
    const sedes = {!! json_encode($sedes_) !!};
    $('#sede_seleccionada')
    $('input[name="daterange"]').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
    
    sedes.forEach(ele => {
        let disab, icono, tip_btn = 'btn-info';

        if(ele.lat){
            
            if(ele.cont == 0) disab = 'disabled', tip_btn = 'btn-danger';
            let btn_ver = `<a href="/sede/${ele.sede_id}" target='_blank' rel='noopener noreferrer'
                data-toggle="tooltip" 
                title="Ver cursos realizados en esta Localidad"
                class="btn btn-social-icon ${tip_btn} btn-sm" ${disab}>
                <i class="fa fa-eye fa-lg"></i>
            </a>`     

            if(ele.movil == 1){
                icono = blueIcon
            }else{
                icono = orangeIcon
            }
            markerSede(ele,icono,btn_ver)

        }
    });

    function markerSede(sede, icono,btn_ver){
        L.marker([sede.lat, sede.lng],{icon: icono}).addTo(map)
        .bindPopup(`<b>${sede.nombre}</b><br> Cantidad ${sede.cont} <br> ${btn_ver}`);
    }

</script> 
<script src="{{ asset('js/admin/curso/tablas.js') }}"></script>
<script src="{{ asset('js/menu.js') }}"></script>

@endsection
@endsection