<div class="modal fade" id="modal-alta-alumno" >
    <div class="modal-dialog modal-lg" style="width: 60%">

        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h3 class="modal-title text-center">Formulario de inscripción de alumno a curso:</h3>
                <h3 class="modal-title text-center bg-aqua-gradient">"{{$curso->titulo->nombre}}"</h3>
            </div>

            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')

                <form action="/alumnos" id="formulario-alumno" method="POST" class="form validity">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="curso_id" value="{{ $curso->id }}">

                    <!-- Row Datos Personales -->
                    <legend><em>Datos Personales</em></legend>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Apellido:</label>
                                <input name="apellido" id="apellido" type="text" maxlength="50" class="form-control text-bold" placeholder="campo requerido" required>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input name="nombre" id="nombre" type="text" maxlength="50" class="form-control text-bold" placeholder="campo requerido" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Fecha Nac.:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="date" name="fecha_nac" id="fecha_nac" class="form-control pull-right" autocomplete="off" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Sexo:</label>
                                <select name="sexo" id="sexo" placeholder="campo no obligatorio" data-header="Seleccione.." class="selectpicker form-control">
                                    <option value="Masculino">Masculino</option>
                                    <option value="Femenino">Femenino</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>DNI:</label>
                                <input name="dni" id="dni" type="number" maxlength="8" class="form-control" placeholder="ej: 34478385" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>CUIL:</label>
                                <input name="cuil" id="cuil" type="number" maxlength="11" class="form-control" autocomplete="off" placeholder="ej: 20344783854">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nacionalidad:</label>
                                <select name="pais_id" id="pais_id" placeholder="campo obligatorio" data-header="Seleccione país" class="selectpicker form-control" data-style="btn-primary">
                                    @foreach($paises as $pais)
                                        <option value="{{$pais->id}}">{{$pais->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Estado Civil:</label>
                                <select name="estado_civil" id="estado_civil" placeholder="campo obligatorio" data-header="Seleccione estado civil del estudiante" class="selectpicker form-control" data-style="btn-primary">
                                    <option value="soltero">Soltero/a</option>
                                    <option value="casado">Casado/a</option>
                                    <option value="divorciado">Divorciado/a</option>
                                    <option value="viudo">Viudo/a</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Localidad:</label>
                                <select name="localidad_id" id="localidad_id" placeholder="campo requerido" data-header="Selecciona localidad de residencia" class="selectpicker form-control" data-live-search="true">
                                    @foreach($localidades as $localidad)
                                        <option value="{{$localidad->id}}" data-subtext="({{$localidad->provincia->nombre}})">{{$localidad->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Domicilio:</label>
                                <input name="direccion" id="direccion" type="text" maxlength="50" class="form-control" placeholder="ej: 9 de Julio 1266" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Grupo Sanguineo:</label>
                                <select name="sangre" id="sangre" placeholder="campo obligatorio" data-header="Seleccione tipo de sangre" class="selectpicker form-control" data-style="btn-danger">
                                    <option value=null>sin especificar</option>
                                    <option value="ab+">AB+</option>
                                    <option value="ab-">AB-</option>
                                    <option value="a+">A+</option>
                                    <option value="a-">A-</option>
                                    <option value="b+">B+</option>
                                    <option value="b-">B-</option>
                                    <option value="0+">0+</option>
                                    <option value="0-">0-</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Estudios Cursados:</label>
                            <select name="estudios" id="estudios" placeholder="campo opcional" data-header="Selecciona un curso" class="selectpicker form-control" data-live-search="true">
                                <option value="Primaria Incompleta">Primaria Incompleta&nbsp;</option>
                                <option value="Primaria Completa">Primaria Completa&nbsp;</option>
                                <option value="Secundaria Incompleta">Secundaria Incompleta&nbsp;</option>
                                <option value="Secundaria Completa">Secundaria Completa&nbsp;</option>
                                <option value="Terciario Incompleto ">Terciario Incompleto &nbsp;</option>
                                <option value="Terciario Completo">Terciario Completo&nbsp;</option>
                                <option value="Universitario Incompleto">Universitario Incompleto&nbsp;</option>
                                <option value="Universitario Completo">Universitario Completo&nbsp;</option>
                            </select>
                        </div>
                    </div>

                    <!-- Row Informacion de Contacto -->
                    <legend><em>Información de Contacto</em></legend>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Email:</label>
                                <input name="email" id="email" type="email" maxlength="50" class="form-control" placeholder="campo opcional" autocomplete="off" >
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Celular:</label>
                                <input name="telefono" id="telefono" type="number" maxlength="15" class="form-control" placeholder=" ej 3764111222" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <label>Teléfono fijo:</label>
                                <input name="telefono2" id="telefono2" type="number" maxlength="15" class="form-control" placeholder="opcional" autocomplete="off">
                            </div>
                        </div>

                    </div>

                    <legend><em></em></legend>

                    <!-- Row Discapacidad -->
                    <div class="row" style='margin-top: 0px;margin-bottom: 0px;'>

                        <div class="col-md-3 form-group" >
                            <label>Discapacidad:</label>
                            <p>
                            <input type="checkbox"
                                name="discapacidad" id="discapacidad"
                                data-toggle="toggle"
                                data-on="Si"
                                data-off="No"
                                data-onstyle="warning"
                                data-offstyle="success" data-width="90">
                            </p>
                        </div>

                        <div class="col-md-3 form-group" id='carnet_discap' hidden>
                            <label>Tiene Carnet:</label>
                            <p>
                            <input type="checkbox"
                                name="carnet_discapacidad" id="carnet_discapacidad"
                                {{--checked--}}
                                data-toggle="toggle"
                                data-on="Si"
                                data-off="No"
                                data-onstyle="warning"
                                data-offstyle="success" data-width="90">
                            </p>
                        </div>

                        <div class="col-md-6 form-group" id='detalle_discap' hidden>
                            <label>Detalle</label>
                            <input name="detalle_discapacidad" id="detalle_discapacidad" class="form-control">
                        </div>
                        
                    </div>

                    <legend><em></em></legend>

                    <!-- Row Mano Hábil y Trabajo -->
                    <div class="row">

                        <div class="col-md-3">
                            <labels>Mano hábil:</label>
                            <p>
                            <input type="checkbox"
                                name="mano_habil" id="mano_habil"
                                checked
                                data-toggle="toggle"
                                data-on="derecha"
                                data-off="izquierda"
                                data-onstyle="info"
                                data-offstyle="success"
                                data-width="90">
                            </p>
                        </div>

                        <div class="col-md-3">
                            <labels>Trabaja:</label>
                            <p>
                            <input type="checkbox"
                                name="trabaja" id="trabaja"
                                {{--checked--}}
                                data-toggle="toggle"
                                data-on="Si"
                                data-off="No"
                                data-onstyle="warning"
                                data-offstyle="success" data-width="90">
                            </p>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-check" style="">
                                <input type="checkbox" class="form-check-input" id="realizo_tareas" name="realizo_tareas" value="1">
                                <label class="form-check-label" for="realizo_tareas"><label>Realizo alguna vez tareas relacionadas al curso</label></label>
                            </div>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" name="interes_trabajar" id="interes_trabajo" value="1">
                                <label class="form-check-label" for="trabaja"><label>Interés en trabajar en este oficio</label></label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Rubro de Trabajo:</label>
                                <input name="rubro_trabaja" id="rubro_trabaja" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Descripción Adicional:</label>
                                <textarea name="descripcion" id="descripcion" class="form-control" rows="1" value="" maxlength="200" placeholder="campo opcional (máximo 200 caracteres)" style="resize: none;"></textarea>
                            </div>
                        </div>

                    </div>

                    <br>
                    
                    <button id="boton_submit_crear" type="submit" class="btn btn-primary hide"></button>

                </form>

                <br>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn bg-teal-gradient" onclick="$('#boton_submit_crear').click()">Confirmar Inscripción</button>
            </div>

        </div>

    </div>
</div>

