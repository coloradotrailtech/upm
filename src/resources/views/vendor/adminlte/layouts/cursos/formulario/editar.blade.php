<div class="modal fade" id="modal_update_data">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Actualizar datos del curso</h4>
            </div>
            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form id="form-update" action="" method="POST" accept-charset="UTF-8" autocomplete="off">
                    <input name="_method" type="hidden" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label>Sede:</label>
                        <select id="sede_id-update"
                                name="sede_id"
                                class="selectpicker form-control"
                                data-live-search="true"
                                title="Selecciona donde se dicatará el curso"
                                data-header="Sede del curso (campo obligatorio)"
                                required>
                            <optgroup label="Sedes Estables">
                                @foreach($sedes as $sede)
                                <option value="{{$sede->id}}">{{$sede->getnombre()}}</option>
                                @endforeach
                            </optgroup>
                            <optgroup label="Sedes Móviles">
                                @foreach($sedes_moviles as $sede)
                                <option value="{{$sede->id}}">{{$sede->getnombre()}}</option>
                                @endforeach
                            </optgroup>
                        </select> 
                    </div>      
                    <div class="form-group">
                        <label>Título:</label>                    
                        <select id="titulo_id-update"
                                name="titulo_id"
                                class="selectpicker form-control"
                                data-live-search="true"
                                title="Título a otorgar al aprobar el curso"
                                data-header="Selecciona el título a cursar (campo obligatorio)"
                                required>
                            @foreach($titulos as $titulo)
                            @if($titulo->unidades->count() > 0)
                            <option value="{{$titulo->id}}">{{$titulo->nombre}}</option>
                            @endif                                                       
                            @endforeach
                        </select>
                    </div>      
                    <div class="form-group">
                        <label>Inicio - Finalización:</label>

                        <div class="input-group">

                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                                
                            </div>
                            <input type="date" name="fecha_inicio" id="fecha_inicio" placeholder="Fecha inicio" required>
                            -
                            <input type="date" name="fecha_fin" id="fecha_fin" placeholder="Fecha fin" required>
                        </div>
                        
                    </div> 
                    <div class="form-group">
                        <label>Titular/es:</label>
                        <select id="titulares-update"
                                name="titulares[]"
                                class="selectpicker form-control"
                                data-live-search="true"
                                multiple=""
                                data-max-options="2"
                                title="Selecciona a dos profesores"
                                data-header="Profesores titulares del curso (campo obligatorio)" required>
                            @foreach($profesores as $profesor)
                            <option value="{{$profesor->id}}">{{$profesor->persona->getNombreCompletoAttribute()}} ({{$profesor->titulacion->nombre}})</option>
                            @endforeach
                        </select> 
                    </div>
                    <div class="form-group">
                        <label>Técnico en Segudidad e Higiene:</label>
                        <select id="suplentes-update"
                                name="suplentes[]"
                                class="selectpicker form-control"
                                data-live-search="true"
                                multiple=""
                                data-max-options="2"
                                title="Selecciona a dos profesores (campo obligatorio)"
                                data-header="Profesores suplentes para el curso">
                            @foreach($profesores as $profesor)
                            <option value="{{$profesor->id}}">{{$profesor->persona->getNombreCompletoAttribute()}} ({{$profesor->titulacion->nombre}})</option>
                            @endforeach
                        </select> 
                    </div>
                    <div class="form-group">                           
                        <label>Estado:</label><br>
                        <input  
                            name="activa"
                            class="form-control"
                            data-width="100"
                            id="activa"
                            type="checkbox"                                                                                                                   
                            data-toggle="toggle" 
                            data-on="Activo" 
                            data-off="Inactivo" 
                            data-onstyle="success"> 
                    </div>   
                    <div class="form-group">
                        <label>Comisión:</label>
                        <input name="comision" id="comision-update" type="text" maxlength="50" class="form-control" placeholder="campo requerido" required>
                    </div>  
                    <div class="form-group">
                        <label>Observación:</label>
                        <input id="descripcion_curso" name="descripcion" type="text" maxlength="50" class="form-control" placeholder="campo opcional">
                    </div>
                </form>  
                <br>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn  btn-warning" onclick="comprobar_validez('update')">actualizar curso</button>
            </div>
        </div>
    </div>
</div>