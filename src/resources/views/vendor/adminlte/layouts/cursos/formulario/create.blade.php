<div class="modal fade" id="modal-crear">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Registrar nuevo curso</h4>
            </div>
            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form id="form-create" action="/cursos" method="POST" autocomplete="off" class="form validity">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">   
                    <div class="form-group">
                        <label>Sede:</label>                                              
                        <select name="sede_id"
                                id="sede_id-create"
                                class="selectpicker form-control"
                                data-live-search="true"
                                title="Selecciona donde se dicatará el curso"
                                data-header="Sede del curso (campo obligatorio)"
                                required="">
                            @if(auth()->user()->isEncargadoTemporal() )
                                <optgroup label="Sedes Móviles">
                                    @foreach($sedes_moviles as $sede)
                                    <option value="{{$sede->id}}">{{$sede->getnombre()}}</option>
                                    @endforeach
                                </optgroup>
                            @else
                                <optgroup label="Sedes Estables">
                                    @foreach($sedes as $sede)
                                        <option value="{{$sede->id}}" class="text-bold">{{$sede->getnombre()}}</option>
                                    @endforeach
                                </optgroup>
                                <optgroup label="Sedes Móviles">
                                    @foreach($sedes_moviles as $sede)
                                        <option value="{{$sede->id}}">{{$sede->getnombre()}}</option>
                                    @endforeach
                                </optgroup>
                            @endif
                        </select>                                                                      
                    </div>      
                    <div class="form-group">
                        <label>Título:</label>
                        <select name="titulo_id"
                                id="titulo_id-create"
                                class="selectpicker form-control"
                                data-live-search="true"
                                title="Título a otorgar al aprobar el curso"
                                data-header="Selecciona el título que otorga el curso"
                                required="">
                            @foreach($titulos as $titulo)
                                @if($titulo->unidades->count() > 0)
                                     <option value="{{$titulo->id}}">{{$titulo->nombre}} | {{$titulo->plan}}</option>
                                @endif                                                       
                            @endforeach
                        </select> 
                    </div>    
                    <div class="form-group">
                        <label>Fechas de inicio y finalización:</label>

                        <div class="input-group">

                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                                
                            </div>
                            <input type="date" name="fecha_inicio" id="fecha_inicio" placeholder="Fecha inicio">
                            -
                            <input type="date" name="fecha_fin" id="fecha_fin" placeholder="Fecha fin">
                        </div>
                    </div>                   
                    <div class="form-group">
                        <label>Titular/es:</label>
                        <select name="titulares[]"
                                id="titulares-create"
                                class="selectpicker form-control"
                                data-live-search="true"
                                multiple=""
                                required=""
                                data-max-options="2"
                                title="Selecciona a dos profesores"
                                data-header="Profesores titulares del curso (campo obligatorio)">
                            @foreach($profesores as $profesor)
                            <option value="{{$profesor->id}}">{{$profesor->persona->getNombreCompletoAttribute()}} ({{$profesor->titulacion->nombre}})</option>
                            @endforeach
                        </select> 
                    </div>
                    <div class="form-group">
                        <label>Técnico en Segudidad e Higiene:</label>
                        <select name="suplentes[]" 
                                id="suplentes-create"
                                class="selectpicker form-control"
                                data-live-search="true"
                                multiple=""
                                data-max-options="2"
                                title="Selecciona a dos profesores"
                                data-header="Profesores suplentes para el curso (campo obligatorio)">
                            @foreach($profesores as $profesor)
                            <option value="{{$profesor->id}}">{{$profesor->persona->getNombreCompletoAttribute()}} ({{$profesor->titulacion->nombre}})</option>
                            @endforeach
                        </select> 
                    </div>
                    <div class="form-group">
                        <label>Comisión:</label>
                        <input name="comision" id="comision-create" type="text" maxlength="50" class="form-control" placeholder="campo requerido" required>
                    </div>   
                    <div class="form-group">
                        <label>Observación:</label>
                        <input name="descripcion" type="text" maxlength="50" class="form-control" placeholder="campo opcional">
                    </div>
                    <br>                    
                </form>
                <br>      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn btn-primary" onclick="comprobar_validez('create')">registrar curso</button>
            </div>
        </div>          
    </div>
</div>