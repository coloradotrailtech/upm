<head>
    <meta charset="UTF-8">
    <title> UPM | @yield('htmlheader_title', 'Your title here') </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta id="csrf-token" name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fuentes utilizadas -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500" rel="stylesheet">

    <!-- Theme style -->
    <link href="{{ asset('/css/all.css') }}" rel="stylesheet" type="text/css" />

    <!-- Bootstrap-select -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-select/bootstrap-select.min.css') }}  ">

    <!-- Bootstrap-toggle -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-toggle/bootstrap-toggle.min.css') }}  ">

    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables-cloud/css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables-cloud/css/responsive.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables-cloud/css/resalte.css') }}">

    <!-- ICheck -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/iCheck/all.css') }}  ">

    <!-- Sweet Alert -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/sweetalert/sweetalert2.min.css') }}  ">

    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}  ">

    <!-- datepicker -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datepicker/bootstrap-datepicker.min.css') }}  ">

    <!-- date-range-picker -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}  ">

    <!-- croppie -->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/croppie/css/croppie.css') }}">

    <!-- Licha Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styleLicha.css') }}">
    
    @yield('estilos_del_modulo', '')
    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!} ;
    </script>

    <!-- FormValidate JS estilo ↓-->
    <link rel="stylesheet" href="{{asset('plugins/Form-ValidityJS/css/style.css')}}" />

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/dataTables.checkboxes.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/select.dataTables.min.css')}}">
    

    <!-- Charts JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.6.0/chart.min.js" integrity="sha512-GMGzUEevhWh8Tc/njS0bDpwgxdCJLQBWG3Z2Ct+JGOpVnEmjvNx6ts4v6A2XJf1HOrtOsfhv3hBKpK9kE5z8AQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> 

    <!-- Leaflet -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin=""/>
    <!-- Leaflet Draw-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw.js" integrity="sha512-ozq8xQKq6urvuU6jNgkfqAmT7jKN2XumbrX1JiB3TnF7tI48DPI4Gy1GXKD/V3EExgAs1V+pRO7vwtS1LHg0Gw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/1.0.4/leaflet.draw-src.css" integrity="sha512-vJfMKRRm4c4UupyPwGUZI8U651mSzbmmPgR3sdE3LcwBPsdGeARvUM5EcSTg34DK8YIRiIo+oJwNfZPMKEQyug==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Leaflet StyleEditor-->
    <script src="https://cdn.jsdelivr.net/npm/leaflet-styleeditor@0.1.21/dist/javascript/Leaflet.StyleEditor.min.js" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-styleeditor@0.1.21/dist/css/Leaflet.StyleEditor.min.css">
    <!-- Impresora leaflet -->
    <script src="https://cdn.jsdelivr.net/npm/leaflet-easyprint@2.1.9/dist/bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/leaflet-easyprint@2.1.9/libs/leaflet.min.css" rel="stylesheet">
    <!-- Markers Leaflet -->
    <script src="{{ asset('markers-leaflet/js/leaflet-color-markers.js') }}"></script>


</head>
