<!-- Main Footer -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b data-toggle="tooltip" title="Build: Abril 3">Version</b> 1.0.4
    </div>
    <strong><b style="font-family: 'Apple Color Emoji';">Universidad Popular de Misiones</b> | @php echo date("Y"); @endphp © <b style="color: #721c24">Colorado Trail Tech</b>  <b style="margin-bottom: 5%">🥾</b> </strong>
</footer>
