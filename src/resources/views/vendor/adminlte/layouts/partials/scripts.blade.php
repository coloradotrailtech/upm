<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{ asset('/js/app.js') }}" type="text/javascript"></script>

<!-- Bootstrap-select -->
<script src="{{ asset('plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-select/i18n/defaults-es_ES.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('plugins/datatables-cloud/js/spanish.traslate.js') }}"></script>
<script src="{{ asset('plugins/datatables-cloud/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables-cloud/js/dataTables.responsive.min.js') }}"></script>

<!-- Bootstrap-toggle -->
<script src="{{ asset('plugins/bootstrap-toggle/bootstrap-toggle.min.js') }}"></script>

<!-- ICheck -->
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>

<!-- Sweet Alert -->
<script src="{{ asset('plugins/sweetalert/sweetalert2.min.js') }}"></script>

<!-- datepicker -->
<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.min.js') }}"></script>
<script charset="UTF-8"  src="{{ asset('plugins/datepicker/locales/bootstrap-datepicker.es.js') }}"></script>

<!-- date-range-picker -->
<script src="{{ asset('plugins/daterangepicker/es.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- Croppie -->
<script src="{{ asset('plugins/croppie/js/deploy.js') }}"></script>
<script src="{{ asset('plugins/croppie/js/croppie.min.js') }}"></script>

<!-- Wow -->
<script src="{{ asset('js/wow.min.js') }}"></script>
<script>
    new WOW().init();
</script>

<!-- Notificaciones -->
<script src="{{ asset('js/admin/notificaciones.js') }}"></script>


@yield('scripts_del_modulo', '')

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
<script>
    window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
    ]) !!}
    ;
</script>

<script src="{{asset('plugins/Form-ValidityJS/js/jquery.validity.min.js')}}"></script>
<script src="{{asset('plugins/Form-ValidityJS/js/script.js')}}"></script>

<script src="{{asset('plugins/datatables/dataTables.checkboxes.min.js')}}"></script>


<script charset="UTF-8"  src="{{ asset('plugins/pdfmake/build/pdfmake.min.js') }}"></script>

<!-- Exportación -->
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"> </script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"> </script>
<script src="https://unpkg.com/xlsx@0.17.2/dist/xlsx.full.min.js"></script>
<script src="https://unpkg.com/file-saverjs@latest/FileSaver.min.js"></script>
<script src="https://unpkg.com/tableexport@latest/dist/js/tableexport.min.js"></script>
<!-- /Exportación -->