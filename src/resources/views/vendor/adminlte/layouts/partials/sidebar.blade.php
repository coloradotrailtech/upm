<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('img/usuarios/').'/'.Auth::user()->imagen }}" class="img-circle" alt="" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{-- trans('adminlte_lang::message.online') --}}{{auth()->user()->rol->nombre}}</a>
            </div>
        </div>
        @endif        

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header"></li>       
            <!-- Parámetros Generales -->
            @if(Auth::user()->isAdmin())
                <li id="side-general-li" class="treeview">
                    <a href="#">
                        <i class="fa fa-cogs" aria-hidden="true"></i>
                        <span>Parametros Generales</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul id="side-general-ul" class="treeview-menu">
                        <!-- Cursos -->
                        <li id="side-ele-users"><a href="/usuarios"><i class="fa fa-users"></i><span> Usuarios</span></a></li>
                        <li id="side-ele-tipomaterial"><a href="/tiposmateriales"><i class="fa fa-cubes"></i><span> Tipos Materiales</span></a></li>
                        <li id="side-ele-lugares-localidades"><a href="/localidades"><i class="fa fa-circle-o"></i> Localidades</a></li>
                    </ul>
                </li>                                        
                
            @endif


            @if(Auth::user()->isAdmin() == true )
                <!-- Sedes -->
                <li id="side-ele-sedes">
                    <a href="/sedes"><i class="fa fa-institution"></i>
                    <span> Sedes</span></a>
                </li>
                <!-- Encargados -->
                <li id="side-ele-encargados">
                    <a href="/encargados"><i class="fa fa-id-badge"></i>
                    <span> Encargados</span></a>
                </li>
            @endif

            @if(!auth()->user()->isEncargadoSede() && !auth()->user()->isBedel() && !auth()->user()->isEncargadoTemporal() )
            <!-- Depósitos -->
            <li id="side-ele-depositos"><a href="/depositos"><i class="fa fa-cubes"></i><span> 
                Depósitos</span></a>
            </li>

            <!-- Materiales -->
            <li id="side-ele-materiales"><a href="/materiales"><i class="fa fa-flask"></i><span> 
                Materiales</span></a>
            </li>
            @endif
            <!-- Títulos -->
            <li id="side-ele-titulos"><a href="/titulos" target="_blank"><i class="fa fa-list-alt" aria-hidden="true"></i><span> 
                Oficios | Planificaciones</span></a>
            </li>

            @if(Auth::user()->isEncargadoDeposito() != true )
            <!-- Profesores -->
            <li id="side-ele-profesores"><a href="/profesores"><i class="fa fa-graduation-cap"></i><span> Profesores</span></a></li>
            <!-- Alumnos -->
            <li id="side-ele-estudiantes"><a href="/estudiantes" target="_blank" rel="noopener noreferrer"><i class="fa fa-users"></i><span> Estudiantes</span></a></li>
            @endif

            <!-- Cursos -->
            @if(Auth::user()->isEncargadoDeposito() != true )
                <li id="side-ele-cursos">
                    <a href="#">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                        <span>Cursos</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    
                    <ul id="side-general-ul" class="treeview-menu">
                        <li>
                            <a href="/cursos"><i class="fa fa-graduation-cap"></i><span> 
                                Lista cursos</span>
                            </a>
                        </li>
                        
                        <li id="side-ele-users">
                            <a href="/cursos/alumnado"><i class="fa fa-users"></i><span> 
                                Alumnos por oficio</span>
                            </a>
                        </li>
                    </ul>
                </li>
            @endif

            <!-- Estadisticas -->
            @if(Auth::user()->isAdmin())
            <li id="side-ele-cursos">
                <a href="#">
                    <i class="fa fa-pie-chart" aria-hidden="true"></i>
                    <span>Estadísticas</span>
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                
                <ul id="side-general-ul" class="treeview-menu">
                    
                    <li id="side-ele-users">
                        <a href="/cursos/alumnado"><i class="fa fa-bar-chart" aria-hidden="true"></i><span> 
                            Alumnos por oficio</span>
                        </a>
                    </li>
                    
                    <li id="side-ele-users">
                        <a href="/cursos/localidades"><i class="fa fa-map-o" aria-hidden="true"></i><span> 
                            Localidades</span>
                        </a>
                    </li>        

                    <li id="side-ele-users">
                        <a href="/estudiantes/estadistica"><i class="fa fa-users" aria-hidden="true"></i><span> 
                            Estudiantes</span>
                        </a>
                    </li>  


                    <!--
                    <li id="side-ele-users">
                        <a href="/resenas"><i class="fa fa-users" aria-hidden="true"></i><span> 
                            Reseñas cursos</span>
                        </a>
                    </li>  
                    -->
                </ul>
            </li>
            @endif

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
