  <!-- Logo -->
  <a href="{{ url('/home') }}" class="logo animated pulse">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>UPM</b></span>
      <!-- logo for regular state and mobile devices -->
      <img src="{{asset('img/letras.png')}}{{-- Gravatar::get($user->email) --}}" class="user-image" alt="UPM"
          style="max-width: 100%; max-height: 85%;" />
  </a>