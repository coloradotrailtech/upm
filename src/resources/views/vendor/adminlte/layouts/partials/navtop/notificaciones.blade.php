<li class="dropdown notifications-menu" onclick="cambiar_estado()">
    <!-- Menu toggle button -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-bell-o"></i>
        @if ($notificaciones->count() > 0)
        <span class="label label-warning">{{$notificaciones->count()}}</span>
        @endif
    </a>
    <ul class="dropdown-menu">
        @if ($notificaciones->count() > 0)
        <li class="header">Tienes <b>{{$notificaciones->count()}}</b> notificaciones sin leer</li>

        @foreach($notificaciones as $notificacion)
        @if(!$notificacion->ocultar && !$notificacion->estado_leido)
        <li>
            <!-- Inner Menu: contains the notifications -->
            <ul class="menu">
                <li>
                    <!-- start notification -->
                    <a href="/notificaciones">
                    {!! $notificacion->mensaje !!}
                    </a>
                </li><!-- end notification -->
            </ul>
        </li>
        @endif
        @endforeach

        @else
        <li class="header">No tienes notificaciones nuevas</li>
        @endif       
        <li class="footer"><a data-toggle="tooltip" data-placement="bottom"  title="ir al historial de notificaciones" href="/notificaciones">ver todas</a></li>  
    </ul>
</li>