<!-- Main Header -->
<header class="main-header">

@include('adminlte::layouts.partials.navtop.logo')

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                
                <!-- Messages: style can be found in dropdown.less-->
                {{--@include('vendor.adminlte.layouts.partials.navtop.mensajes')--}}
                <!-- /.messages-menu -->
               
                <!-- Notifications Menu -->
                @include('vendor.adminlte.layouts.partials.navtop.notificaciones')
                         
                <!-- Tasks Menu -->
                {{--@include('vendor.adminlte.layouts.partials.navtop.tareas')--}}
                
                <!-- User Menu -->
                @include('vendor.adminlte.layouts.partials.navtop.menu_user')
                
                <!-- Control Sidebar Toggle Button -->
               {{-- @include('vendor.adminlte.layouts.partials.navtop.configuraciones') --}} 

            </ul>
        </div>
    </nav>
</header>
