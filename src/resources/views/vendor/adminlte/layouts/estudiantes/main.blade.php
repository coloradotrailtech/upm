@extends('adminlte::layouts.app')

@section('htmlheader_title')
Estudiantes
@endsection

@section('contentheader_title')
Estudiantes
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Estudiantes</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> registros de Estudiantes UPM</h3>
                </div>
                <div class="box-body">
                    @include('vendor.adminlte.layouts.partials.msj_acciones')     
                    <table id="datatable-estudiantes" class="table table-striped table-hover" cellspacing="0" width="100%">
                    
                        <thead>
                            <tr>                                                                
                                <th class="text-center">    Apellido    </th>
                                <th class="text-center">    Nombre      </th>
                                <th class="text-center">    Matrícula   </th>
                                <th class="text-center">    DNI         </th>
                                <th class="text-center">    Teléfono    </th>
                                <th class="text-center">    Localidad   </th>
                                <th class="text-center">    Acciones    </th>
                            </tr>
                        </thead>

                        <tbody id="tbody_estudiantes"></tbody>

                        <tfoot>
                            <tr>                  
                                <th class="text-center">    Apellido    </th>
                                <th class="text-center">    Nombre      </th>
                                <th class="text-center">    Mat         </th>
                                <th class="text-center">    DNI         </th>
                                <th class="text-center">    Teléfono    </th>
                                <th class="text-center">    Localidad   </th>
                                <th class="text-center">    Acciones    </th>
                            </tr>
                        </tfoot>

                    </table>                                                                       
                </div>
            </div>

        </div>
    </div>
</div>

@endsection

@section('scripts_del_modulo')
<script type="text/javascript">
    const estudiantes = {!! json_encode($estudiantes) !!};
    const tbody_estudiantes = document.getElementById('tbody_estudiantes')    

    estudiantes.forEach(estu => {
        let tr_estu = document.createElement('tr')
        
        let td_estu = `
            <tr>
                <td class='text-uppercase' ><strong> ${estu.apellido} </strong></td>
                <td class='text-uppercase' ><strong> ${estu.nombre} </strong></td>
                <td class="text-uppercase" ><strong>  ${estu.id} </strong></td>
                <td > ${estu.dni} </td>
                <td > ${estu.telefono} </td>
                <td > ${estu.localidad} </td>

                <td class="text-center" width="15%" >
                    <a href="/estudiantes/${estu.id}" data-toggle="tooltip"
                        title="Visualizar legajo e historial estudiante"
                        class="btn btn-sm btn-social-icon btn-info "
                        target='_blank' rel='noopener'>
                        <i class="fa fa-eye fa-lg"></i>
                    </a>
                </td>
            </tr>
        `
        tr_estu.innerHTML = td_estu
        tbody_estudiantes.appendChild(tr_estu)
    })

</script>
<script src="{{ asset('js/menu.js') }}"></script>
<script src="{{ asset('js/admin/estudiante.js') }}"></script>
<script src="{{ asset('js/admin/curso/tablas.js') }}"></script>
@endsection
