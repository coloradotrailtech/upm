<div class="modal fade" id="modal-update">
    <div class="modal-dialog" style="width: 60%">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h3 class="modal-title text-center">Formulario de estudiante:</h3>
            </div>
            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')


                <form id="form-update" action="" method="POST" class="form validity">
                    <input name="_method" type="hidden" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                    <legend style="margin-top: 2%"><em>Datos Personales</em></legend>


                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Apellido:</label>
                                <input name="apellido" id="apellido" type="text" maxlength="50"
                                    class="form-control text-bold" placeholder="campo requerido" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input name="nombre" id="nombre" type="text" maxlength="50"
                                    class="form-control text-bold" placeholder="campo requerido" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Fecha Nac.:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="date" name="fecha_nac" id="fecha_nac_edit"
                                        class="form-control pull-right" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Sexo:</label>
                                <select name="sexo" id="sexo" placeholder="campo no obligatorio"
                                    data-header="Seleccione.." class="selectpicker form-control">
                                    <option value="Masculino">Masculino</option>
                                    <option value="Femenino">Femenino</option>
                                </select>
                            </div>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>DNI:</label>
                                <input name="dni" id="dni" type="number" maxlength="8" class="form-control"
                                    placeholder="ej: 34478385" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>CUIL:</label>
                                <input name="cuil" id="cuil" type="number" maxlength="11" class="form-control"
                                    placeholder="ej: 20344783854">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Nacionalidad:</label>
                                <select name="pais_id" id="pais_id" placeholder="campo obligatorio"
                                    data-header="Seleccione país" class="selectpicker form-control"
                                    data-style="btn-primary">
                                    @foreach($paises as $pais)
                                    <option value="{{$pais->id}}">{{$pais->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Estado Civil:</label>
                                <select name="estado_civil" id="estado_civil" placeholder="campo obligatorio"
                                    data-header="Seleccione estado civil del estudiante"
                                    class="selectpicker form-control" data-style="btn-primary">
                                    <option value="soltero">Soltero/a</option>
                                    <option value="casado">Casado/a</option>
                                    <option value="divorciado">Divorciado/a</option>
                                    <option value="viudo">Viudo/a</option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Localidad:</label>
                                <select name="localidad_id" id="localidad_id" placeholder="campo requerido"
                                    data-header="Selecciona localidad de residencia" class="selectpicker form-control"
                                    data-live-search="true">
                                    @foreach($localidades as $localidad)
                                    <option value="{{$localidad->id}}"
                                        data-subtext="({{$localidad->provincia->nombre}})">{{$localidad->nombre}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Domicilio:</label>
                                <input name="direccion" id="direccion" type="text" maxlength="50" class="form-control"
                                    placeholder="ej: 9 de Julio 1266" required>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Grupo Sanguineo:</label>
                                <select name="sangre" id="sangre" placeholder="campo obligatorio"
                                    data-header="Seleccione tipo de sangre" class="selectpicker form-control"
                                    data-style="btn-danger">
                                    <option value="ab+">AB+</option>
                                    <option value="ab-">AB-</option>
                                    <option value="a+">A+</option>
                                    <option value="a-">A-</option>
                                    <option value="b+">B+</option>
                                    <option value="b-">B-</option>
                                    <option value="0+">0+</option>
                                    <option value="0-">0-</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>Estudios Cursados:</label>
                            <input type="text" name="myEducacion" id="myEducacion">
                            <select name="estudios" id='estudios' placeholder="campo obligatorio" data-header="Seleccionar"
                                class="selectpicker form-control" data-live-search="false">
                                <option value="null">Selccionar</option>
                                <option value="Primaria Incompleta">Primaria Incompleta</option>
                                <option value="Primaria Completa">Primaria Completa</option>
                                <option value="Secundaria Incompleta">Secundaria Incompleta</option>
                                <option value="Secundaria Completa">Secundaria Completa</option>
                                <option value="Terciario Incompleto ">Terciario Incompleto &nbsp;</option>
                                <option value="Terciario Completo">Terciario Completo&nbsp;</option>
                                <option value="Universitario Incompleto">Universitario Incompleto&nbsp;</option>
                                <option value="Universitario Completo">Universitario Completo&nbsp;</option>
                            </select>
                        </div>
                        
                    </div>

                    <legend style="margin-top: 5%"><em>Información de Contacto</em></legend>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Email:</label>
                                <input name="email" id="email" type="email" maxlength="50" class="form-control"
                                    placeholder="campo opcional">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Celular:</label>
                                <input name="telefono" id="telefono" type="text" maxlength="15" class="form-control"
                                    placeholder=" ej 3764111222">
                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <label>Teléfono fijo:</label>
                                <input name="telefono2" id="telefono2" type="text" maxlength="15" class="form-control" placeholder="">
                            </div>
                        </div>

                    </div>


                    <div class="row" style="margin-top: 1%; font-size: 150%">
                        <div class="col-md-3">
                            <div title="Seleccione la mano que utiliza el estudiante para escribir/trabjar"
                                style="margin-bottom: 5%;">
                                <label><strong>Posee alguna discapacidad:</strong></label>
                                <input type="checkbox" name="discapacidad" id="discapacidad" checked
                                    data-toggle="toggle" data-on="Si" data-on-label="1" data-off="No" data-off-label="0"
                                    data-onstyle="warning" data-offstyle="success">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div title="Seleccione la mano que utiliza el estudiante para escribir/trabjar"
                                style="margin-bottom: 5%;">
                                <label><strong>Carnet Discapacidad:</strong></label>
                                <input type="checkbox" name="carnet_discapacidad" id="carnet_discapacidad" checked
                                    data-toggle="toggle" data-on="Si" data-off-label="1" data-off="No"
                                    data-off-label="0" data-onstyle="info" data-offstyle="success">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div title="Seleccione la mano que utiliza el estudiante para escribir/trabjar"
                                style="margin-bottom: 5%;">
                                <label style="padding-right: 70px"><strong>Mano <br>habil:</strong></label>
                                <input type="checkbox" name="mano_habil" id="mano_habil" checked data-toggle="toggle"
                                    data-on="derecha" data-off="izquierda" data-onstyle="info" data-offstyle="success"
                                    data-width="90">
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 2%; font-size:20px">
                        <div class="col-md-8" style="margin-left: 2%">
                            <div class="row">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="trabaja-estudiante" name="trabaja">
                                    <label class="form-check-label" for="trabaja"><label>Trabaja:</label></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Rubro de Trabajo:</label>
                                <input name="rubro_trabaja" id="rubro_trabaja_estudiante" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Descripción Adicional:</label>
                                <input name="descripcion" id="descripcion_estudiante" class="form-control" maxlength="200"
                                   placeholder="campo opcional (max 200 caracteres)">
                            </div>
                        </div>
                    </div>
                    <br>
                    <button id="boton_submit_update" type="submit" class="btn btn-primary hide"></button>
                </form>
                <br>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn btn-warning" onclick="$('#boton_submit_update').click()">Actualizar Datos</button>
            </div>

        </div>
    </div>
</div>
