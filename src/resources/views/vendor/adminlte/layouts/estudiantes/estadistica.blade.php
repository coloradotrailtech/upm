@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Estadísticas de Estudiantes
@endsection

@section('contentheader_title')
Estadísticas de Estudiantes
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Estadísticas de Estudiantes </a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">

                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Estadísticas de los Estudiantes </h3>
                </div>

                <div class="box-body ">

                    <div>
                        <div><h4> Según género </h4></div>                       

                        <br>

                        <table class="table table-hover" id='table-curso' cellspacing="0" width='100%' style='margin-top: 20px;margin-bottom:0px;'>
                            <thead>
                                <tr>
                                    <th class="text-center"> Cantidad Matriculados  </th>
                                    <th class="text-center"> Masculino              </th>
                                    <th class="text-center"> Femenino               </th>
                                </tr>    
                            </thead>
                                <tr>
                                    <td class="text-center"> {{ $cont_sex['tot']  }} </td>
                                    <td class="text-center"> {{ $cont_sex['mas']  }} => {{ $cont_sex['p_mas'] }}% </td>
                                    <td class="text-center"> {{ $cont_sex['fem']  }} => {{ $cont_sex['p_fem'] }}% </td>
                                </tr>
                            <tbody>

                            </tbody>
                        </table>

                        <br>

                        <div id='div-chart'>
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>  

                    <hr>

                    <div>
                        <div><h4> Trabaja </h4></div>
                        <br>

                        <table class="table table-hover" id='table-curso' cellspacing="0" width='100%' style='margin-top: 20px;margin-bottom:0px;'>
                            <thead>
                                <tr>
                                    <th class="text-center"> Trabaja  </th>
                                    <th class="text-center"> No Trabaja              </th>
                                </tr>    
                            </thead>
                                <tr>
                                    <td class="text-center"> {{ $cont_trab['trabaja']  }} => {{ $cont_trab['p_trab'] }}% </td>
                                    <td class="text-center"> {{ $cont_trab['no_trabaja']  }} => {{ $cont_trab['p_no_trab'] }}% </td>
                                </tr>
                            <tbody>

                            </tbody>
                        </table>

                        <br>

                        <div id='div-chart'>
                            <canvas id="myChart_trabaja"></canvas>
                        </div>

                    </div>

                    <hr>

                    <div>

                        <div><h4> Según edades </h4></div>

                        <br>
                        
                        <table class="table table-hover" id='table-curso' cellspacing="0" width='80%' style='margin-bottom:0px;'>
                            <thead>
                                <tr>
                                    <th class="text-center"> < 20           </th>
                                    <th class="text-center"> Entre 20 y 30  </th>
                                    <th class="text-center"> Entre 30 y 40  </th>
                                    <th class="text-center"> Entre 40 y 50  </th>
                                    <th class="text-center"> Entre 50 y 60  </th>
                                    <th class="text-center"> Entre 60 y 70  </th>
                                    <th class="text-center"> Entre 70 y 80  </th>
                                    <th class="text-center"> > 80  </th>
                                </tr>    
                            </thead>
                                
                            <tbody>
                                <tr>
                                    <td class="text-center"> {{ $data_edades['cont_10'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_20'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_30'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_40'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_50'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_60'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_70'] }} </td>
                                    <td class="text-center"> {{ $data_edades['cont_80'] }} </td>
                                </tr>
                            </tbody>
                        </table>

                        <br>

                        <div id='div-chartBar'>
                            <canvas id="myChart_bar"></canvas>
                        </div>                       

                    </div>                    
                </div>   
                
                <div class="box-footer">
                    <button type="button" class="btn btn-info" data-toggle='modal' data-target='#rango_etario'>
                        Exportar por rango etario
                    </button>
                    <button type="button" class="btn btn-info" data-toggle='modal' data-target='#por_categoria'>
                        Exportar por Categoria
                    </button>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal Rango Etario -->
<div class="modal fade" id="rango_etario" tabindex="-1" role="dialog" aria-labelledby="modal_etario" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modal_etario">Exportar info por rango etario de alumno</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <form action="/estudiantes/export_rango_etario" method="get">
      <div class="modal-body">
        <p>
            <span><b>Rango deseado: </b></span>
            <select name="ed_min" id="ed_min" class="selectpicker">
                <option selected>Edad Min</option>
                @for($i=18; $i<=90; $i++)
                <option value="{{$i}}">{{$i}}</option>

                @endfor
            </select>
            <select name="ed_max" id="ed_max" class="selectpicker">
                <option selected>Edad Max</option>
                @for($j=18; $j<=90; $j++)
                <option value="{{$j}}">{{$j}}</option>
                @endfor
            </select>
        </p>
        <p>
            <span><b>Estado de Curso: </b></span>
            <select name="est_deseado" id="" class="selectpicker">
                <option value="todos">      Todos       </option>
                <option value="aprobado">   Aprobados   </option>
                <option value="libre">      Libres      </option>
                <option value="abandono">   Abandonados </option>
            </select>
        </p>
      </div>

      <div class="modal-footer">
        <input type="submit" id='rango_etario' class="btn" title="Exportar alumnos por rango etario">
      </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Exportar por Categoria -->
<div class="modal fade" id="por_categoria" tabindex="-1" role="dialog" aria-labelledby="modal_categoria" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modal_categoria">Exportar info por Categoria de Oficio</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <form action="/estudiantes/export_por_categoria" method="get">
      <div class="modal-body">
        <p>
            <span><b>Categoría deseado: </b></span>
            <select name="por_cat" id="por_cat" class="selectpicker">
                <option value='' selected>Seleccionar categoría</option>
                @foreach($categorias as $cat)
                <option value="{{$cat['id']}}">{{$cat['nombre']}}</option>           
                @endforeach
            </select>
        </p>
        <p>
            <span><b>Estado de Curso: </b></span>
            <select name="est_deseado" id="" class="selectpicker">
                <option value="todos">      Todos       </option>
                <option value="aprobado">   Aprobados   </option>
                <option value="libre">      Libres      </option>
                <option value="abandono">   Abandonados </option>
            </select>
        </p>
      </div>

      <div class="modal-footer">
        <input type="submit" id='rango_etario' class="btn" title="Exportar alumnos por rango etario">
      </div>
      </form>

    </div>
  </div>
</div>
    
@section('scripts_del_modulo')    


<script text='text/javascript'>

    const categorias = @json($categorias);
    console.log(categorias)
    const contadores = @json($cont_sex);
    
    const ctx = document.getElementById('myChart').getContext('2d');
    const myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ['% Masculinos ', '% Femenino '],
            datasets: [{
                label: '',
                data: [ 
                    contadores['p_mas'],
                    contadores['p_fem']
                ],
                backgroundColor: [
                    'rgba(98, 167, 175, 0.6)',
                    'rgba(248, 188, 179, 0.9)'
                ],
                borderColor: [                    
                    'rgb(149, 185, 244)',
                    'rgb(228, 146, 148)'
                ]
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: false,
                    display: false
                },
                x: {
                    beginAtZero: false,
                    display: false
                }
            }
        }
    });



    const data_edades = @json($data_edades);
    const ctx_bar = document.getElementById('myChart_bar').getContext('2d');
    const myChart_bar = new Chart(ctx_bar, {
        type: 'bar',
        data: {
            labels: ['< 20', 'Entre 20 y 30', 'Entre 30 y 40', 'Entre 40 y 50', 
                'Entre 50 y 60', 'Entre 60 y 70', 'Entre 70 y 80', '> 80'],
            datasets: [{
                label: 'Cantidad',
                data: [ 
                    data_edades['cont_10'],
                    data_edades['cont_20'],
                    data_edades['cont_30'],
                    data_edades['cont_40'],
                    data_edades['cont_50'],
                    data_edades['cont_60'],
                    data_edades['cont_70'],
                    data_edades['cont_80'],
                ],
                backgroundColor: [
                    'rgba(185, 207, 189, 1)'
                ]
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    display: true
                },
                x: {
                    beginAtZero: false,
                    display: true
                }
            }
        }
    });

    
    const cont_trab = @json($cont_trab);
    console.log(cont_trab)
    const ctx_trabaja = document.getElementById('myChart_trabaja').getContext('2d');    
    const myChart_trab = new Chart(ctx_trabaja, {
        type: 'pie',
        data: {
            labels: ['% Trabaja ', '% No Trabaja '],
            datasets: [{
                label: '',
                data: [ 
                    cont_trab['p_trab'],
                    cont_trab['p_no_trab']
                ],
                backgroundColor: [
                    '#ebddc4',
                    '#cabeb3'
                ],
                borderColor: [                    
                    '#ecc9ab',
                    '#857b70'
                ]
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: false,
                    display: false
                },
                x: {
                    beginAtZero: false,
                    display: false
                }
            }
        }
    });
    


</script>


<script src="{{ asset('js/menu.js') }}"></script>
@endsection
@endsection