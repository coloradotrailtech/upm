
@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Estudiantes
@endsection

@section('contentheader_title')
Estudiantes
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Legajo de Estudiante UPM</h3>
                </div>
                <div class="box-body">  
                    <h4><strong>Datos Personales</strong> </h4>
                    <table class="table table-striped table-hover" cellspacing="0" width="100%">
                    
                        <thead>
                            <tr>
                                <th> ID             </th>
                                <th> Apellido       </th>
                                <th> Nombre         </th>
                                <th> DNI            </th>
                                <th> Fecha Nac      </th>
                                <th> Sangre         </th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td> {{ $estudiante->id          }} </td>
                                <td class='text-uppercase'> {{ $persona->apellido }}</td>
                                <td class='text-uppercase'> {{ $persona->nombre   }}</td>
                                <td> {{ $persona->dni           }} </td>
                                <td> {{ $persona->fecha_nac->format('d/m/Y') }}</td>
                                <td> {{ $persona->sangre }}</td>
                            </tr>
                        </tbody>

                        <thead>
                            <tr>
                                <th> Estado Civil   </th>
                                <th> Estudios       </th>
                                <th> Mano habil     </th>
                                <th> Trabaja        </th>
                                <th> Rubro          </th>
                                <th> Discapacidad   </th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> {{ $persona->estado_civil  }} </td>
                                <td> {{ $persona->educacion     }} </td>
                                <td> {{ $persona->mano_habil            }} </td>
                                @if($estudiante->trabaja==0)
                                    <td> No </td>
                                    <td> - </td>
                                @elseif($estudiante->trabaja==1)
                                    <td> Si </td>
                                    <td> {{ $estudiante->rubro_trabaja      }} </td>
                                @endif                               
                                
                                @if($persona->discapacidad==1)
                                    <td> Si 
                                        @if($persona->carnet_discapacidad==1)
                                        - Con carnet
                                        @else
                                        - Sin carnet
                                        @endif
                                    </td>
                                @else
                                    <td>No</td>
                                @endif
                            </tr>
                        </tbody>

                    </table>     
                    <br>
                    <h4><strong>Información de Contacto</strong></h4>
                    <table class="table table-striped table-hover" cellspacing="0" width="100%">
                    
                        <thead>
                            <tr>
                                <th> Email       </th>
                                <th> Celular     </th>
                                <th> Tel.Fijo    </th>
                                <th> Domicilio   </th>
                                <th> Localidad   </th>

                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td> {{ $persona->email     }} </td>
                                <td> {{ $persona->telefono  }} </td>
                                <td> {{ $persona->telefono2 }} </td>
                                <td> {{ $persona->direccion }} </td>
                                <td> {{ $localidad          }} </td>
                            </tr>
                        </tbody>


                    </table> 

                    <hr color="black;" style='border-top: 1px solid #738493;'/>
                    <h4><strong>Cursos Realizados</strong></h4>
                    <h4>
                        Total inscripciones: {{ $contadores['total'] }} | 
                        Aprobados:   {{ $contadores['aprobado']   }} |
                        Abandonados: {{ $contadores['abandono']   }} | 
                        Libre:       {{ $contadores['libre']      }} |
                        Sin estado:  {{ $contadores['sin_estado'] }}                   
                    </h4>
                    <table class="table table-striped table-hover" cellspacing="0" width="100%">
                    
                        <thead>
                            <tr>
                                <th> N°     </th>
                                <th> Curso  </th>
                                <th> Sede   </th>
                                <th> Plan   </th>
                                <th> Inicio </th>
                                <th> Fin    </th>
                                <th> Estado </th>
                            </tr>
                        </thead>


                        <tbody>
                            @foreach($cursos as $curso)
                            <tr>                           
                                <td> {{ $curso['curso']        }} </td>   
                                <td> {{ $curso['titulo']       }} </td>
                                <td> {{ $curso['sede']         }} </td>    
                                <td> {{ $curso['plan']         }} </td>    
                                <td> {{ $curso['fecha_inicio']->format('d/m/Y') }} </td> 
                                <td> {{ $curso['fecha_fin']->format('d/m/Y')    }} </td>  
                                <td> {{ $curso['estado_curso'] }} </td>
                            </tr>
                            @endforeach

                        </tbody>

                        <tfoot>
                            <tr>

                            </tr>
                        </tfoot>
                    </table>
                    
                </div> 
                <div class="box-footer">
                    <a onclick="completar_campos( {{$estudiante->id}},{{$estudiante->persona}},{{$estudiante}} )" title="Editar este registro" class="btn btn-sm btn-social-icon btn-warning ">
                        <i class="fa fa-pencil"></i>
                    </a>

                    <a onclick="abrir_modal_borrar({{$estudiante->id}})" title="Eliminar este registro" class="btn btn-sm btn-social-icon btn-sm btn-danger">
                        <i class="fa fa-trash"></i></a>
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>
@include('vendor.adminlte.layouts.estudiantes.formulario.editar')
@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/estudiante.js') }}"></script>
<script src="{{ asset('js/menu.js') }}"></script>
@endsection






