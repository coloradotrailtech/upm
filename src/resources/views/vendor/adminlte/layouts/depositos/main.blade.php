
@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Depósitos
@endsection

@section('contentheader_title')
Depósitos
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-cubes"></i> Depósitos</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Registros</h3>
                </div>
                <div class="box-body ">   
                    @include('vendor.adminlte.layouts.partials.msj_acciones')
                    <table id="datatable-depositos" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center">N°</th>
                                <th class="text-center">Dirección</th>
                                <th class="text-center">Localidad</th>
                                <th class="text-center">Sede</th>
                                <th class="text-center">Descripción</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($depositos as $deposito)
                            <tr>
                                <td class="text-center text-bold">{{$deposito->id}}</td>
                                <td class="text-center text-bold">{{$deposito->direccion}}</td>
                                <td class="text-center">{{$deposito->localidad->nombre}}</td>

                                @if($deposito->sede)
                                <td class="text-center">{{$deposito->sede->localidad->nombre}} {{$deposito->sede->direccion}}</td>
                                @else
                                <td class="text-center">-</td>
                                @endif
                                
                                                            
                                <td class="text-center">{{$deposito->descripcion}}</td>
                                <td class="text-center">
                                    <a href="{{ route('depositos.show', $deposito->id) }}" data-toggle="tooltip" title="Visualizar el detalle de este registro" class="btn btn-social-icon btn-info btn-sm"><i class="fa fa-eye fa-lg"></i></a>
                                    @if(Auth::user()->isAdmin() || Auth::user()->isEncargadoDeposito())
                                    <a onclick="completar_campos({{$deposito}})" data-toggle="tooltip" title="Editar este registro" class="btn btn-social-icon btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                    <a onclick="abrir_modal_borrar({{$deposito->id}})" data-toggle="tooltip" title="Eliminar este registro" class="btn btn-social-icon btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr> 
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                            <th class="text-center">N°</th>
                                <th class="text-center">Dirección</th>
                                <th class="text-center">Localidad</th>
                                <th class="text-center">Sede</th>
                                <th class="text-center">Descripción</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </tfoot>
                    </table>
                </div> 
                <div class="box-footer">
                    <button title="Registrar nuevo deposito" type="button" id="boton-modal-crear" class="btn btn-primary pull-right" data-toggle="modal" data-toggle="tooltip" data-target="#modal-crear">
                        <i class="fa fa-plus-circle"></i> &nbsp;registrar depósito
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>

@include('vendor.adminlte.layouts.depositos.formulario.create')
@include('vendor.adminlte.layouts.depositos.formulario.editar')
@include('vendor.adminlte.layouts.depositos.formulario.confirmar')

@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/deposito.js') }}"></script>
@endsection







