
<div class="box box-primary animated fadeIn">
    {{--
    <div class="box-header with-border">
        <i class="fa fa-list" aria-hidden="true"></i>
        <h3 class="box-title"> Transferencia de Inventario</h3>
    </div>
    --}}
    <div class="box-body ">
        <table id="datatable-depositos" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="text-center">Material</th>
                <th class="text-center">Stock Depósito origen</th>
                <th class="text-center">Stock Depósito destino</th>
                <th class="text-center">Cantidad a enviar</th>
            </tr>
            </thead>
            <tbody>
            @foreach($materiales_deposito_origen as $material_en_origen)    <!-- $materiales_deposito_origen = MaterialesDeposito. Viende desde DepositosController -->
                <tr>
                    <td class="text-center text-bold">{{$material_en_origen->material->nombre}}</td>
                    <td class="text-center">{{$material_en_origen->stock}} {{$material_en_origen->material->umedida->abreviatura}}</td>
                    <td class="text-center"><span id="span-info-{{$material_en_origen->id}}">
                        {{$deposito_destino->get_stock_material($material_en_origen->material->id)}}
                        {{$material_en_origen->material->umedida->abreviatura}}</span>
                    </td>
                    <td>
                        <input id="input-cantidad-{{$material_en_origen->id}}"
                               type="number"
                               min="0"
                               max="{{$material_en_origen->stock}}"
                               class="form-control"
                               placeholder="0"
                               onkeyup="stock_destino_update(
                                   '{{$material_en_origen->id}}', '{{$material_en_origen->material->id}}',
                                       '{{$material_en_origen->material->nombre}}', '{{$material_en_origen->material->umedida->abreviatura}}',
                                       '{{$deposito_destino->get_stock_material($material_en_origen->material->id)}}',
                                       '{{$material_en_origen->stock}}')"
                               onchange="stock_destino_update(
                                   '{{$material_en_origen->id}}', '{{$material_en_origen->material->id}}',
                                       '{{$material_en_origen->material->nombre}}','{{$material_en_origen->material->umedida->abreviatura}}',
                                       '{{$deposito_destino->get_stock_material($material_en_origen->material->id)}}',
                                       '{{$material_en_origen->stock}}')"
                        >
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th class="text-center">Material</th>
                <th class="text-center">Stock Depósito origen</th>
                <th class="text-center">Stock Depósito destino</th>
                <th class="text-center">Cantidad a enviar</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="col-md-12">
                {{--
                <p class="pull-left form-text text-muted"><strong>Información:</strong> el valor sugerido en algunas leyendas equivale al monto calculado automáticamente por sistema para gastos compartidos entre varios inquilinos de un edificio.</p>
                --}}
                <div class="pull-right">
                    <button onclick="enviar_transferencia({{$deposito_origen->id}},{{$deposito_destino->id}})" title="Registrar impuestos" class="btn btn-primary btn-sm">
                        <i class="fa fa-search" aria-hidden="true"></i> &nbsp;registrar transferencia
                    </button>
                </div>
                <!-- Boton invisible -->
{{--
                <a href="{{ route('contabilidad.verBoleta')}}" target="_blank" title="Ver el detalle de esta boleta / Imprimir" class="btn btn-social-icon btn-sm btn-info">
                    <i class="fa fa-list"></i>
                </a>    --}}          <!-- Boton invisible -->
            </div>
        </div>
    </div>
</div>

<script>
    function enviar_transferencia(id_dep_origen, id_dep_destino) {
        if (lista_update.length > 0) { // si la lista está vacía se informa al usuario al respecto
            var lista = [];
            lista_update.forEach(function (valor) { // se convierte el array en una coleccion de objetos
                lista.push(valor)
            });

            Swal.fire({
                title: 'Atención',
                text: "¿Se encuentra seguro?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2aaddd',
                cancelButtonColor: '#d63f16',
                confirmButtonText: 'confirmar transferencia',
                cancelButtonText: 'cancelar'
            }).then((result) => {
                if (result.value) {
                    console.log(lista);
                    $.ajax({// se envía
                        url: '/transferir_materiales',
                        data: {
                            lista: lista,
                            id_dep_origen:id_dep_origen,
                            id_dep_destino:id_dep_destino
                        },
                        type: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            Swal.fire({
                                title: 'Transferencia efectuada',
                                text: "Se enviaron materiales de Deposito "+id_dep_origen+" a Deposito "+id_dep_destino,
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#2aaddd',
                                cancelButtonColor: '#d63f16',
                                confirmButtonText: 'Bien',
                            }).then((result) => {
                                if (result.value) {





                                }
                            })


                            });


                        }
                    });
                }
            });
        } else {
            Swal.fire({
                type: 'error',
                title: 'Atención',
                text: 'Ninguna cantidad fue actualizada'
            });
        }
    }
</script>
