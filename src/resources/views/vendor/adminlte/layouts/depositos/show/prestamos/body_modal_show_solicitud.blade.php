<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label><strong>Fecha:</strong></label>
            <span class="form-control">{{$solicitudprestamoshow->created_at->format('d/m/Y')}}</span>
        </div>
        <div class="form-group">
            <label><strong>Curso:</strong></label>
            <span class="form-control">
                {{$solicitudprestamoshow->curso->titulo->nombre}}
                {{$solicitudprestamoshow->curso->titulo->plan}}</span>
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <label><strong>Listado de materiales:</strong></label>
            <div style="background-color: #FFFFFF;">
                <div style="padding: 1%;">
                    <table id="datatable-solicitudprestamo-show" class="display" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center">Material</th>
                                <th class="text-center">Cantidad</th>
                                <th class="text-center">Unidad medida</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($solicitudprestamoshow->materialessolicitudprestamo
                            as
                            $materialsolicitudprestamo)
                            <tr>
                                <td>{{$materialsolicitudprestamo->material->nombre}}
                                    ({{$materialsolicitudprestamo->material->tipomaterial->familiamaterial->nombre}}
                                    - {{$materialsolicitudprestamo->material->tipomaterial->nombre}})
                                </td>
                                <td class="text-center">{{$materialsolicitudprestamo->cantidad}}
                                </td>
                                <td class="text-center">
                                    {{$materialsolicitudprestamo->material->umedida->nombre}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>