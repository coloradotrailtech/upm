@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Alta de préstamo
@endsection

@section('contentheader_title')
Alta de préstamo
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-cubes"></i> Depósitos</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Datos sobre la solicitud recibida</h3>
                </div>
                <div class="box-body ">
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Curso solicitante:</strong></label>
                                <span class="form-control"> {{$solicitudprestamo->curso->titulo->nombre}} |
                                    {{$solicitudprestamo->curso->titulo->plan}}</span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><strong>Fecha de inicio y fin:</strong></label>
                                <span class="form-control"> {{$solicitudprestamo->curso->get_rango_fechas()}}</span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><strong>Estado:</strong></label>
                                <span class="form-control"> {{$solicitudprestamo->curso->determinar_estado()}}</span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><strong>Fecha solicitud:</strong></label>
                                <span class="form-control"> {{$solicitudprestamo->created_at->format('d/m/Y')}}</span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><strong>Cant. Alumnos inscriptos:</strong></label>
                                <span class="form-control"> {{$solicitudprestamo->curso->alumnos->count()}}</span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="margin-bottom: 1%"><strong>Materiales solicitados:</strong></label>
                                <div class="row">
                                    @foreach($solicitudprestamo->materialessolicitudprestamo as
                                    $materialsolicitudprestamo)
                                    <div class="col-md-3">
                                        <ul>
                                            <li><b>{{$materialsolicitudprestamo->nombre_material}}</b></li>
                                        </ul>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Formulario de alta de préstamo</h3>
                </div>
                <div class="box-body ">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="margin-bottom: 1%"><strong>Estado solicitud:</strong></label>
                                <select name="estadosolicitudprestamo_id" id="estadosolicitudprestamo_id"
                                    placeholder="campo requerido" class="selectpicker form-control"
                                    data-live-search="true">
                                    @foreach($estadossolicitudes as $estado)

                                    @if($estado->id > 1)
                                    <option value="{{$estado->id}}">{{$estado->nombre}}</option>
                                    @endif

                                    @endforeach
                                </select>
                            </div>
                            <label style="margin-bottom: 1%, margin-top: 1%"><strong>Materiales en
                                    depósito:</strong></label>
                            <table id="datatable-depositos" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">Material</th>
                                        <th class="text-center">Stock actual/actualizado</th>
                                        <th class="text-center">Cantidad a transferir</th>
                                        <th class="text-center">Observación</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($solicitudprestamo->deposito->materialesdeposito as $material)
                                    <tr>
                                        <td class="text-center text-bold">{{$material->material->nombre}}
                                        </td>
                                        <td class="text-center"><span
                                                id="span-info-{{$material->material->id}}">{{$material->stock}}
                                                {{$material->material->umedida->abreviatura}} / {{$material->stock}}
                                                {{$material->material->umedida->abreviatura}}
                                            </span></td>

                                     
                                        <td>

                                            <input id="input-cantidad-{{$material->material->id}}" type="number" min="0"
                                                max="{{$material->stock}}" class="form-control input_aceptado"
                                                placeholder="ingrese una cantidad..."
                                                onkeyup="carga_lista('{{$material->material->id}}', '{{$material->id}}',  '{{$material->material->umedida->abreviatura}}', '{{$material->stock}}')"
                                                onchange="carga_lista('{{$material->material->id}}', '{{$material->id}}',  '{{$material->material->umedida->abreviatura}}', '{{$material->stock}}')">
                                        </td>

                                        <td>
                                            <input id="input-observacion-{{$material->material->id}}" type="text"
                                                maxlength="500" class="form-control input_aceptado" disabled
                                                placeholder="campo opcional"
                                                onkeyup="carga_lista('{{$material->material->id}}', '{{$material->id}}',  '{{$material->material->umedida->abreviatura}}', '{{$material->stock}}')"
                                                onchange="carga_lista('{{$material->material->id}}', '{{$material->id}}',  '{{$material->material->umedida->abreviatura}}', '{{$material->stock}}')">
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="text-center">Material</th>
                                        <th class="text-center">Stock actual/actualizado</th>
                                        <th class="text-center">Cantidad a transferir</th>
                                        <th class="text-center">Observación</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="{{ route('depositos.show', $solicitudprestamo->deposito->id) }}" data-toggle="tooltip"
                        title="Volver a la pantalla anterior" class="btn btn-default btn-sm"><i
                            class="fa fa-lg fa-arrow-left"></i> volver</a>
                    <div class="pull-right">
                        <a onclick="mandar_lista_materiales('{{$solicitudprestamo->deposito->id}}', '{{$solicitudprestamo->id}}')"
                            data-toggle="tooltip"
                            title="Proceder a registrar la transferencia de recursos del depósito al curso solicitante"
                            class="btn btn-primary btn-sm">
                            <i class="fa fa-lg fa-pencil"></i>
                            registrar préstamo
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/deposito/alta_prestamo.js') }}"></script>
@endsection