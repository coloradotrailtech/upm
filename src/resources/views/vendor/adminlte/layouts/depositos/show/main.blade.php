@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Detalle depósito
@endsection

@section('contentheader_title')
Depósito {{$deposito->id}} ({{$deposito->direccion}}, {{$deposito->localidad->nombre}})
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-cubes"></i> Depósitos</a></li>
@endsection

@section('main-content')



<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <input class="hide" value="{{$deposito->id}}" id="deposito_id">
            @include('vendor.adminlte.layouts.partials.msj_acciones')
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab" aria-expanded="true">
                            <i class="fa fa-list-ul fa-lg" aria-hidden="true"></i>
                            &nbsp;&nbsp;Detalle del registro y listado de stock
                        </a>
                    </li>
                    <li class="">
                        <a href="#tab_2" data-toggle="tab" aria-expanded="false">
                            <i class="fa fa-lg fa-cubes" aria-hidden="true"></i>
                            &nbsp;&nbsp;Préstamos
                        </a>
                    </li>
                    <li class="">
                        <a href="#tab_3" data-toggle="tab" aria-expanded="false">
                            <i class="fa fa-lg fa-list-alt" aria-hidden="true"></i>
                            &nbsp;&nbsp;  Solicitudes&nbsp;  
                            @if($deposito->solicitudespendientes->count()>0)
                            <span class="label label-warning pull-right"><i
                                    class="icon fa fa-lg fa-exclamation-circle"></i></span>
                            @endif
                        </a>
                        
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        @include('vendor.adminlte.layouts.depositos.show.info_general.main')
                    </div>
                    <div class="tab-pane" id="tab_2">
                    @include('vendor.adminlte.layouts.depositos.show.prestamos.main')
                    </div>
                    <div class="tab-pane" id="tab_3">
                        @include('vendor.adminlte.layouts.depositos.show.solicitudes.main')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






@include('vendor.adminlte.layouts.depositos.show.solicitudes.modal_show_prestamo')
@include('vendor.adminlte.layouts.depositos.show.prestamos.modal_show_solicitud')
@include('vendor.adminlte.layouts.depositos.formulario.editar')
@include('vendor.adminlte.layouts.depositos.formulario.confirmar')

@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/deposito.js') }}"></script>
@endsection