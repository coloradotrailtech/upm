<div class="row">
    <div class="col-md-12">
        <label style="margin-bottom: 1%;"><strong>Listado de materiales:</strong></label>
        <table id="datatable-depositos" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th class="text-center">Material</th>
                    <th class="text-center">Tipo</th>
                    <th class="text-center">Uso</th>
                    <th class="text-center">Stock total</th>
                    <th class="text-center">Stock disponible</th>
                    <th class="text-center">Stock prestado</th>
                </tr>
            </thead>
            <tbody>
                @foreach($deposito->materialesdeposito as $material)
                <tr>
                    <td class="text-center text-bold">{{$material->material->nombre}}</td>
                    <td class="text-center">{{$material->material->tipomaterial->nombre}}</td>
                    <td class="text-center">{{$material->material->tipomaterial->familiamaterial->nombre}}</td>
                    <td class="text-center">{{$material->stock_total()}} {{$material->material->umedida->abreviatura}}</td>
                    <td class="text-center">{{$material->stock}}
                        {{$material->material->umedida->abreviatura}}</td>
                    <td class="text-center">{{$material->stock_prestado()}}
                        {{$material->material->umedida->abreviatura}}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th class="text-center">Material</th>
                    <th class="text-center">Tipo</th>
                    <th class="text-center">Uso</th>
                    <th class="text-center">Stock total</th>
                    <th class="text-center">Stock disponible</th>
                    <th class="text-center">Stock prestado</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>