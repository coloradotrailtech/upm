
 @if($solicitudes_historial->count()>0)
 <ul class="todo-list">
    @foreach($solicitudes_historial as $solicitudprestamo)
    <li>
        <span class="" style="cursor: move">
            <i class="fa fa-ellipsis-v"></i>
            <i class="fa fa-ellipsis-v"></i>
        </span>
        <a id="a-solicitud-historial-{{$solicitudprestamo->id}}" data-toggle="collapse" data-parent="#accordion"
            href="#collapse-solicitud-historial-{{$solicitudprestamo->id}}" class="collapsed" aria-expanded="false">
            <span onclick="instanciar_tabla('solicitud-historial', '{{$solicitudprestamo->id}}')" class="todo-item text"
                style="color: {{$solicitudprestamo->color_estado()}}">
                Fecha: {{$solicitudprestamo->created_at->format('d/m/Y')}} | Curso:
                {{$solicitudprestamo->curso->titulo->nombre}}
                {{$solicitudprestamo->curso->titulo->plan}} | Estado:
                {{$solicitudprestamo->estadosolicitudprestamo->nombre}}</span>
        </a>
        <div id="collapse-solicitud-historial-{{$solicitudprestamo->id}}" class="panel-collapse collapse"
            aria-expanded="false" style="height: 0px;">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><strong>Fecha:</strong></label>
                            <span class="form-control">{{$solicitudprestamo->created_at->format('d/m/Y')}}</span>
                        </div>
                        <div class="form-group">
                            <label><strong>Curso:</strong></label>
                            <span class="form-control">
                                {{$solicitudprestamo->curso->titulo->nombre}}
                                {{$solicitudprestamo->curso->titulo->plan}}</span>
                        </div>
                        <label><strong>Observaciones:</strong></label>
                        <div class="well" style="background-color: white;">
                            {{$solicitudprestamo->estadosolicitudprestamo->nombre}}</div>


                        @if($solicitudprestamo->prestamo)
                        <a onclick="mostrar_detalle_prestamo('{{$solicitudprestamo->prestamo->id}}')"
                            data-toggle="tooltip" title="Conocer el detalle de este registro" class="btn
                                        btn-info"><i class="fa fa-list"></i> ver el préstamo asociado</a>
                        @endif


                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><strong>Listado de materiales:</strong></label>
                            <div style="background-color: #FFFFFF;">
                                <div style="padding: 1%;">
                                    <table id="datatable-solicitud-historial-{{$solicitudprestamo->id}}" class="display"
                                        width="100%" cellspacing="0">
                                        <thead id="thead-solicitud-historial-{{$solicitudprestamo->id}}">
                                            <tr>
                                                <th class="text-center">Material</th>
                                                <th class="text-center">Cantidad</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($solicitudprestamo->materialessolicitudprestamo as
                                            $materialsolicitudprestamo)                                          
                                            <tr>                                            
                                                <td>{{$materialsolicitudprestamo->nombre_material}}
                                                </td>
                                                <td class="text-center">{{$materialsolicitudprestamo->cantidad}}
                                                </td>                                            
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot id="tfoot-solicitud-historial-{{$solicitudprestamo->id}}">
                                            <tr>
                                                <th class="text-center">Material</th>
                                                <th class="text-center">Cantidad</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
    @endforeach
</ul>
@else
<div class="alert alert-warning alert-dismissible">
    <h4><i class="icon fa fa-exclamation-circle"></i> Información</h4> No existen registros coincidentes con los parámetros solicitados.          
</div>
@endif



