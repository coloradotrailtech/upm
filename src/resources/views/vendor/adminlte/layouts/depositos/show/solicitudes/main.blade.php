<br>
<div class="row">
    <div class="col-md-8">
    <h4 style="margin-bottom: 2%;">Solicitudes pendientes:</h4>
    </div>
    @if($deposito->solicitudespendientes->count()>0)
    <div class="col-md-12">
        <div class="well">
            <div class="row">
                <div class="col-md-5">
                    <div class="control-group">
                        <select data-size="10" id="materiales-solicitud-pendiente" title="Filtre aquí materiales"
                            data-header="Seleccione uno o más materiales" class="selectpicker form-control" multiple
                            data-live-search="true" data-actions-box="true" data-container="body" onchange="filtrar('{{$deposito->id}}', 'solicitud-pendiente')">
                            @foreach($deposito->materialessolicitudespendientes() as $material)
                            <option value="{{$material->id}}"
                                id="materiales-solicitud-pendiente-{{$material->id}}"
                                nombre="{{$material->nombre}}">
                                {{$material->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="control-group">
                        <select data-size="10" id="solicitantes-solicitud-pendiente" title="Filtre aquí solicitantes"
                            data-header="Seleccione uno o más cursos solicitantes" class="selectpicker form-control"
                            multiple data-live-search="true" data-actions-box="true" onchange="filtrar('{{$deposito->id}}', 'solicitud-pendiente')">
                            @foreach($deposito->solicitantessolicitudespendientes() as $curso)
                            <option value="{{$curso->id}}" data-subtext="{{$curso->titulo->plan}}">
                                {{$curso->titulo->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="control-group">
                        <select data-size="10" id="fechas-solicitud-pendiente" title="Filtre aquí fechas de solicitudes"
                            data-header="Seleccione una o más fechas de solicitud" class="selectpicker form-control"
                            multiple data-live-search="true" data-actions-box="true" onchange="filtrar('{{$deposito->id}}', 'solicitud-pendiente')">
                            @foreach($deposito->fechassolicitudespendientes() as $fecha)
                            <option value="{{$fecha}}">{{$fecha->format('d/m/Y')}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<br>

<div class="row">
    <div class="col-md-12">
        @if($deposito->solicitudespendientes->count()>0)
        <div id="ul-solicitud-pendiente">
                @include('vendor.adminlte.layouts.depositos.show.solicitudes.div_solicitudes_pendiente')
            </div>
        @else
        <div class="alert alert-info alert-dismissible">
            <h4><i class="icon fa fa-info-circle"></i> Información</h4> Por el momento aún no existe
            ninguna solicitud de materiales o recursos pendiente de respuesta.
        </div>
        @endif
    </div>
</div>
<hr>


<div class="row">
    <div class="col-md-8">
        <h4 style="margin-bottom: 2%;">Historial de Solicitudes:</h4>
    </div>
    @if($deposito->solicitudeshistorico->count()>0)
    <div class="col-md-12">
        <div class="well">
            <div class="row">
                <div class="col-md-5">
                    <div class="control-group">
                        <select data-size="10" id="materiales-solicitud-historial" title="Filtre aquí materiales"
                            data-header="Seleccione uno o más materiales" class="selectpicker form-control" multiple
                            data-live-search="true" data-actions-box="true" data-container="body" onchange="filtrar('{{$deposito->id}}', 'solicitud-historial')">
                            @foreach($deposito->materialessolicitudeshistorico() as $material)
                            <option value="{{$material->id}}"
                                id="materiales-solicitud-historial-{{$material->id}}"
                                nombre="{{$material->nombre}}">
                                {{$material->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="control-group">
                        <select data-size="10" id="solicitantes-solicitud-historial" title="Filtre aquí solicitantes"
                            data-header="Seleccione uno o más cursos solicitantes" class="selectpicker form-control"
                            multiple data-live-search="true" data-actions-box="true" onchange="filtrar('{{$deposito->id}}', 'solicitud-historial')">
                            @foreach($deposito->solicitantessolicitudeshistorico() as $curso)
                            <option value="{{$curso->id}}" data-subtext="{{$curso->titulo->plan}}">
                                {{$curso->titulo->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="control-group">
                        <select data-size="10" id="fechas-solicitud-historial" title="Filtre aquí fechas de solicitudes"
                            data-header="Seleccione una o más fechas de solicitud" class="selectpicker form-control"
                            multiple data-live-search="true" data-actions-box="true" onchange="filtrar('{{$deposito->id}}', 'solicitud-historial')">
                            @foreach($deposito->fechassolicitudeshistorico() as $fecha)
                            <option value="{{$fecha}}">{{$fecha->format('d/m/Y')}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<br>

<div class="row">
    <div class="col-md-12">
        @if($deposito->solicitudeshistorico->count()>0)
            <div id="ul-solicitud-historial">
                @include('vendor.adminlte.layouts.depositos.show.solicitudes.div_solicitudes_historial')
            </div>
        @else
        <div class="alert alert-warning alert-dismissible">
            <h4><i class="icon fa fa-exclamation-circle"></i> Información</h4> Aún no se ha contestado
            ninguna solicitud de materiales o recursos para algún curso solicitante.          
        </div>
        @endif
    </div>
</div>

<br>
<div class="box-footer">
    <a href="{{ route('depositos.index') }}" data-toggle="tooltip" title="Volver a la pantalla anterior"
        class="btn btn-default btn-sm"><i class="fa fa-lg fa-arrow-left"></i> volver</a>
</div>
