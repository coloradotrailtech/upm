@if($solicitudes_pendiente->count()>0)
<ul class="todo-list">
    @foreach($solicitudes_pendiente as $solicitudprestamo)
    <li>
        <span class="" style="cursor: move">
            <i class="fa fa-ellipsis-v"></i>
            <i class="fa fa-ellipsis-v"></i>
        </span>
        <div class="tools">
            <a style="color: #DD4B39" href="{{ route('ingresar_prestamo', $solicitudprestamo->id) }}"
                data-toggle="tooltip" title="Contestar a la solicitud"><i class="fa fa-pencil"></i></a>
        </div>
        <a id="a-solicitud-pendiente-{{$solicitudprestamo->id}}" data-toggle="collapse" data-parent="#accordion"
            href="#collapse-solicitud-pendiente-{{$solicitudprestamo->id}}" class="collapsed" aria-expanded="false">
            <span onclick="instanciar_tabla('solicitud-pendiente', '{{$solicitudprestamo->id}}')"
                class="todo-item text">
                Fecha: {{$solicitudprestamo->created_at->format('d/m/Y')}} | Curso:
                {{$solicitudprestamo->curso->titulo->nombre}}
                {{$solicitudprestamo->curso->titulo->plan}} | Cantidad Materiales
                solicitados:
                {{$solicitudprestamo->materialessolicitudprestamo->count()}}</span>
        </a>
        <div id="collapse-solicitud-pendiente-{{$solicitudprestamo->id}}" class="panel-collapse collapse"
            aria-expanded="false" style="height: 0px;">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><strong>Fecha:</strong></label>
                            <span class="form-control">{{$solicitudprestamo->created_at->format('d/m/Y')}}</span>
                        </div>
                        <div class="form-group">
                            <label><strong>Curso:</strong></label>
                            <span class="form-control">
                                {{$solicitudprestamo->curso->titulo->nombre}}
                                {{$solicitudprestamo->curso->titulo->plan}}</span>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><strong>Listado de materiales:</strong></label>
                            <div style="background-color: #FFFFFF;">
                                <div style="padding: 1%;">
                                    <table id="datatable-solicitud-pendiente-{{$solicitudprestamo->id}}" class="display"
                                        width="100%" cellspacing="0">
                                        <thead id="thead-solicitud-pendiente-{{$solicitudprestamo->id}}">
                                            <tr>
                                                <th class="text-center">Material</th>
                                                <th class="text-center">Cantidad</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($solicitudprestamo->materialessolicitudprestamo
                                            as
                                            $materialsolicitudprestamo)
                                            <tr>
                                                <td>{{$materialsolicitudprestamo->nombre_material}}
                                                </td>
                                                <td class="text-center">{{$materialsolicitudprestamo->cantidad}}
                                                </td>
                                              
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot id="tfoot-solicitud-pendiente-{{$solicitudprestamo->id}}">
                                            <tr>
                                                <th class="text-center">Material</th>
                                                <th class="text-center">Cantidad</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
    @endforeach
</ul>
@else
<div class="alert alert-warning alert-dismissible">
    <h4><i class="icon fa fa-exclamation-circle"></i> Información</h4> No existen registros coincidentes con los
    parámetros solicitados.
</div>
@endif