<div class="modal fade" id="modal-show-prestamo">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Detalle del préstamo</h4>
            </div>
            <div class="modal-body">

                <div class="box-body" id="body-modal-show-prestamo">
               
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">volver</button>
            </div>
        </div>
    </div>
</div>