<br>
<div class="row">
    <div class="col-md-8">
        <h4 style="margin-bottom: 2%;">Listado de préstamos activos:</h4>
    </div>
    @if($deposito->prestamosactivos->count()>0)
    <div class="col-md-12">
        <div class="well">
            <div class="row">
                <div class="col-md-5">
                    <div class="control-group">
                        <select data-size="10" name="materiales-prestamo-realizado" id="materiales-prestamo-realizado" onchange="filtrar('{{$deposito->id}}', 'prestamo-realizado')" title="Filtre aquí materiales"
                            data-header="Seleccione uno o más materiales" class="selectpicker form-control" multiple
                            data-live-search="true" data-actions-box="true" data-container="body">
                            @foreach($deposito->materialesdeposito as $materialdeposito)
                            <option value="{{$materialdeposito->id}}"
                                id="materiales-prestamo-realizado-{{$materialdeposito->id}}"
                                nombre="{{$materialdeposito->material->nombre}}"
                                data-subtext="{{$materialdeposito->material->tipomaterial->familiamaterial->nombre}} - {{$materialdeposito->material->tipomaterial->nombre}}">
                                {{$materialdeposito->material->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="control-group">
                        <select data-size="10" id="solicitantes-prestamo-realizado" title="Filtre aquí solicitantes"
                            data-header="Seleccione uno o más cursos solicitantes" class="selectpicker form-control"
                            multiple data-live-search="true" data-actions-box="true" onchange="filtrar('{{$deposito->id}}', 'prestamo-realizado')">
                            @foreach($deposito->solicitantesprestamosactivos() as $curso)
                            <option value="{{$curso->id}}" data-subtext="{{$curso->titulo->plan}}">
                                {{$curso->titulo->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="control-group">
                        <select data-size="10" id="fechas-prestamo-realizado" title="Filtre aquí fechas de préstamos"
                            data-header="Seleccione una o más fechas de préstamos" class="selectpicker form-control"
                            multiple data-live-search="true" data-actions-box="true" onchange="filtrar('{{$deposito->id}}', 'prestamo-realizado')">
                            @foreach($deposito->fechasprestamosactivos() as $fecha)
                            <option value="{{$fecha}}">{{$fecha->format('d/m/Y')}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<br>
<div class="row">
    <div class="col-md-12">
        @if($deposito->prestamosactivos->count()>0)
        <div id="ul-prestamo-realizado">
        @include('vendor.adminlte.layouts.depositos.show.prestamos.div_prestamos_realizado')
        </div>
        @else
        <div class="alert alert-info alert-dismissible">
            <h4><i class="icon fa fa-exclamation-circle"></i> Información</h4> Cuando existan préstamos a cursos que
            permanezcan activos figurarán aquí.
        </div>
        @endif
    </div>
</div>
<br>
<hr>
<br>
<div class="row">
    <div class="col-md-8">
        <h4 style="margin-bottom: 2%;">Historial de préstamos:</h4>
    </div>
    @if($deposito->prestamosinactivos->count()>0)
    <div class="col-md-12">
        <div class="well">
            <div class="row">
                <div class="col-md-5">
                    <div class="control-group">
                        <select data-size="10" id="materiales-prestamo-culminado" title="Filtre aquí materiales"
                            data-header="Seleccione uno o más materiales" class="selectpicker form-control" multiple
                            data-live-search="true" data-actions-box="true" data-container="body" onchange="filtrar('{{$deposito->id}}', 'prestamo-culminado')">
                            @foreach($deposito->materialesdeposito as $materialdeposito)
                            <option id="materiales-prestamo-culminado-{{$materialdeposito->id}}"
                                value="{{$materialdeposito->id}}"
                                nombre="{{$materialdeposito->material->nombre}}"
                                data-subtext="{{$materialdeposito->material->tipomaterial->familiamaterial->nombre}} - {{$materialdeposito->material->tipomaterial->nombre}}">
                                {{$materialdeposito->material->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="control-group">
                        <select data-size="10" id="solicitantes-prestamo-culminado" title="Filtre aquí solicitantes"
                            data-header="Seleccione uno o más cursos solicitantes" class="selectpicker form-control"
                            multiple data-live-search="true" data-actions-box="true" onchange="filtrar('{{$deposito->id}}', 'prestamo-culminado')">
                            @foreach($deposito->solicitantesprestamosinactivos() as $curso)
                            <option value="{{$curso->id}}" data-subtext="{{$curso->titulo->plan}}">
                                {{$curso->titulo->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="control-group">
                        <select data-size="10" id="fechas-prestamo-culminado" title="Filtre aquí fechas de préstamos"
                            data-header="Seleccione una o más fechas de préstamos" class="selectpicker form-control"
                            multiple data-live-search="true" data-actions-box="true" onchange="filtrar('{{$deposito->id}}', 'prestamo-culminado')">
                            @foreach($deposito->fechasprestamosinactivos() as $fecha)
                            <option value="{{$fecha}}">{{$fecha->format('d/m/Y')}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
<br>
<div class="row">
    <div class="col-md-12">
        @if($deposito->prestamosinactivos->count()>0)
        <div id="ul-prestamo-culminado">
        @include('vendor.adminlte.layouts.depositos.show.prestamos.div_prestamos_culminado')
        </div>
        @else
        <div class="alert alert-info">
            <h4><i class="icon fa fa-exclamation-circle"></i> Información</h4> Cuando existan préstamos que hayan
            finalizado figurarán aquí.
        </div>
        @endif
    </div>
</div>
<br>
<div class="box-footer">
    <a href="{{ route('depositos.index') }}" data-toggle="tooltip" title="Volver a la pantalla anterior"
        class="btn btn-default btn-sm"><i class="fa fa-lg fa-arrow-left"></i> volver</a>
</div>