@if($prestamos_culminado->count()>0)
<ul class="todo-list">
    @foreach($prestamos_culminado as $prestamo)
    <li>
        <span class="" style="depositor: move">
            <i class="fa fa-ellipsis-v"></i>
            <i class="fa fa-ellipsis-v"></i>
        </span>
        <a id="a-prestamo-culminado-{{$prestamo->id}}" data-toggle="collapse" data-parent="#accordion"
            href="#collapse-prestamo-culminado-{{$prestamo->id}}" class="collapsed" aria-expanded="false">
            <span onclick="instanciar_tabla('prestamo-culminado', '{{$prestamo->id}}')" class="todo-item text">
                Fecha: {{$prestamo->created_at->format('d/m/Y')}} |
                <span style="color:black;">Curso: {{$prestamo->curso->titulo->nombre}}
                    {{$prestamo->curso->titulo->plan}}</span> |
                Cantidad Materiales transferidos: {{$prestamo->materialesprestamos->count()}}
            </span>
        </a>
        <div id="collapse-prestamo-culminado-{{$prestamo->id}}" class="panel-collapse collapse" aria-expanded="false"
            style="height: 0px;">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><strong>Fecha:</strong></label>
                            <span class="form-control">{{$prestamo->created_at->format('d/m/Y')}}</span>
                        </div>
                        <div class="form-group">
                            <label><strong>Solicitante:</strong></label>
                            <span class="form-control">{{$prestamo->curso->titulo->nombre}} |
                                {{$prestamo->curso->titulo->plan}}</span>
                        </div>
                        <a onclick="mostrar_detalle_solicitud('{{$prestamo->solicitudprestamo_id}}')"
                            data-toggle="tooltip" title="Conocer el detalle de este registro" class="btn
                                        btn-info"><i class="fa fa-list"></i> ver la solicitud asociada</a>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label><strong>Listado de materiales:</strong></label>
                            <div style="background-color: #FFFFFF;">
                                <div style="padding: 1%;">
                                    <table id="datatable-prestamo-culminado-{{$prestamo->id}}" class="display"
                                        width="100%" cellspacing="0">
                                        <thead id="thead-prestamo-culminado-{{$prestamo->id}}">
                                            <tr>
                                                <th class="text-center">Material</th>
                                                <th class="text-center">Cantidad prestada</th>
                                                <th class="text-center">Unidad medida</th>
                                                <th class="text-center">Observación</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($prestamo->materialesprestamos as $materialprestamo)
                                            @if(isset($ids_solicitados) && in_array($materialprestamo->materialdeposito_id, $ids_solicitados))
                                                <tr style="background-color: #f4b669">
                                            @else
                                                <tr>
                                            @endif
                                                <td>{{$materialprestamo->materialdeposito->material->nombre}}
                                                    ({{$materialprestamo->materialdeposito->material->tipomaterial->familiamaterial->nombre}}
                                                    -
                                                    {{$materialprestamo->materialdeposito->material->tipomaterial->nombre}})
                                                </td>
                                                <td class="text-center">
                                                    {{$materialprestamo->cantidad_prestada}}
                                                </td>
                                                <td class="text-center">
                                                    {{$materialprestamo->materialdeposito->material->umedida->nombre}}
                                                </td>
                                                @if($materialprestamo->observacion)
                                                <td>{{$materialprestamo->observacion}}</td>
                                                @else
                                                <td class="text-center">-</td>
                                                @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot id="tfoot-prestamo-culminado-{{$prestamo->id}}">
                                            <tr>
                                                <th class="text-center">Material</th>
                                                <th class="text-center">Cantidad prestada</th>
                                                <th class="text-center">Unidad medida</th>
                                                <th class="text-center">Observación</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>
    @endforeach
</ul>
@else
<div class="alert alert-warning alert-dismissible">
    <h4><i class="icon fa fa-exclamation-circle"></i> Información</h4> No existen registros coincidentes con los
    parámetros solicitados.
</div>
@endif