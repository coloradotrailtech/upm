<div class="box-body">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label><strong>Fecha:</strong></label>
                <span class="form-control">{{$prestamoshow->created_at->format('d/m/Y')}}</span>
            </div>
            <div class="form-group">
                <label><strong>Solicitante:</strong></label>
                <span class="form-control">{{$prestamoshow->curso->titulo->nombre}} |
                    {{$prestamoshow->curso->titulo->plan}}</span>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label><strong>Listado de materiales:</strong></label>
                <div style="background-color: #FFFFFF;">
                    <div style="padding: 1%;">
                        <table id="datatable-prestamo-show" class="display" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th class="text-center">Material</th>
                                    <th class="text-center">Cantidad prestada</th>
                                    <th class="text-center">Unidad medida</th>
                                    <th class="text-center">Observación</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($prestamoshow->materialesprestamos as $materialprestamo)
                                <tr>
                                    <td>{{$materialprestamo->materialdeposito->material->nombre}}
                                        ({{$materialprestamo->materialdeposito->material->tipomaterial->familiamaterial->nombre}}
                                        - {{$materialprestamo->materialdeposito->material->tipomaterial->nombre}})

                                    </td>
                                    <td class="text-center">{{$materialprestamo->cantidad_prestada}}</td>
                                    <td class="text-center">
                                        {{$materialprestamo->materialdeposito->material->umedida->nombre}}
                                    </td>
                                    @if($materialprestamo->observacion)
                                    <td>{{$materialprestamo->observacion}}</td>
                                    @else
                                    <td class="text-center">-</td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>