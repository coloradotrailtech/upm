<br>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label><strong>Dirección:</strong></label>
            <span class="form-control">{{$deposito->direccion}}</span>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label><strong>Ciudad:</strong></label>
            <span class="form-control">{{$deposito->localidad->nombre}}</span>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label><strong>Descripción:</strong></label>
            <span class="form-control">{{$deposito->descripcion}}</span>
        </div>
    </div>
</div>
<br>
@include('vendor.adminlte.layouts.depositos.show.info_general.materiales')
<br>
<div class="box-footer">
    <a href="{{ route('depositos.index') }}" data-toggle="tooltip" title="Volver a la pantalla anterior"
        class="btn btn-default btn-sm"><i class="fa fa-lg fa-arrow-left"></i> volver</a>
    <div class="pull-right">
        <a href="{{ route('pantalla_transferir_stock', $deposito->id) }}" data-toggle="tooltip"
            title="Transferir stock disponible a otro depósito" class="btn btn-info btn-sm"><i
                class="fa fa-lg fa-truck"></i> Transferir</a>
        <span>&nbsp;<b>|</b>&nbsp;</span>
        <a href="{{ route('pantalla_agregar_inventario', $deposito->id) }}" data-toggle="tooltip"
            title="Incluir un nuevo material al inventario del depósito" class="btn btn-success btn-sm"><i
                class="fa fa-lg fa-cube"></i> Agregar a inventario</a>
        <a href="{{ route('pantalla_administrar_stock', $deposito->id) }}" data-toggle="tooltip"
            title="Actualizar el stock de materiales del depósito" class="btn btn-primary btn-sm"><i
                class="fa fa-lg fa-pencil-square-o"></i> Administrar Stock</a>
        <span>&nbsp;<b>|</b>&nbsp;</span>
        <a onclick="completar_campos({{$deposito}})" data-toggle="tooltip"
            title="Editar los datos básicos de este registro" class="btn btn-social-icon btn-warning btn-sm"><i
                class="fa fa-lg fa-pencil"></i></a>
        <a onclick="abrir_modal_borrar({{$deposito->id}})" data-toggle="tooltip" title="Eliminar este registro"
            class="btn btn-social-icon btn-sm btn-danger"><i class="fa fa-lg fa-trash"></i></a>
    </div>
</div>