@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Transferencia desde Depo. {{$deposito->localidad->nombre}} en {{$deposito->direccion}}
@endsection

@section('contentheader_title')
Transferencias desde Depósito {{$deposito->id}} ({{$deposito->direccion}}, {{$deposito->localidad->nombre}})
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-cubes"></i> Depósitos</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title">Selecione destino y cantidades para efectuar una transferencia</h3>
                </div>
                <div class="box-body ">
                    <div class="row">
                        <div class="col-md-12">
                            @include('vendor.adminlte.layouts.partials.msj_acciones')
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><strong>Origen:</strong></label>
                                <span class="form-control"><strong>Depósito {{$deposito->id}}</strong> &nbsp;&nbsp;{{$deposito->direccion }}, {{$deposito->localidad->nombre}}</span>
                            </div>
                        </div>
                        <input class="hidden" id="origen_id" value="{{$deposito->id}}">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label><strong>Destino:</strong></label>
                                <select class="selectpicker form-control" id="dep_destino" title="Seleccione Deposito...">
                                    <option value=null >Seleccione Deposito..</option>
                                    @foreach($depositos->where('id', '!=', $deposito->id) as $dep_destino)
                                        <option value="{{$dep_destino->id}}"><strong>Depósito {{$dep_destino->id}}</strong> &nbsp;&nbsp;|&nbsp;&nbsp; {{$dep_destino->direccion }}, {{$deposito->localidad->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!--
                        <div class="col-md-3">
                            <div title="Cambiar la acción para la pantalla" style="margin-top: 6%;">
                                <label></label>
                                <input type="checkbox"
                                       onchange="cambiar()"
                                       checked
                                       data-toggle="toggle"
                                       data-on="Corregir"
                                       data-off="Quitar"
                                       data-onstyle="warning"
                                       data-offstyle="danger">
                            </div>
                        </div>
                        -->
                    </div>

            <!-- Debajo se carga tabla dinamica -->
            <br>
            <div id="tabla_stock_deposito">
            </div>

        </div>
    </div>
</div>


@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/deposito_transferir_stock.js') }}"></script>
@endsection











































