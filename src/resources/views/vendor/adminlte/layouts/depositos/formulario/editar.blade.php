<div class="modal fade" id="modal-update">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Actualizar datos del depósito</h4>
            </div>
            <div class="modal-body">
                 @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form id="form-update" action="" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                    <input name="_method" type="hidden" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label>Dirección:</label>
                        <input name="direccion" id="direccion" type="text" maxlength="50" class="form-control" placeholder="campo requerido" required>
                    </div>
                    <div class="form-group">
                        <label>Descripción:</label>
                        <input name="descripcion" id="descripcion" type="text" maxlength="50" class="form-control" placeholder="campo requerido" required>
                    </div>
                    <div class="form-group">
                        <label>Localidad:</label>
                        <select  name="localidad_id" id="localidad_id" placeholder="campo requerido" class="selectpicker form-control" data-live-search="true">
                            @foreach($localidades as $localidad)
                                <option value="{{$localidad->id}}">{{$localidad->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Sede:</label>
                        <select name="sede_id" id="sede_id" placeholder="campo opcional" class="selectpicker form-control" data-live-search="true">
                            @foreach($sedes as $sede)
                                <option value="{{$sede->id}}">{{$sede->localidad->nombre}}</option>
                            @endforeach
                        </select> 
                    </div>
                    <button id="boton_submit_update" type="submit" class="btn btn-primary hide"></button>
                </form>  
                <br>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn  btn-warning" onclick="$('#boton_submit_update').click()">actualizar depósito</button>
            </div>
        </div>
    </div>
</div>