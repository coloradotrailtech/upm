@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Detalle depósito
@endsection

@section('contentheader_title')
Detalle depósito
@endsection

@section('contentheader_description')
detalle
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-cubes"></i> Depósitos</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Listado de existencias</h3>
                </div>
                <div class="box-body ">
                    <div class="row">
                        <div class="col-md-12">
                            @include('vendor.adminlte.layouts.partials.msj_acciones')
                            <div title="Cambiar la acción para la pantalla" style="margin-bottom: 1%;">
                                <label><strong>Acción:</strong></label>
                                <input type="checkbox" onchange="cambiar()" checked data-toggle="toggle"
                                    data-on="Corregir" data-off="Quitar" data-onstyle="warning" data-offstyle="danger">
                            </div>
                            <table id="datatable-depositos" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">Material</th>
                                        <th class="text-center">Tipo</th>
                                        <th class="text-center">Uso</th>
                                        <th class="text-center">Stock actual</th>
                                        <th class="text-center">Cantidad a corregir</th>
                                        <th class="text-center">Stock final</th>
                                        <th class="text-center">Seleccionar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($deposito->materialesdeposito as $material)
                                    <tr>
                                        <td class="text-center text-bold">{{$material->material->nombre}}</td>
                                        <td class="text-center">{{$material->material->tipomaterial->nombre}}</td>
                                        <td class="text-center">
                                            {{$material->material->tipomaterial->familiamaterial->nombre}}</td>
                                        <td class="text-center">{{$material->stock}}
                                            {{$material->material->umedida->abreviatura}}</td>
                                        <td>
                                            <input id="input-cantidad-{{$material->id}}" type="number"
                                                min="-{{$material->stock}}" max="1000000" class="form-control"
                                                placeholder="corrección al stock..."
                                                onkeyup="carga_lista_update('{{$material->id}}', '{{$material->material->umedida->abreviatura}}', '{{$material->stock}}')"
                                                onchange="carga_lista_update('{{$material->id}}', '{{$material->material->umedida->abreviatura}}', '{{$material->stock}}')">
                                        </td>
                                        <td class="text-center"><span
                                                id="span-info-{{$material->id}}">{{$material->stock}}
                                                {{$material->material->umedida->abreviatura}}</span></td>
                                        <td class="text-center" width="10%">
                                            @if($material->stock_disponible() === $material->stock)
                                            <input type="checkbox" data-toggle="tooltip"
                                                title="Solo podrá eliminar materiales de los cuales se dispongan todas las unidades en existencia en el depósito"
                                                id="check-eliminar-{{$material->id}}"
                                                onchange="carga_lista_trash('{{$material->id}}')" />
                                            @else
                                            <input type="checkbox" data-toggle="tooltip"
                                                title="Solo podrá eliminar materiales de los cuales se dispongan todas las unidades en existencia en el depósito"
                                                disabled />
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="text-center">Material</th>
                                        <th class="text-center">Tipo</th>
                                        <th class="text-center">Uso</th>
                                        <th class="text-center">Stock actual</th>
                                        <th class="text-center">Cantidad a corregir</th>
                                        <th class="text-center">Stock final</th>
                                        <th class="text-center">Seleccionar</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="{{ route('depositos.show', $deposito->id) }}" data-toggle="tooltip"
                        title="Volver a la pantalla anterior" class="btn btn-default btn-sm"><i
                            class="fa fa-lg fa-arrow-left"></i> volver</a>
                    <div class="pull-right">
                        <a onclick="mandar_lista_update({{$deposito->id}})" data-toggle="tooltip"
                            title="Proceder a la actualización de las cantidades de stock de materiales del depósito"
                            id="boton-update" class="btn btn-warning btn-sm">
                            <i class="fa fa-lg fa-pencil"></i>
                            corregir stock
                        </a>
                        <a onclick="mandar_lista_borrar({{$deposito->id}})" data-toggle="tooltip"
                            title="Proceder a la eliminación de materiales del depósito." id="boton-eliminar"
                            class="btn btn-danger btn-sm">
                            <i class="fa fa-lg fa-trash"></i>
                            quitar existencias
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/deposito_administrar_stock.js') }}"></script>
@endsection