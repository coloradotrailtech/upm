@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Agregar a inventario
@endsection

@section('contentheader_title')
Agregar a inventario
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-cubes"></i> Depósitos</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Listado de existencias</h3>
                </div>
                <div class="box-body ">
                    <div class="row">
                        <div class="col-md-12">
                            @include('vendor.adminlte.layouts.partials.msj_acciones')
                            <table id="datatable-depositos" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">Material</th>
                                        <th class="text-center">Tipo</th>
                                        <th class="text-center">Uso</th>
                                        <th class="text-center">Unidad medida</th>
                                        <th class="text-center">Stock inicial</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($materiales as $material)
                                    <tr>
                                        <td class="text-center text-bold">{{$material->nombre}}</td>
                                        <td class="text-center">{{$material->tipomaterial->nombre}}</td>
                                        <td class="text-center">
                                            {{$material->tipomaterial->familiamaterial->nombre}}</td>
                                        <td class="text-center">{{$material->umedida->nombre}}
                                            ({{$material->umedida->abreviatura}})</td>
                                        <td>
                                            <input id="input-cantidad-{{$material->id}}" type="number" min="0"
                                                max="1000000" class="form-control"
                                                placeholder="Introduzca una cantidad inicial..."
                                                onkeyup="carga_lista_update('{{$material->id}}')"
                                                onchange="carga_lista_update('{{$material->id}}')">
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                    <th class="text-center">Material</th>
                                        <th class="text-center">Tipo</th>
                                        <th class="text-center">Uso</th>
                                        <th class="text-center">Unidad medida</th>
                                        <th class="text-center">Stock inicial</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="{{ route('depositos.show', $deposito->id) }}" data-toggle="tooltip"
                        title="Volver a la pantalla anterior" class="btn btn-default btn-sm"><i
                            class="fa fa-lg fa-arrow-left"></i> volver</a>
                    <div class="pull-right">
                        <a onclick="mandar_lista_update({{$deposito->id}})" data-toggle="tooltip"
                            title="Proceder a incorporar los nuevos materiales al inventario del depósito"
                            id="boton-update" class="btn btn-success btn-sm">
                            <i class="fa fa-lg fa-pencil"></i>
                            agregar a inventario
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/deposito_agregar.js') }}"></script>
@endsection