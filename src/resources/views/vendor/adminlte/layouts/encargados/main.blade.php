@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Encargados
@endsection

@section('contentheader_title')
Encargados
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-id-badge"></i> Encargados</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Info básica de los encargados de sedes o aulas moviles</h3>
                </div>
                <div class="box-body ">
                    @include('vendor.adminlte.layouts.partials.msj_acciones')
                    <table id="encargados" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center">Apellido y Nombre</th>
                                <th class="text-center">Sede</th>
                                <th class="text-center">Teléfono</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Domicilio</th>
                                <th class="text-center">Fecha de alta</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($encargados as $encargado)
                            <tr>
                                @if($encargado->persona)
                                    <td class="text-center">{{$encargado->persona->apellido}},
                                        {{$encargado->persona->nombre}}</td>
                                    <td class="text-teal">{{$encargado->sede}}</td>
                                    <td class="text-center">{{$encargado->persona->telefono}} 📞</td>
                                    <td class="text-center">{{$encargado->persona->email}}</td>
                                    <td class="text-center">{{$encargado->persona->direccion}} -
                                        {{$encargado->persona->localidad->nombre}}</td>
                                    <td class="text-center">{{$encargado->created_at->format('d/m/Y') }}</td>
                                    <td class="text-center">
                                        <a onclick="completar_campos({{$encargado}})" title="Editar este registro"
                                           class="btn btn-social-icon btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                        <a onclick="abrir_modal_borrar({{$encargado->id}})" title="Eliminar este registro"
                                           class="btn btn-social-icon btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                    </td>
                                @else
                                    @if($encargado->user)
                                        <td class="text-center">{{$encargado->user->name}}</td>
                                    @else
                                        <td class="text-center">-</td>
                                    @endif
                                    <td class="text-center text-bold">@if($encargado->sede){{$encargado->sede->localidad->nombre}} | {{$encargado->sede->direccion}}@else -@endif</td>
                                    <td class="text-center">no registrado</td>
                                    <td class="text-center">no registrado</td>
                                    <td class="text-center">no registrado</td>
                                    <td class="text-center">{{$encargado->created_at->format('d/m/Y') }}</td>
                                    <td class="text-center">
                                        <a onclick="completar_campos({{$encargado}})" title="Editar este registro"
                                           class="btn btn-social-icon btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                        <a onclick="abrir_modal_borrar({{$encargado->id}})" title="Eliminar este registro"
                                           class="btn btn-social-icon btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                    </td>
                                @endif

                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-center">Apellido y Nombre</th>
                                <th class="text-center">Sede</th>
                                <th class="text-center">Teléfono</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Domicilio</th>
                                <th class="text-center">Fecha de alta</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="box-footer">
                    {{--
                    <button title="Registrar un encargado" type="button" id="boton-modal-crear"
                        class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-crear">
                        <i class="fa fa-plus-circle"></i> &nbsp;registrar encargado
                    </button>
                    --}}
                    <div class="alert alert-info">
                        <span class="fa fa-info-circle"></span>
                        Para registrar un encargado debe darlo de alta en <b>Parametros Generales → Usuarios</b>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@include('vendor.adminlte.layouts.encargados.formulario.create')

@include('vendor.adminlte.layouts.encargados.formulario.editar')
@include('vendor.adminlte.layouts.encargados.formulario.confirmar')

@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/encargado.js') }}"></script>
<script src="{{ asset('js/menu.js') }}"></script>
@endsection