<div class="modal fade" id="modal-update">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Actualizar datos de Profesores</h4>
            </div>
            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form id="form-update" action="" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">                                                                           <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Apellido:</label>
                                <input name="apellido" type="text" maxlength="50" class="form-control" placeholder="campo requerido" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input name="nombre" type="text" maxlength="50" class="form-control" placeholder="campo requerido" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email:</label>
                                <input name="email" type="email" maxlength="50" class="form-control" placeholder="campo requerido" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Teléfono:</label>
                                <input name="telefono" type="number" maxlength="20" class="form-control" placeholder="campo requerido" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Sede:</label>
                        <select name="sede_id" placeholder="no obligatorio" data-header="Selecciona Sede" class="selectpicker form-control" data-live-search="true">
                            @foreach($sedes as $sede)
                                <option value="{{$sede->id}}">{{$sede->localidad->nombre}}</option>
                            @endforeach
                        </select>
                    </div>

                    <legend style="margin-top: 10%">Datos Personales</legend>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Sexo:</label>
                                <select name="sexo" placeholder="campo no obligatorio" data-header="Seleccione.." class="selectpicker form-control">
                                    <option value="Masculino">Masculino</option>
                                    <option value="Femenino">Femenino</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Fecha Nac:</label>
                                <input name="fecha_nac" id="fecha_nac" type="date" placeholder="campo requerido"  class="form-control datepicker" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>DNI:</label>
                                <input name="nombre" type="number" maxlength="8" class="form-control" placeholder="campo requerido" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Domicilio:</label>
                                <input name="direccion" type="text" maxlength="50" class="form-control" placeholder="direccion de residencia del docente" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Localidad:</label>
                                <select name="localidad_id" placeholder="campo requerido" data-header="Selecciona una localidad" class="selectpicker form-control" data-live-search="true">
                                    @foreach($localidades as $localidad)
                                        <option value="{{$localidad->id}}" data-subtext="({{$localidad->provincia->nombre}})">{{$localidad->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Descripción Adicional:</label>
                                <textarea name="descripcion" id="descripcion" class="form-control" rows="2" value="" maxlength="200" placeholder="campo opcional (máximo 200 caracteres)"></textarea>


                            </div>
                        </div>
                    </div>

                    <br>
                    <button id="boton_submit_update" type="submit" class="btn btn-primary hide"></button>
                </form>  
                <br>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn  btn-warning" onclick="$('#boton_submit_update').click()">actualizar datos de docente</button>
            </div>
        </div>
    </div>
</div>