
@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Sedes
@endsection

@section('contentheader_title')
Listado de Sedes
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-institution"></i> Sedes</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Registros</h3>
                </div>
                <div class="box-body ">   
                    @include('vendor.adminlte.layouts.partials.msj_acciones')
                    <table id="datatable-sedes" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center">Sede</th>
                                <th class="text-center">Dirección</th>
                                <th class="text-center">Tipo</th>
                                <th class="text-center">Encargado</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sedes as $sede)
                            <tr>
                                @if($sede['movil'])
                                    <td> 
                                        <div class="bg-blue-active text-uppercase text-bold" 
                                            style='padding-left:15px;padding-top:4px;padding-bottom:4px;'> 
                                            {{$sede->localidad->nombre}}  
                                        </div>                                        
                                    </td>                                    
                                @else
                                    <td> 
                                        <div class="bg-orange-active text-uppercase text-bold" 
                                            style='padding-left:15px;padding-top:4px;padding-bottom:4px;'> 
                                            {{$sede->localidad->nombre}}  
                                        </div>                                        
                                    </td>                                    
                                @endif
                                
                                <td class="text-center">{{$sede->direccion}}</td>
                                @if($sede->movil)
                                    <td class="text-center">móvil</td>
                                @elseif($sede->central)
                                    <td class="text-center text-bold text-capitalize">CENTRAL</td>
                                @else
                                    <td class="text-center">-</td>
                                @endif
                                @if($sede->encargado)
                                    <td class="text-center"><div class='bg-olive-active' >{{$sede->encargado->user->name}}</div></td>
                                @else
                                    <td class="text-center"><div class='bg-navy-active' > sin asignar </div>  </td>
                                @endif
                                <td class="text-center">
                                    <a onclick="completar_campos({{$sede}})" title="Editar este registro" class="btn btn-social-icon btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                    <a onclick="abrir_modal_borrar({{$sede->id}})" title="Eliminar este registro" class="btn btn-social-icon btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr> 
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-center">Sede</th>
                                <th class="text-center">Dirección</th>
                                <th class="text-center">Tipo</th>
                                <th class="text-center">Encargado</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </tfoot>
                    </table>
                </div> 
                <div class="box-footer">
                    <button title="Registrar una sede" type="button" id="boton-modal-crear" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-crear">
                        <i class="fa fa-plus-circle"></i> &nbsp;registrar sede
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>

@include('vendor.adminlte.layouts.sedes.formulario.create')
@include('vendor.adminlte.layouts.sedes.formulario.editar')
@include('vendor.adminlte.layouts.sedes.formulario.confirmar')

@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/sede.js') }}"></script>
<script src="{{ asset('js/menu.js') }}"></script>
@endsection







