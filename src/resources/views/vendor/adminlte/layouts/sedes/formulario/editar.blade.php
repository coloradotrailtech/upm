<div class="modal fade" id="modal-update">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Actualizar datos de Sede</h4>
            </div>
            <div class="modal-body">
                 @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form id="form-update" action="" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                    <input name="_method" type="hidden" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Dirección:</label>
                                <input name="direccion" type="text" maxlength="50" class="form-control" placeholder="campo requerido" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Localidad:</label>
                                <select  name="localidad_id" placeholder="campo requerido" class="selectpicker form-control" data-live-search="true">
                                    @foreach($localidades as $localidad)
                                        <option value="{{$localidad->id}}">{{$localidad->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label>Descripción Adicional:</label>
                        <input name="descripcion" type="text-area" maxlength="200" class="form-control" placeholder="campo opcional">
                    </div>
                    <div title="Cambiar la acción para la pantalla" style="margin-bottom: 1%;">
                        <label><strong>Móvil:</strong></label>
                        <input type="checkbox" id="movil"
                               checked
                               data-toggle="toggle"
                               data-on="Si"
                               data-off="No"
                               data-onstyle="info"
                               data-offstyle="grey">
                    </div>

                    <button id="boton_submit_update" type="submit" class="btn btn-primary hide"></button>
                </form>  
                <br>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn  btn-warning" onclick="$('#boton_submit_update').click()">actualizar sede</button>
            </div>
        </div>
    </div>
</div>