<div class="modal fade" id="modal-crear">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Registrar Sede</h4>
            </div>
            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form action="/sedes" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Dirección:</label>
                                <input name="direccion" type="text" maxlength="50" class="form-control" placeholder="campo requerido" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Localidad:</label>
                                <select  name="localidad_id" placeholder="campo requerido" class="selectpicker form-control" data-live-search="true" data-toggle="tooltip" title="Listado de municipios que no tienen sede de UPM">
                                    @foreach($localidades as $localidad)
                                        {{--@if(is_null($localidad->sede))--}}
                                            <option value="{{$localidad->id}}">{{$localidad->nombre}}</option>
                                        {{--@endif --}}
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Descripción Adicional:</label>
                        <input name="descripcion" type="textarea" maxlength="200" class="form-control" placeholder="campo opcional">
                    </div>
                    <div title="Cambiar la acción para la pantalla" style="margin-bottom: 1%;">
                        <label title="una sede temporal es la cual al no tener mas cursos activos en vigencia deja de tener a un encargado a su cargo"><strong>Sede Temporal:</strong></label>
                        <input type="checkbox" name="movil"
                               checked
                               data-toggle="toggle"
                               data-on="si"
                               data-off="no"
                               data-onstyle="info"
                               data-offstyle="grey">
                    </div>
                    <br>
                    <button id="boton_submit_crear" type="submit" class="btn btn-primary hide"></button>
                </form>
                <br>      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn btn-primary" onclick="$('#boton_submit_crear').click()">registrar sede</button>
            </div>
        </div>          
    </div>
</div>