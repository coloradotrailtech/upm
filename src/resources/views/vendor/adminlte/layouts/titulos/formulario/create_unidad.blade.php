

@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Alta de una Clase
@endsection

@section('contentheader_title')
Alta de una nueva clase
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Oficios / Títulos</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Alta de una Clase</h3>
                </div>
                <div class="box-body ">  
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            @include('vendor.adminlte.layouts.partials.msj_acciones')
                            <input id="titulo_id" class="hide" value="{{$titulo->id}}">    
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Tema:</strong></label>
                                <input id="nombre" type="text" maxlength="190" class="form-control" placeholder="campo requerido" required>
                            </div>
                        </div>                        
                        <div class="col-md-2">                       
                            <div class="form-group">
                                <label><strong>Carga horaria:</strong></label>
                                <input id="carga_horaria" type="number" max="100000" min="1" class="form-control" placeholder="campo requerido" required>                                                                  
                            </div>                                             
                        </div>  
                        <div class="col-md-2" style="margin-left: -2%">
                            <div class="form-group" style="width: 60%">
                                <label><strong>&nbsp;</strong></label>
                                <select id="unidad_tiempo" placeholder="campo requerido" class="selectpicker form-control" data-live-search="true">
                                    <option value="minutos">Minutos</option>
                                    <option value="horas">Horas</option>
                                </select>
                            </div>                           
                        </div>  
                        <div class="col-md-2" style="margin-left: -5%">
                            <div class="form-group ">
                                <label><strong>Modalidad:</strong></label>
                                <select id="modalidad_id" placeholder="campo requerido" class="selectpicker form-control" data-live-search="true">        
                                    @foreach($modalidades as $modalidad)
                                    <option value="{{$modalidad->id}}">{{$modalidad->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>      
                        </div>  
                    </div>  
                    <div class="row">                     
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><strong>Seguridad e higiene:</strong></label>
                                <input id="seh" type="number" max="100000" min="1" class="form-control" placeholder="cantidad de minutos" data-toggle="tooltip" title="cantidad de minutos de duración de la sección SeH">
                            </div> 
                        </div> 
                        <div class="col-md-2">                       
                            <div class="form-group">                           
                                <label><strong>Tipo de clase:</strong></label><br>
                                <input  
                                    id="auditable"
                                    type="checkbox"                                                                                                                   
                                    data-toggle="toggle" 
                                    data-on="Auditable" 
                                    data-off="No auditable" 
                                    data-onstyle="warning">                                                                                               
                            </div>   
                        </div>  
                    </div>                    
                    <hr>
                    <div class="row" >
                        <div class="col-md-12">
                            <label style="margin-bottom: 1%;"><strong>Planificación:</strong></label>
                        </div> 
                    </div>    
                    <div class="well well-lg">
                        <div class="row" >                           
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Objetivo:</strong></label>
                                    <input id="objetivo" type="text" maxlength="300" class="form-control" placeholder="campo opcional " required>
                                </div>
                            </div> 
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Contenido:</strong></label>
                                    <input id="contenido" type="text" maxlength="400" class="form-control" placeholder="campo opcional" required>                                                                   </div>
                            </div> 
                            <div class="col-md-4">                       
                                <div class="form-group">
                                    <label><strong>Actividad:</strong></label>
                                    <input id="actividad" type="text" maxlength="300" class="form-control" placeholder="campo opcional" required>
                                </div>                                             
                            </div>                             
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><strong>Evaluación:</strong></label>
                                    <input id="evaluacion" type="text" maxlength="300" class="form-control" placeholder="campo opcional" required>
                                </div>      
                            </div>  
                            <div class="col-md-4">
                                <div class="form-group" style="margin-top: 5%;">                           
                                    <label><strong>&nbsp;</strong></label>
                                    <button id="addRow" class="btn btn-primary btn-sm" type="button"><i class="fa fa-plus-circle"></i> &nbsp;Agregar contenido</button>                                                                                           
                                </div>  

                            </div>  
                        </div> 
                    </div> 
                    <br>
                    <div class="row">
                        <div class="col-md-12">                                                     
                            <table id="example" class="display" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Objetivo</th>
                                        <th>Contenido</th>
                                        <th>Actividad</th>
                                        <th>Evaluación</th>
                                    </tr>
                                </thead>
                                <tbody>                                                                                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Objetivo</th>
                                        <th>Contenido</th>
                                        <th>Actividad</th>
                                        <th>Evaluación</th>
                                    </tr>
                                </tfoot>
                            </table><br>
                            <button class="btn btn-danger btn-sm" id="button"><i class="fa fa-trash"></i>&nbsp;Borrar fila seleccionada</button>
                        </div> 
                    </div> 
                </div> 
                <div class="box-footer">                   
                    <a href="{{ route('titulos.show', $titulo->id) }}" data-toggle="tooltip" title="Volver a la pantalla anterior" class="btn btn-default btn-sm"><i class="fa fa-lg fa-arrow-left"></i> volver</a>                    
                    <div class="pull-right">       
                        <a onclick="enviar()"
                           data-toggle="tooltip" 
                           title="Proceder a registrar la nueva clase" 
                           id="boton-update" 
                           class="btn btn-primary btn-sm">
                            registrar clase
                        </a>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/unidad.js') }}"></script>
<script>
    var pantalla = "crear";
</script>
@endsection











































