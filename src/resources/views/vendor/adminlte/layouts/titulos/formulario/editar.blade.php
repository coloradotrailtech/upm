<div class="modal fade" id="modal-update">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Actualizar datos del título</h4>
            </div>
            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form id="form-update" action="" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                    <input name="_method" type="hidden" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input name="nombre" id="nombre" type="text" maxlength="50" class="form-control"
                                    placeholder="campo requerido" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Plan:</label>
                                @if($titulo->planificacion_aprobada)
                                    <input type="text" name="plan" id="plan" style="width:100%" readonly="readonly" name="date">
                                @else
                                    <div class="input-append date datepicker">
                                        <input type="text" name="plan" id="plan" style="width:100%" readonly="readonly" name="date">
                                        <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Categoría:</label>
                                <select name="categoria_id" id="categoria_id" placeholder="campo requerido"
                                    class="selectpicker form-control" data-live-search="true">
                                    @foreach($categorias as $categoria)
                                    <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Planificación:</label><br>
                                <input type="checkbox" name="planificacion_aprobada" id="planificacion_aprobada" data-toggle="toggle" data-width="100%" data-on="Aprobada"
                                    data-off="No aprobada" data-onstyle="info" data-offstyle="default">
                            </div>
                        </div>
                    </div>
                    <button id="boton_submit_update" type="submit" class="btn btn-primary hide"></button>
                </form>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn  btn-warning" onclick="$('#boton_submit_update').click()">actualizar
                    título</button>
            </div>
        </div>
    </div>
</div>