<div class="modal fade" id="modal-create-material-titulo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Registrar Recurso</h4>
            </div>
            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form action="/store_material_titulo" method="POST" class="form validity">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="titulo_id" value="{{$titulo->id}}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input name="nombre" type="text" maxlength="50" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Cantidad:</label>
                                <input name="cantidad" type="number" min="1" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Uso:</label>
                                <select name="familiamaterial_id" class="form-control" data-live-search="true">
                                    @foreach($familiamateriales as $fmaterial)
                                        <option value="{{$fmaterial->id}}">{{$fmaterial->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <button id="boton_submit_crear_recurso" type="submit" class="btn btn-primary hide"></button>
                </form>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn btn-primary" onclick="$('#boton_submit_crear_recurso').click()">registrar
                    recurso</button>
            </div>
        </div>
    </div>
</div>
