@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Oficios
@endsection

@section('contentheader_title')
Oficios/Títulos
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Oficios/Títulos</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Listado</h3>
                </div>
                <div class="box-body ">   
                    <div>
                        <a href="/libro_aula" class='btn btn-danger' title='Libro Aula' type='button' >
                            Libro Aula
                        </a>
                    </div>
                    <hr>
                    @include('vendor.adminlte.layouts.partials.msj_acciones')
                    <table id="datatable-titulos" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Categoría</th>
                                <th class="text-center">Planificación</th>
                                <th class="text-center">Última cursada</th>
                                <th class="text-center">Planificación Aprobada</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($titulos as $titulo)
                            <tr>
                                <td class="text-center text-bold text-info">{{$titulo->nombre}} <div class="bg-blue-active"> {{$titulo->plan}}</div></td>
                                <td class="text-center">{{$titulo->categoria->nombre}}</td>
                                @if(count($titulo->unidades) > 0)
                                <td class="text-center">
                                    <a href="{{ route('titulos.show', $titulo->id) }}"><b>{{$titulo->unidades->count()}} Clase/s</b>
                                </td>
                                @else
                                <td class="text-center bg-warning">Sin planificación</td>
                                @endif
                                @if($titulo->ultima_cursada())
                                <td class="text-center"><em>{{$titulo->ultima_cursada()->fecha_inicio->format('d/m/Y')}} ~ {{$titulo->ultima_cursada()->fecha_fin->format('d/m/Y')}}</em></td>
                                @else
                                <td class="text-center"><b>-</b></td>
                                @endif
                                @if($titulo->planificacion_aprobada)
                                    <td class="text-center bold text-green">
                                        <b>Si</b>
                                    </td>
                                @else
                                    <td class="text-center text-orange">No</td>
                                    
                                @endif
                                <td class="text-center" width='15%'>    
                                    
                                    <a href="{{ route('titulos.show', $titulo->id) }}" data-toggle="tooltip" title="Ver planificación de este oficio" class="btn btn-social-icon btn-info btn-xs"><i class="fa fa-eye fa-lg"></i></a>
                                    @if(!auth()->user()->isEncargadoDeposito() && !auth()->user()->isBedel())
                                    <a onclick="completar_campos({{$titulo}})" data-toggle="tooltip"
                                        title="Editar datos del oficio" class="btn btn-social-icon btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                                    <!-- <a onclick="abrir_modal_borrar({{$titulo->id}})" title="Eliminar este registro" class="btn btn-social-icon btn-sm btn-danger"><i class="fa fa-trash"></i></a> -->
                                    @endif

                                    <a href="/titulos/export_info/{{ $titulo->id }}"
                                        data-toggle="tooltip"
                                        title="Exportar info en excel"
                                        class="btn btn-xs btn-success">
                                        <i class="fa fa-lg fa-file-excel-o"></i>
                                    </a>                                        
                                
                                
                                    <a onclick="exportar_pdf({{ $titulo }})"
                                        data-toggle="tooltip"
                                        title="Exportar info en pdf"
                                        id='btnExport_PDF'
                                        class="btn btn-xs btn-danger">
                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                    </a>
                                    <a onclick="libro_aula({{ $titulo }})"
                                        data-toggle="tooltip"
                                        title="Exportar caratula Libro Aula"
                                        id='libro_aula'
                                        class="btn btn-xs btn-danger">
                                        <i class="fa fa-file" aria-hidden="true"></i>
                                    </a>
                                                                   
                                    
                                    
                                </td>
                            </tr> 
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Categoría</th>
                                <th class="text-center">Planificación</th>
                                <th class="text-center">Última cursada</th>
                                <th class="text-center">Planificación Aprobada</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                @if(!auth()->user()->isBedel() && !auth()->user()->isEncargadoDeposito())
                <div class="box-footer">
                    <button title="Registrar una sede" type="button" id="boton-modal-crear" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-crear">
                        <i class="fa fa-plus-circle"></i> &nbsp;registrar oficio
                    </button>
                </div>
@endif
            </div>
        </div>
    </div>
</div>

@include('vendor.adminlte.layouts.titulos.formulario.create')
@include('vendor.adminlte.layouts.titulos.formulario.editar')
@include('vendor.adminlte.layouts.titulos.formulario.confirmar')
@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/menu.js') }}"></script>
<script src="{{ asset('js/admin/titulo.js') }}"></script>
<script src="{{ asset('js/admin/curso/imagenes.js') }}"></script>
<script src="{{ asset('js/admin/logos.js') }}"></script>
@endsection
