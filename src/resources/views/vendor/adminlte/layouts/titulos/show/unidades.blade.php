<div class="col-md-12">
    <label style="margin-bottom: 1%;"><strong>Planificación de clases:</strong></label>
    <ul class="todo-list">
        @foreach($titulo->unidades->sortBy('numero_clase') as $unidad)
        <li>
            <span class="" style="cursor: move">
                <i class="fa fa-ellipsis-v"></i>
                <i class="fa fa-ellipsis-v"></i>
            </span>

            @if(!$titulo->planificacion_aprobada) <!--solo si planificacion NO esta Aprobada, se muestran botones editar/borrar "unidad"-->
                <div class="tools">
                    <i onclick="redireccionar_editar_unidad({{$unidad->id}})" class="fa fa-edit" data-toggle="tooltip"
                        title="Actualizar este registro de clase"></i>
                    <i onclick="abrir_modal_borrar_unidad({{$unidad->id}})" class="fa fa-trash-o" data-toggle="tooltip"
                        title="Borrar este registro de clase"></i>
                </div>
            @endif
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$unidad->id}}" class="collapsed"
                aria-expanded="false">
                <span id_unidad="{{$unidad->id}}" numero_clase="{{$unidad->numero_clase}}" class="todo-item text">Clase
                    Nº
                    {{$unidad->numero_clase}} ({{$unidad->modalidad->nombre}}).
                    Tema: {{$unidad->nombre}}</span>
            </a>
            <div id="collapse{{$unidad->id}}" class="panel-collapse collapse" aria-expanded="false"
                style="height: 0px;">
                <div class="box-body">
                    <label style="margin-bottom: 1%;"><strong>Carga horaria:</strong><small class="label label-primary"
                            style="font-size: 75%"><i class="fa fa-clock-o"></i> {{$unidad->carga_horaria}}
                            {{$unidad->unidad_tiempo}}</small></label>
                    <div class="table-responsive" style="background-color: #FFFFFF;">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Objetivos</th>
                                    <th>Contenidos</th>
                                    <th>Actividades</th>
                                    <th>Tipo de evaluación</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($unidad->contenidos as $contenido)
                                <tr>
                                    <td>{{$contenido->objetivos}}</td>
                                    <td>{{$contenido->contenidos}}</td>
                                    <td>{{$contenido->actividades}}</td>
                                    <td>{{$contenido->tipo_evaluacion}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    @if($unidad->seh)
                    <label style="margin-bottom: 1%;"><strong>Seguridad e Higiene:</strong><span
                            class="label label-success" style="font-size: 75%"><i class="fa fa-clock-o"></i>
                            {{$unidad->seh}} minutos</span></label>

                    @endif
                    @if($unidad->auditable =='Si')
                    <span>&nbsp;&nbsp;&nbsp;<b>|</b></span>
                        <label style="margin-bottom: 1%; font-size: 90%" class="label label-warning"><strong>Clase
                                auditable</strong></label>
                        @endif
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</div>