@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Detalle del registro y plan de clases
@endsection

@section('contentheader_title')
Planificación de oficio: <em><strong style="color: #09436e">{{$titulo->nombre}}</strong> | plan de
    {{$titulo->plan}}</em>
@if($titulo->planificacion_aprobada)
<span class="text-green">Planificación Aprobada</span>
@endif
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb')
<!--
<li><a href="#"><i class="fa fa-graduation-cap"></i> Oficios</a></li>-->
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Detalles del título de oficio y plan de clases</h3>
                </div>
                <div class="box-body ">
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <input id="titulo_id" class="hide" value="{{$titulo->id}}">
                            @include('vendor.adminlte.layouts.partials.msj_acciones')
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Nombre:</strong></label>
                                <span
                                    class="form-control bg-blue-active text-uppercase"><strong>{{$titulo->nombre}}</strong>
                                    | {{$titulo->plan}}</span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><strong>Rubro</strong></label>
                                <span class="form-control" data-toggle="tooltip"
                                    title="Rubro al que pertenece el Oficio">{{$titulo->categoria->nombre}}
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><strong>Carga horaria:</strong></label>
                                <span id="span_carga" class="form-control">{{$titulo->obtener_carga_horaria_total("horas")}} horas</span>
                            </div>
                        </div>                        
                    </div>
                    
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group" style="max-width: 70%">
                                <label><strong>Fecha de alta:</strong></label>
                                <span class="form-control">{{$titulo->created_at->format('d/m/Y')}}</span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" style="max-width: 90%">
                                <label><strong>Última modificación:</strong></label>
                                @if($titulo->updated_at)
                                <span class="form-control">{{$titulo->updated_at->format('d/m/Y')}}</span>
                                @else
                                <span class="form-control">{{$titulo->created_at->format('d/m/Y')}}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group" style="max-width: 80%">
                                <label><strong>Planificación:</strong></label>
                                @if($titulo->planificacion_aprobada)
                                <span class="form-control bg-green">Aprobada</span>
                                @else
                                <span class="form-control bg-gray" data-toggle="tooltip"
                                    title="La planificacion de este oficio aún no esta confirmada y aún puede editarse">No
                                    aprobada</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="margin-bottom: 1%;"><strong>Recursos necesarios:</strong></label>
                            <a onclick="$('#modal-create-material-titulo').modal('show')" data-toggle="tooltip"
                                title="Registrar un nuevo recurso" class="btn btn-primary btn-sm pull-right"><i
                                    class="fa fa-lg fa-plus-circle"></i> indicar recurso</a>
                            @include('vendor.adminlte.layouts.titulos.show.recursos')
                        </div>
                    </div>
                    <hr>
                    <div class="row" id="row_unidades">
                        @if(count($titulo->unidades)< 1) <div class="col-md-12">
                            <label style="margin-bottom: 1%;"><strong>Planificación de clases:</strong></label>
                            <div class="alert alert-warning alert-dismissible">
                                <h4><i class="icon fa fa-info-circle"></i> ¡Atención!</h4>
                                El título no posee planificación de clases. Para que el mismo sea utilizable debe poseer
                                asociada una
                                planificación de clases. Puede ingresar una dando un click al botón:
                                <b><i class="fa fa-lg fa-pencil-square-o"></i> planificar nueva clase</b>.
                            </div>
                    </div>
                    @else
                    @include('vendor.adminlte.layouts.titulos.show.unidades')
                    @endif
                </div>
            </div>
            <div class="box-footer">
                <a href="{{ route('titulos.index') }}" data-toggle="tooltip" title="Volver a la pantalla anterior"
                    class="btn btn-default btn-sm"><i class="fa fa-lg fa-arrow-left"></i> volver</a>
                <div class="pull-right">



                    @if($titulo->planificacion_aprobada != true && !auth()->user()->isEncargadoDeposito())
                    <a href="{{ route('titulos.edit', $titulo->id) }}" data-toggle="tooltip"
                        title="Planificar una nueva clase para este título" class="btn btn-primary btn-sm"><i
                            class="fa fa-lg fa-pencil-square-o"></i> planificar nueva clase</a>
                    <span>&nbsp;<b>|</b>&nbsp;</span>
                    @endif


                    <a href="/titulos/export_info/{{ $titulo->id }}"
                        title="Exportar para Excel"
                        class="btn btn-sm btn-success">
                        <i class="fa fa-lg fa-file-excel-o"></i>
                    </a>

                    @if(!auth()->user()->isEncargadoDeposito())
                    <a onclick="completar_campos({{$titulo}})" data-toggle="tooltip"
                        title="Editar los datos básicos de este registro"
                        class="btn btn-social-icon btn-warning btn-sm"><i class="fa fa-lg fa-pencil"></i></a>
                    <a onclick="abrir_modal_borrar({{$titulo->id}})" data-toggle="tooltip"
                        title="Eliminar este registro" class="btn btn-social-icon btn-sm btn-danger"><i
                            class="fa fa-lg fa-trash"></i></a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@include('vendor.adminlte.layouts.titulos.formulario.editar')
@include('vendor.adminlte.layouts.titulos.formulario.confirmar')
@include('vendor.adminlte.layouts.titulos.formulario.create_material_titulo')
@include('vendor.adminlte.layouts.titulos.formulario.confirmar_material')

@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/menu.js') }}"></script>

@if($titulo->planificacion_aprobada == false)
<!-- Jquery UI -->
<script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
@endif
<!-- Bootstrap -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>

<!-- datepicker -->
<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.min.js') }}"></script>
<script charset="UTF-8" src="{{ asset('plugins/datepicker/locales/bootstrap-datepicker.es.js') }}"></script>


<script src="{{ asset('js/admin/titulo.js') }}"></script>
<script>
    if ({{Session::has('nuevo-titulo')}}) {
    Swal.fire({
        title: 'Exito',
        text: "Se acaba de registrar un nuevo título. Para que el mismo sea utilizable debe poseer asociada una planificación de clases. ¿Desea planificar una clase ahora?",
        type: 'success',
        showCancelButton: true,
        confirmButtonText: 'planificar nueva clase',
        cancelButtonText: 'cancelar'
    }).then((result) => {
        if (result.value) {
            window.location.href = '/titulos/' + {{$titulo->id}} + '/edit';
        }
    });
}
</script>
@endsection