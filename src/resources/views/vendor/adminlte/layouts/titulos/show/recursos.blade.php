<div style="margin-top: 1%">
    <table id="datatable-titulos" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center" title="recurso material" data-toggle="tooltip">Nombre</th>
                <th class="text-center">Cantidad</th>
                <th class="text-center">Uso</th>
                <th class="text-center">Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($titulo->materiales_titulos as $material)
            <tr>
                <td class="text-center text-bold text-info">{{$material->nombre}}
                <td class="text-center text-bold text-info">{{$material->cantidad}}
                <td class="text-center text-bold text-info">{{$material->familiamaterial->nombre}}
                </td>
                <td class="text-center">
                    <a onclick="abrir_modal_borrar_recurso({{$material->id}})" title="Eliminar este registro"
                        class="btn btn-social-icon btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th class="text-center" title="recurso material" data-toggle="tooltip">Nombre</th>
                <th class="text-center">Cantidad</th>
                <th class="text-center">Uso</th>
                <th class="text-center">Acciones</th>
            </tr>
        </tfoot>
    </table>
</div>
