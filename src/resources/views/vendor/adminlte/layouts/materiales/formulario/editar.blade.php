<div class="modal fade" id="modal-update">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Actualizar material</h4>
            </div>
            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form id="form-update" action="" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                    <input name="_method" type="hidden" value="PUT">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label>Nombre:</label>
                        <input name="nombre" id="nombre" type="text" maxlength="50" class="form-control" placeholder="campo requerido" required>
                    </div>   
                    <div class="form-group">                    
                        <label>Familia de material:</label>                    
                        <select name="tipomaterial_id" id="tipomaterial_id" placeholder="campo requerido" data-header="Selecciona un ítem" class="selectpicker form-control" data-live-search="true">                                                   
                            @foreach($familias_materiales as $familia)
                            <optgroup label="Tipo: {{$familia->nombre}}">
                                @foreach($familia->tiposmateriales as $tipo)                                  
                                <option value="{{$tipo->id}}">{{$tipo->nombre}}</option>                                                                    
                                @endforeach
                            </optgroup>
                            @endforeach                              
                        </select> 
                    </div>  
                    <div class="form-group">
                        <label>Unidad de medida:</label>
                        <select name="umedida_id" id="umedida_id" placeholder="campo requerido" data-header="Selecciona un ítem" class="selectpicker form-control" data-live-search="true">                          
                            @foreach($tipos_unidades as $tipoMedida)
                            <optgroup label="Medidas de {{$tipoMedida}}">
                                @foreach($unidades_medidas as $unidad)
                                @if($tipoMedida === $unidad->tipoMedida)
                                <option value="{{$unidad->id}}" data-subtext="({{$unidad->abreviatura}})">{{$unidad->nombre}}</option>                                                    
                                @endif                                    
                                @endforeach
                            </optgroup>
                            @endforeach                                                                                
                        </select> 
                    </div>                      
                    <button id="boton_submit_update" type="submit" class="btn btn-primary hide"></button>
                </form>  
                <br>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn  btn-warning" onclick="$('#boton_submit_update').click()">actualizar material</button>
            </div>
        </div>
    </div>
</div>