@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Materiales
@endsection

@section('contentheader_title')
Materiales
@endsection

@section('contentheader_description')

@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-flask"></i> Materiales</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Registros</h3>
                </div>
                <div class="box-body ">
                    @include('vendor.adminlte.layouts.partials.msj_acciones')
                    <table id="datatable-materiales" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Tipo</th>
                                <th class="text-center" title="el uso puede ser de herramienta o insumo">Uso</th>
                                <th class="text-center">Unidad de medida</th>
                                <th class="text-center">Acciones</th>
                                <th class="text-center none"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($materiales as $material)
                            <tr>
                                <td class="text-center text-bold">{{$material->nombre}}</td>
                                <td class="text-center">{{$material->tipomaterial->nombre}}</td>
                                <td class="text-center">{{$material->tipomaterial->familiamaterial->nombre}}</td>
                                <td class="text-center">{{$material->umedida->nombre}}
                                    ({{$material->umedida->abreviatura}})</td>
                                @if(Auth::user()->isAdmin() || Auth::user()->isEncargadoDeposito())
                                <td class="text-center">
                                    <a onclick="completar_campos({{$material}})" title="Editar este registro"
                                        class="btn btn-social-icon btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                    <a onclick="abrir_modal_borrar({{$material->id}})" title="Eliminar este registro"
                                        class="btn btn-social-icon btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                                @else
                                <td class="text-center">
                                    -
                                </td>
                                @endif

                                <td>

                                    @if($material->depositos_disponibles()->count() > 0)

                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center text-bold">Depósito</th>
                                                <th class="text-center text-bold">Cantidad en stock</th>
                                                <th class="text-center text-bold">Cantidad prestada</th>
                                                <th class="text-center text-bold">Acción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($material->depositos_disponibles() as $deposito)
                                            <tr>
                                                <td class="text-center text-bold">{{$deposito->localidad->nombre}} -
                                                    {{$deposito->direccion}}</td>
                                                <td class="text-center">{{$deposito->get_stock_material($material->id)}}
                                                    {{$material->umedida->abreviatura}}</td>
                                                <td class="text-center">
                                                    {{$deposito->get_cantidad_material_prestado($material->id)}}
                                                    {{$material->umedida->abreviatura}}</td>
                                                <td class="text-center">
                                                    <a href="{{ route('depositos.show', $deposito->id) }}"
                                                        data-toggle="tooltip"
                                                        title="Visualizar el detalle de este registro"
                                                        class="btn btn-social-icon btn-info btn-sm"><i
                                                            class="fa fa-eye fa-lg"></i></a>



                                                    <a href="{{ route('pantalla_transferir_stock', $deposito->id) }}"
                                                        data-toggle="tooltip"
                                                        title="Transferir stock disponible a otro depósito"
                                                        class="btn btn-social-icon btn-primary btn-sm"><i class="fa fa-lg fa-truck"></i>
                                                        </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                    @else

                                    <div class="alert alert-info alert-dismissible">
                                        <h4><i class="icon fa fa-info-circle"></i> Información</h4> Este material no
                                        está siendo utilizado por ningún depósito.
                                    </div>


                                    @endif

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Tipo</th>
                                <th class="text-center">Uso</th>
                                <th class="text-center">Unidad de medida</th>
                                <th class="text-center">Acciones</th>
                                <th class="text-center none"></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="box-footer">
                    <button title="Registrar un material" type="button" id="boton-modal-crear"
                        class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-crear">
                        <i class="fa fa-plus-circle"></i> &nbsp;registrar material
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>

@include('vendor.adminlte.layouts.materiales.formulario.create')
@include('vendor.adminlte.layouts.materiales.formulario.editar')
@include('vendor.adminlte.layouts.materiales.formulario.confirmar')

@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/material.js') }}"></script>
@endsection