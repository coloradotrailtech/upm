<div class="modal fade" id="modal-crear">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Registrar Tipo de Material</h4>
            </div>
            <div class="modal-body">
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form action="/tiposmateriales" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">                                                                             
                    <div class="form-group">
                        <label>Nombre:</label>
                        <input name="nombre" type="text" maxlength="50" class="form-control" placeholder="campo requerido" required>
                    </div>                   
                    <div class="form-group">
                        <label>Familia de Materiales:</label>
                        <select  name="familiamaterial_id" placeholder="campo requerido" class="selectpicker form-control" data-live-search="true">
                            @foreach($familias_materiales as $familia_material)
                                <option value="{{$familia_material->id}}">{{$familia_material->nombre}}</option>
                            @endforeach
                        </select> 
                    </div>
                    <br>
                    <button id="boton_submit_crear" type="submit" class="btn btn-primary hide"></button>
                </form>
                <br>      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn btn-primary" onclick="$('#boton_submit_crear').click()">registrar tipo de material</button>
            </div>
        </div>          
    </div>
</div>