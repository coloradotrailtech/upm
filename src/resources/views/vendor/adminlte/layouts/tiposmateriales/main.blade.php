
@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
    Tipos de Materiales
@endsection

@section('contentheader_title')
    Tipos de Materiales
@endsection

@section('contentheader_description')
registros almacenados
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-cogs"></i> Generales</a></li>
<li class="active">Tipos de Materiales</li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Registros</h3>
                </div>
                <div class="box-body ">   
                    @include('vendor.adminlte.layouts.partials.msj_acciones')
                    <table id="datatable-tiposmateriales" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Familia</th>
                                <th class="text-center">Cantidad de Materiales de este tipo</th>
                                <th class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tipos_materiales as $tipomaterial)
                            <tr>
                                <td class="text-center text-bold">{{$tipomaterial->nombre}}</td>
                                <td class="text-center">{{$tipomaterial->familiamaterial->nombre}}</td>
                                <td class="text-center">{{$tipomaterial->materiales->count()}}</td>
                                <td class="text-center">
                                    <a onclick="completar_campos({{$tipomaterial}})" title="Editar este registro" class="btn btn-social-icon btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                    <a onclick="abrir_modal_borrar({{$tipomaterial->id}})" title="Eliminar este registro" class="btn btn-social-icon btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div> 
                <div class="box-footer">
                    <button title="Registrar una localidad" type="button" id="boton-modal-crear" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-crear">
                        <i class="fa fa-plus-circle"></i> &nbsp;registrar nuevo tipo
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>

@include('vendor.adminlte.layouts.tiposmateriales.formulario.create')
@include('vendor.adminlte.layouts.tiposmateriales.formulario.editar')
@include('vendor.adminlte.layouts.tiposmateriales.formulario.confirmar')

@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/admin/tipos_materiales.js') }}"></script>
<script src="{{ asset('js/menu.js') }}"></script>
@endsection







