<div class="modal fade" id="modal-crear">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h3 class="modal-title">Complete el Formulario de Profesor</h3>
            </div>
            <div class="modal-body">
                
                @include('vendor.adminlte.layouts.partials.msj_lista_errores')
                <form action="/profesores" method="POST" class="form validity">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">                    
                        <label>Formación o profesión:</label>
                        <select name="titulacion_id" placeholder="campo requerido" data-header="Selecciona un ítem" class="selectpicker form-control" data-live-search="true">
                            @foreach($titulaciones as $titulacion)
                                <option value="{{$titulacion->id}}">{{$titulacion->nombre}}</option>
                            @endforeach                              
                        </select> 
                    </div>

                    <legend>Datos Personales</legend>                                                                                               
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Apellido:</label>
                                <input name="apellido" type="text" maxlength="50" class="form-control" placeholder="campo requerido" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input name="nombre" type="text" maxlength="50" class="form-control" placeholder="campo requerido" required>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Fecha Nac:</label>
                                <input name="fecha_nac" id="fecha_nac" type="date" placeholder="campo requerido"  class="form-control datepicker">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Sexo:</label>
                                <select name="sexo" placeholder="campo no obligatorio" data-header="Seleccione.." class="selectpicker form-control">
                                    <option value="Masculino">Masculino</option>
                                    <option value="Femenino">Femenino</option>
                                </select>
                            </div>
                        </div>                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>DNI:</label>
                                <input name="dni" type="number" maxlength="8" class="form-control" placeholder="campo requerido" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Estado Civil:</label>
                                <select name="estado_civil" placeholder="campo obligatorio"
                                    data-header="Seleccione estado civil"
                                    class="selectpicker form-control" data-style="btn-primary">
                                    <option value="soltero">    Soltero/a       </option>
                                    <option value="casado">     Casado/a        </option>
                                    <option value="divorciado"> Divorciado/a    </option>
                                    <option value="viudo">      Viudo/a         </option>

                                </select>
                            </div>
                        </div>
                    </div>


                    

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nacionalidad:</label>
                                <select name="pais_id" placeholder="campo obligatorio" data-header="Seleccione país"
                                    class="selectpicker form-control" data-style="btn-primary">
                                    @foreach($paises as $pais)
                                    <option value="{{$pais->id}}">{{$pais->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Localidad:</label>
                                <select name="localidad_id" placeholder="campo requerido" data-header="Selecciona una localidad" class="selectpicker form-control" data-live-search="true">
                                    @foreach($localidades as $localidad)
                                        <option value="{{$localidad->id}}" data-subtext="({{$localidad->provincia->nombre}})">{{$localidad->nombre}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Domicilio:</label>
                                <input name="direccion" type="text" maxlength="50" class="form-control" placeholder="direccion de residencia del docente">
                            </div>
                        </div>    
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email:</label>
                                <input name="email" type="email" maxlength="50" class="form-control" placeholder="campo opcional">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Teléfono:</label>
                                <input name="telefono" type="number" maxlength="20" class="form-control" placeholder="campo requerido" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div title="Seleccione la mano hábil"
                                style="margin-bottom: 5%;">
                                <label style="padding-right: 70px">Mano hábil:</label>
                                <input type="checkbox" onchange="cambiar()" name="mano_habil" checked
                                    data-toggle="toggle" data-on="derecha" data-off="izquierda" data-onstyle="info"
                                    data-offstyle="success" data-width="100%">
                            </div>
                        </div>
                    </div>
                    

                    <br>
                    <button id="boton_submit_crear" type="submit" class="btn btn-primary hide"></button>
                </form>
                <br>      
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">volver</button>
                <button type="button" class="btn btn-primary" onclick="$('#boton_submit_crear').click()">registrar profesor</button>
            </div>
        </div>          
    </div>
</div>