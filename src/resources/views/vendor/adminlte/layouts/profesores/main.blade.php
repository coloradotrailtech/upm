@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Profesores
@endsection

@section('contentheader_title')
Profesores
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-users"></i> Profesores</a></li>


@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Registros</h3>
                </div>
                <div class="box-body" style="">
                    @include('vendor.adminlte.layouts.partials.msj_acciones')
                    <table id="datatable-profesores" class="display" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center"> Apellido y Nombre  </th>
                                <th class="text-center"> Rubro/Profesión    </th>
                                <th class="text-center"> Localidad          </th>
                                <th class="text-center"> Teléfono           </th>
                                <th class="text-center"> Email              </th>                                                                
                                <th class="text-center"> Acciones           </th>
                                <th class="text-center none">Fecha de Alta  </th>
                                <th class="text-center none">Domicilio      </th>
                                <th class="text-center none">Estado Civil   </th>
                                <th class="text-center none">Mano Hábil     </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($profesores as $profesor)
                            <tr>
                                <td class="text-uppercase" width='30%' style='padding-left:30px'>
                                    <strong>{{$profesor->persona->getNombreCompletoAttribute()}}</strong>
                                </td>
                                <td width='10%' >
                                    {{$profesor->titulacion->nombre}}
                                </td>
                                <td width='15%'>
                                    {{$profesor->persona->localidad->nombre}}
                                </td> 
                                <td width='15%'>
                                    {{$profesor->persona->telefono}}
                                </td>
                                <td class="text-center" width='20%'>
                                    {{$profesor->persona->email}}
                                </td>
                                                               
                                <td class="text-center" width='10%'>
                                    <a onclick="completar_campos({{$profesor}})" title="Editar este registro"
                                        class="btn btn-social-icon btn-warning btn-sm"><i class="fa fa-pencil"></i>
                                    </a>
                                    @if(Auth::user()->isAdmin())
                                    <a onclick="abrir_modal_borrar({{$profesor->id}})" title="Eliminar este registro"
                                        class="btn btn-social-icon btn-sm btn-danger"><i class="fa fa-trash"></i>
                                    </a>
                                    @endif
                                </td>
                                <td class="text-center">
                                    {{$profesor->created_at->format('d/m/Y')}}
                                </td>
                                <td class="text-center">
                                    {{$profesor->persona->direccion}}
                                </td>
                                <td class="text-center">
                                    {{$profesor->persona->estado_civil}}
                                </td>
                                <td class="text-center">
                                    {{$profesor->persona->mano_habil}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-center"> Apellido y Nombre  </th>
                                <th class="text-center"> Formación/Profesión</th>
                                <th class="text-center"> Teléfono           </th>
                                <th class="text-center"> Email              </th>
                                <th class="text-center"> Domicilio          </th>
                                <th class="text-center"> Acciones           </th>
                                <th class="text-center none">Fecha de Alta  </th>                                
                                <th class="text-center none">Estado Civil   </th>
                                <th class="text-center none">Mano Hábil     </th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="box-footer">
                    <button title="Registrar un profesor" type="button" id="boton-modal-crear"
                        class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-crear">
                        <i class="fa fa-plus-circle"></i> &nbsp;registrar profesor
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>

@include('vendor.adminlte.layouts.profesores.formulario.create')
@include('vendor.adminlte.layouts.profesores.formulario.editar')
@include('vendor.adminlte.layouts.profesores.formulario.confirmar')

@endsection

@section('scripts_del_modulo')
<script src="{{ asset('js/menu.js') }}"></script>
<script src="{{ asset('js/admin/profesor.js') }}"></script>
@endsection
