@extends('adminlte::layouts.app')

@section('estilos_del_modulo')
<!--Si hubiera alguno iría acá-->
@endsection

@section('htmlheader_title')
Libro Aula
@endsection

@section('contentheader_title')
Libro Aula
@endsection

@section('contentheader_description')
@endsection

@section('breadcrumb')
<li><a href="#"><i class="fa fa-graduation-cap"></i> Libro Aula</a></li>
@endsection

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-list" aria-hidden="true"></i>
                    <h3 class="box-title"> Recursos</h3>
                </div>
                <div class="box-body ">
                    <div>
                        <h4>Planillas</h4>
                        
                        <div class="col-md-2">
                            <a href="https://drive.google.com/file/d/1SYrO--LihOHD2_gBvSmXBu9vAZoaqmlH/view?usp=sharing" 
                                target="_blank" rel="noopener noreferrer" class='btn btn-instagram'>
                                Asistencias 
                            </a> 
                        </div>
                        <div class="col-md-2">
                            <a href="https://drive.google.com/file/d/1G-afjPQGUjgsbxnEE--_CWuVQfHH7NwB/view?usp=sharing" 
                                target="_blank" rel="noopener noreferrer" class='btn btn-instagram'>
                                Calificaciones
                            </a>    
                        </div>
                        <div class="col-md-2">
                            <a href="https://drive.google.com/file/d/18u_k0EP674bf2HP7wSkrFI5jGsDVaJi_/view?usp=sharing" 
                                target="_blank" rel="noopener noreferrer" class='btn btn-instagram'>
                                Control stock
                            </a>    
                        </div>
                    </div>
                    <br><br>
                    <div>
                        <h4>Justificaciones</h4>
                        
                        <div class="col-md-2">
                            <a href="https://drive.google.com/file/d/1r6Zzp_Vd7SK92pq1zx7jxOLlXgImB38_/view?usp=sharing" 
                                target="_blank" rel="noopener noreferrer" class='btn btn-twitter'>
                                Inasitencia
                            </a> 
                        </div>
                        <div class="col-md-2">
                            <a href="https://drive.google.com/file/d/1YQIE8hVHVr-uPz9cLU1kyonGSioGMnw4/view?usp=sharing" 
                                target="_blank" rel="noopener noreferrer" class='btn btn-twitter'>
                                Tardanza
                            </a>    
                        </div>
                        <div class="col-md-2">
                            <a href="https://drive.google.com/file/d/1A_34NkxWqQo6lnGiMuE9K4d79lASUsCN/view?usp=sharing" 
                                target="_blank" rel="noopener noreferrer" class='btn btn-twitter'>
                                Retiro alumno
                            </a>    
                        </div>
                    </div>
                    <br><br>
                    <div>
                        <h4>Caratulas</h4>
                        <div class="col-md-6">
                            <select class="col-md-6" name="" id="">
                                @foreach($titulos as $titulo)
                                <option value="">{{$titulo->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <button></button>
                        </div>
                    </div>
                    
                </div>
                <div class="box-footer">
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts_del_modulo')
<script src="{{ asset('js/menu.js') }}"></script>

@endsection