{{--@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
<body class="hold-transition login-page">
    <div id="app">
        <div class="login-box">
            
         
            <div class="login-logo">
                <img src="{{ asset('/img/logo-up.png') }}" class="img-circle" alt="User Image"/><br>
                <a href="{{ url('/home') }}"><b>Universidad Popular</b> de <b>Misiones</b></a>
            </div><!-- /.login-logo -->

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-box-body">
        <p class="login-box-msg"> Identificate para poder iniciar sesión</p>
        <form action="{{ url('/login') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group has-feedback">
                <input type="email" class="form-control" placeholder="{{ trans('adminlte_lang::message.email') }}" name="email"/>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}" name="password"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> Recuérdame
                        </label>
                    </div>
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
                </div><!-- /.col -->
            </div>
        </form>



        <a href="{{ url('/password/reset') }}">Recuperar mi contraseña</a><br>

    </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->
    </div>
    @include('adminlte::layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>

@endsection
--}}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        UPM | Acceso al sistema
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- CSS Files -->
    <link href="{{asset('css/animate.css')}}" rel="stylesheet" />
    <link href="{{asset('plugins/ui-kit/assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{ asset('plugins/ui-kit/assets/css/now-ui-kit.css?v=1.2.0')}}" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="{{asset('plugins/ui-kit/assets/demo/demo.css') }}" rel="stylesheet" />
</head>

<body class="login-page sidebar-collapse">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="400">
    <div class="container">
        <div class="dropdown button-dropdown">
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-header">Dropdown header</a>
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Separated link</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">One more separated link</a>
            </div>
        </div>


        <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="../assets/img/blurred-image-1.jpg">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank">
                        <i class="fab fa-twitter"></i>
                        <p class="d-lg-none d-xl-none">Twitter</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank">
                        <i class="fab fa-facebook-square"></i>
                        <p class="d-lg-none d-xl-none">Facebook</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
                        <i class="fab fa-instagram"></i>
                        <p class="d-lg-none d-xl-none">Instagram</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- End Navbar -->
<div class="page-header clear-filter " filter-color="white"> {{-- orange--}}
    @php
        $dia=\Carbon\Carbon::today()->dayOfWeek
    @endphp

    @if($dia < 4 )
        <div class="page-header-image" style="background-attachment: fixed; object-fit: cover; background-image:url({{asset('img/login/1.jpg')}})"></div>
    @elseif((4<= $dia) && ($dia < 6) )
        <div class="page-header-image" style="background-attachment: fixed; object-fit: cover; background-image:url({{asset('img/login/2.jpg')}})"></div>
    @elseif((6<= $dia) && ($dia <=7 ) )
        <div class="page-header-image" style="background-attachment: fixed; object-fit: cover; background-image:url({{asset('img/login/3.jpg')}})"></div>
    @endif
    <div class="content animated fadeInUpBig">
        <div class="container">
            <div class="col-md-4 ml-auto mr-auto">
                <div class="card card-login card-plain">
                    <form method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="card-header text-center">
                            <div class="">
                                <img src="{{ asset('/img/login/logo-up.png') }}" class="img-circle" alt="User Image"/><br>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="input-group no-border input-lg has-feedback">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                          <i class="now-ui-icons users_circle-08"></i>
                                        </span>
                                    </div>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">

                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{-- $errors->first('email') --}}Credenciales no hallados en nuestros registros de usuarios</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="input-group no-border input-lg">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                          <i class="now-ui-icons text_caps-small"></i>
                                        </span>
                                    </div>
                                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="card-footer text-center" style="color: #28a1c4">
                            <button type="submit" class="btn btn-primary btn-round btn-lg btn-block">Ingresar</button>
                            <div class="pull-left">
                                <h6>
                                    <a href="#pablo" class="link">Recuperar contraseña</a>
                                </h6>
                            </div>
                            <div class="pull-right">
                                <h6>
                                    <a href="#pablo" class="link">Necesitas ayuda?</a>
                                </h6>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<!--   Core JS Files   -->
<script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="../assets/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="../assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="{{asset('plugins/ui-kit/assets/js/plugins/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<!--  Google Maps Plugin    -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="{{asset('plugins/ui-kit/assets/js/now-ui-kit.js?v=1.2.0')}}" type="text/javascript"></script>


<script>
    //Creamos dos variables que tendrán las expresiones regulares a ser comprobadas
    //Una para el email y otra para verrficar solo letras
    var expr = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    var expr1 = /^[a-zA-Z]*$/;

    $(document).ready(function () {
        $("#boton").click(function (){ //función para el boton de enviar
            //recolectamos en variables, lo que tenga cada input.
            //Para mejor manipulación en los if's
            var email = $("#email").val();
            var passw = $("#password").val();
            var repass = $("#repass").val();

            //Secuencia de if's para verificar contenido de los inputs

            //Verifica que no este vacío y que sean letras
            if(email == "" || !expr1.test(email)){
                $("#mensaje1").fadeIn("slow"); //Muestra mensaje de error
                return false;                  // con false sale de la secuencia
            }
            else{


                $("#mensaje3").fadeOut();

                if(passw != repass){
                    $("#mensaje4").fadeIn("slow");
                    return false;
                }
            }

        });//fin click

        /*Las siguientes funciones son una mejora al ejemplo anterior que mostré
         * Si el mensaje se mostró, el usuario tenía que volver a oprimir el boton
         * de registrar para que el error se ocultará (si era el caso).
         *
         *Con estas funciones de keyup, el mensaje de error se muestra y
         * se ocultará automáticamente, si el usuario escribe datos admitidos.
         * Sin necesidad de oprimir de nuevo el boton de registrar.
         *
         * La función keyup lee lo último que se ha escrito y comparamos
         * con nuestras condiciones, si cumple se quita el error.
         *
         * Es cuestión de analizar un poco para entenderlas
         * Cualquier duda, comenten
         * */

        $("#email").keyup(function(){
            if( $(this).val() != "" && expr.test($(this).val())){
                $("#mensaje3").fadeOut();
                return false;
            }
        });

        var valido=false;
        $("#repass").keyup(function(e) {
            var pass = $("#pass").val();
            var re_pass=$("#repass").val();

            if(pass != re_pass)
            {
                $("#repass").css({"background":"#F22" }); //El input se pone rojo
                valido=true;
            }
            else if(pass == re_pass)
            {
                $("#password").css({"background":"#8F8"}); //El input se ponen verde
                $("#mensaje4").fadeOut();
                valido=true;
            }
        });//fin keyup repass

    });//fin ready
</script>
</body>

</html>
