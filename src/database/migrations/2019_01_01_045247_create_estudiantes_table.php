<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstudiantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudiantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned()->unique();
            $table->foreign('persona_id')->references('id')->on('personas')->onDelete('cascade');

            $table->boolean('trabaja')->nullable();
            $table->string('rubro_trabaja')->nullable();   #rubro en que trabaja el estudiante (texto libre)

            $table->string('descripcion', 500)->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }


    public function down()
    {
        Schema::dropIfExists('estudiantes');
    }
}
