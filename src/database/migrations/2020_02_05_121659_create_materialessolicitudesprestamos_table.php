<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialessolicitudesprestamosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materialessolicitudesprestamos', function (Blueprint $table) {
            $table->increments('id');
            $table->double('cantidad')->nullable();
            $table->string('observacion', 500)->nullable();
            $table->integer('materialtitulo_id')->unsigned();
            $table->foreign('materialtitulo_id')->references('id')->on('materialestitulos')->onDelete('cascade');
            $table->integer('solicitudprestamo_id')->unsigned();
            $table->foreign('solicitudprestamo_id')->references('id')->on('solicitudesprestamos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materialessolicitudesprestamos');
    }
}
