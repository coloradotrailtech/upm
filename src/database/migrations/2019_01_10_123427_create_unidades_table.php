<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('unidades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('carga_horaria')->nullable();
            $table->string('unidad_tiempo')->nullable();            
            $table->string('numero_clase')->nullable();
            $table->string('nombre')->nullable();
            $table->string('seh')->nullable();
            $table->enum('auditable', ['Si', 'No'])->nullable();            
            $table->integer('titulo_id')->unsigned();
            $table->foreign('titulo_id')->references('id')->on('titulos')->onDelete('cascade');
            $table->integer('modalidad_id')->unsigned();
            $table->foreign('modalidad_id')->references('id')->on('modalidades')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('unidades');
    }

}
