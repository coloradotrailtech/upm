<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudprestamoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudesprestamos', function (Blueprint $table) {
            $table->increments('id');                       
            $table->integer('estadosolicitudprestamo_id')->unsigned();
            $table->foreign('estadosolicitudprestamo_id')->references('id')->on('estadossolicitudesprestamos')->onDelete('cascade');
            $table->integer('curso_id')->unsigned();
            $table->foreign('curso_id')->references('id')->on('cursos')->onDelete('cascade');
            $table->integer('deposito_id')->unsigned();
            $table->foreign('deposito_id')->references('id')->on('depositos')->onDelete('cascade');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudesprestamos');
    }
}
