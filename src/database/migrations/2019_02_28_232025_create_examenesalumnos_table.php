<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamenesalumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examenesalumnos', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('asistio')->nullable();
            $table->string('motivo_inasistencia', 500)->nullable();
            $table->double('nota')->nullable();
            $table->enum('situacion', ['Aprobado', 'Desaprobado'])->nullable();
            $table->string('observacion', 500)->nullable();
            $table->integer('examen_id')->unsigned()->nullable();
            $table->foreign('examen_id')->references('id')->on('examenes')->onDelete('cascade');
            $table->integer('alumno_id')->unsigned()->nullable();
            $table->foreign('alumno_id')->references('id')->on('alumnos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('examenesalumnos');
    }
}
