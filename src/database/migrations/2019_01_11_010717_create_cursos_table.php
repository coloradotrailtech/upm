<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('activa')->nullable();
            $table->integer('titulo_id')->unsigned();
            $table->foreign('titulo_id')->references('id')->on('titulos')->onDelete('cascade');
            $table->dateTime('fecha_inicio')->nullable();
            $table->string('comision')->nullable();            
            $table->dateTime('fecha_fin')->nullable();
            $table->integer('sede_id')->unsigned();
            $table->foreign('sede_id')->references('id')->on('sedes')->onDelete('cascade');
            $table->string('descripcion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos');
    }
}
