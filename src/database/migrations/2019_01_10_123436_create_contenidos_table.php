<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContenidosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('contenidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contenidos', 300)->nullable();
            $table->string('actividades', 300)->nullable();
            $table->string('objetivos', 400)->nullable();
            $table->string('tipo_evaluacion', 300 )->nullable();
            $table->integer('unidad_id')->unsigned();
            $table->foreign('unidad_id')->references('id')->on('unidades')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('contenidos');
    }

}
