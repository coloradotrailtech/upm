<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialestransferenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materialestransferencias', function (Blueprint $table) {
            $table->increments('id');
	    $table->double('cantidad');
            $table->string('observacion');
            $table->integer('material_id')->unsigned();
            $table->foreign('material_id')->references('id')->on('materiales')->onDelete('cascade');
            $table->integer('transferencia_id')->unsigned();
            $table->foreign('transferencia_id')->references('id')->on('transferencias')->onDelete('cascade'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materialestransferencias');
    }
}
