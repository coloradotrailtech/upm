<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSedesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sedes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('localidad_id')->unsigned();
            $table->foreign('localidad_id')->references('id')->on('localidades')->onDelete('cascade');
            $table->string('direccion')->nullable();
            $table->integer('encargado_id')->unsigned()->nullable();
            $table->foreign('encargado_id')->references('id')->on('encargados')->onDelete('cascade');
            $table->boolean('movil')->nullable();
            $table->boolean('central')->nullable();
            $table->boolean('activa')->nullable();

            $table->string('descripcion')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('sedes');
    }

}
