<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposmaterialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tiposmateriales', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('familiamaterial_id')->unsigned();
            $table->foreign('familiamaterial_id')->references('id')->on('familiasmateriales')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tiposmateriales');
    }
}
