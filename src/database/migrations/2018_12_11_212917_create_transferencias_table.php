<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferenciasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('transferencias', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha_envio')->nullable();
            $table->integer('origen_id')->unsigned();
            $table->foreign('origen_id')->references('id')->on('depositos')->onDelete('cascade');
            $table->integer('destino_id')->unsigned();
            $table->foreign('destino_id')->references('id')->on('depositos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('transferencias');
    }

}
