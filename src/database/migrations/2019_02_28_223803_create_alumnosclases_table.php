<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumnosclasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnosclases', function (Blueprint $table) {
		$table->increments('id');
		$table->boolean('asistio')->nullable();
		$table->string('motivo_inasistencia',500)->nullable();
  		$table->enum('desempeno', ['bueno', 'regular', 'excelente', 'malo']);
		$table->integer('alumno_id')->unsigned()->nullable();
		$table->foreign('alumno_id')->references('id')->on('alumnos')->onDelete('cascade');    
		$table->integer('clase_id')->unsigned()->nullable();
		$table->foreign('clase_id')->references('id')->on('clases')->onDelete('cascade');  
		$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnosclases');
    }
}
