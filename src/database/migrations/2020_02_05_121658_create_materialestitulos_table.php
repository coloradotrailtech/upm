<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialestitulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materialestitulos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 500)->nullable();
            $table->double('cantidad')->nullable();
            $table->integer('familiasmateriales_id')->unsigned();
            $table->foreign('familiasmateriales_id')->references('id')->on('familiasmateriales')->onDelete('cascade');
            $table->integer('titulo_id')->unsigned();
            $table->foreign('titulo_id')->references('id')->on('titulos')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materialestitulos');
    }
}
