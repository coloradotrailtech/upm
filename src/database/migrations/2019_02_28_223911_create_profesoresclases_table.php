<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesoresclasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesoresclases', function (Blueprint $table) {
            $table->increments('id');
	    $table->boolean('asistio')->nullable();
	    $table->string('motivo_inasistencia',500)->nullable();
	    $table->string('observacion',500)->nullable();
	    $table->integer('profesorcurso_id')->unsigned()->nullable();
	    $table->foreign('profesorcurso_id')->references('id')->on('profesorescursos')->onDelete('cascade');    
	    $table->integer('clase_id')->unsigned()->nullable();
	    $table->foreign('clase_id')->references('id')->on('clases')->onDelete('cascade');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profesoresclases');
    }
}
