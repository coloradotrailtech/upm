<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellido');
            $table->enum('sexo', ['Femenino', 'Masculino']);
            $table->string('dni')->unique()->nullable();
            $table->string('cuil')->unique()->nullable();
            $table->date('fecha_nac')->nullable();
            $table->string('telefono')->nullable();
            $table->string('telefono2')->nullable();
            $table->string('email')->nullable();                        
            $table->string('foto_perfil')->nullable();
            $table->string('direccion')->nullable();
            $table->string('descripcion', 500)->nullable();
            $table->integer('localidad_id')->unsigned();
            $table->foreign('localidad_id')->references('id')->on('localidades')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('pais_id')->references('id')->on('paises')->onDelete('cascade');   #pais de nacimiento
            $table->integer('pais_id')->unsigned()->nullable();
            $table->string('sangre')->nullable();   #Grupo Sanguineo
            $table->enum('estado_civil', ['soltero', 'casado', 'divorciado', 'viudo'])->nullable();   #Grupo Sanguineo
            $table->enum('mano_habil', ['derecha', 'izquierda'])->nullable();
            $table->boolean('discapacidad')->nullable();
            $table->boolean('carnet_discapacidad')->nullable();
            $table->enum('educacion', ['Primaria Incompleta', 'Primaria Completa', 'Secundaria Incompleta', 'Secundaria Completa','Terciario Incompleto','Terciario Completo','Universitario Incompleto','Universitario Completo']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
