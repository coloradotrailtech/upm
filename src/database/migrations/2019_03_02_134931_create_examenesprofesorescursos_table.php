<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamenesprofesorescursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examenesprofesorescursos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('examen_id')->unsigned()->nullable();
            $table->foreign('examen_id')->references('id')->on('examenes')->onDelete('cascade'); 
            $table->integer('profesorcurso_id')->unsigned()->nullable();
            $table->foreign('profesorcurso_id')->references('id')->on('profesorescursos')->onDelete('cascade');              
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('examenesprofesorescursos');
    }
}
