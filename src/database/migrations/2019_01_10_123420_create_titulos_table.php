<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTitulosTable extends Migration {


    #ACLARACION: ellos llaman a los titulos "oficios"
    public function up() {
        Schema::create('titulos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('plan')->nullable();
            $table->integer('categoria_id')->unsigned();
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->boolean('planificacion_aprobada')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('titulos');
    }

}
