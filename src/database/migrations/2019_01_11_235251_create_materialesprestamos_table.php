<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialesprestamosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materialesprestamos', function (Blueprint $table) {
            $table->increments('id');
            $table->double('cantidad_prestada');
            $table->double('cantidad_devuelta')->nullable();
            $table->string('observacion', 500)->nullable();
            $table->integer('materialdeposito_id')->unsigned();
            $table->foreign('materialdeposito_id')->references('id')->on('materialesdepositos')->onDelete('cascade');
            $table->integer('prestamo_id')->unsigned();
            $table->foreign('prestamo_id')->references('id')->on('prestamos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materialesprestamos');
    }
}
