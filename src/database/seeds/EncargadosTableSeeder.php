<?php

use Illuminate\Database\Seeder;

class EncargadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('encargados')->insert([
            'persona_id'=>'3',
            'horario_enrada'=>null,
            'horario_salida'=>null,
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
