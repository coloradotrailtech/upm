<?php

use Illuminate\Database\Seeder;

class MaterialesdepositosTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        
        DB::table('materialesdepositos')->insert([
            'material_id' => '1',
            'deposito_id' => '1',
            'stock' => '40',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('materialesdepositos')->insert([
            'material_id' => '2',
            'deposito_id' => '1',
            'stock' => '50',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('materialesdepositos')->insert([
            'material_id' => '3',
            'deposito_id' => '1',
            'stock' => '80',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('materialesdepositos')->insert([
            'material_id' => '4',
            'deposito_id' => '1',
            'stock' => '20',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('materialesdepositos')->insert([
            'material_id' => '5',
            'deposito_id' => '1',
            'stock' => '30',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('materialesdepositos')->insert([
            'material_id' => '6',
            'deposito_id' => '1',
            'stock' => '45',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('materialesdepositos')->insert([
            'material_id' => '7',
            'deposito_id' => '1',
            'stock' => '150',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('materialesdepositos')->insert([
            'material_id' => '8',
            'deposito_id' => '1',
            'stock' => '8',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('materialesdepositos')->insert([
            'material_id' => '9',
            'deposito_id' => '1',
            'stock' => '79',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('materialesdepositos')->insert([
            'material_id' => '10',
            'deposito_id' => '1',
            'stock' => '26',
            'created_at' => date('Y-m-d H:m:s')
        ]);
       
    }

}
