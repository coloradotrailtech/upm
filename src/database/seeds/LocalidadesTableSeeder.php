<?php

use Illuminate\Database\Seeder;

class LocalidadesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
             
        DB::table('localidades')->insert([
            'nombre' => 'Posadas',
            'cod_postal' => '3300',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Garupá',
            'cod_postal' => '3300',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Oberá',
            'cod_postal' => '3300',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Candelaria',
            'cod_postal' => '3308',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Colonia Profundidad',
            'cod_postal' => '3308',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Fachinal',
            'cod_postal' => '3300',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Cerro Azul',
            'cod_postal' => '3313',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Leandro N. Alem',
            'cod_postal' => '3315',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'San José',
            'cod_postal' => '3306',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Apóstoles',
            'cod_postal' => '3350',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Concepción de la Sierra',
            'cod_postal' => '3355',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Azara',
            'cod_postal' => '3351',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'San Ignacio',
            'cod_postal' => '3322',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Jardín América',
            'cod_postal' => '3328',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Puerto Rico',
            'cod_postal' => '3334',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Capioví',
            'cod_postal' => '3332',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Colonia Victoria',
            'cod_postal' => '3382',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Wanda',
            'cod_postal' => '3376',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Eldorado',
            'cod_postal' => '3380',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Puerto Iguazú',
            'cod_postal' => '3370',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Campo Grande',
            'cod_postal' => '3362',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Campo Viera',
            'cod_postal' => '3362',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Aristóbulo del Valle',
            'cod_postal' => '3364',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'El Soberbio',
            'cod_postal' => '3364',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Alba Posse',
            'cod_postal' => '3363',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'San Javier',
            'cod_postal' => '3357',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'San Pedro',
            'cod_postal' => '3364',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => 'Montecarlo',
            'cod_postal' => '3384',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('localidades')->insert([
            'nombre' => '25 de Mayo',
            'cod_postal' => '3363',
            'provincia_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

       
    }

}
