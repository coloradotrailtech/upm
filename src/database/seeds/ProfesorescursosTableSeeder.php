<?php

use Illuminate\Database\Seeder;

class ProfesorescursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profesorescursos')->insert([
            'rol' => 'titular',
            'curso_id' => '1',
            'profesor_id' => '1',
            'created_at' => date('Y-m-d H:m:s'),
        ]);
        
        DB::table('profesorescursos')->insert([
            'rol' => 'suplente',
            'curso_id' => '1',
            'profesor_id' => '2',
            'created_at' => date('Y-m-d H:m:s'),
        ]);

    }
}
