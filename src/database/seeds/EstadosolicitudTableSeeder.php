<?php

use Illuminate\Database\Seeder;

class EstadosolicitudTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {      
        DB::table('estadossolicitudesprestamos')->insert([
            'nombre' => 'Enviado',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('estadossolicitudesprestamos')->insert([
            'nombre' => 'Aceptado',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('estadossolicitudesprestamos')->insert([
            'nombre' => 'Rechazado',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('estadossolicitudesprestamos')->insert([
            'nombre' => 'Aceptado pero no se pudo cumplir con todo lo solicitado',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('estadossolicitudesprestamos')->insert([
            'nombre' => 'Aceptado con recursos en remplazo de otros',
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
