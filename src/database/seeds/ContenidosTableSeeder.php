<?php

use Illuminate\Database\Seeder;

class ContenidosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contenidos')->insert([         
            'objetivos' => 'Análisis de nuestro cuerpo. Moldes.',
            'contenidos' => 'Anatomía del juego. Principios del juego.',
            'actividades' => 'Aprender la forma del cuerpo y como se pueden lastimar si algo pasa mal.',
            'tipo_evaluacion' => '14',
            'unidad_id' => '6',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('contenidos')->insert([
            'objetivos' => 'Reglamento de faltas e infraciones.',
            'contenidos' => 'Introdiccion al reglamento de faltas.',
            'actividades' => 'Estudio del reglamento con escenarios prácticos de aplicación',
            'tipo_evaluacion' => '14',
            'unidad_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        /** ARBITRO DE FUTBOL */
        DB::table('contenidos')->insert([
            'objetivos' => 'Conocer el programa general del curso.',
            'contenidos' => 'Análisis del reglamento y perfil del árbitro',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico',
            'tipo_evaluacion' => '14',
            'unidad_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('contenidos')->insert([
            'objetivos' => 'Elementos fundamentales para el partido',
            'contenidos' => 'Elementos necesarios para disputar un partido.',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico',
            'tipo_evaluacion' => '14',
            'unidad_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('contenidos')->insert([                                       #5
            'objetivos' => 'Elementos fundamentales para el partido',
            'contenidos' => 'Regla N° 1: terreno de juego',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico',
            'tipo_evaluacion' => '14',
            'unidad_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('contenidos')->insert([
            'objetivos' => 'Elementos fundamentales para el partido',
            'contenidos' => 'Regla N° 2: El balón',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico',
            'tipo_evaluacion' => '14',
            'unidad_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('contenidos')->insert([
            'objetivos' => 'Elementos fundamentales para el partido',
            'contenidos' => 'Regla N° 3: Número de jugadores',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico',
            'tipo_evaluacion' => '14',
            'unidad_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('contenidos')->insert([                                       #8
            'objetivos' => 'Elementos fundamentales para el partido',
            'contenidos' => 'El capitan de Equipo: atribuciones y responsabilidades. Articulo 209 reglamento general AFA.',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico',
            'tipo_evaluacion' => '14',
            'unidad_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        //UNIDAD 2
        DB::table('contenidos')->insert([
            'objetivos' => 'Que el alumno conozca en forma correcta los elementos fundamentales y necesarios para poder desarrollar un partido de futbol',
            'contenidos' => 'Regla N°4 : Equipo de jugadores',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico',
            'tipo_evaluacion' => '14',
            'unidad_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('contenidos')->insert([                                               #10
            'objetivos' => 'Articulo 208  Disposiciones y deberes para los clubes',
            'contenidos' => 'Regla N°4 : Equipo de jugadores',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico',
            'tipo_evaluacion' => '14',
            'unidad_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('contenidos')->insert([
            'objetivos' => 'Que el alumno conozca  en forma correcta los elementos fundamentales y necesarios para poder desarrollar un partido de futbol',
            'contenidos' => 'El equipo arbitral ',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico',
            'tipo_evaluacion' => '14',
            'unidad_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('contenidos')->insert([
            'objetivos' => 'Que el alumno conozca en forma correcta los elementos fundamentales y necesarios para poder desarrollar un partido de futbol',
            'contenidos' => 'Regla N° 5: El árbitro. Autoridad, poderes y decisiones',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico',
            'tipo_evaluacion' => '14',
            'unidad_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('contenidos')->insert([
            'objetivos' => 'Que el alumno conozca en forma correcta los elementos fundamentales y necesarios para poder desarrollar un partido de futbol',
            'contenidos' => 'Regla N°6 : El árbitro asistente: Autoridad, deberes y finalidad',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico',
            'tipo_evaluacion' => '14',
            'unidad_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        //UNIDAD 3
        DB::table('contenidos')->insert([
            'objetivos' => 'Que el alumno aplique de manera imparcial sus conocimientos',
            'contenidos' => 'Preparación psicofísica del equipo arbiral',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico',
            'tipo_evaluacion' => '14',
            'unidad_id' => '3',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('contenidos')->insert([                                                   #15
            'objetivos' => 'Que el alumno aplique de manera imparcial sus conocimientos',
            'contenidos' => 'Historia del Arbitraje Argentino y Misionero',
            'actividades' => 'Desarrollo, análisis, debate y trabajo práctico ',
            'tipo_evaluacion' => '14',
            'unidad_id' => '3',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('contenidos')->insert([
            'objetivos' => 'Que el alumno aplique de manera imparcial sus conocimientos',
            'contenidos' => 'Perfil del Arbitro Argentino : Virtudes y posibilidades de proyección',
            'actividades' => 'Desarrollo , análisis , comentarios y debate',
            'tipo_evaluacion' => '14',
            'unidad_id' => '3',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('contenidos')->insert([
            'objetivos' => 'Que el alumno aplique de manera imparcial sus conocimientos',
            'contenidos' => 'El arbitro y los demas actores del fútbol',
            'actividades' => 'Desarrollo , análisis , comentarios y debate',
            'tipo_evaluacion' => '14',
            'unidad_id' => '3',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        //UNIDAD 4 ↓
    }
}
