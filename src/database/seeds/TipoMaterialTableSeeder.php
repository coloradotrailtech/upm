<?php

use Illuminate\Database\Seeder;

class TipoMaterialTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::table('tiposmateriales')->insert([
            'nombre' => 'Martillo',
	        'familiamaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('tiposmateriales')->insert([
            'nombre' => 'Destornilladores',
	        'familiamaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        
    }

}
