<?php

use Illuminate\Database\Seeder;

class CursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cursos')->insert([
            'fecha_inicio' => date('Y-m-d H:m:s'),
            'fecha_fin' => date('Y-m-d H:m:s'),
            'sede_id' => '1',
            'titulo_id' => '14',
            'comision' => 'Turno tarde',
            'activa' => true,
            'descripcion' => 'lindo',
            'created_at' => date('Y-m-d H:m:s'),
        ]);

    }
}
