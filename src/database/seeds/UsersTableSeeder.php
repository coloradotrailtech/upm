<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'JP Cáceres',
            'rol_id'=>'1',
            'email' => 'jpcaceres.nea@gmail.com',
            'password' => bcrypt('123123'),
            'imagen' => 'perfil-jp.png',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('users')->insert([
            'name' => 'Hacho',
            'email' => 'hacho_k@outlook.com',
            'rol_id'=>'1',
            'password' => bcrypt('123123'),
            'imagen' => 'horacio_kuszniruk.jpg',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('users')->insert([
            'name' => 'Lisandro Blanco',
            'rol_id'=>'1',
            'email' => 'lisandro.blanco73@gmail.com',
            'imagen' => 'lisandro_blanco.jpg',
            'password' => bcrypt('123123'),
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('users')->insert([
            'name'=>'Laura Taid',
            'rol_id'=>'1',
            'email'=>'laura.taid@gmail.com',
            'imagen' => 'laura_taid.jpg',
            'password' => bcrypt('123123'),
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
