<?php

use Illuminate\Database\Seeder;

class PersonasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('personas')->insert([
            'nombre' => 'Juan Pablo',
            'apellido' => 'Cáceres',
            'sexo' => 'Masculino',
            'dni' => '34478385',
            'fecha_nac' => '1989-05-10',
            'telefono' => '3743499820',
            'email' => 'jpaulnava@gmail.com',
            'direccion' => 'Monteagudo 695',
            'foto_perfil' => 'sin_imagen.png',
            'localidad_id' => '1',
            'user_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('personas')->insert([
            'nombre' => 'Horacio Alejandro',
            'apellido' => 'Kuszniruk',
            'sexo' => 'Masculino',
            'dni' => '35777888',
            'fecha_nac' => '1991-12-16',
            'telefono' => '3752499820',
            'email' => 'hacho.kuszniruk@gmail.com',
            'direccion' => 'Corientes 2247',
            'foto_perfil' => 'sin_imagen.png',
            'localidad_id' => '5',
            'user_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('personas')->insert([ #PROPIETARIA DEPTO JUAMPY
            'nombre' => 'Lisandro',
            'apellido' => 'Blanco',
            'sexo' => 'masculino',
            'dni' => '34555666',
            'fecha_nac' => '1989-06-27',
            'telefono' => '3743002233',
            'email' => 'lisandro.blanco73@gmail.com',
            'direccion' => '3 de Febrero 1575',
            'foto_perfil' => 'sin_imagen.png',
            'localidad_id' => '1',
            'user_id' => '3',
            'created_at' => date('Y-m-d H:m:s')
        ]);


        DB::table('personas')->insert([
            'nombre' => 'Laura Noemi',
            'apellido' => 'Traid',
            'sexo' => 'Femenino',
            'dni' => '31872627',
            'fecha_nac' => '1987-11-15',
            'telefono' => '3743002233',
            'email' => 'laura.traid@gmail.com',
            'direccion' => 'Roque Pérez 387',
            'foto_perfil' => 'sin_imagen.png',
            'localidad_id' => '1',
            'user_id' => '4',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('personas')->insert([
            'nombre' => 'Juan',
            'apellido' => 'Camaño',
            'sexo' => 'masculino',
            'dni' => '33085694',
            'fecha_nac' => '1988-04-08',
            'telefono' => '3743002233',
            'email' => 'jcamano@gmail.com',
            'direccion' => 'Los Proceres 822',
            'foto_perfil' => 'sin_imagen.png',
            'localidad_id' => '1',

            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('personas')->insert([
            'nombre' => 'Camila',
            'apellido' => 'Venialgo',
            'sexo' => 'Femenino',
            'dni' => '36087955',
            'fecha_nac' => '1991-06-25',
            'telefono' => '3743002233',
            'email' => 'camilavenialgo@gmail.com',
            'direccion' => 'Carlos Culmey 1034',
            'foto_perfil' => 'sin_imagen.png',
            'localidad_id' => '1',

            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
