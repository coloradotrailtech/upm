<?php

use Illuminate\Database\Seeder;

class TitulacionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('titulaciones')->insert([
            'nombre' => 'Maestro Mayor de Obra',
            'nivel' =>'Pregrado',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulaciones')->insert([
            'nombre' => 'Profesorado en Ciencias Agrarias',
            'nivel' =>'Pregrado',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulaciones')->insert([
            'nombre' => 'Chef',
            'nivel' =>'Pregrado',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulaciones')->insert([
            'nombre' => 'Ingeniero Agrónomo',
            'nivel' =>'Grado',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulaciones')->insert([
            'nombre' => 'Arquitecto',
            'nivel' =>'Grado',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulaciones')->insert([
            'nombre' => 'Profesorado en Educación Física',
            'nivel' =>'Pregrado',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('titulaciones')->insert([
            'nombre' => 'Licenciado en Sistemas',
            'nivel' =>'Grado',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('titulaciones')->insert([
            'nombre' => 'Ingeniero Forestal',
            'nivel' =>'Grado',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulaciones')->insert([
            'nombre' => 'Ingeniero Electromecanico',
            'nivel' =>'Grado',
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
