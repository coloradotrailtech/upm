<?php

use Illuminate\Database\Seeder;

class ProfesoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profesores')->insert([
            'persona_id'=>'5',
            'titulacion_id'=>'9',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('profesores')->insert([
            'persona_id'=>'6',
            'titulacion_id'=>'6',
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
