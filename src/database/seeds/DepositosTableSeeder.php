<?php

use Illuminate\Database\Seeder;

class DepositosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        DB::table('depositos')->insert([
            'direccion' => 'Centenario 2044',
            'descripcion' => 'Depósito de la sede centro',
            'sede_id' => '1',
            'localidad_id' => '1',
            'habilitado' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('depositos')->insert([
            'direccion' => 'Km 2 Teresa de Calcuta',
            'descripcion' => 'Depósito de la sede Eldorado',
            'sede_id' => '3',
            'localidad_id' => '19',
            'habilitado' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('depositos')->insert([
            'direccion' => 'Córdoba 130',
            'descripcion' => 'Depósito de la sede Oberá',
            'sede_id' => '2',
            'localidad_id' => '3',
            'habilitado' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
