<?php

use Illuminate\Database\Seeder;

class AlumnosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alumnos')->insert([
            'curso_id' => '1',
            'estudiante_id' => '1',
            'created_at' => date('Y-m-d H:m:s'),
        ]);

        DB::table('alumnos')->insert([
            'curso_id' => '1',
            'estudiante_id' => '2',
            'created_at' => date('Y-m-d H:m:s'),
        ]);
    }
}
