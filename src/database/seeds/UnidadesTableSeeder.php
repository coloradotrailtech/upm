<?php

use Illuminate\Database\Seeder;

class UnidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* ARBITRO DE FUTBOL */
        DB::table('unidades')->insert([
            'nombre' => 'Introducción al curso. Elementos necesarios para disputar un partido de fútbol',
            'seh' => '',
            'auditable' => 'No',
            'modalidad_id' => '1',
            'numero_clase' => '1',
            'titulo_id' => '22',
            'carga_horaria' => '180',
            'unidad_tiempo' => 'minutos',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('unidades')->insert([
            'nombre' => 'Elementos y actores fundamentales que garantizan el juego',
            'seh' => '',
            'auditable' => 'No',
            'modalidad_id' => '1',
            'numero_clase' => '2',
            'titulo_id' => '22',
            'carga_horaria' => '180',
            'unidad_tiempo' => 'minutos',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('unidades')->insert([
            'nombre' => 'Elementos y actores fundamentales que garantizan el juego',
            'seh' => '',
            'auditable' => 'No',
            'modalidad_id' => '1',
            'numero_clase' => '3',
            'titulo_id' => '22',
            'carga_horaria' => '180',
            'unidad_tiempo' => 'minutos',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('unidades')->insert([
            'nombre' => 'Reglas que definen modalidades del juego',
            'seh' => '',
            'auditable' => 'No',
            'modalidad_id' => '1',
            'numero_clase' => '4',
            'titulo_id' => '22',
            'carga_horaria' => '180',
            'unidad_tiempo' => 'minutos',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('unidades')->insert([                             #5
            'nombre' => 'Reglas que definen modalidades del juego',
            'seh' => '',
            'auditable' => 'No',
            'modalidad_id' => '1',
            'numero_clase' => '5',
            'titulo_id' => '22',
            'carga_horaria' => '180',
            'unidad_tiempo' => 'minutos',
            'created_at' => date('Y-m-d H:m:s')
        ]);






        DB::table('unidades')->insert([
            'nombre' => 'Análisis de nuestro cuerpo. Moldes.',
            'seh' => '45',
            'auditable' => 'Sí',
            'modalidad_id' => '1',
            'numero_clase' => '1',
            'titulo_id' => '14',
            'carga_horaria' => '90', 
            'unidad_tiempo' => 'minutos',        
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
