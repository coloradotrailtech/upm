<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PaisesTableSeeder::class);
        $this->call(ProvinciasTableSeeder::class);
        $this->call(LocalidadesTableSeeder::class);
        $this->call(PersonasTableSeeder::class);
        $this->call(TitulacionesTableSeeder::class);    #titulos de los profesionales que dan cursos
        $this->call(CategoriasTableSeeder::class);      #categorias de los titulos que otorga UPM
        $this->call(TitulosTableSeeder::class);         #titulos que se otorgan en cursos de UPM
        $this->call(ProfesoresTableSeeder::class);
        $this->call(FamiliaMaterialTableSeeder::class);
        $this->call(TipoMaterialTableSeeder::class);
        $this->call(UmedidasTableSeeder::class);
        $this->call(MaterialesTableSeeder::class);
        $this->call(SedesTableSeeder::class);
        $this->call(DepositosTableSeeder::class);
        $this->call(MaterialesdepositosTableSeeder::class);
        $this->call(ModalidadesTableSeeder::class);
        $this->call(UnidadesTableSeeder::class);
        $this->call(ContenidosTableSeeder::class);
        $this->call(CursosTableSeeder::class);
        $this->call(EstudiantesTableSeeder::class);
        $this->call(AlumnosTableSeeder::class);
        $this->call(ProfesorescursosTableSeeder::class);
        $this->call(EstadosolicitudTableSeeder::class);
    }

}
