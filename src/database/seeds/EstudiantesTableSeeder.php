<?php

use Illuminate\Database\Seeder;

class EstudiantesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estudiantes')->insert([
            'persona_id'=>'1',           
            'created_at' => date('Y-m-d H:m:s')
        ]);  

        DB::table('estudiantes')->insert([
            'persona_id'=>'3',           
            'created_at' => date('Y-m-d H:m:s')
        ]); 
    }
}
