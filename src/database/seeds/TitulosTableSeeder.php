<?php

use Illuminate\Database\Seeder;

class TitulosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('titulos')->insert([  #1
            'nombre' => 'Pintor de Obra',
            'plan'=>'01-2019',
            'categoria_id' => '9',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulos')->insert([
            'nombre' => 'Soldador',
            'plan'=>'01-2019',
            'categoria_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulos')->insert([
            'nombre' => 'Auxiliar en Construcción',
            'plan'=>'01-2019',
            'categoria_id' => '9',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulos')->insert([
            'nombre' => 'Auxiliar en Instalaciones Sanitarias',
            'plan'=>'01-2019',
            'categoria_id' => '9',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulos')->insert([                              #5
            'nombre' => 'Reparador de PC',
            'plan'=>'01-2019',
            'categoria_id' => '3',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulos')->insert([
            'nombre' => 'Montador Electricista',
            'plan'=>'01-2019',
            'categoria_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
       

        DB::table('titulos')->insert([
            'nombre' => 'Pintura Decorativa',
            'plan'=>'01-2019',
            'categoria_id' => '10',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('titulos')->insert([
            'nombre' => 'Auxiliar el Electricidad Domiciliaria',
            'plan'=>'01-2019',
            'categoria_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('titulos')->insert([                              #10
            'nombre' => 'Confeccionista a Medida',
            'plan'=>'01-2019',
            'categoria_id' => '10',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulos')->insert([
            'nombre' => 'Vitrofusión',
            'plan'=>'01-2019',
            'categoria_id' => '6',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulos')->insert([
            'nombre' => 'Fotografia',
            'plan'=>'01-2019',
            'categoria_id' => '5',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('titulos')->insert([  #13
            'nombre' => 'Operador de PC',
            'plan'=>'01-2019',
            'categoria_id' => '3',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('titulos')->insert([
            'nombre' => 'Arbitro de Rugby',
            'plan'=>'01-2019',
            'categoria_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('titulos')->insert([
            'nombre' => 'Carpintería',
            'plan'=>'01-2019',
            'categoria_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);

//        DB::table('titulos')->insert([
//            'nombre' => 'Refrigeración',
//            'plan'=>'01-2019',
//'categoria_id' => null,
//            'created_at' => date('Y-m-d H:m:s')
//        ]);
        
        DB::table('titulos')->insert([
            'nombre' => 'Control de Plagas',
            'plan'=>'01-2019',
            'categoria_id' => '8',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulos')->insert([                          #18
            'nombre' => 'Jardineria, Poda y Huerta',
            'plan'=>'01-2019',
            'categoria_id' => '8',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulos')->insert([
            'nombre' => 'Huerta',
            'plan'=>'01-2019',
            'categoria_id' => '8',
            'created_at' => date('Y-m-d H:m:s')
        ]);
       
        DB::table('titulos')->insert([
            'nombre' => 'Telar de Peine',
            'plan'=>'01-2019',
            'categoria_id' => '6',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('titulos')->insert([
            'nombre' => 'Porcelana Fría',
            'plan'=>'01-2019',
            'categoria_id' => '6',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('titulos')->insert([
            'nombre' => 'Cesteria Ecológica',
            'plan'=>'01-2019',
            'categoria_id' => '6',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulos')->insert([
            'nombre' => 'Panadería',
            'plan'=>'01-2019',
            'categoria_id' => '7',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('titulos')->insert([                          #25
            'nombre' => 'Arbitro de Fútbol',
            'plan'=>'01-2019',
            'categoria_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
