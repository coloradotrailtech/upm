<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('categorias')->insert([
            'nombre' => 'Deportes',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Trabajos para Hogar',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('categorias')->insert([   #3
            'nombre' => 'Informatica',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Seguridad Laboral',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Recreación',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('categorias')->insert([   #6
            'nombre' => 'Artesanal',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Cocina y Reposteria',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Jardineria y Horticultura',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('categorias')->insert([   #9
            'nombre' => 'Trabajo en Obras',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('categorias')->insert([
            'nombre' => 'Diseño',
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }

}
