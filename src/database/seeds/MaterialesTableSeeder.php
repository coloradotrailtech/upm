<?php

use Illuminate\Database\Seeder;

class MaterialesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('materiales')->insert([
            'nombre' => 'Martillo neumático',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        
        DB::table('materiales')->insert([
            'nombre' => 'Destornillador phillips',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        
        DB::table('materiales')->insert([
            'nombre' => 'Serrucho',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        
        DB::table('materiales')->insert([
            'nombre' => 'Sierra',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        
        DB::table('materiales')->insert([               #5
            'nombre' => 'Martillo',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        
        DB::table('materiales')->insert([
            'nombre' => 'Tornillo autorroscante',
            'umedida_id' => '1',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        
        DB::table('materiales')->insert([
            'nombre' => 'Llave inglesa',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        
        DB::table('materiales')->insert([
            'nombre' => 'Pinza',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        
        DB::table('materiales')->insert([
            'nombre' => 'Botella de aceite 1L',
            'umedida_id' => '1',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        
        DB::table('materiales')->insert([               #10
            'nombre' => 'Tijeras',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Pizarra',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Fibron',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Bolígrafo',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Hoja A4',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('materiales')->insert([               #15
            'nombre' => 'Hoja A3',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Hoja Oficio',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Cañon de Proyección',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Bowl',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Cuchara de Madera',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([               #20
            'nombre' => 'Vaso Medidor',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Diario',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Revista',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Camilla',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Pelota Fútbol',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([               #25
            'nombre' => 'Pelota Rugby',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Pelota Voley',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Pelota Basquet',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Pelota Tenis',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')     #28
        ]);



        /* Insumos ↓*/
        DB::table('materiales')->insert([
            'nombre' => 'Extracto de Malta',
            'umedida_id' => '5',
            'tipomaterial_id' => '1',       #1: insumo | 2: herramienta
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([                   #30
            'nombre' => 'Manteca',
            'umedida_id' => '5',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Margarina',
            'umedida_id' => '5',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Levadura',
            'umedida_id' => '1',
            'tipomaterial_id' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('materiales')->insert([
            'nombre' => 'Harina Integral',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Harina Harina de Centeno',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Harina 000',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Harina 0000',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Harina 00000',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Sal Fina',
            'umedida_id' => '5',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Sal Gruesa',
            'umedida_id' => '5',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Azúcar',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Grasa Vacuna',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Escencia de Vainilla',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Polvo para Hornear',
            'umedida_id' => '5',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Leche',
            'umedida_id' => '19',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Membrillo',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Batata',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Queso Cremoso',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Dulce de Leche',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Ricota',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Fécula de Maíz',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Fécula de Mandioca',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Coco rallado',
            'umedida_id' => '5',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('materiales')->insert([
            'nombre' => 'Aceite de Girasol',
            'umedida_id' => '19',           #19: litro
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Aceite de Oliva',
            'umedida_id' => '19',           #19: litro
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Tomate',
            'umedida_id' => '2',           #2: kilo
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Papa',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Cebolla',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Zapallo',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Huevo',
            'umedida_id' => '1',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('materiales')->insert([
            'nombre' => 'Ceréza',
            'umedida_id' => '2',
            'tipomaterial_id' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);




    }

}
