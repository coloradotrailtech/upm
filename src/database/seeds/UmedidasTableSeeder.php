<?php

use Illuminate\Database\Seeder;

class UmedidasTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */



    public function run() {

//      Unidad básica
        DB::table('umedidas')->insert([
            'nombre' => 'Unidad',
            'tipoMedida' => 'Unidad básica',
            'abreviatura' => 'u',            
            'created_at' => date('Y-m-d H:m:s')
        ]);


//      Unidades de medida de masa.

        DB::table('umedidas')->insert([
            'nombre' => 'Kilo',
            'tipoMedida' => 'Masa',
            'abreviatura' => 'kg',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Hectogramo',
            'tipoMedida' => 'Masa',
            'abreviatura' => 'hg',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Decagramo',
            'tipoMedida' => 'Masa',
            'abreviatura' => 'dag',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Gramo',
            'tipoMedida' => 'Masa',
            'abreviatura' => 'g',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Decigramo',
            'tipoMedida' => 'Masa',
            'abreviatura' => 'dg',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Centigramo',
            'tipoMedida' => 'Masa',
            'abreviatura' => 'cg',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Miligramo',
            'tipoMedida' => 'Masa',
            'abreviatura' => 'mg',
            'created_at' => date('Y-m-d H:m:s')
        ]);

//        Unidades de medida de longitud.

        DB::table('umedidas')->insert([
            'nombre' => 'Kilómetro',
            'tipoMedida' => 'Longitud',
            'abreviatura' => 'km',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Hectómetro',
            'tipoMedida' => 'Longitud',
            'abreviatura' => 'hm',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Decámetro',
            'tipoMedida' => 'Longitud',
            'abreviatura' => 'dam',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Metro',
            'tipoMedida' => 'Longitud',            
            'abreviatura' => 'm',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Decímetro',
            'tipoMedida' => 'Longitud',            
            'abreviatura' => 'dm',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Centímetro',
            'tipoMedida' => 'Longitud',            
            'abreviatura' => 'cm',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Milímetro',
            'tipoMedida' => 'Longitud',            
            'abreviatura' => 'mm',
            'created_at' => date('Y-m-d H:m:s')
        ]);

//        Unidad de medida de capacidad.

        DB::table('umedidas')->insert([
            'nombre' => 'Kilolitro',
            'tipoMedida' => 'Capacidad',            
            'abreviatura' => 'kl',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Hectolitro',
            'tipoMedida' => 'Capacidad',            
            'abreviatura' => 'hl',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Decalitro',
            'tipoMedida' => 'Capacidad',            
            'abreviatura' => 'dal',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Litro',
            'tipoMedida' => 'Capacidad',            
            'abreviatura' => 'l',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Decilitro',
            'tipoMedida' => 'Capacidad',            
            'abreviatura' => 'dl',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Centilitro',
            'tipoMedida' => 'Capacidad',            
            'abreviatura' => 'cl',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Mililitro',
            'tipoMedida' => 'Capacidad',            
            'abreviatura' => 'ml',
            'created_at' => date('Y-m-d H:m:s')
        ]);

//        Unidad de medida de superficie.

        DB::table('umedidas')->insert([
            'nombre' => 'Kilómetro cuadrado',
            'tipoMedida' => 'Superficie',            
            'abreviatura' => 'km²',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Hectómetro cuadrado',
            'tipoMedida' => 'Superficie',            
            'abreviatura' => 'hm²',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Decámetro cuadrado',
            'tipoMedida' => 'Superficie',            
            'abreviatura' => 'dam²',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Metro cuadrado',
            'tipoMedida' => 'Superficie',            
            'abreviatura' => 'm²',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Decímetro cuadrado',
            'tipoMedida' => 'Superficie',            
            'abreviatura' => 'dm²',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Centímetro cuadrado',
            'tipoMedida' => 'Superficie',            
            'abreviatura' => 'cm²',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Milímetro cuadrado',
            'tipoMedida' => 'Superficie',            
            'abreviatura' => 'mm²',
            'created_at' => date('Y-m-d H:m:s')
        ]);

//        Unidad de medida de volumen.

        DB::table('umedidas')->insert([
            'nombre' => 'Kilómetro cúbico',
            'tipoMedida' => 'Volumen',            
            'abreviatura' => 'km³',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Hectómetro cúbico',
            'tipoMedida' => 'Volumen',            
            'abreviatura' => 'hm³',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Decámetro cúbico',
            'tipoMedida' => 'Volumen',            
            'abreviatura' => 'dam³',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Metro cúbico',
            'abreviatura' => 'm³',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Decímetro cúbico',
            'tipoMedida' => 'Volumen',            
            'abreviatura' => 'dm³',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Centímetro cúbico',
            'tipoMedida' => 'Volumen',            
            'abreviatura' => 'cm³',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('umedidas')->insert([
            'nombre' => 'Milímetro cúbico',
            'tipoMedida' => 'Volumen',            
            'abreviatura' => 'mm³',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        
    }

}
