<?php

use Illuminate\Database\Seeder;

class FamiliaMaterialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('familiasmateriales')->insert([
            'nombre' => 'Insumo',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('familiasmateriales')->insert([
            'nombre' => 'Herramienta',
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
