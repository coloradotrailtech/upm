<?php

use Illuminate\Database\Seeder;

class SedesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sedes')->insert([
            'localidad_id' => '1',
            'movil' => '0',
            'direccion' => 'Centenario 2044',
            'activa' => '1',
            'central'=>'1',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('sedes')->insert([
            'localidad_id' => '3',
            'direccion' => 'Córdoba 130',
            'movil' => '0',
            'activa' => '1',
            'central'=>'0',
            'created_at' => date('Y-m-d H:m:s')
        ]);
        DB::table('sedes')->insert([
            'localidad_id' => '19',
            'direccion' => 'Km 2 Teresa de Calcuta',
            'activa' => '1',
            'movil' => '0',
            'central'=>'0',
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
