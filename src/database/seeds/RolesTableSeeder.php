<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'nombre' => 'Admin',
            'nivel_acceso' => '1',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('roles')->insert([
            'nombre' => 'Encargado de Sede',
            'nivel_acceso' => '2',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('roles')->insert([
            'nombre' => 'Encargado de Deposito',
            'nivel_acceso' => '3',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('roles')->insert([
            'nombre' => 'Bedel',
            'nivel_acceso' => '4',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('roles')->insert([
            'nombre' => 'Responsable Aulas Moviles',
            'nivel_acceso' => '4',
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
