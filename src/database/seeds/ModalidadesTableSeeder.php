<?php

use Illuminate\Database\Seeder;

class ModalidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modalidades')->insert([
            'nombre' => 'Defensa oral',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('modalidades')->insert([
            'nombre' => 'Teoría escrito',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('modalidades')->insert([
            'nombre' => 'Práctico escrito',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('modalidades')->insert([
            'nombre' => 'Teórico-Práctico escrito',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('modalidades')->insert([
            'nombre' => 'Teoría',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('modalidades')->insert([
            'nombre' => 'Taller',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('modalidades')->insert([
            'nombre' => 'Teoría-Taller',
            'created_at' => date('Y-m-d H:m:s')
        ]);

        DB::table('modalidades')->insert([
            'nombre' => 'Trabajo de campo',
            'created_at' => date('Y-m-d H:m:s')
        ]);

    }
}
