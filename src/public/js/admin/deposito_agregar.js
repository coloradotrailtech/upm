$("#side-ele-depositos").addClass("active");

//Datatable
var table = $('#datatable-depositos').DataTable({
    language: tabla_traducida,
    responsive: false
});

var lista_update = [];

function carga_lista_update(id) {
    /**
     * Este proceso se encarga de registrar y actualizar cada vez que el usuario escribe un dato nuevo 
     * en el campo de corrección de unidades.
     */

    cantidad = parseFloat($('#input-cantidad-' + id).val());

    if (cantidad > -1) {
        if (cantidad < 1000000) {
            if ($('#input-cantidad-' + id).val() !== '') {
                if ((id, cantidad)) {
                    lista_update[id] = {
                        id: id,
                        cantidad: cantidad
                    };
                }
            } else {
                lista_update.splice(id, 1);
            }
        } else {
            Swal.fire({
                type: 'error',
                title: 'Atención',
                text: 'El stock final no puede ser mayor a un millón'
            });
            $('#input-cantidad-' + id).val("");
        }
    } else if ($('#input-cantidad-' + id).val() !== '') {
        Swal.fire({
            type: 'error',
            title: 'Atención',
            text: 'El stock final del meterial no puede ser negativo'
        });
        $('#input-cantidad-' + id).val("");
    }

    console.log(lista_update);
}

function mandar_lista_update(id_deposito) {
    if (lista_update.length > 0) { // si la lista está vacía se informa al usuario al respecto

        var lista = [];
        lista_update.forEach(function (valor) { // se convierte el array en una coleccion de objetos
            lista.push(valor)
        });

        Swal.fire({
            title: 'Atención',
            text: "¿Se encuentra seguro?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'agregar',
            cancelButtonText: 'cancelar'
        }).then((result) => {
            if (result.value) {
                $.ajax({// se envía
                    url: '/agregar_existencia',
                    data: {
                        id_deposito: id_deposito,
                        lista: lista
                    },
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        window.location.href = '/depositos/' + id_deposito;
                    }
                });
            }
        });
    } else {
        Swal.fire({
            type: 'error',
            title: 'Atención',
            text: 'Ningún material fue incorporado'
        });
    }
}


