/**
 * Created by jpaul on 08/01/2019.
 */
//Esto es para que el sistema deje seleccionada la opcion en la barra menu de la izquierda
$("#side-ele-encargados").addClass("active");

/*
function completar_campos(encargado) {
    $('#nombre').val(encargado.nombre); $('#apellido').val(encargado.apellido);
    $('#titulacion_id').val(encargado.titulacion_id).trigger("change");
    $('#localidad_id').val(encargado.localidad_id).trigger("change");
    $('#dni').val(encargado.dni); $('#email').val(encargado.persona.email); $('#dni').val(encargado.dni);
    $('#form-update').attr('action', '/localidades/' + localidad.id);
    $('#modal-update').modal('show');
}
 */
function abrir_modal_borrar(id) {
    $('#form-borrar').attr('action', '/encargados/' + id);
    $('#modal-borrar').modal('show');
}

//Datatable
var table = $('#encargados').DataTable({
    language: tabla_traducida,
    responsive: true
});

//Datatables | filtro individuales - instanciación de los filtros
$('#encargados tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title !== 'Acciones') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});

//Datatables | asocio el evento sobre el body de la tabla para que resalte fila y columna
$('#datatable-encargados tbody').on('mouseenter', 'td', function () {
    var colIdx = table.cell(this).index().column;
    $(table.cells().nodes()).removeClass('highlight');
    $(table.column(colIdx).nodes()).addClass('highlight');
});