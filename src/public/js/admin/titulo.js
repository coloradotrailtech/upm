$("#side-ele-titulos").addClass("active");

function completar_campos(titulo) {
    $('#nombre').val(titulo.nombre);
    $('#plan').val(titulo.plan);
    $('#categoria_id').val(titulo.categoria_id).trigger("change");
    $('#form-update').attr('action', '/titulos/' + titulo.id);
    $('#modal-update').modal('show');
}

function abrir_modal_borrar(id) {
    $('#form-borrar').attr('action', '/titulos/' + id);
    $('#modal-borrar').modal('show');
}

function abrir_modal_borrar_recurso(id) {
    $('#form-borrar-recurso').attr('action', '/destroy_recurso/' + id);
    $('#modal-borrar-recurso').modal('show');
}

function abrir_modal_borrar_unidad(id) {
    $('#form-borrar').attr('action', '/unidades/' + id);
    $('#modal-borrar').modal('show');
}

//Datatable
var table = $('#datatable-titulos').DataTable({
    language: tabla_traducida,
    responsive: true
});

//Datatables | filtro individuales - instanciación de los filtros
$('#datatable-titulos tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title !== 'Acciones') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        } else {
            $(this).html('');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});

//Datatables | ocultar/visualizar columnas dinámicamente
$('a.toggle-vis').on('click', function (e) {
    e.preventDefault();
    // Get the column API object
    var column = table.column($(this).attr('data-column'));
    // Toggle the visibility
    column.visible(!column.visible());
    instaciar_filtros();
});

//Datatables | asocio el evento sobre el body de la tabla para que resalte fila y columna
$('#datatable-titulos tbody').on('mouseenter', 'td', function () {
    var colIdx = table.cell(this).index().column;
    $(table.cells().nodes()).removeClass('highlight');
    $(table.column(colIdx).nodes()).addClass('highlight');
});

$('.selectpicker').selectpicker();

function redireccionar_editar_unidad(id) {
    window.location.href = '/unidades/' + id + '/edit';
}

$(document).ready(function () {
    instanciar_sort_list();
});


function instanciar_sort_list() {

    let resguardo = $(".todo-list").html();

    $(".todo-list").on("sortupdate", function (event, ui) {
        Swal.fire({
            title: 'Atención',
            text: "¿Desea registrar el nuevo orden de su planificación de clases ahora?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'actualizar',
            cancelButtonText: 'cancelar'
        }).then((result) => {
            if (result.value) {
                let planificacion_clases = [];
                $(this).find('.todo-item').each(function (i) {
                    planificacion_clases.push({
                        unidad_id: $(this).attr('id_unidad'),
                        numero_clase: i + 1
                    });
                });
                enviar_orden_planificacion(planificacion_clases);
            } else {
                $(".todo-list").html(resguardo);
            }
        });
    });
}

function enviar_orden_planificacion(planificacion_clases) {
    $.ajax({ // se envía
        url: '/ordenar_planificacion',
        data: {
            titulo_id: $('#titulo_id').val(),
            planificacion_clases: planificacion_clases,
        },
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#row_unidades').html(data.html);
            instanciar_sort_list();
            Swal.fire("¡Exito!", "La planificación fue actualizada", "success");
        }
    });
}


$(".datepicker").datepicker({
    format: "mm-yyyy",
    startView: "years",
    minViewMode: "months",
    autoclose: true
});


//Exportar pdf

async function exportar_pdf (planif){
    var las_unidades = []
    las_unidades = planif['unidades']
    
    var carga_total = 0;
    var carga_clase = 0;

    var cuerpo = [[`#`,`Clase`,`Tiempo`]];
    var clase;

    las_unidades.forEach((e,i)=>{
        carga_clase = parseInt(e.carga_horaria);
        carga_total = carga_total + carga_clase;
        carga_clase = 0;

        clase = [i+1,e.nombre,`${e.carga_horaria} min.`]
        cuerpo.push(clase)
    })
    carga_total = carga_total/60;
    
    const doc_Content = {
        content: [
            {
                alignment: 'right', 
                margin: [0, 0, 0, 5],
                image: imagenes.logo_upm, 
                width: 130
            },
            { 
              text:  `Oficio ${planif['nombre']} Plan - ${planif['plan']}`, style: 'header' 
            },
            {
                text:`\nCategoría: ${planif.categoria['nombre']} \n 
                Carga horaria: ${carga_total} Hs \n 
                Cantidad clases: ${las_unidades.length} \n \n`, 
                style: 'myStyle'
            },
            {
                layout: 'lightHorizontalLines', // optional
                table: {
                    headerRows: 1,
                    widths: [ 25, '*', 100],
                    body: cuerpo
                }
            }
        ],
      
        styles: {
          header: {
            fontSize: 16,
            bold: true,
            //lineHeight: 5
          },
          myStyle: {
            italics: false,
            alignment: 'left'
          }
        }
    };

    pdfMake.createPdf(doc_Content).download(`Oficio ${planif['nombre']} - Plan ${planif['plan']}`);

}


//Caratula Libro Aula
async function libro_aula (planif){   

    console.log(planif)
    
    var doc_Content = {
        pageSize: 'A4',
        pageOrientation: 'landscape',
        pageMargins: [30, 30, 40, 30],
        content:[
            {
                columns:[
                    {
                        width: 40,
                        height: 520,
                        image: logos.la_img_izq
                    },
                    {
                        table: {
                            widths: ['*'],
                            body: [
            
                                [
                                    {
                                        margin: [0, 50, 0, 0],
                                        border: [false, false, false, false],
                                        alignment: 'center',
                                        image: logos.la_img_logo,
                                        width: 230
                                    }
                                ],
                                [
                                    {
                                        margin: [0, 50, 0, 0],
                                        border: [false, false, false, false],
                                        alignment: 'center',
                                        fontSize: 20,
                                        lineHeight: 1.5,
                                        bold: true,
                                        text: `${planif['nombre']} Plan - ${planif['plan']}`
                                    }
                                ]
                            ]
                        }
                    },
                    {
                        margin: [11, 0, 0, 0],
                        width: 30,
                        height: 520,
                        image: logos.la_img_der
                    }       
                ]
            }
        ]
        ,
        styles:[

        ]
    }

    pdfMake.createPdf(doc_Content).download(`Caratula Libro Aula ${planif['nombre']} - Plan ${planif['plan']}`);
}




async function all_libro_aula (curso){
    var f_ini = moment(curso.fecha_inicio).format('D/MM/YYYY');
    var f_fin = moment(curso.fecha_fin).format('D/MM/YYYY');
    var aux   = ``;

    curso.profes.forEach((e,i) => {
        aux = aux + ` ${e.apellido.toUpperCase()}, ${e.nombre.toUpperCase()}  |`
    });
    
    var doc_Content = {
        pageSize: 'A4',
        pageOrientation: 'landscape',
        pageMargins: [0,20,0,0],
        content:[
            {
                columns: [
                    {
                        table: {
                            widths: ['*'],
                            body: [
            
                                [
                                    {
                                        margin: [0, 0, 0, 0],
                                        border: [false, false, false, false],
                                        alignment: 'center',
                                        image: logos.la_img_logo,
                                        width: 170
                                    }
                                ],
                                [
                                    {
                                        margin: [60, 5, 60, 0],
                                        border: [false, false, false, false],
                                        alignment: 'justify',
                                        fontSize: 13,
                                        lineHeight: 1,
                                        bold: true,
                                        text: `Sede ${curso.sede.localidad.nombre} | ${curso.titulo.nombre} ${curso.titulo.plan}  | ${curso.comision} |  \n Profesores: ${aux} \n Inicio ${f_ini} - Fin ${f_fin} `
                                    }
                                ],
                                [
                                    {
                                        margin: [0, 0, 0, 0],
                                        border: [false, false, false, false],
                                        alignment: 'center',
                                        fontSize: 12,
                                        lineHeight: 1.2,
                                        bold: true,
                                        decoration: 'underline',
                                        text: `INSTRUCCIONES PARA EL USO DEL LIBRO DE AULA:`
                                    }
                                ],
                                [
                                    {
                                        margin: [60, 0, 60, 0],
                                        border: [false, false, false, false],
                                        alignment: 'justify',
                                        fontSize: 12,
                                        lineHeight: 1,
                                        bold: true,
                                        text: `Estimados instructores, el libro de aula es un elemento pedagógico de gran valor didáctico e informativo,  por tal razón, se solicita a los señores instructores el estricto cumplimiento de estas instrucciones dispuestas para apreciar mejor su valor.`
                                    }
                                ]
                            ]
                        }
                    },
                    
                ]
            },
            {
                margin: [65, 5, 60, 0],
                text: 'Se deberá tener en cuenta al completar el libro de aula que:'
            },
            {
                margin: [65, 5, 60, 0],
                alignment: 'justify',
                ul: [
                    '1. La letra a utilizar sea CLARA y que se cuide en todo momento la PROLIJIDAD, respetando las reglas ortográficas, evitando las tachaduras o enmiendas, o el uso de corrector, ya que pueden confundir a quien deba leer e interpretar la información que ha escrito.',
                    '2. Se debe explicitar la fecha de clase, numero de clase, contenidos/temas, actividades, evaluación  taller o practica en obra, evaluación y firma.',
                    '3. En la columna de HORARIO DE ENTRADA Y SALIDA se completara correctamente el inicio finalización de la clase, taller o práctica en obra. Teniendo cuidado de no tacharlos, remarcar o falsear la información brindada porque de eso dependerá la percepción de los honorarios de cada instructor.',
                    '4. Temas/contenidos: (¿Qué temas aborde?) explicitar los temas desarrollados  en dichas clases, se debe ser sintético, concreto y reflejar la realidad de lo trabajado',
                    '5. Actividades desarrolladas: (¿Cómo?) se debe escribir que actividad que realizaron los alumnos en clases, taller, o practica en obra, para lograr los temas planificados.',
                    '6. Observaciones: está destinada a registrar toda aquella información que el instructor considere que sea oportuno explicitar como novedad sugeridad durante la clase, taller o practica en obra.',
                    '7. Se anexa planilla listado de alumnos de asistencia.',
                    '8. Se anexa planillas de calificaciones con listado de alumnos: explicitar las notas de las evaluaciones, recuperatorio, práctica en obra, calificación final.',
                    '9. Se anexa planilla para informes de avances del curso del instructor del oficio  base a la planificación propuesta.',
                    '10. Se anexa planilla de informe de obras del instructor de seguridad e higiene laboral.'
                ]
            },
        ]
    }
    
    pdfMake.createPdf(doc_Content).download(`Instrucciones Libro Aula ${curso.titulo.nombre}`);

}

