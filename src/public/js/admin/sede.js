$("#side-ele-sedes").addClass("active");

function completar_campos(sede) {
    $('#direccion').val(sede.direccion);
    $('#movil').val(sede.direccion);
    $('#direccion').val(sede.direccion);
    $('#localidad_id').val(sede.localidad_id).trigger("change");
    $('#descripcion').val(sede.descripcion);
    $('#form-update').attr('action', '/sedes/' + sede.id);
    $('#modal-update').modal('show');
}

function abrir_modal_borrar(id) {
    $('#form-borrar').attr('action', '/sedes/' + id);
    $('#modal-borrar').modal('show');
}

//Datatable
var table = $('#datatable-sedes').DataTable({
    language: tabla_traducida,
    responsive: true
});

//Datatables | filtro individuales - instanciación de los filtros
$('#datatable-sedes tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title !== 'Acciones') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        }else{
            $(this).html('');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});

//Datatables | asocio el evento sobre el body de la tabla para que resalte fila y columna
$('#datatable-sedes tbody').on('mouseenter', 'td', function () {
    var colIdx = table.cell(this).index().column;
    $(table.cells().nodes()).removeClass('highlight');
    $(table.column(colIdx).nodes()).addClass('highlight');
});