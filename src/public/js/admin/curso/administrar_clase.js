$("#side-ele-cursos").addClass("active");

//Datatable
var table = $('#datatable-clases').DataTable({
    language: tabla_traducida,
    responsive: false,
    // "paging": false,
    "ordering": false,
    //   "searching": false
});

//Datatables | filtro individuales - instanciación de los filtros
$('#datatable-examenes tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title !== 'Acciones') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});

//Datatables | asocio el evento sobre el body de la tabla para que resalte fila y columna
$('#datatable-examenes tbody').on('mouseenter', 'td', function () {
    var colIdx = table.cell(this).index().column;
    $(table.cells().nodes()).removeClass('highlight');
    $(table.column(colIdx).nodes()).addClass('highlight');
});

$('.selectpicker').selectpicker();
$('#profesores').val("").trigger("change");

$(".datepicker").datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    language: 'es'
});


var lista_inasistencias = [];

function cambiar_asistencia(id) {
    if ($('#check-asistencia-' + id).prop('checked')) {
        $('#input-motivo-' + id).attr('disabled', true);
        $('#input-motivo-' + id).val("");
        lista_inasistencias.splice(id, 1);
    }else{
        $('#input-motivo-' + id).attr('disabled', false);
        carga_motivo_inasistencia(id);
    }
}


function carga_motivo_inasistencia(id) {
    let motivo_inasistencia = $('#input-motivo-' + id).val();
    lista_inasistencias[id] = {
        alumno_id: id,
        motivo_inasistencia: motivo_inasistencia
    }
}


function mandar_lista(id_curso) {

    let profesores = $("#profesores").val();
    let fecha = $("#fecha").val();
    let unidad_id = $("#unidad_id").val();

    if (profesores.length > 0 && fecha !== '' && unidad_id !== '') {
     
        let lista_ina = [];

        lista_inasistencias.forEach(function (valor) { // se convierte el array en una coleccion de objetos
            lista_ina.push(valor)
        });

        Swal.fire({
            title: 'Atención',
            text: "¿Se encuentra seguro?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#489E48',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'registrar',
            cancelButtonText: 'cancelar'
        }).then((result) => {
            if (result.value) {
                $.ajax({// se envía
                    url: '/registrar_clase',
                    data: {
                        unidad_id: unidad_id,
                        curso_id: id_curso,
                        fecha: fecha,
                        profesores: profesores,
                        lista_ina: lista_ina
                    },
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        window.location.href = '/cursos/' + id_curso;
                    }
                });
            }
        });
    } else {
        let msj_error = "";
        if (fecha == '') {
            msj_error = "No seleccionó la fecha de la clase";
        } else if (unidad_id === '') {
            msj_error = "No seleccionó ninguna unidad a desarrollar en la clase";
        } else if (profesores.length == 0) {
            msj_error = "No seleccionó ningún profesor";
        } 
        Swal.fire({
            type: 'error',
            title: 'Atención',
            text: msj_error
        });
    }
}