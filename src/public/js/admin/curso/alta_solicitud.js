$("#side-ele-curso").addClass("active");

//Datatable
var table = $('#datatable-solicitud').DataTable({
    language: tabla_traducida,
    responsive: false
});
//Datatables | filtro individuales - instanciación de los filtros
$('#datatable-solicitud tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title !== 'Acciones') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});

//Datatables | asocio el evento sobre el body de la tabla para que resalte fila y columna
$('#datatable-depositos tbody').on('mouseenter', 'td', function () {
    var colIdx = table.cell(this).index().column;
    $(table.cells().nodes()).removeClass('highlight');
    $(table.column(colIdx).nodes()).addClass('highlight');
});

$('.selectpicker').selectpicker();

var lista_materiales = [];

function carga_lista(id) {
    /**
     * Este proceso se encarga de registrar y actualizar cada vez que el usuario escribe un dato nuevo 
     * en el campo de corrección de unidades.
     */

    if ($('#input-cantidad-' + id).val() === "") {
        lista_materiales.splice(id, 1);
    } else {
        cantidad = parseFloat($('#input-cantidad-' + id).val());
        if (cantidad > 0) {
            lista_materiales[id] = {
                materialtitulo_id: id,
                cantidad: cantidad
            }
        } else {
            lista_materiales.splice(id, 1);
        }
    }
}

function mandar_lista(id) {
    if (lista_materiales.length > 0) { // si la lista está vacía se informa al usuario al respecto
        Swal.fire({
            title: 'Enviar solicitud',
            text: "¿Se encuentra seguro de enviar la solicitud?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#449D44',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'enviar',
            cancelButtonText: 'cancelar'
        }).then((result) => {
            if (result.value) {

                let lista = [];
                lista_materiales.forEach(function (valor) { // se convierte el array en una coleccion de objetos
                    lista.push(valor)
                });

                $.ajax({ // se envía
                    url: '/registrar_solicitud',
                    data: {
                        curso_id: id,
                        lista: lista
                    },
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        window.location.href = '/cursos/' + id;
                    }
                });
            }
        });
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Atención',
            text: 'No se indicó ninguna cantidad a solicitar'
        });
    }
}