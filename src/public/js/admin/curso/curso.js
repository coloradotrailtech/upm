$("#side-ele-cursos").addClass("active");
$(document).ready(function(){
    $(".sidebar-toggle").click();
});

//Date range picker
$(".datepicker").datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    language: 'es'
});

$('.selectpicker').selectpicker('deselectAll', { language: 'ES' });

function completar_campos(curso) {

    let fecha_inicio = moment(curso.fecha_inicio,'DD/MM/YYYY').format('YYYY-MM-DD');
    let fecha_fin = moment(curso.fecha_fin,'DD/MM/YYYY').format('YYYY-MM-DD');

    $('#sede_id-update').val(curso.sede_id).trigger("change");
    $('#titulo_id-update').val(curso.titulo_id).trigger("change");
    $('#fecha_inicio').val(fecha_inicio);
    $('#fecha_fin').val(fecha_fin);
    $('#titulares-update').val(curso.titulares).trigger("change");
    $('#suplentes-update').val(curso.suplentes).trigger("change");
    $('#comision-update').val(curso.comision);
    $('#descripcion_curso').val(curso.descripcion);
    if (curso.activa === 1) {
        $('#activa').bootstrapToggle('on');
    }
    $('#form-update').attr('action', '/cursos/' + curso.id);
    $('#modal_update_data').modal('show');
}

function completar_campos_examen(examen) {
    let fecha_examen = moment(examen.fecha,'DD/MM/YYYY').format('YYYY-MM-DD')
    $('#fecha_examen').val(fecha_examen);
    $('#modalidad_examen').val(examen.modalidad).trigger("change");    
    $('#unidad_id_examen').val(examen.unidades).trigger("change");
    $('#profesores_examen').val(examen.ids_profesores).trigger("change");
    $('#form-update-examenes').attr('action', '/examenes/' + examen.id);
    $('#modal-update-examenes').modal('show');
}

function completar_campos_clase(clase) {
    let fecha_clase = moment(clase.fecha, "DD/MM/YYYY").format('YYYY-MM-DD')
    $('#fecha_miclase').val(fecha_clase);
    $('#unidad_id_clase').val(clase.unidad_id).trigger("change");
    $('#profesores_clase').val(clase.ids_profesores).trigger("change");
    $('#form-update-clases').attr('action', '/clases/' + clase.id);
    $('#modal-update-clases').modal('show');
}

function completar_campos_asistencia(asistencia) {
    $('#alumno_asistencia').html(asistencia.alumno);
    $('#motivo_inasistencia_asistencia').val(asistencia.motivo_inasistencia);
    if (asistencia.asistio === 1) {
        $('#asistio_asistencia').bootstrapToggle('on');
    }
    cambiar_asistencia('clase');
    $('#form-update-asistencias').attr('action', '/clases_alumnos/' + asistencia.id);
    $('#modal-update-asistencias').modal('show');
}

function completar_campos_nota(nota) {
    $('#alumno_nota').html(nota.alumno);
    $('#motivo_inasistencia_nota').val(nota.motivo_inasistencia);
    if (nota.asistio === 1) {
        $('#asistio_nota').bootstrapToggle('on');
    }
    $('#situacion-nota').val(nota.situacion).trigger("change");
    cambiar_asistencia('nota');
    $('#form-update-notas').attr('action', '/examenes_alumnos/' + nota.id);
    $('#modal-update-notas').modal('show');
}

function cambiar_asistencia(seccion) {
    if (seccion == "nota") {
        if($('#asistio_nota').prop('checked')){
            $('#nota_nota').attr('disabled', false);
            $('#motivo_inasistencia_nota').attr('disabled', true);
        }else{
            $('#nota_nota').attr('disabled', true);
            $('#motivo_inasistencia_nota').attr('disabled', false);
        }
    } else if (seccion == "clase") {
        if($('#asistio_asistencia').prop('checked')){
            $('#motivo_inasistencia_asistencia').attr('disabled', true);
        }else{
            $('#motivo_inasistencia_asistencia').attr('disabled', false);
        }
    }
}

function completar_campos_estudiante(persona) {

    //Formatear fecha
    let fecha_nac_formateada = moment(persona.fecha_nac,'YYYY-MM-DD hh:dd:ss').format('YYYY-MM-DD');
    $('#apellido').val(persona.apellido);
    $('#nombre').val(persona.nombre);
    $('#fecha_nac').val(fecha_nac_formateada);
    $('#sexo').val(persona.sexo).trigger("change");
    $('#dni').val(persona.dni);
    $('#cuil').val(persona.cuil);
    $('#pais_id').val(persona.pais_id).trigger("change");
    $('#estado_civil').val(persona.estado_civil).trigger("change");
    $('#localidad_id').val(persona.localidad_id).trigger("change");
    $('#direccion').val(persona.direccion);
    $('#sangre').val(persona.sangre).trigger("change");
    $('#estudios').val(persona.educacion).trigger("change");
    $('#email').val(persona.email);
    $('#telefono').val(persona.telefono);
    $('#telefono2').val(persona.telefono2);
    if (persona.discapacidad === 1) $('#discapacidad').bootstrapToggle('on');
    if (persona.carnet_discapacidad === 1) $('#carnet_discapacidad').bootstrapToggle('on');
    if (persona.mano_habil === 1) $('#mano_habil').bootstrapToggle('on');
    $('#app').addClass("modal-open");
    $('#modal-alta-alumno').modal('show');

}

function abrir_modal_borrar(id, lugar) {
    $('#form-borrar').attr('action', '/' + lugar + '/' + id);
    $('#modal-borrar').modal('show');
}


function redireccionar_editar_unidad(id) {
    window.location.href = '/unidades/' + id + '/edit';
}



var bandera = false;

$('#titulares-create').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    if (isSelected && !bandera) {
        bandera = true;
        desactivar_seleccion('titulares-create', 'suplentes-create');
    } else {
        bandera = false;
    }
});


$('#suplentes-create').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    if (isSelected && !bandera) {
        bandera = true;
        desactivar_seleccion('suplentes-create', 'titulares-create');
    } else {
        bandera = false;
    }
});


$('#titulares-update').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    if (isSelected && !bandera) {
        bandera = true;
        desactivar_seleccion('titulares-update', 'suplentes-update');
    } else {
        bandera = false;
    }
});


$('#suplentes-update').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    if (isSelected && !bandera) {
        bandera = true;
        desactivar_seleccion('suplentes-update', 'titulares-update');
    } else {
        bandera = false;
    }
});


function desactivar_seleccion(entrada, salida) {
    let arreglo = $('#' + entrada).val();
    let arreglo2 = $('#' + salida).val();
    arreglo.forEach(function (element) {
        let indice = arreglo2.indexOf(element); // obtenemos el indice       
        if (indice > -1) {
            arreglo2.splice(indice, 1);
        }
    });
    $('#' + salida).val(arreglo2).trigger("change");
}

function comprobar_validez(form) {
    let sede_id   = $('#sede_id-' + form).val(),
        comision  = $('#comision-' + form).val(),
        titulo_id = $('#titulo_id-' + form).val();

    const data = {
        sede_id: sede_id,
        comision: comision,
        titulo_id: titulo_id
    }
    
    $.ajax({// se envía
        url: '/comprobar_validez',
        type: 'GET',
        data: data,
        dataType: 'json',
        success: function (data) {
            if (data === "no") {
                $("#form-" + form).submit();
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Atención',
                    text: 'Ya existe un curso activo para la sede, título y comisión escogidos'
                });
            }
        }
    });
}

function comprobar_estudiante(id_del_curso) {
    let persona;
    Swal.fire({
        icon: 'info',
        title: 'DNI del Estudiante',
        input: 'number',
        inputAttributes: {
            autocapitalize: 'off'
        },
        confirmButtonText: 'Buscar',
        showLoaderOnConfirm: true,
        showCloseButton: true,
        preConfirm: (dni) => {
            let curso_id = id_del_curso;
            return fetch(`/getEstudiante/${dni}/${curso_id}`)
                .then(response => {
                    //response.ok es true si no existio error 404 (Red)
                    if (!response.ok) {
                        throw new Error(response.statusText)
                    }
                    return response.json();
                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `No se ha encontrado al estudiante`
                    )
                    //Si se va a cargar un alumno nuevo, no se ingresa DNI ↓
                    document.getElementById("formulario-alumno").reset();
                    $('#dni').val(dni);
                    $('#modal-alta-alumno').modal('show');
                })
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.value == -1) {
            Swal.fire({
                icon: 'warning',
                title: '✋🏻 Ya esta inscripto ✋🏻',                
                text: 'Este estudiante ya se encontraba registrado en el curso.'
            });
        } else {
            if (result.value) { 
                Swal.fire({
                    icon: 'success',
                    title: 'Persona encontrada',
                    showConfirmButton: false,
                    timer: 1500
                  })             
                console.log(result.value)
                persona = result.value.persona;
                //$('#tabla_cursadas').empty();
                let tableCursadas = ''
                let bdy_table = ''
                let color = ''                

                if(result.value.cursada !== undefined){

                    result.value.cursada.forEach(element => {
                        switch (element.estado_curso) {
                            case 'aprobado':
                                color = 'success';
                                break;
                            case 'libre':
                            case 'abandono':
                                color = 'danger';
                                break;
                            default:
                                element.estado_curso = 'cursando'
                                color = 'info';//cursando
                        }
                        bdy_table = bdy_table + `
                            <tr>
                                <td>
                                    <a href="/cursos/${element.curso_id}" target="_blank">
                                        ${ element.curso_id } - ${ element.nombre }
                                    </a>                                
                                </td>
                                <td>${ element.comision }</td>
                                <td>
                                    <span class="label label-light label-${color}">${element.estado_curso}</span>
                                </td>
                            </tr>
                        `
                    });

                    tableCursadas = `
                        <div>

                            <h4>Datos</h4>
                            <div class='row'>
                                <div class='col-xs-4'>
                                    <p><strong>Apellido:</strong><span>${persona.apellido}</span></p>
                                </div>
                                <div class='col-xs-4'>
                                    <p><strong>Nombre:</strong><span>${persona.nombre}</span> </p>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col-xs-4'>
                                    <p><strong>DNI:</strong><span>${persona.dni}</span></p>
                                </div>
                                <div class='col-xs-4'>
                                    <p><strong>Contacto:</strong><span>${persona.telefono}</span> 
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive-md">
                            <h4>Historial de cursos realizados</h4>
                            <table class='table table-striped table-hover'>
                                <thead>
                                <tr>
                                    <th> Curso    </th>
                                    <th> Comisión </th>
                                    <th> Estado   </th>
                                </tr>
                                </thead>
                                <tbody>
                                    ${ bdy_table }
                                </tbody>
                            </table>  
                        </div>            
                    `
                    $('#tabla_cursadas').html(tableCursadas);
                    $('#modal_historial_alumno').modal('show');
                    $('#modal_footer_inscripcion').html(`
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" onclick='continuar_inscripcion(${JSON.stringify(persona)})'>Continuar con inscripcición</button>
                    `)
                }else{                    
                    completar_campos_estudiante(persona);
                }
            }
        }

    }).catch((err)=>{
        Swal.fire({
            icon: 'error',
            title: 'Oops... Ha ocurrido un error',
            text: `${err}`
        })
    })
}


async function continuar_inscripcion(persona){
    completar_campos_estudiante(persona);
    $('#modal_historial_alumno').modal('hide');
}


let carnet_discap   = document.getElementById('carnet_discap')
let detalle_discap  = document.getElementById('detalle_discap')

$('input[name=discapacidad]').on('change', function(ev) {

    self = $(this);

    if(self.is(':checked')){
        carnet_discap.removeAttribute('hidden')
        detalle_discap.removeAttribute('hidden')
    }else{
        carnet_discap.setAttribute('hidden','hidden')
        detalle_discap.setAttribute('hidden','hidden')
    }
});