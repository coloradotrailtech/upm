$("#side-ele-cursos").addClass("active");

//Datatable
var table = $('#datatable-examenes').DataTable({
    language: tabla_traducida,
    responsive: false,
    "paging": false,
    "ordering": false,
    "searching": true
});

//Datatables | filtro individuales - instanciación de los filtros
$('#datatable-examenes tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title !== 'Situación' && title !== 'Asistió') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});

//Datatables | asocio el evento sobre el body de la tabla para que resalte fila y columna
$('#datatable-examenes tbody').on('mouseenter', 'td', function () {
    var colIdx = table.cell(this).index().column;
    $(table.cells().nodes()).removeClass('highlight');
    $(table.column(colIdx).nodes()).addClass('highlight');
});

$('.selectpicker').selectpicker();
$('#profesores').val("").trigger("change");
$('#unidades').val("").trigger("change");

$(".datepicker").datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    language: 'es'
});

var lista_notas = [];

function cambiar_asistencia(id) {

    let situacion = $('#situacion-' + id).val();
    let motivo_inasistencia = $('#input-motivo-' + id).val();

    if ($('#check-asistencia-' + id).prop('checked')) {
        $('#situacion-' + id).attr('disabled', false);
        $('#input-motivo-' + id).attr('disabled', true);
        $('#input-motivo-' + id).val("");
        lista_notas.splice(id, 1);
    } else {
        lista_notas[id] = {
            alumno_id: id,
            situacion: "",
            asistio: $('#check-asistencia-' + id).prop('checked'),
            motivo_inasistencia: ""
        }       
        $('#situacion-' + id).val("").trigger("change");
        $('#situacion-' + id).attr('disabled', true);        
        $('#input-motivo-' + id).attr('disabled', false);
    }
}


function carga_lista(id) {
    let situacion = $('#situacion-' + id).val();
    let asistio = $('#check-asistencia-' + id).prop('checked');
    let motivo_inasistencia = $('#input-motivo-' + id).val();

    lista_notas[id] = {
        alumno_id: id,
        situacion: situacion,
        asistio: asistio,
        motivo_inasistencia: motivo_inasistencia
    }
}

function cant_elementos() {
    let cant = 0;
    lista_notas.forEach(function (valor) {
        if (valor) { cant++; }
    });
    return cant;
}

function mandar_lista(id_curso) {
    let profesores = $("#profesores").val();
    let fecha = $("#fecha").val();
    let modalidad = $("#modalidad").val();
    let unidades = $("#unidades").val();

    if (cant_elementos() == cantidad_alumnos && profesores.length > 0 && fecha !== '' && modalidad !== '' && unidades.length > 0) {
        var lista = [];
        lista_notas.forEach(function (valor) { // se convierte el array en una coleccion de objetos
            lista.push(valor)
        });
        Swal.fire({
            title: 'Atención',
            text: "¿Se encuentra seguro?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#489E48',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'registrar',
            cancelButtonText: 'cancelar'
        }).then((result) => {
            if (result.value) {
                $.ajax({// se envía
                    url: '/registrar_examen',
                    data: {
                        curso_id: id_curso,
                        fecha: fecha,
                        unidades: unidades,
                        modalidad_id: modalidad,
                        profesores: profesores,
                        lista: lista
                    },
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        window.location.href = '/cursos/' + id_curso;
                    }
                });
            }
        });
    } else {
        let msj_error = "";
        if (fecha == '') {
            msj_error = "No seleccionó la fecha de la evaluación";
        } else if (modalidad === '') {
            msj_error = "No seleccionó una modalidad de evaluación";
        } else if (unidades.length == 0) {
            msj_error = "No seleccionó ninguna unidad a evaluar";
        } else if (profesores.length == 0) {
            msj_error = "No seleccionó ningún evaluador";
        } else if (cant_elementos() != cantidad_alumnos) {
            msj_error = "Existen alumnos a los cuales no les asignó una nota";
        }
        Swal.fire({
            icon: 'error',
            title: 'Atención',
            text: msj_error
        });
    }
}
