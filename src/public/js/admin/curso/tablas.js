//Datatable
var table_cursos = $('#datatable-cursos').DataTable({
    language: tabla_traducida,
    responsive: true
});

$(document).ready(function () {
    $('#myTable_alumnado').DataTable({
        language: tabla_traducida,
        paging: false
    });
});

//Datatables | filtro individuales - instanciación de los filtros
$('#datatable-cursos tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title !== 'Acciones') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table_cursos.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});

//Datatable
var table_materiales = $('#datatable-materiales-unidades').DataTable({
    language: tabla_traducida,
    responsive: true
});

var table = "";
//************************ SOLAPA ALUMNOS *******************************/////////////////////

$(document).ready(function () {
    table = $('#datatable-alumnos').DataTable({
        language: tabla_traducida,
        dom: 'Bfrtip',
        lengthMenu: [
            [-1, 10],
            ['Todos', '10']
        ],

        buttons: [
            /*
            'excelHtml5',
            'pdfHtml5'*/
            'pageLength',
            {

                extend: 'excelHtml5',
                exportOptions: {
                    columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,/*12,*/13,/*14,*/15, 16, 17, 18, 19]
                }
            },
        ],
        responsive: true
    });

    //Luego de instanciar la DataTable ocultamos botones de exportacion
    $(".buttons-pdf").hide();
    $(".buttons-excel").hide();

    //Ocultamos algunas columnas por default
    table.columns([0, 6, 7, 8, 9, 10, 11, 13, 15, 16, 17, 18, 19]).visible(false);

    //Ocultar Columnas Dinamicamente
    $('toggle-vis').on('click', function (e) {
        e.preventDefault();
        var column = table.column($(this).attr('data-column'));
        column.visible(!column.visible());
        if (column.visible()) {
            $(e.target).css("font-weight", "bold");
        } else {
            $(e.target).css("font-weight", "normal");
        }
    });

    $('#btn-exportar-excel').click(function () {
        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                return (!$(table.row(dataIndex).node()).hasClass('alumno-desafectable'));
            }
        );
        table.draw();
        $('.buttons-excel').click();
        $.fn.dataTable.ext.search.pop();
        table.draw();
    });


    //Datatables | filtro individuales - instanciación de los filtros
    $('#datatable-alumnos tfoot th').each(function () {
        var title = $(this).text();
        if (title !== "") {
            if (title !== 'Acciones') { //ignoramos la columna de los botones
                $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
            }
        }
    });

    //Datatables | filtro individuales - búsqueda
    table.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });

    //Datatables | ocultar/visualizar columnas dinámicamente
    $('a .toggle-vis').on('click', function (e) {
        e.preventDefault();
        // Get the column API object
        var column = table.column($(this).attr('data-column'));
        // Toggle the visibility
        column.visible(!column.visible());
        // instaciar_filtros();
    });

});

// Constancia Alumno Regular
function constancia_alu_reg(curso, mat) {
    let alumno;
    curso.alumnos.forEach(alu => {
        if (alu.estudiante.id == mat) {
            alumno = alu;
            console.log(alu.estudiante.persona.apellido)
        }

    });

    let date = new Date()
    let fecha = fecha_de_hoy(date);

    const alu_dni = alumno.estudiante.persona.dni;
    let dni_;

    if (alu_dni.length == 7) {
        dni_ = alu_dni.substr(0, 1) + '.' + alu_dni.substr(-6, 3) + '.' + alu_dni.substr(-3, 3)
    } else if (alu_dni.length == 8) {
        dni_ = alu_dni.substr(0, 2) + '.' + alu_dni.substr(-6, 3) + '.' + alu_dni.substr(-3, 3)
    } else if (alu_dni.length == 9) {
        dni_ = alu_dni.substr(0, 3) + '.' + alu_dni.substr(-6, 3) + '.' + alu_dni.substr(-3, 3)
    } else {
        dni_ = alu_dni
    }


    let nombre_completo = alumno.estudiante.persona.apellido.toUpperCase() + ", " + alumno.estudiante.persona.nombre.toUpperCase();

    const doc_Content = {
        pageSize: 'A4',
        pageMargins: [40, 40, 40, 40],
        content: [
            {

                columns: [
                    {
                        width: 40,
                        height: 750,
                        image: imagenes.columna_izquierda
                    },
                    {
                        table: {
                            widths: ['*'],
                            body: [
                                [
                                    {
                                        width: 220,
                                        alignment: 'center',
                                        border: [false, false, false, false],
                                        margin: [0, 30, 0, 0],
                                        image: imagenes.cons_alu_reg,
                                        //image: imagenes.logo_superior
                                    }
                                ],
                                [
                                    {
                                        text: `En el presente se certifica que ${nombre_completo} con DNI: ${dni_} es alumno regular de la UNIVERSIDAD POPULAR DE MISIONES de la Fundación MBO'E, se encuentra realizando el curso en oficio de ${curso.titulo.nombre} correspondiente a ${curso.comision}.`,
                                        //text:`Por la presente se certifica que ${nombre_completo} con DNI: ${dni_} se presentó en el día de la fecha a rendir examen correspondiente al curso en oficio de ${curso.titulo.nombre}, dictado en la la UNIVERSIDAD POPULAR DE MISIONES de la Fundación MBO'E.`, 
                                        style: 'const_style',
                                        margin: [10, 30, 0, 0],
                                        border: [false, false, false, false]
                                    }
                                ],
                                [
                                    {
                                        text: `A su pedido de presentar ante las autoridades que la requieran.`,
                                        style: 'const_style',
                                        border: [false, false, false, false]
                                    }
                                ],
                                [
                                    {
                                        text: `Se extiende el presente certificado, en la ciudad de Posadas, el día ${fecha}.`,
                                        style: 'const_style',
                                        border: [false, false, false, false]
                                    }
                                ],
                                [
                                    {
                                        width: 100,
                                        alignment: 'right',
                                        margin: [0, 50, 0, 0],
                                        border: [false, false, false, false],
                                        image: imagenes.firma_laura
                                    }
                                ]
                            ]
                        }
                    },
                    {
                        margin: [10, 30, 0, 0],
                        width: 20,
                        height: 680,
                        image: imagenes.columna_derecha
                    }
                ],
                columnGap: 10
            }
        ],

        styles: {
            header: {
                fontSize: 16,
                bold: true,
                //lineHeight: 5
            },
            const_style: {
                margin: [10, 10, 0, 0],
                fontSize: 12,
                lineHeight: 1.2,
                italics: false,
                alignment: 'justify',
            }
        }
    };

    pdfMake.createPdf(doc_Content).download(`Const Alumno Regular - ${nombre_completo}`);
}

/** Al dar click en exportar Excel se ocultaran filas de quienes son alumnos "Libres"*/

function instaciar_filtros() {

    //Datatables | filtro individuales - instanciación de los filtros
    $('#datatable-alumnos tfoot th').each(function () {
        var title = $(this).text();
        if (title !== "") {
            if (title !== 'Acciones') { //ignoramos la columna de los botones
                $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
            }
        }
    });


    //Datatables | filtro individuales - búsqueda
    table.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });
}

/** funcion para cambiar estado de alumno */
function editarEstadoCurso(estado_actual_alumno, alumno_id, seteo_global) {
    (async function getFruit() {
        const { value: select_estados } = await Swal.fire({
            title: 'Seleccione el nuevo estado del alumno',
            input: 'select',
            inputOptions: {
                'libre': 'Libre',
                null: 'Actualmente cursando',
                'abandono': 'Abandono el curso',
                'aprobado': 'Aprobado ✔️ ',
            },
            inputPlaceholder: 'seleccione condición del alumno',
            showCancelButton: true,

            inputValidator: (value) => {
                //estado_actualizado= value,
                return new Promise((resolve) => {
                    if (value === estado_actual_alumno) {
                        resolve('Este ya es el estado del alumno.')
                    } else {
                        resolve()
                    }
                })
            }
        })

        if (select_estados) {

            let url = `/cambiar_estado_curso/${alumno_id}/${select_estados}`;
            if (seteo_global) {
                console.log(ids_alumnos_certificados_generados);
                url = `/setear_estado_masivo/${JSON.stringify(ids_alumnos_certificados_generados)}/curso/${select_estados}`;
            }

            return fetch(url)
                .then(response => {
                    //response.ok es true si no sucedio un error 404 (Red)
                    if (!response.ok) {
                        throw new Error(response.statusText)
                    }
                    location.reload();
                    //window.location.href = '/cursos/' + curso_id + '#tab_2';
                    return response.json();
                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `No se ha encontrado al estudiante`
                    )
                })
            allowOutsideClick: () => !Swal.isLoading()
        }
    })()
}

/** Cambiar estados de certificados para alumnos aprobados*/
function editarCertificado(estado_actual_alumno, alumno_id, seteo_global) {
    (async function getFruit() {
        const { value: select_estados } = await Swal.fire({
            title: 'Especifique el estado del Título',
            text: '¿En qué estado se encuentra la certificación de alumno aprobado?',
            input: 'select',
            inputOptions: {
                null: 'Sin gestionar',
                'tramitando': 'En trámite',
                'impreso': 'Impreso',
                'entregado': 'Entregado a alumno ✔️ ',
            },
            inputPlaceholder: 'seleccione condición del alumno',
            showCancelButton: true,

            inputValidator: (value) => {
                //estado_actualizado= value,
                return new Promise((resolve) => {
                    if (value === estado_actual_alumno) {
                        resolve('Este ya es el estado del certificado.')
                    } else {
                        resolve()
                    }
                })
            }
        })

        if (select_estados) {
            let url = `/cambiar_estado_certificado/${alumno_id}/${select_estados}`;
            if (seteo_global) {
                console.log(ids_alumnos_certificados_generados);
                url = `/setear_estado_masivo/${JSON.stringify(ids_alumnos_certificados_generados)}/certificado/${select_estados}`;
            }
            return fetch(url)
                .then(response => {
                    //response.ok es true si no sucedio un error 404 (Red)
                    if (!response.ok) {
                        throw new Error(response.statusText)
                    }
                    location.reload();
                    //window.location.href = '/cursos/' + curso_id + '#tab_2';
                    return response.json();

                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `No se ha encontrado al estudiante`
                    )
                })
            allowOutsideClick: () => !Swal.isLoading()
        }
    })()
}

//************************ FIN solapa ALUMNOS *******************************//

//Datatable
function instanciar_tabla(sector, id) {
    if ($('#a-' + sector + '-' + id).hasClass('collapsed')) {
        $('#datatable-' + sector + '-' + id).DataTable({
            language: tabla_traducida,
            responsive: true
        });
    } else {
        $('#datatable-' + sector + '-' + id).DataTable().destroy();
    }
}


async function configurar_certificado(curso){

    let horas = $("#span_carga").attr("carga");
    $('#nombre_curso_').html(`<br><b>${curso.titulo.nombre}</b>, con una carga horaria de <b> ${horas}. </b>`)
    $('#fecha_cert').html(`Fecha ${fecha_de_hoy(new Date())}`)
    $('#modal_set_certificados').modal('show')
}

var certificados_a_generar = [];
var ids_alumnos_certificados_generados = [];

async function imprimir_certificados(curso) {
    certificados_a_generar = [];
    ids_alumnos_certificados_generados = [];
    const verbo_certificado = document.getElementById('verbo_certificado').value
    const curso_taller      = document.getElementById('curso_taller').value

    const firmas_cert    = document.getElementById('firmas_disponibles').value
    const img_firma_cert = imagenes[`${firmas_cert}`]

    const frase_certificado = `${verbo_certificado} ${curso_taller}`
    console.log(frase_certificado)

    Swal.fire({
        title: 'Generar certificados',
        text: "¿Para cuáles alumnos desea generar los certificados?",
        type: 'warning',
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Aprobados',
        confirmButtonText: 'Seleccionados'
    }).then((result) => {
        if (result.value) {
            $("input[type=checkbox]:checked").each(function () {
                //cada elemento seleccionado
                if ($(this).hasClass("check_alumno")) {
                    let alumno = JSON.parse($("#alumno-" + $(this).val()).attr("alumno"));
                    ids_alumnos_certificados_generados.push($(this).val());
                    armar_lista_certificados(alumno, curso, frase_certificado,img_firma_cert);
                }
            });
            descargar_certificado("seleccionados",curso.id);
        } else if (result.dismiss === "cancel") {
            // para no instanciar un botón más habilité la cruz de cerrar y uso el de cancelar 
            // para el propósito.
            //
            $(".aprobado").each(function () {
                //cada elemento seleccionado
                let alumno = JSON.parse($(this).attr("alumno"));
                let alumno_id = JSON.parse($(this).attr("alumno_id"));
                ids_alumnos_certificados_generados.push(alumno_id);
                armar_lista_certificados(alumno, curso, frase_certificado,img_firma_cert);
            });
            descargar_certificado("aprobados",curso.id);
        }
    })
}

function seteo_masivo_curso() {
    ids_alumnos_certificados_generados = [];
    $("input[type=checkbox]:checked").each(function () {
        //cada elemento seleccionado
        if ($(this).hasClass("check_alumno")) {
            let alumno_id = $(this).val();
            ids_alumnos_certificados_generados.push(alumno_id);
        }
    });
    if (ids_alumnos_certificados_generados.length > 0) {
        editarEstadoCurso(null, null, true);
    } else {
        Swal.fire(
            'Información',
            'Seleccione a los alumnos a los cuales cambiarle el estado.',
            'info'
        );
        table.columns([0]).visible(true);
    }
}

function seteo_masivo_certificado() {
    ids_alumnos_certificados_generados = [];
    Swal.fire({
        title: 'Seteo masivo de estado',
        text: "¿A cuáles alumnos desea cambiarle el estado?",
        type: 'warning',
        showCloseButton: true,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'aprobados',
        confirmButtonText: 'seleccionados'
    }).then((result) => {
        if (result.value) {
            $("input[type=checkbox]:checked").each(function () {
                //cada elemento seleccionado
                if ($(this).hasClass("check_alumno")) {
                    let alumno_id = $(this).val();
                    ids_alumnos_certificados_generados.push(alumno_id);
                }
            });
            if (ids_alumnos_certificados_generados.length > 0) {
                editarCertificado(null, null, true);
            } else {
                Swal.fire(
                    'Información',
                    'Seleccione a los alumnos a los cuales cambiarle el estado.',
                    'info'
                );
                table.columns([0]).visible(true);
            }
        } else if (result.dismiss === "cancel") {
            /*
             * para no instanciar un botón más habilité la cruz de cerrar y uso el de cancelar 
             * para el propósito.
             */
            $(".aprobado").each(function () {
                //cada elemento seleccionado
                let alumno_id = JSON.parse($(this).attr("alumno_id"));
                ids_alumnos_certificados_generados.push(alumno_id);
            });
            editarCertificado(null, null, true);
        }
    })
}

//======> Armado de Certificados
function armar_lista_certificados(alumno, data_curso, frase, firmas) {

    const nombre = alumno.apellido + " " + alumno.nombre;
    const dni = alumno.dni
    const curso = data_curso.titulo.nombre;
    const horas = $("#span_carga").attr("carga");
    const fecha = fecha_de_hoy(new Date());

    let dni_;

    if (dni.length == 7) {
        dni_ = dni.substr(0, 1) + '.' + dni.substr(-6, 3) + '.' + dni.substr(-3, 3)
    } else if (dni.length == 8) {
        dni_ = dni.substr(0, 2) + '.' + dni.substr(-6, 3) + '.' + dni.substr(-3, 3)
    } else if (dni.length == 9) {
        dni_ = dni.substr(0, 3) + '.' + dni.substr(-6, 3) + '.' + dni.substr(-3, 3)
    } else {
        dni_ = dni
    }


    certificados_a_generar.push({
        columns: [
            {
                width: 45,
                height: 360,
                image: imagenes.columna_izquierda
            },
            {

                table: {
                    widths: ['*'],
                    body: [

                        [{
                            margin: [26, 0, 0, 0],
                            border: [false, false, false, false],
                            alignment: 'center',
                            image: imagenes.logo_superior,
                            width: 200

                        },],
                        [{
                            margin: [11, 30, 0, 0],
                            border: [false, false, false, false],
                            alignment: 'justify',
                            fontSize: 11,
                            lineHeight: 1.5,
                            text: `Se extiende el presente certificado al Sr/a ${nombre.toUpperCase()}, DNI: ${dni_}, quien ${frase} ${curso.toUpperCase()}, con una carga horaria de ${horas}.`
                            
                        },

                        ],
                        [{
                            margin: [26, 0, 0, 20],
                            border: [false, false, false, false],
                            alignment: 'right',
                            fontSize: 11,
                            text: `POSADAS, MISIONES,  ${fecha} `
                        },],
                        [{
                            margin: [11, 0, 0, 0],
                            border: [false, false, false, false],
                            alignment: 'center',
                            image: firmas,
                            width: 370
                        },],
                    ]
                }

            },
            {
                margin: [11, 0, 0, 0],
                width: 20,
                height: 359,
                image: imagenes.columna_derecha
            },
        ],
        // optional space between columns
        columnGap: 10
    });
}

function descargar_certificado(tipo,curso_id) {

    let dd = {
        // a string or { width: number, height: number }
        pageSize: 'A5',
        pageOrientation: 'landscape',
        // [left, top, right, bottom]
        pageMargins: [30, 30, 40, 30],
        content: certificados_a_generar
    };

    if (certificados_a_generar.length > 0) {
        console.log(certificados_a_generar)
        pdfMake.createPdf(dd).download(`Certificados curso_id ${curso_id}.pdf`);

        Swal.fire({
            title: 'Seteo masivo de estado',
            text: "¿Desea cambiarle el estado a estos alumnos?",
            type: 'warning',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'actualizar estado'
        }).then((result) => {
            if (result) {
                url = `/setear_estado_masivo/${JSON.stringify(ids_alumnos_certificados_generados)}/certificado/tramitando`;
                return fetch(url)
                    .then(response => {
                        $(".check_alumno").prop("checked", false);
                        table.columns([0]).visible(false);
                        location.reload();
                    });
            }
            $(".check_alumno").prop("checked", false);
            table.columns([0]).visible(false);
        });


    } else if (tipo === "seleccionados") {
        Swal.fire(
            'Información',
            'Seleccione a los alumnos a generar certificados.',
            'info'
        );
        table.columns([0]).visible(true);
    } else {
        Swal.fire(
            '¡Atención!',
            'No se generó ningún certificado.',
            'warning'
        );
    }
}

function fecha_de_hoy(date) {
    
    let fecha = ''
    let day = date.getDate()
    let month = date.getMonth()
    let year = date.getFullYear()
    const mes = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']

    fecha = `${day} de ${mes[month]} de ${year}`;

    return fecha;
}