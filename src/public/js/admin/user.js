$("#side-general-li").addClass("active");
$("#side-general-ul").addClass("menu-open");
$("#side-ele-users").addClass("active");

function completar_campos(usuario) {
    $('#update-name').val(usuario.name);
    $('#update-email').val(usuario.email);
    $('#rol_usuario').val(usuario.rol_id).trigger("change");
    $('#form-update').attr('action', '/usuarios/' + usuario.id);
    $('#modal-update').modal('show');
}

function abrir_modal_borrar(id) {
    $('#form-borrar').attr('action', '/usuarios/' + id);
    $('#modal-borrar').modal('show');
}

//Datatable
var table = $('#datatable-usuarios').DataTable({
    language: tabla_traducida,
    responsive: true
});

$('.selectpicker').selectpicker();


// Enviar datos.
function mandar(tipo_form) { //tipo_form puede ser create o update
    var redireccion = "/usuarios";

    //// Este método sirve para ver el contenido del formdata
    //for (var pair of formData.entries())
    //{
    // console.log(pair[0]+ ', '+ pair[1]); 
    //}

    var form = $("#form-" + tipo_form);
    var url = form.attr("action");
    var token = $("#token-" + tipo_form).val();
    var formData = new FormData(document.getElementById("form-" + tipo_form));

    if ((tipo_form === 'create')&&(imagen_cropie.create !== '')) {
        formData.append('imagen', imagen_cropie.create)
    } else if (imagen_cropie.update !== ''){
        formData.append('imagen', imagen_cropie.update)
    } 

    $.ajax(url, {
        headers: {"X-CSRF-TOKEN": token},
        method: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {           
            window.location.href = redireccion;
        },
        error: function () {
            console.log('Upload error');
            alert('Se ha producido un error, verifique los datos proporcionados');
        }
    });
}

/********* Ocultar o Mostrar dropdown de Sedes por si user es encargado *********/
$("#rol_id").change(function () {
    var opcion_select = $('#rol_id').val();

    if (opcion_select == "2") {  //Si es "ENCARGADO DE SEDE"
        $("#div_sedes").removeClass('hide');
        $('#div_sedes').addClass('animated');
        $('#div_sedes').addClass('pulse');
    }
    else{
        $('#div_sedes').addClass('hide');
        $('#sede_id').val(null); //reseteamos el select para que no grabe cualquier valor anterior
    }
})
