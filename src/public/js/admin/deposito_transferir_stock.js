$("#side-ele-depositos").addClass("active");

//Datatable
var table = $('#datatable-depositos').DataTable({
    language: tabla_traducida,
    responsive: false
});

/****************************************************************************************/

function cambiar() {
    column_input.visible(!column_input.visible());
    column_info.visible(!column_info.visible());
    column_check.visible(!column_check.visible());

    if (column_check.visible()) {
        $('#boton-eliminar').show();
        $('#boton-update').hide();
    } else {
        $('#boton-eliminar').hide();
        $('#boton-update').show();
    }
}

var column_input = table.column("2"), column_info = table.column("3"), column_check = table.column("4");
column_check.visible(false);
$('#boton-eliminar').hide();
$('.selectpicker').selectpicker();

var lista_update = [], lista_trash = [];



function stock_destino_update(id, material_id, material_nombre, umedida, stock_d, stock_o) {

    console.log(id,umedida, stock_o);
    /**
     * Este metodo registra y actualizar cada vez que el usuario escribe un dato nuevo
     * en el campo de corrección de unidades.
     * id= material_deposito (fila de tabla con stock en la vista)
     * umedida= umedida material
     * stock= stock en deposito destino antes de actualizar cantidades
     */
    cantidad = parseFloat($('#input-cantidad-' + id).val());

    cant_stock = parseFloat(stock_d);
    stock_origen= parseFloat(stock_o);
    stock_final = cantidad + cant_stock;    //stock_final en deposito destino

    console.log(cantidad, cant_stock);
    if (stock_final > -1 ) { //verificamos que lo la cantidad a enviar esté disponible
        if (cantidad<=stock_origen) {  //reemplazar por verificacion de cantidad disponible
            if ($('#input-cantidad-' +id).val() !== '') {
                if ((id, cantidad)) {
                    lista_update[id] = {
                        id: id,
                        cantidad: cantidad,
                        material_id: material_id,
                        nombre: material_nombre,
                        umedida: umedida
                    };
                }
            } else {
                $('#span-info-' + id).html((cant_stock) + " " + umedida);
                lista_update.splice(id, 1);
            }
            $('#span-info-' + id).html((cant_stock + cantidad) + " " + umedida);


        } else {
            Swal.fire({
                type: 'error',
                title: 'Atención',
                text: 'No se puede enviar mas unidades que las disponibles en el Deposito de origen',
            });
            $('#span-info-' + id).html((cant_stock) + " " + umedida);
            $('#input-cantidad-' +id).val("");
        }
    }
    console.log(lista_update);
}


function carga_lista_trash(id) {

    /**
     * Este proceso se encarga de registrar y eliminar cada vez que el usuario tilda o destilda un registro a eliminar
     */
    console.log(lista_trash.indexOf(id));
    if (lista_trash.indexOf(id) < 0) {
        lista_trash.push(id);
    } else {
        lista_trash.splice(lista_trash.indexOf(id), 1);
    }
}


function mandar_lista_update(id_deposito) {
    if (lista_update.length > 0) { // si la lista está vacía se informa al usuario al respecto

        var lista = [];
        lista_update.forEach(function (valor) { // se convierte el array en una coleccion de objetos
            lista.push(valor)
        });

        Swal.fire({
            title: 'Atención',
            text: "¿Se encuentra seguro?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#2aaddd',
            cancelButtonColor: '#d63f16',
            confirmButtonText: 'actualizar',
            cancelButtonText: 'cancelar'
        }).then((result) => {
            if (result.value) {
                $.ajax({// se envía
                    url: '/actualizar_existencia',
                    data: {
                        lista: lista
                    },
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        window.location.href = '/pantalla_administrar_stock/' + id_deposito;
                    }
                });
            }
        });
    } else {
        Swal.fire({
            type: 'error',
            title: 'Atención',
            text: 'Ninguna cantidad fue actualizada'
        });
    }
}

//al seleccionar deposito de destino se trae el objeto deposito destino y se renderiza la tabla con su stock
$('#dep_destino').change(function () {
    //temporizador
    let timerInterval
    Swal.fire({
            title: 'Buscando inventario..',
            html: '',
            timer: 1010,
            onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {Swal.getContent().querySelector('strong').textContent = Swal.getTimerLeft() }, 100)
            },
        onClose: () => {
            clearInterval(timerInterval)
        }
    }).then((result) => {
        if (result.dismiss === Swal.DismissReason.timer) {
            console.log('Cerrado por el temporizador')
    }
})
    //fin de temporizador

    lineas = []
    $.ajax({
        url: '/buscar_stock_deposito',
        data: {
            id_deposito_origen: $('#origen_id').val(),
            id_deposito_destino: $('#dep_destino').val(),

        },
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#tabla_stock_deposito').html(data)
            instanciar_elementos() // se instancian nuevamente la tabla y las máscaras para las fechas
        }
    })
});

function instanciar_elementos() {
    var table = $('#datatable-depositos').DataTable({
        'language': tabla_traducida // esta variable esta instanciada donde están declarados todos los js.
    });
    /************************************************************/
    //Datatables | filtro individuales - instanciación de los filtros
    $('#datatable-depositos tfoot th').each(function () {
        var title = $(this).text();
        if (title !== "") {
            if (title !== 'Acciones') { //ignoramos la columna de los botones
                $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
            }else{
                $(this).html('');
            }
        }
    });

//Datatables | filtro individuales - búsqueda
    table.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });

//Datatables | ocultar/visualizar columnas dinámicamente
    $('a.toggle-vis').on('click', function (e) {
        e.preventDefault();
        // Get the column API object
        var column = table.column($(this).attr('data-column'));
        // Toggle the visibility
        column.visible(!column.visible());
        instaciar_filtros();
    });

//Datatables | asocio el evento sobre el body de la tabla para que resalte fila y columna
    $('#datatable-depositos tbody').on('mouseenter', 'td', function () {
        var colIdx = table.cell(this).index().column;
        $(table.cells().nodes()).removeClass('highlight');
        $(table.column(colIdx).nodes()).addClass('highlight');
    });
    /**************************************************************/
}

function enviar_transferencia(id_dep_origen, id_dep_destino) {
    if (lista_update.length > 0) { // si la lista está vacía se informa al usuario al respecto
        var lista = [];
        lista_update.forEach(function (valor) { // se convierte el array en una coleccion de objetos
            lista.push(valor)
        });

        Swal.fire({
            title: 'Atención',
            text: "¿Se encuentra seguro?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#2aaddd',
            cancelButtonColor: '#d63f16',
            confirmButtonText: 'confirmar transferencia',
            cancelButtonText: 'cancelar'
        }).then((result) => {
            if (result.value) {
                console.log(lista);
            $.ajax({// se envía
                url: '/transferir_materiales',
                data: {
                    lista: lista,
                    id_dep_origen:id_dep_origen,
                    id_dep_destino:id_dep_destino
                },
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    Swal.fire({
                        title: 'Transferencia efectuada',
                        text: "Se enviaron materiales de Deposito "+id_dep_origen+" a Deposito "+id_dep_destino,
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#2aaddd',
                        cancelButtonColor: '#d63f16',
                        confirmButtonText: 'Bien',
                    }).then((result) => {
                        if (result.value) {
                            remito={
                                items: lista,
                                dep_origen: id_dep_origen,
                                dep_destino: id_dep_destino,
                            }
                        //window.location.href = '/pantalla_administrar_stock/' + id_dep_destino;
                            //window.location.href = '/pantalla_administrar_stock/' + id_dep_destino;
                            /*Una vez que se obtiene el id de comprobante grabado arriba, se rellena el recibo por pantalla */
                            //remito.nro_comprobante = 1;      //OK

                            //recibo.imp_total = montoTotal_Absoludo;
                            console.log("El contenido del array 'recibo' es:");
                            console.log(remito);
                            var array = JSON.stringify(remito);
                            console.log(array);
                            var link = 'http://localhost:8000/comprobantes/remito?&datos_remito=' + encodeURIComponent(array);
                            window.open(link);
                            window.location.href = '/pantalla_administrar_stock/' + id_dep_destino;

                        }
                })
                }
            });
        }
    });
    } else {
        Swal.fire({
            type: 'error',
            title: 'Atención',
            text: 'Ninguna cantidad fue actualizada'
        });
    }
}
