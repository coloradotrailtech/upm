$("#side-general-li").addClass("active");
$("#side-general-ul").addClass("menu-open");
$("#side-ele-lugares-localidades").addClass("active");

function completar_campos(localidad) {
    $('#nombre').val(localidad.nombre);
    $('#provincia_id').val(localidad.provincia_id).trigger("change");
    $('#cod_postal').val(localidad.cod_postal);
    $('#form-update').attr('action', '/localidades/' + localidad.id);
    $('#modal-update').modal('show');
}

function abrir_modal_borrar(id) {
    $('#form-borrar').attr('action', '/localidades/' + id);
    $('#modal-borrar').modal('show');
}

//Datatable
$(document).ready(function() {
    $('#example').DataTable({
        language: tabla_traducida,
        responsive: true
    });
});

$('.selectpicker').selectpicker();