$("#side-ele-depositos").addClass("active");

//Datatable
var table = $('#datatable-depositos').DataTable({
    language: tabla_traducida,
    responsive: false
});

//Datatables | filtro individuales - instanciación de los filtros
$('#datatable-depositos tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title == 'Material' || title == 'Stock actual/actualizado') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});

$('.selectpicker').selectpicker();

var lista_materiales = [];

function limpiar_linea(id, abreviatura, cant_stock) {
    $('#span-info-' + id).html((cant_stock) + " " + abreviatura + " / " + (cant_stock) + " " + abreviatura);
    $('#input-cantidad-' + id).val("");
    $('#input-observacion-' + id).val('');
    $('#input-observacion-' + id).attr('disabled', true);
    $('#li-material-' + id).css("color", "black");
}


function carga_lista(id, id_material_deposito, abreviatura, stock, cantidad_solicitada) {
    /**
     * Este proceso se encarga de registrar y actualizar cada vez que el usuario escribe un dato nuevo 
     * en el campo de corrección de unidades.
     */

    cantidad = parseFloat($('#input-cantidad-' + id).val());
    cant_stock = parseFloat(stock);
    stock_final = cant_stock - cantidad;

    if ($('#input-cantidad-' + id).val() !== '') {
        if (cantidad > 0) {
            if (stock_final > -1) {

                lista_materiales[id] = {
                    id: id_material_deposito,
                    cantidad: cantidad,
                    observacion: $('#input-observacion-' + id).val()
                };
                $('#span-info-' + id).html((cant_stock) + " " + abreviatura + " / " + (cant_stock - cantidad) + " " + abreviatura);
                $('#input-observacion-' + id).attr('disabled', false);

            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Atención',
                    text: 'El stock final en depósito del meterial no puede ser negativo'
                });
                limpiar_linea(id, abreviatura, cant_stock);
            }
        } else {
            Swal.fire({
                type: 'error',
                title: 'Atención',
                text: 'La cantidad ingresada debe ser mayor a 1'
            });
            limpiar_linea(id, abreviatura, cant_stock);
        }
    } else {
        lista_materiales.splice(id, 1);
        limpiar_linea(id, abreviatura, cant_stock);
    }
}


function cargar_cantidad_sugerida(id, id_material_deposito, abreviatura, stock, cantidad_solicitada) {
    /**
     * Este método se usa en los casos en que el concepto es un concepto compartido y en el contrato se 
     * especifica que se está sujeto a gastos compartidosestá sugerido a modo de placerholder entonces
     * al dar un click en ese campor el mismo se complete con el monto sugerido
     */

    if (($('#input-cantidad-' + id).val() === '')) {
        $('#input-cantidad-' + id).val(cantidad_solicitada);
        $('#input-observacion-' + id).attr('disabled', false);
        carga_lista(id, id_material_deposito, abreviatura, stock, cantidad_solicitada);
    }
}



function mandar_lista_materiales(deposito_id, solicitudprestamo_id) {
    if (lista_materiales.length > 0) { // si la lista está vacía se informa al usuario al respecto
        enviar_a_server(deposito_id, solicitudprestamo_id);
    } else if ($('#estadosolicitudprestamo_id').val() == 3) { //id_rechazado
        enviar_a_server(deposito_id, solicitudprestamo_id);
    } else {
        Swal.fire({
            type: 'error',
            title: 'Atención',
            text: 'Ninguna cantidad fue actualizada'
        });
    }
}



function enviar_a_server(deposito_id, solicitudprestamo_id){
    var lista = [];
    lista_materiales.forEach(function (valor) { // se convierte el array en una coleccion de objetos
        lista.push(valor)
    });

    Swal.fire({
        title: 'Atención',
        text: "¿Se encuentra seguro?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'registrar',
        cancelButtonText: 'cancelar'
    }).then((result) => {
        if (result.value) {
            $.ajax({ // se envía
                url: '/registrar_prestamo',
                data: {
                    solicitudprestamo_id: solicitudprestamo_id,
                    estadosolicitudprestamo_id: $('#estadosolicitudprestamo_id').val(),
                    lista: lista
                },
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    window.location.href = '/depositos/' + deposito_id;
                }
            });
        }
    });
}