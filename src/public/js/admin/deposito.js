$("#side-ele-depositos").addClass("active");

var tablas_dinamicas = [];
var datos_tablas_dinamicas = [];
var funciones_tablas_dinamicas = [];
var ids_tablas = {
    'prestamo-realizado': [],
    'prestamo-culminado': [],
    'solicitud-historial': [],
    'solicitud-pendiente': []
}

function completar_campos(deposito) {
    $('#direccion').val(deposito.direccion);
    $('#sede_id').val(deposito.sede_id).trigger("change");
    $('#localidad_id').val(deposito.localidad_id).trigger("change");
    $('#descripcion').val(deposito.descripcion);
    $('#form-update').attr('action', '/depositos/' + deposito.id);
    $('#modal-update').modal('show');
}

function abrir_modal_borrar(id) {
    $('#form-borrar').attr('action', '/depositos/' + id);
    $('#modal-borrar').modal('show');
}

$('.selectpicker').val("").trigger("change");

//Datatable
var table = $('#datatable-depositos').DataTable({
    language: tabla_traducida,
    responsive: true
});

//Datatables | filtro individuales - instanciación de los filtros
$('#datatable-depositos tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title !== 'Acciones') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        } else {
            $(this).html('');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});


//Datatables | asocio el evento sobre el body de la tabla para que resalte fila y columna
$('#datatable-depositos tbody').on('mouseenter', 'td', function () {
    var colIdx = table.cell(this).index().column;
    $(table.cells().nodes()).removeClass('highlight');
    $(table.column(colIdx).nodes()).addClass('highlight');
});


//Datatable

function instanciar_tabla(sector, id) {

    let nombre_tabla = sector + '-' + id;

    if ($('#a-' + nombre_tabla).hasClass('collapsed')) {

        ids_tablas[sector].push(id);

        tablas_dinamicas[nombre_tabla] = $('#datatable-' + nombre_tabla).DataTable({
            language: tabla_traducida,
            responsive: true
        });

        funciones_tablas_dinamicas[nombre_tabla] = function (settings, data, dataIndex) {          
            if ( settings.nTable.id === 'datatable-' + nombre_tabla ) {
                let data_field_name = $('#Material-pie-' + nombre_tabla).val();
                if (data_field_name === "") {
                    return true;
                } else {
                    let names_capo = data_field_name.toLowerCase().split(',');
                    let name = data[0].toLowerCase();
                    let verdad = false
                    names_capo.forEach(function (element) {
                        let elemento_formateado = element.replace(/^\s*|\s*$/g, "");
                        if (elemento_formateado !== "" && name.includes(elemento_formateado)) {
                            verdad = true;
                        }
                    });
                    return verdad;
                }
                return false;                
              }else{
                return true;
              }           
        }

        $.fn.dataTable.ext.search.push(funciones_tablas_dinamicas[nombre_tabla]);

        // Setup - add a text input to each footer cell
        $('#datatable-' + nombre_tabla + ' tfoot th').each(function () {
            let title = $(this).text() + '-pie-' + nombre_tabla;
            $(this).html('<input id="' + title + '" type="text" placeholder="Buscar ' + $(this).text() + '" />');
        });

        // Apply the search
        tablas_dinamicas[nombre_tabla].columns().every(function () {
            datos_tablas_dinamicas[nombre_tabla] = this;
            $('input', this.footer()).on('keyup change', function () {
                if (this.id !== "Material-pie-" + nombre_tabla) {
                    if (datos_tablas_dinamicas[nombre_tabla].search() !== this.value) {
                        datos_tablas_dinamicas[nombre_tabla].search(this.value).draw();
                    }
                } else {
                    datos_tablas_dinamicas[nombre_tabla].draw();
                }
            });
        });
        
        let valor = "";

        $('#materiales-' + sector).val().forEach(function (element_id) {
            valor = valor + $('#materiales-'+sector+'-' + element_id).attr('nombre') + ', ';
        });
        $('#Material-pie-' + nombre_tabla).val(valor.substring(0, valor.length - 2));

        tablas_dinamicas[nombre_tabla].draw();
                

    } else {
        ids_tablas[sector].splice(ids_tablas[sector].indexOf(id), 1);

        let index = $.fn.dataTable.ext.search.indexOf(funciones_tablas_dinamicas[nombre_tabla]);
        $.fn.dataTable.ext.search.splice(index, 1);

        $('#datatable-' + nombre_tabla).DataTable().destroy();
        $('#tfoot-' + nombre_tabla).html(document.getElementById('thead-' + nombre_tabla).innerHTML);

    }
}


function filtrar(id, div) {

    let url = "";
    if (div.split('-')[0] === "solicitud") {
        url = '/filtrar_solicitudes';
    } else {
        url = '/filtrar_prestamos';
    }

    if (ids_tablas[div] !== undefined) {
        ids_tablas[div].forEach(function (element) {
            instanciar_tabla(div, element);
        });
    }
    $.ajax({ // se envía
        url: url,
        data: {
            tipo: div.split('-')[1],
            materiales_id: $('#materiales-' + div).val(),
            solicitantes_id: $('#solicitantes-' + div).val(),
            fechas: $('#fechas-' + div).val(),
            id: id
        },
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#ul-' + div).html(data.html);
        }
    });
}


function mostrar_detalle_solicitud(id) {
    $.ajax({ // se envía
        url: '/detalle_solicitud',
        data: {
            id: id
        },
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#body-modal-show-solicitud').html(data.html);
            $('#modal-show-solicitud').modal('show');
            var table_modal_solicitud = $('#datatable-solicitudprestamo-show').DataTable({
                language: tabla_traducida,
                responsive: true
            });
        }
    });
}

function mostrar_detalle_prestamo(id) {
    $.ajax({ // se envía
        url: '/detalle_prestamo',
        data: {
            id: id
        },
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#body-modal-show-prestamo').html(data.html);
            $('#modal-show-prestamo').modal('show');
            var table_modal_prestamo = $('#datatable-prestamo-show').DataTable({
                language: tabla_traducida,
                responsive: true
            });
        }
    });
}



