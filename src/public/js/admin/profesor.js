/**
 * Created by jpaul on 08/01/2019.
 */

//Esto es para que el sistema deje seleccionada la opcion en la barra menu de la izquierda
$("#side-ele-profesores").addClass("active");

function completar_campos(profesor) {
    
    $('#prof_id').val(profesor.id);
    $('#apellido').val(profesor.persona.apellido);
    $('#nombre').val(profesor.persona.nombre);
    $('#titulacion_id').val(profesor.titulacion_id).trigger("change");
    $('#localidad_id').val(profesor.persona.localidad_id).trigger("change");
    $('#direccion').val(profesor.persona.direccion);
    $('#dni').val(profesor.persona.dni);
    $('#email').val(profesor.persona.email);
    $('#telefono').val(profesor.persona.telefono);
    $('#sexo').val(profesor.persona.sexo).trigger("change");
    $('#estado_civil').val(profesor.persona.estado_civil).trigger('change');
    $('#pais_id').val(profesor.persona.pais_id).trigger('change');
    if (profesor.persona.mano_habil != "derecha") {
        $('#mano_habil').bootstrapToggle('off');
    }else{
        $('#mano_habil').bootstrapToggle('on');
    }
    
    let fecha_nac_formateada = moment(profesor.persona.fecha_nac,'YYYY-MM-DD hh:dd:ss').format('YYYY-MM-DD');
    $('#fecha_nac_edit').val(fecha_nac_formateada);
    $('#desc').val(profesor.descripcion);

    $('#modal-update').modal('show');
}

function abrir_modal_borrar(id) {
    $('#form-borrar').attr('action', '/profesores/' + id);
    $('#modal-borrar').modal('show');
}

//Datatable
var table = $('#datatable-profesores').DataTable({
    language: tabla_traducida,
    responsive: true
});

//Datatables | filtro individuales - instanciación de los filtros
$('#datatable-profesores tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title !== 'Acciones') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        }else{
            $(this).html('');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});

//Datatables | asocio el evento sobre el body de la tabla para que resalte fila y columna
$('#datatable-profesores tbody').on('mouseenter', 'td', function () {
    var colIdx = table.cell(this).index().column;
    $(table.cells().nodes()).removeClass('highlight');
    $(table.column(colIdx).nodes()).addClass('highlight');
});

