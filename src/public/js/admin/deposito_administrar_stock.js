$("#side-ele-depositos").addClass("active");

//Datatable
var table = $('#datatable-depositos').DataTable({
    language: tabla_traducida,
    responsive: false
});
//Datatables | filtro individuales - instanciación de los filtros
$('#datatable-depositos tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title !== 'Acciones') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});

//Datatables | asocio el evento sobre el body de la tabla para que resalte fila y columna
$('#datatable-depositos tbody').on('mouseenter', 'td', function () {
    var colIdx = table.cell(this).index().column;
    $(table.cells().nodes()).removeClass('highlight');
    $(table.column(colIdx).nodes()).addClass('highlight');
});



function cambiar() {
    column_input.visible(!column_input.visible());
    column_info.visible(!column_info.visible());
    column_check.visible(!column_check.visible());

    if (column_check.visible()) {
        $('#boton-eliminar').show();
        $('#boton-update').hide();
    } else {
        $('#boton-eliminar').hide();
        $('#boton-update').show();
    }
}

var column_input = table.column("4"), column_info = table.column("5"), column_check = table.column("6");
column_check.visible(false);
$('#boton-eliminar').hide();
$('.selectpicker').selectpicker();



var lista_update = [], lista_trash = [];

function carga_lista_update(id, abreviatura, stock) {
    /**
     * Este proceso se encarga de registrar y actualizar cada vez que el usuario escribe un dato nuevo 
     * en el campo de corrección de unidades.
     */

    cantidad = parseFloat($('#input-cantidad-' + id).val());
    cant_stock = parseFloat(stock);
    stock_final = cantidad + cant_stock;

    if (stock_final > -1) {
        if (stock_final < 1000000) {
            if ($('#input-cantidad-' + id).val() !== '') {
                if ((id, cantidad)) {
                    lista_update[id] = {
                        id: id,
                        cantidad: stock_final
                    };
                }
            } else {
                lista_update.splice(id, 1);
            }
            $('#span-info-' + id).html((cant_stock + cantidad) + " " + abreviatura);
        } else {
            Swal.fire({
                type: 'error',
                title: 'Atención',
                text: 'El stock final no puede ser mayor a un millón'
            });
            $('#span-info-' + id).html((cant_stock) + " " + abreviatura);
            $('#input-cantidad-' + id).val("");
        }
    } else {
        Swal.fire({
            type: 'error',
            title: 'Atención',
            text: 'El stock final del meterial no puede ser negativo'
        });
        $('#span-info-' + id).html((cant_stock) + " " + abreviatura);
        $('#input-cantidad-' + id).val("");
    }

}


function carga_lista_trash(id) {

    /**
     * Este proceso se encarga de registrar y eliminar cada vez que el usuario tilda o destilda un registro a eliminar
     */
    console.log(lista_trash.indexOf(id));
    if (lista_trash.indexOf(id) < 0) {
        lista_trash.push(id);
    } else {
        lista_trash.splice(lista_trash.indexOf(id), 1);
    }
}


function mandar_lista_borrar(id_deposito) {
    if (lista_trash.length > 0) { // si la lista está vacía se informa al usuario al respecto
        Swal.fire({
            title: '¿Se encuentra seguro?',
            text: "Si por algún motivo se arrepiente en el futuro no se preocupe que puede volver a agregarlo al inventario.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'borrar',
            cancelButtonText: 'cancelar'
        }).then((result) => {
            if (result.value) {
                $.ajax({// se envía
                    url: '/quitar_existencia',
                    data: {
                        lista: lista_trash
                    },
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        window.location.href = '/pantalla_administrar_stock/' + id_deposito;
                    }
                });
            }
        });
    } else {
        Swal.fire({
            type: 'error',
            title: 'Atención',
            text: 'No fue selecionado ningún material'
        });
    }
}


function mandar_lista_update(id_deposito) {
    if (lista_update.length > 0) { // si la lista está vacía se informa al usuario al respecto

        var lista = [];
        lista_update.forEach(function (valor) { // se convierte el array en una coleccion de objetos
            lista.push(valor)
        });

        Swal.fire({
            title: 'Atención',
            text: "¿Se encuentra seguro?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'actualizar',
            cancelButtonText: 'cancelar'
        }).then((result) => {
            if (result.value) {
                $.ajax({// se envía
                    url: '/actualizar_existencia',
                    data: {
                        lista: lista
                    },
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        window.location.href = '/pantalla_administrar_stock/' + id_deposito;
                    }
                });
            }
        });
    } else {
        Swal.fire({
            type: 'error',
            title: 'Atención',
            text: 'Ninguna cantidad fue actualizada'
        });
    }
}


