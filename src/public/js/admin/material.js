$("#side-ele-materiales").addClass("active");

function completar_campos(material) {
    $('#nombre').val(material.nombre);
    $('#umedida_id').val(material.umedida_id).trigger("change");
    $('#tipomaterial_id').val(material.tipomaterial_id).trigger("change");
    $('#form-update').attr('action', '/materiales/' + material.id);
    $('#modal-update').modal('show');
}

function abrir_modal_borrar(id) {
    $('#form-borrar').attr('action', '/materiales/' + id);
    $('#modal-borrar').modal('show');
}

//Datatable
var table = $('#datatable-materiales').DataTable({
    language: tabla_traducida,
    responsive: true
});

$('.selectpicker').selectpicker();

//Datatables | filtro individuales - instanciación de los filtros
$('#datatable-materiales tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title !== 'Acciones') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        }else{
            $(this).html('');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});


//Datatables | asocio el evento sobre el body de la tabla para que resalte fila y columna
$('#datatable-materiales tbody').on('mouseenter', 'td', function () {
    var colIdx = table.cell(this).index().column;
    $(table.cells().nodes()).removeClass('highlight');
    $(table.column(colIdx).nodes()).addClass('highlight');
});



$('#datatable-materiales tbody').on('click', 'tr', function () {
    $('.dtr-details').css('width', '98%');
});
