$("#side-ele-titulos").addClass("active");

function completar_campos(titulo) {
    $('#nombre').val(titulo.nombre);
    $('#titulo_id').val(titulo.categoria_id).trigger("change");
    $('#form-update').attr('action', '/titulos/' + titulo.id);
    $('#modal-update').modal('show');
}

function abrir_modal_borrar(id) {
    $('#form-borrar').attr('action', '/titulos/' + id);
    $('#modal-borrar').modal('show');
}

$('.selectpicker').selectpicker();
$('#recurso').val("").trigger("change");

var lista_contenidos = [],
    item_seleccionado = "",
    counter = 0;

var table = $('#example').DataTable({
    language: tabla_traducida,
    responsive: true,
    paging: false,
    ordering: false,
    info: false,
    bFilter: false
});

$('#example tbody').on('click', 'tr', function () {
    if ($(this).hasClass('selected')) {
        $(this).removeClass('selected');
    } else {
        table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
    item_seleccionado = $.map(table.rows('.selected').data(), function (item) {
        return item[0];
    });
});

$('#button').click(function () {
    table.row('.selected').remove().draw(false);
    if (item_seleccionado === "") {
        Swal.fire({
            type: 'error',
            title: 'Atención',
            text: 'Primero seleccione una fila a borrar'
        });
    } else {
        lista_contenidos.splice(item_seleccionado, 1);
        item_seleccionado = "";
    }
});


$('#addRow').on('click', function () {

    objetivo = $('#objetivo').val();
    contenido = $('#contenido').val();
    actividad = $('#actividad').val();
    evaluacion = $('#evaluacion').val();

    if (objetivo !== "" || contenido !== "" || actividad !== "") {
        lista_contenidos[counter] = {
            objetivos: objetivo,
            contenidos: contenido,
            actividades: actividad,
            tipo_evaluacion: evaluacion
        };
        table.row.add([counter, objetivo, contenido, actividad, evaluacion]).draw(false);
        objetivo = $('#objetivo').val("");
        contenido = $('#contenido').val("");
        actividad = $('#actividad').val("");
        evaluacion = $('#evaluacion').val("");
        counter++;
    } else {
        Swal.fire({
            type: 'error',
            title: 'Atención',
            text: 'Debe ingresar al menos un objetivo, contenido o actividad.'
        });
    }
});

function enviar() { //se envían los datos al controller para que lo registre
    if (lista_contenidos.length > 0) { // si la lista está vacía se informa al usuario al respecto

        if ($('#nombre').val() !== "" || $('#numero_clase').val() !== "" || $('#carga_horaria').val() !== "") {
            var contenidos = [],
                url = '/agregar_unidad',
                type = 'GET';
            lista_contenidos.forEach(function (valor) { // se convierte el array en una coleccion de objetos
                contenidos.push(valor);
            });
            enviar_al_server(contenidos);

        } else {
            Swal.fire({
                type: 'error',
                title: 'Atención',
                text: 'Los campos "Tema", "Número de clase", "Carga horaria" y "Modalidad" son obligatorios'
            });
        }
    } else {
        Swal.fire({
            type: 'error',
            title: 'Atención',
            text: 'Debe ingresar al menos un objetivo, contenido o actividad para que pueda registrar la clase'
        });
    }
}

function precargar_lista_contenidos(id) {
    $.ajax({ // se envía
        url: '/obtener_contenidos',
        data: {
            id: id
        },
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            data.forEach(function (valor) {
                lista_contenidos[counter] = {
                    objetivos: valor.objetivos,
                    contenidos: valor.contenidos,
                    actividades: valor.actividades,
                    tipo_evaluacion: valor.tipo_evaluacion
                };
                counter++;
            });
        }
    });
}

function enviar_al_server(contenidos) {

    if (pantalla === "editar") {
        $.ajax({ // se envía
            url: '/actualizar_unidad',
            data: {
                numero_clase: $('#numero_clase').val(),
                lista_contenidos: contenidos,
                nombre: $('#nombre').val(),
                carga_horaria: $('#carga_horaria').val(),
                unidad_tiempo: $('#unidad_tiempo').val(),
                modalidad_id: $('#modalidad_id').val(),
                seh: $('#seh').val(),
                auditable: $('#auditable').prop("checked"),
                unidad_id: $('#unidad_id').val()
            },
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                window.location.href = '/titulos/' + $('#titulo_id').val();
            }
        });
    } else {
        $.ajax({ // se envía
            url: '/agregar_unidad',
            data: {
                numero_clase: $('#numero_clase').val(),
                lista_contenidos: contenidos,
                nombre: $('#nombre').val(),
                carga_horaria: $('#carga_horaria').val(),
                unidad_tiempo: $('#unidad_tiempo').val(),
                modalidad_id: $('#modalidad_id').val(),
                seh: $('#seh').val(),
                auditable: $('#auditable').prop("checked"),
                titulo_id: $('#titulo_id').val()
            },
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                window.location.href = '/titulos/' + $('#titulo_id').val();
            }
        });
    }
}
