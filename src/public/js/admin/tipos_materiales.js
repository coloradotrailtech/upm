$("#side-general-li").addClass("active");
$("#side-general-ul").addClass("menu-open");
$("#side-ele-tiposmateriales").addClass("active");

function completar_campos(tipomaterial) {
    $('#nombre').val(tipomaterial.nombre);
    $('#familiamaterial_id').val(tipomaterial.familiamaterial_id).trigger("change");
    $('#form-update').attr('action', '/tiposmateriales/' + tipomaterial.id);
    $('#modal-update').modal('show');
}

function abrir_modal_borrar(id) {
    $('#form-borrar').attr('action', '/tiposmateriales/' + id);
    $('#modal-borrar').modal('show');
}

//Datatable
var table = $('#datatable-tiposmateriales').DataTable({
    language: tabla_traducida,
    responsive: true
});
