$("#side-ele-estudiantes").addClass("active");

function completar_campos(estudiante_id, persona, estudiante) {
    console.log(new Date(persona.fecha_nac) );
    let fecha_nac_formateada = moment(persona.fecha_nac,'YYYY-MM-DD hh:dd:ss').format('YYYY-MM-DD');    //formateamos nuestra fecha almacenada en BD de la manera que lo pide el standar para los input date nativos (YYYY-MM-DD)
    $('#apellido').val(persona.apellido);
    $('#nombre').val(persona.nombre);
    $('#fecha_nac_edit').val(fecha_nac_formateada);//new Date(persona.fecha_nac));

    $('#sexo').val(persona.sexo).trigger("change");
    $('#dni').val(persona.dni);
    $('#cuil').val(persona.cuil);
    $('#pais_id').val(persona.pais_id).trigger("change");
    $('#estado_civil').val(persona.estado_civil).trigger("change");
    $('#localidad_id').val(persona.localidad_id).trigger("change");
    $('#direccion').val(persona.direccion);
    $('#sangre').val(persona.sangre).trigger("change");

    const myEducation = new String(persona.educacion);
    $('#myEducacion').val(myEducation);

    $('#estudios').val(persona.educacion).trigger("change");
    $('#email').val(persona.email);
    $('#telefono').val(persona.telefono);
    $('#telefono2').val(persona.telefono2);
    if (persona.discapacidad === 1) {
        $('#discapacidad').bootstrapToggle('on');
    }else {
        $('#discapacidad').bootstrapToggle('off');
    }
    if (persona.carnet_discapacidad === 1) {
        $('#carnet_discapacidad').bootstrapToggle('on');
    }else{
        $('#carnet_discapacidad').bootstrapToggle('off');
    }
    if (persona.mano_habil != "derecha") {
        $('#mano_habil').bootstrapToggle('off');
    }else{
        $('#mano_habil').bootstrapToggle('on');
    }
    //Campos adicionales
    if (estudiante.trabaja === 1) {
        $('#trabaja-estudiante').prop( "checked", true );
    }else{
        $('#trabaja-estudiante').prop( "checked", false );
    }
    if (estudiante.trabaja === 1) {
        $('#trabaja-estudiante').prop( "checked", true );
    }else{
        $('#trabaja-estudiante').prop( "checked", false );
    }
    $('#descripcion_estudiante').val(persona.descripcion);
    $('#rubro_trabaja_estudiante').val(estudiante.rubro_trabaja);

    console.log(estudiante);


    $('#form-update').attr('action', '/estudiantes/' + estudiante_id);
    $('#modal-update').modal('show');
 
    
}

function abrir_modal_borrar(id) {
    $('#form-borrar').attr('action', '/estudiantes/' + id);
    $('#modal-borrar').modal('show');
}

//Datatable
var table = $('#datatable-estudiantes').DataTable({
    language: tabla_traducida,
    responsive: true,
});


//Datatables | filtro individuales - instanciación de los filtros
$('#datatable-estudiantes tfoot th').each(function () {
    var title = $(this).text();
    if (title !== "") {
        if (title !== 'Acciones') { //ignoramos la columna de los botones
            $(this).html('<input nombre="' + title + '" type="text" placeholder="Buscar ' + title + '" />');
        }else{
            $(this).html('');
        }
    }
});

//Datatables | filtro individuales - búsqueda
table.columns().every(function () {
    var that = this;
    $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
            that.search(this.value).draw();
        }
    });
});



//Datatables | asocio el evento sobre el body de la tabla para que resalte fila y columna
$('#datatable-estudiantes tbody').on('mouseenter', 'td', function () {
    var colIdx = table.cell(this).index().column;
    $(table.cells().nodes()).removeClass('highlight');
    $(table.column(colIdx).nodes()).addClass('highlight');
});

$(".datepicker").datepicker({
    format: "dd/mm/yyyy",
    autoclose: true,
    language: 'es'
});