var imagen_cropie = {//Esta variable es necesaria porque en ella se guardan las imégenes resultantes de la interacción con cropie.js
    create: "",
    update: ""
};


function isInPage(node) {
    return (node === document.body) ? false : document.body.contains(node);
}


/**
 * INICIO Croppie.js | create------------------------------------------------------------------------------------------------------------------
 */

//se instancia el plugin para cuando se sube una imagen 

var basic_create = $('#main_cropper_create').croppie({
    enableExif: true,
    viewport: {width: 200, height: 200, type: 'circle'},
    boundary: {width: 300, height: 300},
    update: function (data) {
        basic_create.croppie('result', 'blob').then(function (html) {
            imagen_cropie.create = html
        })
    }
})


//evento sobre el botón subir una imagen

$('.action_upload_create input').on('change', function () {

    $('#main_cropper_create').removeClass('hide');
    $("#video_create").addClass("hide");
    $("#capture_create").addClass("hide");
    $("#start_create").removeClass("hide");

    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#main_cropper_create').croppie('bind', {
                url: e.target.result
            });
        };
        reader.readAsDataURL(this.files[0]);
    }
});

//
//// Este método que se encarga del manejo de la cámara para imágenes nuevas y de su posterior tratamiento mediante croppie 
//
//

if (isInPage(document.querySelector('#canvas_create'))) { //compruebo si el elemento está siendo utilizado en la vista

    var player = document.getElementById('video_create'),
            snapshotCanvas = document.getElementById('canvas_create'),
            captureButton = document.getElementById('capture_create'),
            start_create = document.querySelector('#start_create'),
            videoTracks;

    start_create.addEventListener('click', function (e) {
        e.preventDefault();
        $('#imagen_create').val("");
        $('#main_cropper_create').addClass("hide");
        $("#video_create").removeClass("hide");
        $("#start_create").addClass("hide");
        $("#capture_create").removeClass("hide");

        var handleSuccess = function (stream) {
            // Attach the video stream to the video element and autoplay.
            player.srcObject = stream;
            videoTracks = stream.getVideoTracks();
        };

        captureButton.addEventListener('click', function (e) {
            e.preventDefault();
            var context = snapshotCanvas.getContext('2d');
            // Draw the video frame to the canvas.
            context.drawImage(player, 0, 0, snapshotCanvas.width, snapshotCanvas.height);

            var data_create = snapshotCanvas.toDataURL('image/png');

            $('#main_cropper_create').croppie('bind', {
                url: data_create
            });

            videoTracks.forEach(function (track) {
                track.stop();
            });

            $('#main_cropper_create').removeClass("hide");
            $("#video_create").addClass("hide");
            $("#capture_create").addClass("hide");
            $("#start_create").removeClass("hide");
        });

        navigator.mediaDevices.getUserMedia({video: true}).then(handleSuccess);

    });

}

///**
// * FIN Croppie.js | create------------------------------------------------------------------------------------------------------------------
// */

///**
// * INICIO Croppie.js | update------------------------------------------------------------------------------------------------------------------
// */

////se instancia el plugin para cuando se sube una imagen 

function instanciar_croppie_update(url_imagen) {

    /**
     * El plugin para el update a diferencia del create es necesario reinstanciarlo cada vez que se abre 
     * el modal de update ya que sino no se refresca el cuadro de la imagen y por enede no aparece la imagen.
     */

    if (imagen_cropie.update !== "") {
        /**
         * Acá actualizo dos veces (una en blanco y otra con la imagen)
         * ya que me da problemas con el zoom y de esta manera muestra bien.
         */
        $('#main_cropper_update').croppie('bind', {
            url: ''
        });
        $('#main_cropper_update').croppie('bind', {
            url: url_imagen
        });
    } else {
        var basic_update = $('#main_cropper_update').croppie({
            url: url_imagen,
            enableExif: true,
            viewport: {width: 200, height: 200, type: 'circle'},
            boundary: {width: 300, height: 300},
            update: function (data) {
                basic_update.croppie('result', 'blob').then(function (html) {
                    imagen_cropie.update = html;
                });
            }
        });

        //evento sobre el botón subir una imagen
        $('.action_upload_update input').on('change', function () {

            $('#main_cropper_update').removeClass('hide');
            $("#video_update").addClass("hide");
            $("#capture_update").addClass("hide");
            $("#start_update").removeClass("hide");

            if (this.files && this.files[0]) {
                var reader_update = new FileReader();
                reader_update.onload = function (e) {
                    $('#main_cropper_update').croppie('bind', {
                        url: e.target.result
                    });
                };
                reader_update.readAsDataURL(this.files[0]);
            }

        });
    }
}


//// Este método que se encarga del manejo de la cámara para imágenes nuevas en el formulario de update y de su posterior tratamiento mediante croppie 

if (isInPage(document.querySelector('#canvas_update'))) { //compruebo si el elemento está siendo utilizado en la vista

    var playerUpdate = document.getElementById('video_update'),
            snapshotCanvasUpdate = document.getElementById('canvas_update'),
            captureButtonUpdate = document.getElementById('capture_update'),
            start_update = document.querySelector('#start_update'),
            videoTracksUpdate;



    start_update.addEventListener('click', function (e) {
        e.preventDefault();
        $('#imagen_update').val("");
        $('#main_cropper_update').addClass("hide");
        $("#video_update").removeClass("hide");
        $("#start_update").addClass("hide");
        $("#capture_update").removeClass("hide");


        var handleSuccessUpdate = function (stream) {
            // Attach the video stream to the video element and autoplay.
            playerUpdate.srcObject = stream;
            videoTracksUpdate = stream.getVideoTracks();
        };

        captureButtonUpdate.addEventListener('click', function (e) {
            e.preventDefault();
            var context = snapshotCanvasUpdate.getContext('2d');
            // Draw the video frame to the canvas.
            context.drawImage(playerUpdate, 0, 0, snapshotCanvasUpdate.width, snapshotCanvasUpdate.height);


            var data_update = snapshotCanvasUpdate.toDataURL('image/png');

            $('#main_cropper_update').croppie('bind', {
                url: data_update
            });


            videoTracksUpdate.forEach(function (track) {
                track.stop();
            });

            $('#main_cropper_update').removeClass("hide");
            $("#video_update").addClass("hide");
            $("#capture_update").addClass("hide");
            $("#start_update").removeClass("hide");
        });

        navigator.mediaDevices.getUserMedia({video: true}).then(handleSuccessUpdate);
    });
//
}
/**
 * FIN Croppie.js | update------------------------------------------------------------------------------------------------------------------
 */