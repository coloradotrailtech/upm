console.log('Hola leaflet')
const tilesOpen = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
const mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibGlzYW5kcm9ibGFuY28iLCJhIjoiY2xmcGxncTZzMTZubDN2cWQ4bDdyNXc5ayJ9.0swnauDzvIMKQ-U3gW-Jlw';

//Coordenadas de Posadas
var coormap= [-27.1368, -54.8108]
var map = L.map('map').setView(coormap, 9)

var openstreet    =   L.tileLayer(tilesOpen,{ maxZoom:19,}).addTo(map)
var streets       =   L.tileLayer(mbUrl, {id: 'mapbox/streets-v11', tileSize: 512, zoomOffset: -1})
var grayscale     =   L.tileLayer(mbUrl, {id: 'mapbox/light-v9'   , tileSize: 512, zoomOffset: -1})
var dark_v10      =   L.tileLayer(mbUrl, {id: 'mapbox/dark-v10'   , tileSize: 512, zoomOffset: -1})
var satellite     =   L.tileLayer(mbUrl, {id: 'mapbox/satellite-v9', tileSize: 512, zoomOffset: -1})
var satellite_v11 =   L.tileLayer(mbUrl, {id: 'mapbox/satellite-streets-v11', tileSize: 512, zoomOffset: -1})
var day_v1        =   L.tileLayer(mbUrl, {id: 'mapbox/navigation-day-v1', tileSize: 512, zoomOffset: -1})
var night_v1      =   L.tileLayer(mbUrl, {id: 'mapbox/navigation-night-v1', tileSize: 512, zoomOffset: -1})

var baseLayers = {
    'Openstreet':       openstreet,
    'Street':           streets,    
    'Escala Grises':    grayscale,
    'Oscuro':           dark_v10,
    'Satellite':        satellite,
    'Satellite-calles': satellite_v11,
    'Día':              day_v1,
    'Noche':            night_v1
}

var myGroup_zonas = L.layerGroup();
L.control.layers(baseLayers).addTo(map);


function limpiar_Leaflet(){
    console.log('Limpiar Capas')

    map.removeLayer(map)
}


//StyleEditor
let styleEditor = L.control.styleEditor({
    position: 'topleft',
    colorRamp: ['#004877','#9db290','#0080e2','#74bf60','#ffa100'],
    markers: ['circle-stroked', 'circle', 'square-stroked', 'square']
});
map.addControl(styleEditor)


//Funcion para imprimir mapa
L.easyPrint({
    title: 'Mi botón para imprimir',
    position: 'bottomright',
    sizeModes: ['A4Portrait', 'A4Landscape'],
    elementsToHide: 'p, h2, .leaflet-control-zoom' 
}).addTo(map);
