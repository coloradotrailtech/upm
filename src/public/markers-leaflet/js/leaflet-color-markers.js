console.log('Hola Markers')

var blueIcon = new L.Icon({
	iconUrl: '/markers-leaflet/img/marker-icon-2x-blue.png',
	shadowUrl: '/markers-leaflet/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var goldIcon = new L.Icon({
	iconUrl: '/markers-leaflet/img/marker-icon-2x-gold.png',
	shadowUrl: '/markers-leaflet/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var redIcon = new L.Icon({
	iconUrl: '/markers-leaflet/img/marker-icon-2x-red.png',
	shadowUrl: '/markers-leaflet/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var greenIcon = new L.Icon({
	iconUrl: '/markers-leaflet/img/marker-icon-2x-green.png',
	shadowUrl: '/markers-leaflet/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var orangeIcon = new L.Icon({
	iconUrl: '/markers-leaflet/img/marker-icon-2x-orange.png',
	shadowUrl: '/markers-leaflet/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var yellowIcon = new L.Icon({
	iconUrl: '/markers-leaflet/img/marker-icon-2x-yellow.png',
	shadowUrl: '/markers-leaflet/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var violetIcon = new L.Icon({
	iconUrl: '/markers-leaflet/img/marker-icon-2x-violet.png',
	shadowUrl: '/markers-leaflet/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var greyIcon = new L.Icon({
	iconUrl: '/markers-leaflet/img/marker-icon-2x-grey.png',
	shadowUrl: '/markers-leaflet/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});

var blackIcon = new L.Icon({
	iconUrl: '/markers-leaflet/img/marker-icon-2x-black.png',
	shadowUrl: '/markers-leaflet/img/marker-shadow.png',
	iconSize: [25, 41],
	iconAnchor: [12, 41],
	popupAnchor: [1, -34],
	shadowSize: [41, 41]
});
